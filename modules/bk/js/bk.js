/*******/
function  sortpPost(check){
  console.log('ok');
  var sort = jQuery(check).find('input').val(); 
  var data = {
          'action': 'myfilter',
          'sort': sort,
      };
  jQuery.ajax({
    url: 'https://football-fun.ru/wp-admin/admin-ajax.php', // обработчик
    data: data,
    type: 'POST', // тип запроса
    success:function(data){
      jQuery('.bk_table_list tbody').html(data);
    }
  });
}
jQuery(document).ready(function(){
    jQuery('.otziv').on('click', function(){
        jQuery('.main_sort').find('input').prop('checked', false);
        if(jQuery(this).prop( "checked" )){
            jQuery('.radio_otz').show();
        }else{
            jQuery('.radio_otz').hide();
        }
    })
    jQuery('.main_sort ').on('click', function(){
        jQuery('.main_sort').find('input').prop('checked', false);
        jQuery('.radio_otz').hide();
        jQuery('.otziv').prop('checked', false);
        jQuery(this).find('input').prop('checked', true);
        sortpPost(this);
    })
    jQuery('.radio_otz').on('click', function(){
        sortpPost(this);
    })
})
jQuery(function($){
  jQuery('.bukmekers_btn').on('click', function(){
    jQuery('.main_sort').find('input').prop('checked', false);
    jQuery('.otziv').prop('checked', false);
    var bukmekers = jQuery(this).data('id'); 
    var data = {
            'action': 'myfilter',
            'bukmekers': bukmekers,
        };
    jQuery.ajax({
      url: 'https://football-fun.ru/wp-admin/admin-ajax.php', // обработчик
      data: data,
      type: 'POST', // тип запроса
      success:function(data){
        jQuery('.bk_table_list tbody').html(data);
      }
    });
    return false;
  });
})