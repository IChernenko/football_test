<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 15.06.2018
 * Time: 10:17
 */
class td_wiget_tournament_table extends WP_Widget {

    /*
     * создание виджета
     */
    function __construct() {
        parent::__construct(
            'true_top_widget',
            'Турнирная таблица Лиги', // заголовок виджета
            array( 'description' => 'Позволяет вывести туриную таблицу лиги' ) // описание
        );
    }

    /*
     * фронтэнд виджета
     */
    public function widget( $args, $instance ) {

        $title = apply_filters( 'widget_title', $instance['title'] ); // к заголовку применяем фильтр (необязательно)

        global $post;
        global $dir_img_club;
        $category = get_the_category( $post->ID );
        $liga_obj = td_football_app::getCompetitonObject($category[0]->term_id);

        global $wpdb;
        $name_liga = esc_sql($liga_obj['liga_name']);
        $sql  = "SELECT com_tour.name AS com_comp ,
                        football_club.name AS f_team, football_club.logo_link ,
                        foot_tt.position , foot_tt.old_position ,
                        foot_tt.games AS quantity_games , foot_tt.wins AS winnings , foot_tt.draws AS draws  , foot_tt.loses AS losing ,
                        foot_tt.scored_goals , foot_tt.missed_goals , foot_tt.points
                   FROM foot_football_club_turnirnaya_tablitsa  foot_tt
                   JOIN parser_common_tournament com_tour
                     ON foot_tt.competition_id = com_tour.id
                   JOIN parser_common_club  football_club
                     ON foot_tt.club_id = football_club.id
                  WHERE com_tour.name = '{$name_liga}'
                     OR com_tour.name_post = '{$name_liga}'
               ORDER BY foot_tt.position ASC ";
        $result = $wpdb->get_results( $sql);



        echo $args['before_widget'];

        if ( ! empty( $title ) )
            echo $args['before_title'] . $title . $args['after_title'];
        $i = 1;
        $innerHTM = '<table class="wiget_tournament_table wiget_table_info">';
          $innerHTM .= '<tr>
                           <th></th>
                           <th class="wiget_team">Команда</th>
                           <th>И</th>
                           <th>О</th>
                       </tr>';
          foreach ($result as $item_team) {
            if($i <= 6){
              $innerHTM .= '<tr>
                              <td>'.$i++.'</td>
                              <td class="wiget_team"><img src="'.$dir_img_club.''.$item_team->logo_link.'" alt="'.$item_team->f_team.'" class="logo_club">'.$item_team->f_team.'</td>
                              <td>'.$item_team->quantity_games.'</td>
                              <td>'.$item_team->points.'</td>
                         </tr>';
            }else{
              $innerHTM .= '<tr class="wiget_hide_row" style="display:none;">
                              <td>'.$i++.'</td>
                              <td><img src="'.$dir_img_club.''.$item_team->logo_link.'" alt="'.$item_team->f_team.'" class="logo_club">'.$item_team->f_team.'</td>
                              <td>'.$item_team->quantity_games.'</td>
                              <td>'.$item_team->points.'</td>
                         </tr>';
            }

          }
        $innerHTM .='</table>';
        $innerHTM .='<a class="wiget_a" href="'.$liga_obj["liga_url"].'turnirnaya-tablitsa/">Подробная таблица</a>';

        echo $innerHTM;

        echo $args['after_widget'];
    }

    /*
     * бэкэнд виджета
     */
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        if ( isset( $instance[ 'posts_per_page' ] ) ) {
            $posts_per_page = $instance[ 'posts_per_page' ];
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>">Заголовок</label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'posts_per_page' ); ?>">Количество постов:</label>
            <input id="<?php echo $this->get_field_id( 'posts_per_page' ); ?>" name="<?php echo $this->get_field_name( 'posts_per_page' ); ?>" type="text" value="<?php echo ($posts_per_page) ? esc_attr( $posts_per_page ) : '5'; ?>" size="3" />
        </p>
        <?php
    }

    /*
     * сохранение настроек виджета
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['posts_per_page'] = ( is_numeric( $new_instance['posts_per_page'] ) ) ? $new_instance['posts_per_page'] : '5'; // по умолчанию выводятся 5 постов
        return $instance;
    }
}

/*
 * регистрация виджета
 */
function true_top_posts_widget_load() {
    register_widget( 'td_wiget_tournament_table' );
}
add_action( 'widgets_init', 'true_top_posts_widget_load' );
