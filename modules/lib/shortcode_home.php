<?php
/*Предстоящии 5 матчей*/
function show_top_matches() {
$db = DatabBase();
$sql = "SELECT com_c.name AS competition  ,  cm.play_date , cm.play_time ,  t_1.name AS team_1 ,  t_2.name AS team_2 FROM foot_football_club_matches cm JOIN parser_common_tournament com_c ON cm.competition_id = com_c.id JOIN parser_common_club t_1 ON cm.club_id_1 = t_1.id  JOIN parser_common_club t_2 ON cm.club_id_2 = t_2.id   WHERE cm.status = 0 ORDER BY cm.play_date ASC LIMIT 0 , 4";
$dbresult = $db->prepare($sql);
$dbresult->execute();
$result = $dbresult->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE);
//echo "<pre>"; print_r($result); echo "</pre>";
$innerHTML = "<div class='top_matches_list'>";
foreach ($result as $item_top_match) {
  $date = date_create($item_top_match->play_date);
	$time_play = date_create($item_top_match->play_time);
	$innerHTML .= '<div class="item_top_match">
										<span>'.$item_top_match->competition.'</span>
										<div class="box_top_match">
											<div class="teams"><span>'.$item_top_match->team_1.'</span><span>'.$item_top_match->team_2.'</span></div>
											<div class="data_play_match"><span>'.date_format($date, 'd.m. ') .'</span><span>'.date_format($time_play, 'H:i ').' мск</span></div>
										</div>
								 </div>';
};
$innerHTML .= "</div>";
return $innerHTML;
}
add_shortcode('show_top_matches', 'show_top_matches');

//Тренды по тегам

function show_trends_tag() {
$number = 4;
$numberposts = 20;
$date = getdate();

	
  $todayposts = new WP_Query('showposts=5'.'&year=' .$date["year"].'&monthnum='.$date["mon"].'&day=' .$date["mday"]);
  
    $tagIds = array();
    $tags = array();
  if(count($todayposts->posts)){
    $innerHTML = '<ul class="trend_list_home">';
     foreach ($todayposts->posts as $recent_post) {
        $postTags = wp_get_post_tags($recent_post->ID);
        foreach ($postTags as $tag) {
            if(!isset($tagIds[$tag->term_id])) {
                $tagIds[$tag->term_id] = 1;
                $tags[$tag->term_id] = $tag;
            } else {
                $tagIds[$tag->term_id]++;
            }
        }
    }
    arsort($tagIds);
    $tagIds = array_slice($tagIds, 0, $number, true);
    $innerHTML .= '<li class="trend_name">Сейчас В Тренде</li>';
    foreach ($tagIds as $tagId => $counter) {
        $innerHTML .= '<li><a href="' . get_tag_link($tags[$tagId]->term_id) . '" rel="tag">' . $tags[$tagId]->name . '</a></li>';
    }
    $innerHTML .= '</ul>';
  }
   

return $innerHTML;
}
add_shortcode('show_trends_tag', 'show_trends_tag');

// Блок новостей
function show_title_news_list() {
  $innerHTML = '<ul class="nav_list_news_name">';
   $innerHTML .='
    <li class="name_news_item"> <a href="https://football-fun.ru/category/novosti-futbola/rossiya-novosti/">России</a></li>
    <li class="name_news_item"> <a href="https://football-fun.ru/category/novosti-futbola/angliya-novosti/">Англии</a></li>
    <li class="name_news_item"> <a  href="https://football-fun.ru/category/novosti-futbola/germaniya-novosti/">Германии</a></li>
    <li class="name_news_item"> <a  href="https://football-fun.ru/category/novosti-futbola/ispaniya-novosti/">Испании</a></li>
    <li class="name_news_item"> <a  href="https://football-fun.ru/category/novosti-futbola/italiya-novosti/">Италии</a></li>
    <li class="name_news_item"> <a  href="https://football-fun.ru/category/novosti-futbola/franciya-novosti/">Франции</a></li> 
    <li class="name_news_item name_news_item_parent"><a href="#" class="link">Еще <i class="td-icon-read-down"></i></a>
      <ul class="child_name_news_item">
        <li><a href="https://football-fun.ru/category/novosti-futbola/liga-evropy-novosti/">Лиги Европы</a></li>
        <li><a href="https://football-fun.ru/category/novosti-futbola/liga-chempionov-novosti/">Лиги Чемпионов</a></li>
      </ul>
    </li>';
  $innerHTML .= '</ul>';
  return $innerHTML;
}
add_shortcode('show_title_news_list', 'show_title_news_list');
/*Результаты матчей*/
function result_matches_home() {
$db = DatabBase();
$sql = "SELECT   cm.play_date ,  f_c.name AS competition  ,  t_1.name AS team_1 ,  t_2.name AS team_2 , cm.result_1 , cm.result_2 , cm.result_match   FROM foot_football_club_matches   cm JOIN parser_common_tournament f_c ON cm.competition_id = f_c.id JOIN parser_common_club t_1 ON cm.club_id_1 = t_1.id  JOIN parser_common_club t_2 ON cm.club_id_2 = t_2.id   WHERE cm.status = 1 ORDER BY cm.play_date DESC  LIMIT 0 , 10";
$dbresult = $db->prepare($sql);
$dbresult->execute();
$result = $dbresult->fetchAll( PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE);
//echo "<pre>"; print_r($result); echo "</pre>";
$innerHTML = "<div class='result_matches_home'>";
$innerHTML .= "<div class='block-title'><span>Результаты матчей </span></div>";
  foreach ($result as $item_match) {
  	 $date = date_create($item_match->play_date);
  	switch ($item_match->result_match) {
		  case 1:
		  $teams = '<span class="team_1"><b>'.$item_match->team_1.'</b></span> <span class="result result_1">'.$item_match->result_1.':'.$item_match->result_2.'</span> <span class="team_2">'.$item_match->team_2.'</span>';
		      break;
		  case 2:
		  			  $teams = '<span class="team_1">'.$item_match->team_1.'</span> <span class="result result_1">'.$item_match->result_1.':'.$item_match->result_2.'</span> <span class="team_2"><b>'.$item_match->team_2.'</b></span>';
		      break;
		  case 3:
		      	  $teams = '<span class="team_1">'.$item_match->team_1.'</span> <span class="result result_1">'.$item_match->result_1.':'.$item_match->result_2.'</span> <span class="team_2">'.$item_match->team_2.'</span>';
		      break;
	  }
    $innerHTML .= "<div class='item_result_match item_result_match_teams item_result_match_2'>
    								<span class='play_date'>".date_format($date, 'd.m.')."</span>
    								".$teams."
    							</div>";
}
$innerHTML .= "</div>";
return $innerHTML;
}
add_shortcode('result_matches_home', 'result_matches_home');

/*Расписание матчей*/
function schedule_of_home() {
$db = DatabBase();
$sql = "SELECT   cm.play_date , cm.play_time ,  f_c.name AS competition  ,  t_1.name AS team_1 ,  t_2.name AS team_2 ,  cm.result_match   FROM foot_football_club_matches   cm JOIN parser_common_tournament f_c ON cm.competition_id = f_c.id JOIN parser_common_club t_1 ON cm.club_id_1 = t_1.id  JOIN parser_common_club t_2 ON cm.club_id_2 = t_2.id  WHERE cm.status = 0 ORDER BY cm.play_date DESC  LIMIT 0 , 10";
$dbresult = $db->prepare($sql);
$dbresult->execute();
$result = $dbresult->fetchAll( PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE);
 //echo "<pre>"; print_r($result); echo "</pre>";
$innerHTML = "<div class='result_matches_home'>";
$innerHTML .= "<div class='block-title'><span>Расписание  матчей </span></div>";
  foreach ($result as $item_match) {
  	 $date = date_create($item_match->play_date);
    $innerHTML .= "<div class='item_result_match item_result_match_teams item_result_match_2'>
    								<span class='play_date'>".date_format($date, 'd.m.')."</span>
    								<span class='team_1'>".$item_match->team_1."</span> <span class='result result_1'>-:-</span> <span class='team_2'>".$item_match->team_2."</span>
    							</div>";
  }
$innerHTML .= "</div>";
return $innerHTML;
}
add_shortcode('schedule_of_home', 'schedule_of_home');

/*Топ 100 футболистов The Guardian. */
function show_rating_players_home()
{
  $args = array(
                              'post_type'  => 'footbolist',
                              'meta_key' => '_player_desc_rating_the_guardian',
                              'orderby' => 'meta_value_num',
                              'order' => 'ASC',
                              'posts_per_page' => 10,
                );
  $query = new WP_Query($args); 
  
  $innerHTML .= '<div class="block-title title_block_home"><a href="/the-guardian/">Топ футболистов мира</a>
                  <ul>
                    <li><a href="/the-guardian/">Топ 100 футболистов по версии «The Guardian»</a></li>
                    <li><a href="/luchshij-igrok-goda-po-versii-fifa/">Лучший игрок года по версии ФИФА</a></li>
                    <li><a href="/top-10-futbolistov-za-vsyu-istoriyu/">ТОП-10 лучших футболистов мира за всю историю</a></li>
                  </ul>
                </div>';
  $innerHTML .='<div class="players_list td_block_15 ">';
    $innerHTML .='<div class="td-column-3">';
      $innerHTML .= '<div class="td-block-row">';
    if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); 
      $post_id = get_the_ID();
      $meta_values = get_post_meta( $post_id );
      
    $innerHTML .='  <div class="player_rating_item td-block-span4">';
      $innerHTML .='<div class="td_module_mx4"> ';
       $innerHTML .='<div class="td-module-image">
                        <div class="td-module-thumb"><a href="'.get_the_permalink().'">'.get_the_post_thumbnail().'</a></div>
                     </div>';
       $innerHTML .='<p class="entry-title td-module-title title_player_rating_item"><a href="'.get_the_permalink().'">'.$meta_values["_player_desc_rating_the_guardian"][0].'. '.get_the_title().' <span>('.$meta_values["_player_desc_club"][0].')</span></a></p>';
      $innerHTML .='</div>';
    $innerHTML .='  </div> ';
      
    endwhile;?><?php endif;
      
    $innerHTML .='</div>';
    $innerHTML .='</div>';
      $innerHTML .='<div class="wpb_text_column wpb_content_element  td-load-more-wrap">
                    <div class="wpb_wrapper">
                      <p><a class="td_ajax_load_more td_custom_load_more " href="/the-guardian/" style="margin-top: 30px;">Полная таблица</a></p>
                    </div>
                  </div>';
  $innerHTML .='</div>';



  return $innerHTML;

}
add_shortcode('show_rating_players_home', 'show_rating_players_home');

/*Топ 100 клубов по Фифа */
function show_rating_club_home()
{
  global $wpdb;
  global $dir_img_club;
  $sql= "SELECT com_club.name AS club_name , com_club.logo_link,
                com_country.name AS country , 
                f_posts.post_name,
                top_c.position  
           FROM foot_football_rating_uefa_top_100_clubs top_c 
     INNER JOIN parser_common_club com_club 
             ON top_c.club_id = com_club.id  
     INNER JOIN parser_common_country com_country 
             ON top_c.country_id = com_country.id  
      LEFT JOIN foot_posts f_posts 
             ON com_club.name_post = f_posts.post_title 
            AND f_posts.post_type = 'f-club' 
            AND f_posts.post_status = 'publish' 
          WHERE 1
       ORDER BY top_c.position ASC LIMIT 0 , 10";
  $result = $wpdb->get_results( $sql);
 
  $innerHTML .= '<div class="block-title title_block_home"><a href="/top-100-futbolnyh-klubov-mira/" >Рейтинг футбольных клубов</a></div>';
    $innerHTML .='<div class="players_list td_block_15 ">';
      $innerHTML .='<div class="td-column-3">';
        $innerHTML .= '<div class="td-block-row">';

          foreach ($result as $key => $item_top_club) {
                
              $innerHTML .='  <div class="club_rating_item td-block-span4">';
                $innerHTML .='<div class="td_module_mx4"> ';
                 $innerHTML .='<div class="td-module-image">
                                  <div class="td-module-thumb"><a href="/f-club/'.$item_top_club->post_name.'"> <img src="'.$dir_img_club.''.$item_top_club->logo_link.'" class="logo_club" alt="'.$item_top_club->club_name.'"></a></div>
                               </div>';
                 $innerHTML .='<p class="entry-title td-module-title title_club_rating_item">
                                  <a href="/f-club/'.$item_top_club->post_name.'">
                                    '.$item_top_club->position.' '.$item_top_club->club_name.'  
                                    <span><i class="flag_nt flag_'.translit($item_top_club->country).'" title="'.$item_top_club->country.'" alt="'.$item_top_club->country.'"></i>'.$item_top_club->country.'</span>
                                  </a>
                              </p>';
                $innerHTML .='</div>';
              $innerHTML .='  </div> ';
          }
        $innerHTML .='</div>';
      $innerHTML .='</div>';
    $innerHTML .='</div>';
  $innerHTML .='<div class="wpb_text_column wpb_content_element  td-load-more-wrap">
                    <div class="wpb_wrapper">
                      <p><a class="td_ajax_load_more td_custom_load_more " href="/top-100-futbolnyh-klubov-mira/" style="margin-top: 30px;">Полная таблица</a></p>
                    </div>
                  </div>';
  return $innerHTML;
}
add_shortcode('show_rating_club_home', 'show_rating_club_home');