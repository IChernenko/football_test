<?php
class td_football_bukmekers{

	public function get_count_reviews_parser()
	{
		$data_reviews = td_model_football::get_bk_reviews_data( );
		$data_reviews = td_football_app::array_group($data_reviews);

		foreach ($data_reviews as $key => $value) {
			$post_id = td_model_football::get_post_by_title($key , 'bukmeker');

			update_post_meta($post_id, '_bk_desc_player_reviews', count($value));
			// = array($key , count($value));
		}
		return $count_reviews;

	}
	public function get_count_reviews_last_month_parser()
	{

		$data_reviews = td_model_football::get_bk_reviews_data('30 DAY');
		$data_reviews = td_football_app::array_group($data_reviews);

		if(count($data_reviews) > 1){
			foreach ($data_reviews as $key => $value) {
				$post_id = td_model_football::get_post_by_title($key , 'bukmeker');
				update_post_meta($post_id, '_bk_desc_player_reviews_last_month', count($value));
				

			}
		}else{
			$count_reviews = '0';
		}
		print_r($count_reviews);
		return $count_reviews;
	}
	public function get_count_reviews_last_6_month_parser()
	{

		$data_reviews = td_model_football::get_bk_reviews_data('6 MONTH');
		$data_reviews = td_football_app::array_group($data_reviews);

		if(count($data_reviews) > 1){
			foreach ($data_reviews as $key => $value) {
				$post_id = td_model_football::get_post_by_title($key , 'bukmeker');
		    update_post_meta($post_id, '_bk_desc_player_reviews_last_6_month', count($value));
			}
		}else{
			$count_reviews = '0';
		}

		return $count_reviews;
	}
	public function get_count_reviews_last_1_year_parser()
	{
		$data_reviews = td_model_football::get_bk_reviews_data('1 YEAR');
		$data_reviews = td_football_app::array_group($data_reviews);

		if(count($data_reviews) > 1){
			foreach ($data_reviews as $key => $value) {
				$post_id = td_model_football::get_post_by_title($key , 'bukmeker');
				update_post_meta($post_id, '_bk_desc_player_reviews_last_year', count($value));

			}
		}else{
			$count_reviews = '0';
		}
		
		return $count_reviews;
	}

	public function get_bukmekers_list() { 
	  global $wp_query; 
	  $paged = get_query_var('paged') ? get_query_var('paged') : 1;
	  if($_POST['sort'] != ''){
	      $meta_key = $_POST['sort'];
	      $sort =  'meta_value_num';  
	      $order = 'DESC';
	  }else{
	      $sort =  'date';  $order = 'DESC' /* ASC или DESC*/ ;  $meta_key = "";
	  }
	  $args = array(
	                'post_type'  => 'bukmeker',
	                'publish' => true, 
	                'meta_key' => $meta_key ,
	                'order' => $order,
	                'orderby' => $sort,
	                'post_parent' => 0,
	                'paged' => $paged,   
	                'posts_per_page' => -1,
	                 );
	  if( isset( $_POST['bukmekers'] )){
	        $args['tax_query'] = array(
	            array(
	                'taxonomy' => 'bukmekers',
	                'field' => 'id',
	                'terms' => $_POST['bukmekers']
	            )
	        );
	  }
	  $wp_query = new WP_Query($args); 
	  while ( have_posts() ) : the_post(); 
	     td_bukmekers_template::get_bk_list();
	  endwhile;   
	}
	

}
