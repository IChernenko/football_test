<?php
require_once('ff_option.php');
function ff_trim_text($text , $maxchar = 10)
{
  $text = trim(strip_tags($text));;
  if ( iconv_strlen( $text ,'UTF-8') > $maxchar ){
      $text = mb_substr( $text, 0, $maxchar ) .'...';
  }
  return $text; // выведет: Этот текст...
}
function translit($str) {
    $rus = array("А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я",  'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', "'", ' ');
    $lat = array('a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', '', 'y', '', 'e', 'yu', 'ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', '', 'y', '', 'e', 'yu', 'ya', '' , '_');
    return str_replace($rus, $lat, $str);
}
function array_group($result){
  foreach ( $result as $row ) {
            $var_by_ref = get_object_vars( $row );
            $key = array_shift( $var_by_ref );
            $new_array[$key][] = $row;
        }
  return $new_array;
}
function array_group_by(array $array, $key)
{
  if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
    trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
    return null;
  }
  $func = (!is_string($key) && is_callable($key) ? $key : null);
  $_key = $key;
  // Load the new array, splitting by the target key
  $grouped = [];
  foreach ($array as $value) {
    $key = null;
    if (is_callable($func)) {
      $key = call_user_func($func, $value);
    } elseif (is_object($value) && isset($value->{$_key})) {
      $key = $value->{$_key};
    } elseif (isset($value[$_key])) {
      $key = $value[$_key];
    }
    if ($key === null) {
      continue;
    }
    $grouped[$key][] = $value;
  }
  // Recursively build a nested grouping if more parameters are supplied
  // Each grouped array value is grouped according to the next sequential key
  if (func_num_args() > 2) {
    $args = func_get_args();
    foreach ($grouped as $key => $value) {
      $params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
      $grouped[$key] = call_user_func_array('array_group_by', $params);
    }
  }
  return $grouped;
}
// Сортировка многмерного масива
function SuperUnique($array,$key)
{
   $temp_array = [];
   foreach ($array as &$v) {
       if (!isset($temp_array[$v[$key]]))
       $temp_array[$v[$key]] =& $v;
   }
   $array = array_values($temp_array);
   return $array;

}
// рейтинг в звездочках
function get_stars($reiting){
  switch (true) {
    case  ( $reiting == 0):
      $innerHTML = '<i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>';
      break;
    case  ($reiting < 1):
      $innerHTML = '<i class="star_rat star_half"></i>
                   <i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>';
      break;
    case  ($reiting == 1):
      $innerHTML = '<i class="star_rat star"></i>
                   <i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>';
      break;
    case  ($reiting > 1 and  $reiting < 2):
      $innerHTML = '<i class="star_rat star"></i>
                   <i class="star_rat star_half"></i>
                   <i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>';
      break;
    case  ($reiting == 2):
      $innerHTML = '<i class="star_rat star"></i>
                   <i class="star_rat star"></i>
                   <i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>';
      break;
    case  ($reiting > 2 and  $reiting < 3):
      $innerHTML = '<i class="star_rat star"></i>
                   <i class="star_rat star"></i>
                   <i class="star_rat star_half"></i>
                   <i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>';
      break;
    case  ($reiting == 3):
      $innerHTML = '<i class="star_rat star"></i>
                   <i class="star_rat star"></i>
                   <i class="star_rat star"></i>
                   <i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>';
      break;
    case  ($reiting > 3 and  $reiting < 4):
      $innerHTML = '<i class="star_rat star"></i>
                   <i class="star_rat star"></i>
                   <i class="star_rat star"></i>
                   <i class="star_rat star_half"></i>
                   <i class="star_rat star_empty"></i>';
      break;
    case  ($reiting == 4):
      $innerHTML = '<i class="star_rat star"></i>
                   <i class="star_rat star"></i>
                   <i class="star_rat star"></i>
                   <i class="star_rat star"></i>
                   <i class="star_rat star_empty"></i>';
      break;
    case  ($reiting > 4 and  $reiting < 5):
      $innerHTML = '<i class="star_rat star"></i>
                   <i class="star_rat star"></i>
                   <i class="star_rat star"></i>
                   <i class="star_rat star"></i>
                   <i class="star_rat star_half"></i>';
      break;
    case  ( $reiting == 5):
      $innerHTML = '<i class="star_rat star"></i>
                   <i class="star_rat star"></i>
                   <i class="star_rat star"></i>
                   <i class="star_rat star"></i>
                   <i class="star_rat star"></i>';
      break;
    default:
      $innerHTML = '<i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>
                   <i class="star_rat star_empty"></i>';
      break;
  }


  return $innerHTML;
}

function do_rewrite(){
  // Правило перезаписи
  add_rewrite_rule( '^stadiony/([^/]*)/([^/]*)/?', 'index.php?pagename=stadiony&competition=$matches[1]&competition_type=$matches[2]', 'top' );

  // скажем WP, что есть новые параметры запроса
  add_filter( 'query_vars', function( $vars ){
    $vars[] = 'competition';
    $vars[] = 'competition_type';
    return $vars;
  } );
}
add_action('init', 'do_rewrite');
function DatabBase(){
 $host = 'footba71.mysql.ukraine.com.ua'; // адрес сервера
  $database = 'footba71_db'; // имя базы данных
  $user = 'footba71_db'; // имя пользователя
  $password = 'KHN6Nk5e'; // пароль
  $db = new PDO( 'mysql:host=' . $host . '; charset=utf8; dbname=' . $database, $user, $password );
  return $db;
}
// Общии
/*Социальные сети*/
function social_link( $atts ){
  global $wpdb;
  extract(shortcode_atts(array(
        'slug' => "club",
  ), $atts));
  wp_reset_query();
  $name_post = get_the_title();
  $post_id = get_the_ID();
  $post_metadate = get_post_meta($post_id);

  $table = '';
  $innerHTML = "<div class='social_links'>";
    switch ($slug) {
    case 'world_cup':
        $sql = "SELECT fc.instagram , fc.wikipedia , fc.facebook , fc.twitter , fc.site  FROM foot_football_world_cup  fc
                 WHERE fc.name = 'Чемпионат мира 2018'  ";
        $result = $wpdb->get_results( $sql );
        foreach ($result as $item_social) {
          if($item_social->instagram !== ""){
            $innerHTML .= '<a href="'.$item_social->instagram.'" class="social_link instagram" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a>';
          }
          if($item_social->wikipedia !== ""){
            $innerHTML .= '<a href="'.$item_social->wikipedia.'" class="social_link wikipedia" target="_blank"><i class="fa fa-wikipedia-w" aria-hidden="true"></i>  Wikipedia</a>';
          }
          if($item_social->facebook !== ""){
            $innerHTML .= '<a href="'.$item_social->facebook.'" class="social_link facebook" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i>   Facebook</a>';
          }
          if($item_social->twitter !== ""){
            $innerHTML .= '<a href="'.$item_social->twitter.'" class="social_link twitter" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i>  Twitter</a>';
          }
          if($item_social->site !== ""){
            $innerHTML .= '<a href="'.$item_social->site.'" class="social_link site_globe" target="_blank"><i class="fa fa-globe" aria-hidden="true"></i>  Сайт</a>';
          }
        };
        break;
    case 'club':
       if(trim($post_metadate['_club_desc_instagram'][0]) !== ""){
          $innerHTML .= '<a href="'.$post_metadate['_club_desc_instagram'][0].'" class="social_link instagram" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a>';
        }
        if(trim($post_metadate['_club_desc_wikipedia'][0]) !== ""){
          $innerHTML .= '<a href="'.$post_metadate['_club_desc_wikipedia'][0].'" class="social_link wikipedia" target="_blank"><i class="fa fa-wikipedia-w" aria-hidden="true"></i>  Wikipedia</a>';
        }
        if(trim($post_metadate['_club_desc_facebook'][0]) !== ""){
          $innerHTML .= '<a href="'.$post_metadate['_club_desc_facebook'][0].'" class="social_link facebook" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i>   Facebook</a>';
        }
        if(trim($post_metadate['_club_desc_twiter'][0]) !== ""){
          $innerHTML .= '<a href="'.$post_metadate['_club_desc_twiter'][0].'" class="social_link twitter" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i>  Twitter</a>';
        }
        if(trim($post_metadate['_club_desc_site'][0]) !== ""){
          $innerHTML .= '<a href="'.$post_metadate['_club_desc_site'][0].'" class="social_link site_globe" target="_blank"><i class="fa fa-globe" aria-hidden="true"></i>  Сайт</a>';
        }
        break;
    case 'liga':
        $table = 'foot_football_liga';
        break;
    case 'player':
        if(trim($post_metadate['_player_desc_instagram'][0]) !== ""){
          $innerHTML .= '<a href="'.$post_metadate['_player_desc_instagram'][0].'" class="social_link instagram" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a>';
        }
        if(trim($post_metadate['_player_desc_wikipedia'][0]) !== ""){
          $innerHTML .= '<a href="'.$post_metadate['_player_desc_wikipedia'][0].'" class="social_link wikipedia" target="_blank"><i class="fa fa-wikipedia-w" aria-hidden="true"></i>  Wikipedia</a>';
        }
        if(trim($post_metadate['_player_desc_facebook'][0]) !== "" ){
          $innerHTML .= '<a href="'.$post_metadate['_player_desc_facebook'][0].'" class="social_link facebook" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i>   Facebook</a>';
        }
        if( trim($post_metadate['_player_desc_twiter'][0]) !== ""){
          $innerHTML .= '<a href="'.$post_metadate['_player_desc_twiter'][0].'" class="social_link twitter" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i>  Twitter</a>';
        }
        if( trim($post_metadate['_player_desc_site'][0]) !== ""){
          $innerHTML .= '<a href="'.$post_metadate['_player_desc_site'][0].'" class="social_link site_globe" target="_blank"><i class="fa fa-globe" aria-hidden="true"></i>  Сайт</a>';
        }
        break;
    }
  $innerHTML .= "</div>";
  return $innerHTML;

}
add_shortcode('social_link', 'social_link');
/*вывод записей*/
function view_posts( $atts ){
  extract(shortcode_atts(array(
        'type' => 'articles',
        'limit' => '5',
        'pagination' => 'no',
  ), $atts));
  wp_reset_query();
  global $post;
  $term_id = get_the_tags( $post->ID );
  $tag_id = $term_id[0]->term_id;
  $db = DatabBase();
  echo '<div class="post_list">';
  if($type == 'articles'){
    $sql= "SELECT t.term_id , t.name  FROM foot_term_relationships AS tr JOIN  foot_terms AS t ON tr.term_taxonomy_id = t.term_id JOIN foot_term_taxonomy AS tt ON t.term_id = tt.term_id WHERE tr.object_id = {$post->ID} AND tt.taxonomy = 'topic_article'  LIMIT 0, 5";
    $dbresult = $db->prepare($sql);
    $dbresult->execute();
    $result = $dbresult->fetchAll( PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE);
    $topic_article_id = "";
    foreach ($result as $item_topic_article) {
      $topic_article_id = $item_topic_article->term_id;
    }
    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                  $args = array(
                                'post_type'  => 'post',
                                'publish' => true,
                                'order' => 'ASC',
                                'orderby' => 'data',
                                'paged' => $paged,
                                'posts_per_page' => $limit,
                  );
                  if (!empty($tag_id)) {
                    $args['tax_query'] = array( 'relation'=>'AND' );
                    $args['tax_query'][] = array(
                            'taxonomy' => 'post_tag',
                            'field' => 'id',
                            'terms' => $tag_id,
                            'operator' => 'IN',
                    );
                  }
                  $args['tax_query'][] = array(
                            'taxonomy' => 'topic_article',
                            'field' => 'id',
                            'terms' => $topic_article_id,
                            'operator' => 'IN',
                  );
                  $query = new WP_Query($args);
                  if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
                    <a href="<?php the_permalink() ?>" class="post_item">
                      <div class="img_post">
                        <?php the_post_thumbnail('td_265x198') ?>
                      </div>
                      <span class="name_post"><? the_title() ?></span>
                    </a>
                  <?php endwhile; endif;
  }else{
    $categories = get_the_category( $post->ID );
    var_dump( $categories );
  }
  echo '</div>';
}
add_shortcode('view_posts', 'view_posts');
/*Дочерные страницы*/
function child_post( $atts ){
  extract(shortcode_atts(array(
        'slug' => 'footbolist',
  ), $atts));
  $post_type = '';
  switch ($slug) {
  case 'footbolist':
      $post_type = 'foot_football_world_cup';
      break;
  case 'f-club':
      $post_type = 'foot_football_club';
      break;
  case 'fl':
      $post_type = 'foot_football_liga';
      break;
  case 'bukmeker':
      $post_type = 'foot_football_players';
      break;
  }
  $ancestors = get_ancestors( get_the_ID(), $post_type , 'post_type' );
  $currenturl = get_permalink();
  $i = 0;
  if(empty($ancestors)){
    $get_post_id = get_the_ID();
  }else{
    $get_post_id = $ancestors[0];
  }
      $paged = get_query_var('paged') ? get_query_var('paged') : 1;
      $args = array('post_type'  => 'f-club','publish' => true, 'order' => 'ASC','post_parent' => $get_post_id , 'orderby' => 'menu_order','paged' => $paged,'posts_per_page' => 20,);
      $query = new WP_Query($args); ?>
  <ul class="shild_page_list">
  <? if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();  $i ++;
      if($i == 1): ?>
           <li class="vc_btn3-container vc_btn3-left"><a href="<?php if(empty($ancestors)) echo $currenturl; else the_permalink($ancestors[0]);?>"
           class="vc_general vc_btn3 <?php if(empty($ancestors)) echo 'active'; ?>">Общие даные</a></li>
          <?php endif; ?>
        <?php if($currenturl == get_permalink()): ?>
           <li class="vc_btn3-container vc_btn3-left"><a href="<?php the_permalink()?>" class="vc_general vc_btn3 active"><?php the_title() ?></a></li>
        <?php else:?>
           <li class="vc_btn3-container vc_btn3-left"><a href="<?php the_permalink()?>" class="vc_general vc_btn3"><?php the_title() ?></a></li>
        <?php endif; ?>
  <?php endwhile;?>
  </ul>
  <?php endif;
}
add_shortcode('child_post', 'child_post');
/*вывод новостей*/

/*Шорткод флага*/
function ff_flag ($attr , $text=''){
  $innerHTML = '<span class="county_block">
                  <i class="flag_nt flag_'.translit($text).'" title="'.$text.'" alt="'.$text.'"></i>
                  <span>'.$text.'</span>
                </span>';

  return $innerHTML;
}
add_shortcode('ff_flag', 'ff_flag');

//Шорткод H! заголовка для страниц
function show_meta_h1_page($atts){
  extract(shortcode_atts(array(
        'slug' => '',
  ), $atts));
  if($slug == 'main'){
    $class_main = 'main';
  }
  wp_reset_query();
  global $post;
  $title_page = get_the_title($post->ID);
  $title_parent_page = get_the_title($post->post_parent);
  if($post->post_parent == 0){
    switch ($post->post_type) {
      case 'f-club':
        $title_h1 = ' ФК '.$title_page;
        break;

      default:
        $title_h1 = $title_page;
        break;
    }
  }else{
    switch ($post->post_type) {
      case 'f-club':
        $title_h1 = $title_page.' ФК '.$title_parent_page;
        break;

      default:
        # code...
        break;
    }

  }
  $innerHTML = '<h1 class="main_title_page '.$class_main.'">'.$title_h1.'</h1>';
  return $innerHTML;
}
add_shortcode('show_meta_h1_page', 'show_meta_h1_page');



require_once('shortcode_InternationalGroup.php'); 
require_once('shortcode_liga.php');
require_once('shortcode_club.php');
require_once('shortcode_world_cup.php');
require_once('shortcode_players.php');
require_once('shortcode_home.php');
require_once('shortcode_bookmaker.php');
require_once('shortcode_rating.php');
require_once('common/ff_ajax.php');
require_once('common/ff_sorting.php');
