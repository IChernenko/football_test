<?php
function ff_sorting_table($data_array , $fealds_sort , $sorting ){


	$result_array = json_decode(json_encode($data_array), True);
  
  foreach ($result_array as $key => $row) {
      $volume[$key] = $row[$fealds_sort];
  }
  if($sorting == 'ASC'){
  	$sort = SORT_ASC;
  }elseif($sorting == 'DESC'){
  	$sort = SORT_DESC;
  }

  array_multisort($volume, $sort, $result_array);

  $result = json_decode(json_encode($result_array), FALSE);

  return $result;
}