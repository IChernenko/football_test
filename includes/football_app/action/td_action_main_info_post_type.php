<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;


class td_action_main_info_post_type{

	public  function __construct() {
		
	}

	public function build_main_info_bukmeker()
	{
		Container::make( 'post_meta', "Main inform" )
        ->where( 'post_type', '=', 'bukmeker' )
        ->where( 'post_level', '=' , 1 )
        ->add_fields( [
            Field::make( 'text', 'bk_desc_official_name', 'Официальное название' )->set_width(33),
            Field::make( 'text', 'bk_desc_date_registration', 'Дата регистрации' )->set_width(33),
            Field::make( 'text', 'bk_desc_works_in', 'Работает в' )->set_width(33),
            Field::make( 'text', 'bk_desc_license', 'Лицензия' )->set_width(33),
            Field::make( 'text', 'bk_desc_min_rate', 'Минимальная ставка' )->set_width(33),
            Field::make( 'text', 'bk_desc_tax_win_russia', 'Налог на выигрыш в России' )->set_width(33),
            Field::make( 'text', 'bk_desc_official_site', 'Официальный сайт' )->set_width(33),
            Field::make( 'text', 'bk_desc_download_application', 'Скачать приложение' )->set_width(33),
            Field::make( 'text', 'bk_desc_overall_rating', 'Общий рейтинг' )->set_width(33),
            Field::make( 'text', 'bk_desc_bonus', 'Бонус' )->set_width(80),
            Field::make( 'text', 'bk_desc_bonus_currency', 'Бонус (валюта)' )->set_width(20),
            Field::make( 'text', 'bk_desc_player_reviews_last_month', 'Отзывы за последний месяц' )->set_width(25),
            Field::make( 'text', 'bk_desc_player_reviews_last_6_month', 'Отзывы за последние полгода' )->set_width(25),
            Field::make( 'text', 'bk_desc_player_reviews_last_year', 'Отзывы за последний год' )->set_width(25),
            Field::make( 'text', 'bk_desc_player_reviews', 'Отзывы за все время' )->set_width(25),
            
            



        ]); 
	}

}