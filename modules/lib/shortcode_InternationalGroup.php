<?php 
// Лиги Международные турниры

// Результаты матчей
function result_of_matches_int_g ($atts) {
  extract(shortcode_atts(array(
        'slug' => "prev",
        'limit' => 5,
  ), $atts));
  switch ($slug) {
  case 'prev':
      wp_reset_query();
      $name_liga = get_the_title();
      $innerHTML = result_of_matches_int_g_prev($limit , $name_liga);
      break;
  case 'full':
      wp_reset_query();
      global $post;
      $name_liga = get_the_title($post->post_parent);
      $innerHTML = result_of_matches_int_g_full($limit , $name_liga);
      break;
  }
  return $innerHTML;
}
add_shortcode('result_of_matches_int_g', 'result_of_matches_int_g');

//-- Краткой вывод
function result_of_matches_int_g_prev($limit , $name_liga){
  global $wpdb;
  global $dir_img_club;
  $lim = 'LIMIT 0 , '. $limit;
  $name_liga = esc_sql($name_liga);
  $sql= "SELECT com_t_type.name AS tournament_type, 
                com_t_g.name AS tournament_group , 
                fm.competition_tour ,  
                ft1.name AS team_1 , ft1.logo_link AS logo_t1 , 
                ft2.name AS team_2 , ft2.logo_link AS logo_t2 , 
                fm.play_date , fm.play_time , fm.result_1 , fm.result_2  
           FROM foot_football_club_matches fm 
      LEFT JOIN parser_common_tournament fc 
             ON fm.competition_id = fc.id 
      LEFT JOIN parser_common_tournament_type com_t_type 
             ON fm.competition_type_id = com_t_type.id 
      LEFT JOIN parser_common_tournament_group com_t_g 
             ON fm.competition_group_id = com_t_g.id 
      LEFT JOIN parser_common_club  ft1 
             ON fm.club_id_1 = ft1.id 
      LEFT JOIN parser_common_club  ft2 
             ON fm.club_id_2 = ft2.id  
          WHERE fc.name = '{$name_liga}' 
            AND status = 1  
             OR fc.name_post = '{$name_liga}' 
            AND status = 1  
       ORDER BY fm.play_date DESC {$lim} ";

  $result = $wpdb->get_results( $sql );
  $innerHTML = "<table class='footable next_match match_in_g'>";    
      $innerHTML .= "<thead>
                        <tr>
                            <th style='display: none;'></th>
                            <th class='competition'>Турнир</th>
                            <th data-hide='phone,tablet'>Тур</th>
                            <th data-hide='phone,tablet'>Дата игры</th>
                            <th data-hide='phone,tablet'>Хозяева</th>
                            <th data-hide='phone,tablet'></th>
                            <th data-hide='phone,tablet'>Гости</th>
                        </tr>  
                </thead>";          
      foreach ($result as $item_match) {
        $innerHTML .= '<tr>
                         <td style="display: none;"></td>
                         <td>'.$item_match->tournament_type.', '.$item_match->tournament_group.'</td>
                         <td>'.$item_match->competition_tour.'</td>
                         <td>'.$item_match->play_date.'</td>
                         <td><img src="'.$dir_img_club.''.$item_match->logo_t1.'" alt="'.$item_match->team_1.'" class="logo_club">'.$item_match->team_1.'</td>
                         <td>'.$item_match->result_1.'-'.$item_match->result_2.'</td>
                         <td><img src="'.$dir_img_club.''.$item_match->logo_t2.'" alt="'.$item_match->team_2.'" class="logo_club">'.$item_match->team_2.'</td>
                      </tr>';
      }
  $innerHTML .= "</table>";
  return $innerHTML;
}
//-- Большой вывод
function result_of_matches_int_g_full($limit , $name_liga){
  global $wpdb;
  global $dir_img_club;
  $name_liga = esc_sql($name_liga);
  $t_type = $_GET['t_type'];
  switch ($t_type) {
    case 'preliminary':
      $carent_preliminary = 'carent';
      $tour_type = "AND com_t_type.name = 'Предварительный турнир' ";
      break;
    case 'group':
      $carent_group = 'carent';
      $tour_type = "AND com_t_type.name = 'Групповой турнир' ";
      # code...
      break;
    case 'playoff':
      $carent_playoff = 'carent';
      $tour_type = "AND com_t_type.name = 'Плей-офф' ";
      # code...
      break;
    default:
      $carent_group = 'carent';
      $tour_type = "AND com_t_type.name = 'Групповой турнир' ";
      # code...
      break;
  }
  $innerHTML = '
                <div class="bottom_header_in_g">
                  <a href="?t_type=preliminary" class="'.$carent_preliminary.'">Предварительный турнир</a>
                  <a href="?t_type=group " class="'.$carent_group.'">Групповой турнир</a>
                  <a href="?t_type=playoff" class="'.$carent_playoff.'">Плей-офф</a>
                </div>
                ';    
  $sql= "SELECT com_t_type.name AS tournament_type, 
                com_t_g.name AS tournament_group , 
                fm.competition_tour ,  
                ft1.name AS team_1 , ft1.logo_link AS logo_t1 , 
                ft2.name AS team_2 , ft2.logo_link AS logo_t2 , 
                fm.play_date , fm.play_time , fm.result_1 , fm.result_2  
           FROM foot_football_club_matches fm 
      LEFT JOIN parser_common_tournament fc 
             ON fm.competition_id = fc.id 
      LEFT JOIN parser_common_tournament_type com_t_type 
             ON fm.competition_type_id = com_t_type.id 
      LEFT JOIN parser_common_tournament_group com_t_g 
             ON fm.competition_group_id = com_t_g.id 
      LEFT JOIN parser_common_club  ft1 
             ON fm.club_id_1 = ft1.id 
      LEFT JOIN parser_common_club  ft2 
             ON fm.club_id_2 = ft2.id  
          WHERE fc.name_post = '{$name_liga}'
            AND fm.play_date > '2017-01-01'
            AND status = 1 
                {$tour_type}
       ORDER BY fm.play_date DESC {$lim} ";
  $result = $wpdb->get_results( $sql );
  if($result){
    $innerHTML .= "<table class='footable next_match match_in_g'>";    
      $innerHTML .= "<thead>
                        <tr>
                            <th style='display: none;'></th>
                            <th class='competition'>Турнир</th>
                            <th data-hide='phone,tablet'>Тур</th>
                            <th data-hide='phone,tablet'>Дата игры</th>
                            <th data-hide='phone,tablet'>Хозяева</th>
                            <th data-hide='phone,tablet'></th>
                            <th data-hide='phone,tablet'>Гости</th>
                        </tr>  
                </thead>";          
      foreach ($result as $item_match) {
        $innerHTML .= '<tr>
                         <td style="display: none;"></td>
                         <td>'.$item_match->tournament_group.'</td>
                         <td class="tour_comp">'.$item_match->competition_tour.'</td>
                         <td>'.$item_match->play_date.'</td>
                         <td><img src="'.$dir_img_club.''.$item_match->logo_t1.'" width="20" alt="'.$item_match->team_1.'" class="logo_club">'.$item_match->team_1.'</td>
                         <td>'.$item_match->result_1.'-'.$item_match->result_2.'</td>
                         <td><img src="'.$dir_img_club.''.$item_match->logo_t2.'" width="20" alt="'.$item_match->team_2.'" class="logo_club">'.$item_match->team_2.'</td>
                      </tr>';
      }
    $innerHTML .= "</table>";

  }else{
    $innerHTML .= "<div class='no_matches'>Нет сыгранных матчей</div>";
  }
  return $innerHTML;
}

// Расписания матчей
function schedule_of_matches_clubs_int_g ($atts) {
  extract(shortcode_atts(array(
        'slug' => "prev",
        'limit' => 5,
  ), $atts));
  switch ($slug) {
  case 'prev':
      wp_reset_query();
      $name_liga = get_the_title();
      $innerHTML = schedule_of_matches_clubs_int_g_prev($limit , $name_liga);
      break;
  case 'full':
      wp_reset_query();
      global $post;
      $name_liga = get_the_title($post->post_parent);
      $innerHTML = schedule_of_matches_clubs_int_g_full($limit , $name_liga);
      break;
  }
  return $innerHTML;
}
add_shortcode('schedule_of_matches_clubs_int_g', 'schedule_of_matches_clubs_int_g');
//-- Краткой вывод
function schedule_of_matches_clubs_int_g_prev($limit , $name_liga){
  global $wpdb;
  global $dir_img_club;
  $lim = 'LIMIT 0 , '. $limit;
  $name_liga = esc_sql($name_liga);
  $sql= "SELECT com_t_type.name AS tournament_type, 
                com_t_g.name AS tournament_group , 
                fm.competition_tour ,  
                ft1.name AS team_1 , ft1.logo_link AS logo_t1 , 
                ft2.name AS team_2 , ft2.logo_link AS logo_t2 , 
                fm.play_date , fm.play_time , fm.result_1 , fm.result_2  
           FROM foot_football_club_matches fm 
      LEFT JOIN parser_common_tournament fc 
             ON fm.competition_id = fc.id 
      LEFT JOIN parser_common_tournament_type com_t_type 
             ON fm.competition_type_id = com_t_type.id 
      LEFT JOIN parser_common_tournament_group com_t_g 
             ON fm.competition_group_id = com_t_g.id 
      LEFT JOIN parser_common_club  ft1 
             ON fm.club_id_1 = ft1.id 
      LEFT JOIN parser_common_club  ft2 
             ON fm.club_id_2 = ft2.id  
          WHERE fc.name = '{$name_liga}' 
            AND status = 0
             OR fc.name_post = '{$name_liga}' 
            AND status = 0  
       ORDER BY fm.play_date ASC {$lim} ";

  $result = $wpdb->get_results( $sql );
  $innerHTML = "<table class='footable next_match match_in_g'>";    
      $innerHTML .= "<thead>
                        <tr>
                            <th style='display: none;'></th>
                            <th class='competition'>Турнир</th>
                            <th data-hide='phone,tablet'>Тур</th>
                            <th data-hide='phone,tablet'>Дата игры</th>
                            <th data-hide='phone,tablet'>Время игры</th>
                            <th data-hide='phone,tablet'>Хозяева</th>
                            <th data-hide='phone,tablet'></th>
                            <th data-hide='phone,tablet'>Гости</th>
                        </tr>  
                </thead>";          
      foreach ($result as $item_match) {
        $innerHTML .= '<tr>
                         <td style="display: none;"></td>
                         <td>'.$item_match->tournament_type.', '.$item_match->tournament_group.'</td>
                         <td>'.$item_match->competition_tour.'</td>
                         <td>'.$item_match->play_date.' </td>
                         <td>'.$item_match->play_time.'</td>
                         <td><img src="'.$dir_img_club.''.$item_match->logo_t1.'" alt="'.$item_match->team_1.'" class="logo_club">'.$item_match->team_1.'</td>
                         <td> -:-</td>
                         <td><img src="'.$dir_img_club.''.$item_match->logo_t2.'" alt="'.$item_match->team_2.'" class="logo_club">'.$item_match->team_2.'</td>
                      </tr>';
      }
  $innerHTML .= "</table>";
  return $innerHTML;
}
//-- Большой вывод
function schedule_of_matches_clubs_int_g_full($limit , $name_liga){
  global $wpdb;
  global $dir_img_club;
  $name_liga = esc_sql($name_liga);
  $t_type = $_GET['t_type'];
  switch ($t_type) {
    case 'preliminary':
      $carent_preliminary = 'carent';
      $tour_type = "AND com_t_type.name = 'Предварительный турнир' ";
      break;
    case 'group':
      $carent_group = 'carent';
      $tour_type = "AND com_t_type.name = 'Групповой турнир' ";
      # code...
      break;
    case 'playoff':
      $carent_playoff = 'carent';
      $tour_type = "AND com_t_type.name = 'Плей-офф' ";
      # code...
      break;
    default:
      $carent_playoff = 'carent';
      $tour_type = "AND com_t_type.name = 'Плей-офф' ";
      # code...
      break;
  }
  $innerHTML = '
                <div class="bottom_header_in_g">
                  <a href="?t_type=preliminary" class="'.$carent_preliminary.'">Предварительный турнир</a>
                  <a href="?t_type=group " class="'.$carent_group.'">Групповой турнир</a>
                  <a href="?t_type=playoff" class="'.$carent_playoff.'">Плей-офф</a>
                </div>
                '; 
  $sql= "SELECT com_t_type.name AS tournament_type, 
                com_t_g.name AS tournament_group , 
                fm.competition_tour ,  
                ft1.name AS team_1 , ft1.logo_link AS logo_t1 , 
                ft2.name AS team_2 , ft2.logo_link AS logo_t2 , 
                fm.play_date , fm.play_time , fm.result_1 , fm.result_2  
           FROM foot_football_club_matches fm 
      LEFT JOIN parser_common_tournament fc 
             ON fm.competition_id = fc.id 
      LEFT JOIN parser_common_tournament_type com_t_type 
             ON fm.competition_type_id = com_t_type.id 
      LEFT JOIN parser_common_tournament_group com_t_g 
             ON fm.competition_group_id = com_t_g.id 
      LEFT JOIN parser_common_club  ft1 
             ON fm.club_id_1 = ft1.id 
      LEFT JOIN parser_common_club  ft2 
             ON fm.club_id_2 = ft2.id  
          WHERE fc.name_post = '{$name_liga}' 
            AND status = 0
                {$tour_type}
       ORDER BY fm.play_date ASC {$lim} ";

  $result = $wpdb->get_results( $sql );
  if($result){
  $innerHTML .= "<table class='footable next_match match_in_g'>";    
      $innerHTML .= "<thead>
                        <tr>
                            <th style='display: none;'></th>
                            <th class='competition'>Турнир</th>
                            <th data-hide='phone,tablet'>Тур</th>
                            <th data-hide='phone,tablet'>Дата игры</th>
                            <th data-hide='phone,tablet'>Время игры</th>
                            <th data-hide='phone,tablet'>Хозяева</th>
                            <th data-hide='phone,tablet'></th>
                            <th data-hide='phone,tablet'>Гости</th>
                        </tr>  
                </thead>";          
      foreach ($result as $item_match) {
        $innerHTML .= '<tr>
                         <td style="display: none;"></td>
                         <td> '.$item_match->tournament_group.'</td>
                         <td class="tour_comp">'.$item_match->competition_tour.'</td>
                         <td>'.$item_match->play_date.' </td>
                         <td>'.$item_match->play_time.'</td>
                         <td><img src="'.$dir_img_club.''.$item_match->logo_t1.'" alt="'.$item_match->team_1.'" class="logo_club">'.$item_match->team_1.'</td>
                         <td> -:-</td>
                         <td><img src="'.$dir_img_club.''.$item_match->logo_t2.'" alt="'.$item_match->team_2.'" class="logo_club">'.$item_match->team_2.'</td>
                      </tr>';
      }
  $innerHTML .= "</table>";
  }else{
    $innerHTML .= "<div class='no_matches'>Все матчи сыгранные или еще турнир не начался</div>";
  }
  return $innerHTML;
}





// Турнирная таблица
function turnirnaya_tablitsa_int_g ($atts) {
  extract(shortcode_atts(array(
        'slug' => "prev",
        'limit' => 5,
  ), $atts));
  switch ($slug) {
  case 'prev':
      wp_reset_query();
      $name_liga = get_the_title();
      $innerHTML = turnirnaya_tablitsa_int_g_prev($limit , $name_liga);
      break;
  case 'full':
      wp_reset_query();
      global $post;
      $name_liga = get_the_title($post->post_parent);
      $innerHTML = turnirnaya_tablitsa_int_g_full($limit , $name_liga);
      break;
  }
  return $innerHTML;
}
add_shortcode('turnirnaya_tablitsa_int_g', 'turnirnaya_tablitsa_int_g');
//-- Краткой вывод
function turnirnaya_tablitsa_int_g_prev($limit , $name_liga){
  global $wpdb;
  global $dir_img_club;
  $lim = 'LIMIT 0 , '. $limit;
  $name_liga = esc_sql($name_liga);
  $sql  = "SELECT com_t_g.name AS tournament_group , 
                  football_club.name AS f_team, football_club.logo_link ,  
                  foot_tt.position , foot_tt.old_position , 
                  foot_tt.games AS quantity_games , foot_tt.wins AS winnings , foot_tt.draws AS draws  , foot_tt.loses AS losing , 
                  foot_tt.scored_goals , foot_tt.missed_goals , foot_tt.points 
             FROM foot_football_club_turnirnaya_tablitsa  foot_tt  
             JOIN parser_common_tournament com_tour 
               ON foot_tt.competition_id = com_tour.id 
        LEFT JOIN parser_common_tournament_group com_t_g 
               ON foot_tt.competition_group_id = com_t_g.id 
             JOIN parser_common_club  football_club 
               ON foot_tt.club_id = football_club.id 
            WHERE com_tour.name = '{$name_liga}' 
               OR com_tour.name_post = '{$name_liga}'  
         ORDER BY com_t_g.name ASC , foot_tt.position ASC  {$lim}";
  $result = $wpdb->get_results( $sql);
  $result = array_group($result);
  //echo "<pre>"; print_r($result); echo "</pre>";
 
  foreach ($result as $key => $value) {
     $i =1  ;   
    $innerHTML .= "<div class='row_table_in_g'><table class='turnirnaya_tablitsa_wc turnirnaya_tablitsa_wc_home turnirnaya_tablitsa_in_g'>";
     $innerHTML .= "<tr class='group'><th colspan='9' >".$key."</th> </tr>";
    $innerHTML .= "<tr>
                      <th style='width: 50px;'>№</th>
                      <th class='wc_team'>Команда</th>
                      <th>И</th>
                      <th>О</th>
                  </tr>"; 
    foreach ($value as $item_team) { 
     $innerHTML .= '<tr>
                <td>'.$i++.'</td>
                <td class="team_club"> <img src="'.$dir_img_club.''.$item_team->logo_link.'" alt="'.$item_team->f_team.'" class="logo_club">'.$item_team->f_team.'</td>
                <td>'.$item_team->quantity_games.'</td>
                <td>'.$item_team->points.'</td>
            </tr>';
    }
    $innerHTML .="</table></div>";
  }
  $innerHTML .= "<div class='opis_table opis_table_home'><b>И</b> - игры, <b>В</b> - выигрыши, <b>Н</b> - ничья, <b>П</b> - поражения, <b>ЗГ/з</b> - забитые голы, <b>ПГ</b> - пропущенные голы, <b>О</b> - очки</div>";
  return $innerHTML;
}
//-- Большой вывод
function turnirnaya_tablitsa_int_g_full($limit , $name_liga){
  $t_type = $_GET['t_type'];
  switch ($t_type) {
    case 'preliminary':
      $carent_preliminary = 'carent';
      $tour_type = "AND com_t_type.name = 'Предварительный турнир' ";
      $render = render_playoff_t_tabl($name_liga , $tour_type ,  $limit );
      break;
    case 'group':
      $carent_group = 'carent';
      $tour_type = "AND com_t_type.name = 'Групповой турнир' ";
      $render = render_group_t_tabl($name_liga , $tour_type ,  $limit );
      break;
    case 'playoff':
      $carent_playoff = 'carent';
      $tour_type = "AND com_t_type.name = 'Плей-офф' ";
      $render = render_playoff_t_tabl($name_liga , $tour_type ,  $limit );
      break;
    default:
      $carent_playoff = 'carent';
      $tour_type = "AND com_t_type.name = 'Плей-офф' ";
      $render = render_playoff_t_tabl($name_liga , $tour_type ,  $limit );
      break;
  }
  $innerHTML = '
                <div class="bottom_header_in_g">
                  <a href="?t_type=preliminary" class="'.$carent_preliminary.'">Предварительный турнир</a>
                  <a href="?t_type=group " class="'.$carent_group.'">Групповой турнир</a>
                  <a href="?t_type=playoff" class="'.$carent_playoff.'">Плей-офф</a>
                </div>
                '; 
  $innerHTML .= $render;

  return $innerHTML;
}
function render_group_t_tabl($name_liga , $tour_type ,  $limit){
  global $wpdb;
  global $dir_img_club;
  $name_liga = esc_sql($name_liga);
  $sql  = "SELECT com_t_g.name AS tournament_group , 
                  football_club.name AS f_team, football_club.logo_link ,  
                  foot_tt.position , foot_tt.old_position , 
                  foot_tt.games AS quantity_games , foot_tt.wins AS winnings , foot_tt.draws AS draws  , foot_tt.loses AS losing , 
                  foot_tt.scored_goals , foot_tt.missed_goals , foot_tt.points 
             FROM foot_football_club_turnirnaya_tablitsa  foot_tt  
             JOIN parser_common_tournament com_tour 
               ON foot_tt.competition_id = com_tour.id 
        LEFT JOIN parser_common_tournament_group com_t_g 
               ON foot_tt.competition_group_id = com_t_g.id 
             JOIN parser_common_club  football_club 
               ON foot_tt.club_id = football_club.id 
            WHERE com_tour.name_post = '{$name_liga}'  
         ORDER BY com_t_g.name ASC , foot_tt.position ASC  {$lim}";
  $result = $wpdb->get_results( $sql);
  $result = array_group($result);
  //echo "<pre>"; print_r($result); echo "</pre>";
 
  foreach ($result as $key => $value) {
     $i =1  ;   
    $innerHTML .= "<table class='turnirnaya_tablitsa_wc_home turnirnaya_tablitsa_in_g turnirnaya_tablitsa_in_g_full'>";
     $innerHTML .= "<tr class='group'><th colspan='9' >".$key."</th> </tr>";
    $innerHTML .= "<tr>
                      <th style='width: 50px;'>№</th>
                      <th class='wc_team'>Команда</th>
                      <th>И</th>
                      <th>В</th>
                      <th>Н</th>
                      <th>П</th>
                      <th>ЗГ</th>
                      <th>ПГ</th>
                      <th>О</th>
                  </tr>"; 
    foreach ($value as $item_team) { 
     $innerHTML .= '<tr>
                <td>'.$i++.'</td>
                <td class="team_club"> <img src="'.$dir_img_club.''.$item_team->logo_link.'" alt="'.$item_team->f_team.'" class="logo_club">'.$item_team->f_team.'</td>
                <td>'.$item_team->quantity_games.'</td>
                <td>'.$item_team->winnings.'</td>
                <td>'.$item_team->draws.'</td>
                <td>'.$item_team->losing.'</td>
                <td>'.$item_team->scored_goals.'</td>
                <td>'.$item_team->missed_goals.'</td>
                <td>'.$item_team->points.'</td>
            </tr>';
    }
    $innerHTML .="</table>";
  }
  $innerHTML .= "<div class='opis_table opis_table_home'><b>И</b> - игры, <b>В</b> - выигрыши, <b>Н</b> - ничья, <b>П</b> - поражения, <b>ЗГ/з</b> - забитые голы, <b>ПГ</b> - пропущенные голы, <b>О</b> - очки</div>";
  return $innerHTML;
}
function render_playoff_t_tabl($name_liga , $tour_type ,  $limit){
  global $wpdb;
  global $dir_img_club;
  $name_liga = esc_sql($name_liga);

  $sql= "(SELECT com_t_g.name AS tournament_group , 
                com_t_type.name AS tournament_type, 
                fm.competition_tour , 
                ft1.name AS team_general ,
                ft1.name AS team_1 , ft1.logo_link AS logo_t1 , 
                ft2.name AS team_2 , ft2.logo_link AS logo_t2 , 
                fm.play_date , fm.play_time , fm.result_1 , fm.result_2  , fm.result_extra_time , fm.result_match
           FROM foot_football_club_matches fm 
      LEFT JOIN parser_common_tournament fc 
             ON fm.competition_id = fc.id 
      LEFT JOIN parser_common_tournament_type com_t_type 
             ON fm.competition_type_id = com_t_type.id 
      LEFT JOIN parser_common_tournament_group com_t_g 
             ON fm.competition_group_id = com_t_g.id 
      LEFT JOIN parser_common_club  ft1 
             ON fm.club_id_1 = ft1.id 
      LEFT JOIN parser_common_club  ft2 
             ON fm.club_id_2 = ft2.id  
          WHERE fc.name_post = '{$name_liga}'
            AND fm.competition_tour = 1
                {$tour_type}
       ORDER BY fm.play_date DESC {$lim} )

          UNION

        (SELECT com_t_g.name AS tournament_group , 
                com_t_type.name AS tournament_type, 
                fm.competition_tour , 
                ft2.name AS team_general ,
                ft1.name AS team_1 , ft1.logo_link AS logo_t1 , 
                ft2.name AS team_2 , ft2.logo_link AS logo_t2 , 
                fm.play_date , fm.play_time , fm.result_1 , fm.result_2  , fm.result_extra_time , fm.result_match
           FROM foot_football_club_matches fm 
      LEFT JOIN parser_common_tournament fc 
             ON fm.competition_id = fc.id 
      LEFT JOIN parser_common_tournament_type com_t_type 
             ON fm.competition_type_id = com_t_type.id 
      LEFT JOIN parser_common_tournament_group com_t_g 
             ON fm.competition_group_id = com_t_g.id 
      LEFT JOIN parser_common_club  ft1 
             ON fm.club_id_1 = ft1.id 
      LEFT JOIN parser_common_club  ft2 
             ON fm.club_id_2 = ft2.id  
          WHERE fc.name_post = '{$name_liga}'
            AND fm.competition_tour = 2
                {$tour_type}
       ORDER BY fm.play_date DESC {$lim} )

       ";
  $result = $wpdb->get_results( $sql );
  $result = array_group_by($result , 'tournament_group' , 'team_general');

  //Сортировка масива по ключам
  $result_array = json_decode(json_encode($result), True);
  ksort($result_array , SORT_FLAG_CASE );
  $result = json_decode(json_encode($result_array), FALSE);

  $innerHTML  = '';
  foreach ($result as $key => $item_tournament_group) {
    $innerHTML  .= '<table class="table_tout_in_g">';
      $innerHTML  .= '<tr><th colspan="2" class="tournament_group">'.$key.'</th></tr>';
      foreach ($item_tournament_group as $key => $item_match) {
        $team_winner = '';
        //Подитель в 2 матчах
        if($item_match[0]->result_match > 0 and $item_match[1]->result_match > 0){
          $team_winner = team_winner($item_match[0]->team_1 , $item_match[0]->team_2 , $item_match[0]->result_1 , $item_match[0]->result_2  , $item_match[1]->result_2 , $item_match[1]->result_1);
          $result_1_sum = $item_match[0]->result_1 + $item_match[1]->result_2;
          $result_2_sum = $item_match[0]->result_2 + $item_match[1]->result_1;
        }else{
          $result_1_sum = '-';
          $result_2_sum = '-';
        }
        $result_summ = $result_1_sum.' : '.$result_2_sum;

        //Результат 1 встечи
        if ($item_match[0]->result_match == 0) {
          $result_match = '-:- ';
        }else{
            $result_match = ''.$item_match[0]->result_1.' : '.$item_match[0]->result_2.' <span> '.$item_match[0]->result_extra_time.'</span>';
            
        }
        //Результат 2 встечи
        if ($item_match[1]->result_match == 0) {
          $result_match_2 = '-:-';
        }else{
            $result_match_2 = ''.$item_match[1]->result_1.' : '.$item_match[1]->result_2.' <span>'.$item_match[1]->result_extra_time.'</span>';
        }

        switch ($item_match[0]->result_match) {
          case  1:
            $teams_1 = ' <span> <img src="'.$dir_img_club.''.$item_match[0]->logo_t1.'" width="20" alt="'.$item_match[0]->team_1.'" class="logo_club"> <strong>'.$item_match[0]->team_1.'</strong> </span>
                         <span> <img src="'.$dir_img_club.''.$item_match[0]->logo_t2.'" width="20" alt="'.$item_match[0]->team_2.'" class="logo_club"> '.$item_match[0]->team_2.'</span>';
            break;
          case  2:
            $teams_1 = ' <span> <img src="'.$dir_img_club.''.$item_match[0]->logo_t1.'" width="20" alt="'.$item_match[0]->team_1.'" class="logo_club"> '.$item_match[0]->team_1.'</span>
                         <span> <img src="'.$dir_img_club.''.$item_match[0]->logo_t2.'" width="20" alt="'.$item_match[0]->team_2.'" class="logo_club"><strong> '.$item_match[0]->team_2.'</strong> </span>';
            break;
          case  3:
            $teams_1 = ' <span> <img src="'.$dir_img_club.''.$item_match[0]->logo_t1.'" width="20" alt="'.$item_match[0]->team_1.'" class="logo_club">'.$item_match[0]->team_1.' </span>
                         <span> <img src="'.$dir_img_club.''.$item_match[0]->logo_t2.'" width="20" alt="'.$item_match[0]->team_2.'" class="logo_club"> '.$item_match[0]->team_2.'</span>';
            break;
          default:
            $teams_1 = ' <span> <img src="'.$dir_img_club.''.$item_match[0]->logo_t1.'" width="20" alt="'.$item_match[0]->team_1.'" class="logo_club">'.$item_match[0]->team_1.' </span>
                         <span> <img src="'.$dir_img_club.''.$item_match[0]->logo_t2.'" width="20" alt="'.$item_match[0]->team_2.'" class="logo_club"> '.$item_match[0]->team_2.'</span>';
            break;
          
        }
         switch ($item_match[1]->result_match) {
          case  1:
            $teams_2 = ' <span> <img src="'.$dir_img_club.''.$item_match[1]->logo_t1.'" width="20" alt="'.$item_match[1]->team_1.'" class"logo_club"> <strong>'.$item_match[1]->team_1.'</strong> </span>
                         <span> <img src="'.$dir_img_club.''.$item_match[1]->logo_t2.'" width="20" alt="'.$item_match[1]->team_2.'" class="logo_club"> '.$item_match[1]->team_2.'</span>';
            break;
          case  2:
            $teams_2 = ' <span> <img src="'.$dir_img_club.''.$item_match[1]->logo_t1.'" width="20" alt="'.$item_match[1]->team_1.'" class="logo_club"> '.$item_match[1]->team_1.'</span>
                         <span> <img src="'.$dir_img_club.''.$item_match[1]->logo_t2.'" width="20" alt="'.$item_match[1]->team_2.'" class="logo_club"><strong> '.$item_match[1]->team_2.'</strong> </span>';
            break;
          case  3:
            $teams_2 = ' <span> <img src="'.$dir_img_club.''.$item_match[1]->logo_t1.'" width="20" alt="'.$item_match[1]->team_1.'" class="logo_club">'.$item_match[1]->team_1.' </span>
                         <span> <img src="'.$dir_img_club.''.$item_match[1]->logo_t2.'" width="20" alt="'.$item_match[1]->team_2.'" class="logo_club"> '.$item_match[1]->team_2.'</span>';
            break;
          default:
            $teams_2 = ' <span> <img src="'.$dir_img_club.''.$item_match[1]->logo_t1.'" width="20" alt="'.$item_match[1]->team_1.'" class="logo_club">'.$item_match[1]->team_1.' </span>
                         <span> <img src="'.$dir_img_club.''.$item_match[1]->logo_t2.'" width="20" alt="'.$item_match[1]->team_2.'" class="logo_club"> '.$item_match[1]->team_2.'</span>';
            break;

          
        }
        
        
        $innerHTML  .= '<tr>
                          <td>
                              <div class="match_playof match_club_1">
                                <div class="play_date">'.$item_match[0]->play_date.'</div>
                                <div class="play_time">'.$item_match[0]->play_time.'</div>
                                <div class="club_teams">
                                  '.$teams_1.'
                                </div>
                                <div class="result_match">'.$result_match.'</div>
                              </div>
                              <div class="match_playof match_club_2">
                                <div class="play_date">'.$item_match[1]->play_date.'</div>
                                <div class="play_time">'.$item_match[1]->play_time.'</div>
                                <div class="club_teams">
                                  '.$teams_2.'
                                </div>
                                <div class="result_match">'.$result_match_2.'</div>
                              </div>
                          </td>
                          <td class="td_match_winner">
                            <div class="match_playof match_winner"><strong>'.$team_winner.'</strong></div>
                            <div class="match_playof match_winner"><span class="summ_matches">Сумма мячей: '.$result_summ.'</span></div>
                          </td>
                        </tr>';
      }

    $innerHTML .= '</table>';
  }
  return $innerHTML;

}
function team_winner($team_1 , $team_2 , $result_1_1 , $result_1_2  , $result_2_1 , $result_2_2){
      if($result_1_1 + $result_2_1 > $result_1_2 + $result_2_2){
        return $team_1;
      }elseif($result_1_1 + $result_2_1 < $result_1_2 + $result_2_2){
        return $team_2;
      }elseif($result_1_1 + $result_2_1 == $result_1_2 + $result_2_2){
        if($result_1_2 > $result_2_1 and $result_2_1 !== 0){
          return $team_2;
        }else{
           return $team_1;
        }
      }
    

}

