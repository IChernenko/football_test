<?php
// Футболисты
/*Показать информацию о игроке*/
function show_detal_player() {
	global $wpdb;
	wp_reset_query();
	$name_player = get_the_title();
	$post_id = get_the_ID();
  $post_metadate = get_post_meta($post_id);

	$innerHTML = "<table>";
	$innerHTML .= '
				<tr><td><strong>Имя:</strong></td><td>'.$post_metadate['_player_desc_name_player'][0].'</td></tr>
				<tr><td><strong>Прозвище:</strong></td><td>'.$post_metadate['_player_desc_nickname'][0].'</td></tr>
				<tr><td><strong>Дата рождения:&nbsp;</strong></td><td>'.$post_metadate['_player_desc_birthday'][0].'</td></tr>
				<tr><td><strong>Рост и вес:</strong></td><td>'.$post_metadate['_player_desc_growth'][0].' см, '.$post_metadate['_player_desc_weight'][0].' кг</td></tr>
				<tr><td><strong>Позиция:</strong></td><td>'.$post_metadate['_player_desc_amplua'][0].'</td></tr>
				<tr><td><strong>Сборная:&nbsp;</strong></td> <td>'.$post_metadate['_player_desc_national_team'][0].'</td></tr>
				<tr><td><strong>Текущий клуб:</strong></td><td>«'.$post_metadate['_player_desc_club'][0].'»</td></tr>';
	$innerHTML .= "</table>";
	return $innerHTML;
}
add_shortcode('show_detal_player', 'show_detal_player');
/* Главная статистика футболиста */
function show_players_statistic_main() {
	global $wpdb;
	global $ff_cfg;
	wp_reset_query();
	global $post;
	$name_player = get_the_title($post->post_parent);

	$sql = "SELECT  com_comp.name AS competition,  
									player_stat.went_the_field , 
									player_stat.left_the_field , 
									player_stat.played_minutes , player_stat.goals , 
									player_stat.penalty , player_stat.pass , player_stat.yellow_cards , player_stat.red_cards   
						FROM  {$ff_cfg['db_table']['football_player_statistic']}  player_stat  
						JOIN  {$ff_cfg['db_table']['common_player']} com_player
						  ON  player_stat.player_id = com_player.id 
						JOIN  {$ff_cfg['db_table']['common_tournament']} com_comp 
						  ON  player_stat.competition_id = com_comp.id 
					 WHERE  com_player.name_post = '{$name_player}'  
				ORDER BY  com_comp.name DESC ";
	$result = $wpdb->get_results( $sql);
	$result = array_group($result);
	//echo "<pre>"; print_r($result); echo "</pre>";
	$innerHTML = "<table class='footable table_statistic'>";
	$innerHTML .= "<thead>
									<tr>
											<th style='display: none;'></th>
											<th>Турнир</th> 
											<th data-hide='phone,tablet'>М</th>
											<th data-hide='phone,tablet'>Мин</th>
											<th data-hide='phone,tablet'>С</th>
											<th data-hide='phone,tablet'>З</th>
											<th data-hide='phone,tablet'>В/з</th>
											<th data-hide='phone,tablet'>Г(пен.)</th>
											<th data-hide='phone,tablet'>П</th>
											<th data-hide='phone,tablet'>ЖК</th>
											<th data-hide='phone,tablet'>КК</th>
									</tr>
								</thead>";
	$i = 0;
	foreach ($result as $key => $value) {
		$i ++;
		$innerHTML .= "<tr>";
		$innerHTML .= '<td style="display: none;""></td>';
		$innerHTML .= "<td>".$key."</td>";
	  foreach ($value as $item_stat) {
	  	if($item_stat->went_the_field == 0){
	  		$claim[$i] += 1;
	  	}else{
	  		$claim[$i] += 0;
	  	}
	  	if($item_stat->went_the_field == 1){
	  		$start[$i] += 1;
	  	}else{
	  		$start[$i] += 0;
	  	}
	  	if($item_stat->went_the_field > 1){
	  		$substitution[$i] += 1;
	  	}else{
	  		$substitution[$i] += 0;
	  	}
	  	if($item_stat->went_the_field >= 1){
	  		$matches[$i] += 1;
	  	}else{
	  		$matches[$i] += 0;
	  	}
	  	$played_minutes[$i] += $item_stat->played_minutes;
	  	$total_goals[$i] += $item_stat->goals + $item_stat->penalty;
	  	$pass[$i] += $item_stat->pass;
	  	$yellow_cards[$i] += $item_stat->yellow_cards;
	  	$red_cards[$i] += $item_stat->red_cards;
	  	
	  }
	  $innerHTML .= "<td>".$matches[$i]."</td>";
	  $innerHTML .= "<td>".$played_minutes[$i]."</td>";
	  $innerHTML .= "<td>".$start[$i]."</td>";
	  $innerHTML .= "<td>".$substitution[$i]."</td>";
	  $innerHTML .= "<td>".$claim[$i]."</td>";
	  $innerHTML .= "<td>".$total_goals[$i]."</td>";
	  $innerHTML .= "<td>".$pass[$i]."</td>";
	  $innerHTML .= "<td>".$yellow_cards[$i]."</td>";
	  $innerHTML .= "<td>".$red_cards[$i]."</td>";
	  $innerHTML .= "</tr>";
	}
	   $innerHTML .= '<tr>
	   										<td style="display: none;""></td>
		                    <td><strong>Итого</strong></td>
						            <td><strong>'.array_sum($matches).'</strong></td>
						            <td><strong>'.array_sum($played_minutes).'</strong></td>
						            <td><strong>'.array_sum($start).'</strong></td>
						            <td><strong>'.array_sum($substitution).'</strong></td>
						            <td><strong>'.array_sum($claim).'</strong></td>
						            <td><strong>'.array_sum($total_goals).'</strong></td>
						            <td><strong>'.array_sum($pass).'</strong></td>
						            <td><strong>'.array_sum($yellow_cards).'</strong></td>
						            <td><strong>'.array_sum($red_cards).'</strong></td>
						        </tr>';
	$innerHTML .= "</table>";
		$innerHTML .= "<div class='opis_table'><b>М</b> - матчи, <b>Мин</b> - сыграл минут, <b>С</b> - старт, <b>З</b> - замена, <b>В/з</b> - в заявке, <b>Г(пен.)</b> - голы (ПЕН.), <b>П</b> - пасы, <b>ЖК</b> - желтая карточка, <b>КК</b> - красная карточка</div>";
	return $innerHTML;
}
add_shortcode('show_players_statistic_main', 'show_players_statistic_main');

/*Статистика игрока*/
function show_players_statistic() {
	global $wpdb;
	global $post;
	wp_reset_query();
	$name_player = get_the_title($post->post_parent);
	//print_r($name_player);

	//Выборка статистики по клубам
	$sql = "SELECT  player_stat.id  ,
									DATE_FORMAT(club_m.play_date, '%d.%m.%Y') AS play_date ,  
									com_comp.name AS competition, 
									com_club.name AS football_club , 
									com_club_1.name AS football_team_1 , 
									com_club_2.name AS football_team_2 , 
									club_m.result_1 ,  club_m.result_2 ,  
									player_stat.went_the_field , player_stat.left_the_field , player_stat.played_minutes , player_stat.goals , 
									player_stat.penalty , player_stat.pass , player_stat.yellow_cards , player_stat.red_cards   
						FROM  foot_football_player_statistic player_stat  
						JOIN  parser_common_player com_player
						  ON  player_stat.player_id = com_player.id 
						JOIN  foot_football_club_matches club_m
						  ON  player_stat.m_club_id = club_m.id 
						JOIN  parser_common_tournament com_comp 
						  ON  club_m.competition_id = com_comp.id 
						JOIN  parser_common_club  com_club_1 
						  ON  club_m.club_id_1 = com_club_1.id 
						JOIN  parser_common_club  com_club_2
						  ON  club_m.club_id_2 = com_club_2.id 
						JOIN  foot_football_player_description  player_description 
						  ON  player_stat.player_id = player_description.player_id 
						JOIN  parser_common_club  com_club 
						  ON  player_description.club_id = com_club.id 
					 WHERE  com_player.name_post = '{$name_player}'  
					 	 AND  club_m.status = 1 
				ORDER BY   player_stat.id DESC   ";
	$result = $wpdb->get_results( $sql);

	//Выборка статистики за национальную сборную
	$sql1 = "SELECT player_stat.id  ,
									DATE_FORMAT(national_team_m.play_date, '%d.%m.%Y') AS play_date ,  
									com_comp.name AS competition, 
									com_county.name AS football_club , 
									team_1.name AS football_team_1 , 
									team_2.name AS football_team_2 , 
									national_team_m.result_1 ,  national_team_m.result_2 ,    
									player_stat.went_the_field , player_stat.left_the_field , player_stat.played_minutes , player_stat.goals , 
									player_stat.penalty , player_stat.pass , player_stat.yellow_cards , player_stat.red_cards   
						FROM  foot_football_player_statistic player_stat  
						JOIN  parser_common_player com_player
						  ON  player_stat.player_id = com_player.id 
						JOIN  foot_football_national_team_matches national_team_m
						  ON  player_stat.m_nt_id = national_team_m.id 
						JOIN  parser_common_tournament com_comp 
						  ON  national_team_m.competition_id = com_comp.id
		 	 LEFT JOIN  parser_common_national_team  team_1
						  ON  national_team_m.nt_id_1 = team_1.id
			 LEFT JOIN  parser_common_national_team  team_2
						  ON  national_team_m.nt_id_2 = team_2.id
						JOIN  foot_football_player_description  player_description 
						  ON  player_stat.player_id = player_description.player_id 
						JOIN  parser_common_country  com_county 
						  ON  player_description.national_team_id = com_county.id 
					 WHERE  com_player.name_post = '{$name_player}'  
					 	 AND  national_team_m.status = 1 
				ORDER BY  player_stat.id DESC  ";
	$result_2 = $wpdb->get_results( $sql1);
	
	//Слияния масивов
	$result = array_merge($result , $result_2);
	$result_array = json_decode(json_encode($result), True);
	
	//Сортировка масива по столбке play_date
	function mysort($a, $b) {
    return strtotime($b['play_date']) - strtotime($a['play_date']);
	}
	usort($result_array, 'mysort');
	//echo "<pre>"; print_r($result_array); echo "</pre>";
	$result = json_decode(json_encode($result_array), FALSE);

	$innerHTML = "<table class='footable table_statistic_p'>";
		$innerHTML .= "<thead>
													<tr>
															<th style='display: none;'></th>
															<th>Дата</th> 
															<th class='competition'>Турнир</th>
															<th data-hide='phone,tablet'>Хозяева</th>
															<th data-hide='phone,tablet'>Счет</th>
															<th data-hide='phone,tablet'>Гости</th>
															<th data-hide='phone,tablet'>В</th>
															<th data-hide='phone,tablet'>У</th>
															<th data-hide='phone,tablet'>МИН</th>
															<th data-hide='phone,tablet'>Г</th>
															<th data-hide='phone,tablet'>ПЕН</th>
															<th data-hide='phone,tablet'>П</th>
															<th data-hide='phone,tablet'>ЖК</th>
															<th data-hide='phone,tablet'>КК</th>
													</tr>
									</thead>";
		$played_minutes = "";
		$goals = "";
		$penalty = "";
		$pass = "";
		$yellow_cards = "";
		$red_cards = "";

		foreach ($result as $item_stat) {
			$played_minutes += $item_stat->played_minutes;
			$goals += $item_stat->goals;
			$penalty += $item_stat->penalty;
			$pass += $item_stat->pass;
			$yellow_cards += $item_stat->yellow_cards;
			$red_cards += $item_stat->red_cards;

			if ($item_stat->football_team_1 == $item_stat->football_club) {
			    $football_team_1 = '<strong>'.$item_stat->football_team_1.'</strong>';
			}else{
				 $football_team_1 = $item_stat->football_team_1;
			}


			if ($item_stat->football_team_2 == $item_stat->football_club) {
			    $football_team_2 = '<strong>'.$item_stat->football_team_2.'</strong>';
			}else{
				 $football_team_2 = $item_stat->football_team_2;
			}

			$innerHTML .= '<tr>
													<td style="display: none;""></td>
							            <td>'.$item_stat->play_date.'</td>
							            <td>'.$item_stat->competition.'</td>
							            <td class="ft_1">'.$football_team_1.'</td>
							            <td><strong>'.$item_stat->result_1.'-'.$item_stat->result_2.'</strong></td>
							            <td class="ft_2">'.$football_team_2.'</td>';
			if($item_stat->went_the_field == 0){
		 	   	 $innerHTML .= '<td colspan="8">Неиспользованная замена</td>';
			}else{
			  	 $innerHTML .= '<td>'.$item_stat->went_the_field.'</td>
							            <td>'.$item_stat->left_the_field.'</td>
							            <td>'.$item_stat->played_minutes.'</td>
							            <td>'.$item_stat->goals.'</td>
							            <td>'.$item_stat->penalty.'</td>
							            <td>'.$item_stat->pass.'</td>
							            <td>'.$item_stat->yellow_cards.'</td>
							            <td>'.$item_stat->red_cards.'</td>';
					
			}
		  $innerHTML .= '</tr>';
		}
			$innerHTML .= '<tr>
												<td style="display: none;""></td>
		                    <td ><strong>Итого</strong></td>
						            <td>-</td>
						            <td>-</td>
						            <td>-</td>
						            <td>-</td>
						            <td>-</td>
						            <td>-</td>
						            <td><strong>'.$played_minutes.'</strong></td>
						            <td><strong>'.$goals.'</strong></td>
						            <td><strong>'.$penalty.'</strong></td>
						            <td><strong>'.$pass.'</strong></td>
						            <td><strong>'.$yellow_cards.'</strong></td>
						            <td><strong>'.$red_cards.'</strong></td>
						        </tr>';
	$innerHTML .= "</table>";
	$innerHTML .= "<div class='opis_table'><b>В</b>&nbsp;-&nbsp;вышел на поле, 
	<b>У</b>&nbsp;-&nbsp;ушел с поля, 
	<b>МИН</b>&nbsp;-&nbsp;сыграл минут, 
	<b>Г</b>&nbsp;-&nbsp;забил голов, 
	<b>ПЕН</b>&nbsp;-&nbsp;забил голов с пенальти, 
	<b>П</b>&nbsp;-&nbsp;сделал голевых передач, 
	<b>ЖК</b>&nbsp;-&nbsp;желтые карточки, 
	<b>КК</b>&nbsp;-&nbsp;красные карточки </div>";
	return $innerHTML;
	
}
add_shortcode('show_players_statistic', 'show_players_statistic');

function show_short_statistic_player(){
	global $wpdb;
	global $ff_cfg;
	wp_reset_query();
	global $post;
	$name_player = get_the_title($post->post_parent);

	$sql = "SELECT  com_comp.name AS competition,  
									player_stat.went_the_field , 
									player_stat.left_the_field , 
									player_stat.played_minutes , player_stat.goals , 
									player_stat.penalty , player_stat.pass , player_stat.yellow_cards , player_stat.red_cards   
						FROM  {$ff_cfg['db_table']['football_player_statistic']}  player_stat  
						JOIN  {$ff_cfg['db_table']['common_player']} com_player
						  ON  player_stat.player_id = com_player.id 
						JOIN  {$ff_cfg['db_table']['common_tournament']} com_comp 
						  ON  player_stat.competition_id = com_comp.id 
					 WHERE  com_player.name_post = '{$name_player}'  
				ORDER BY  com_comp.name DESC ";
	$result = $wpdb->get_results( $sql);
	$result = array_group($result);
	//echo "<pre>"; print_r($result); echo "</pre>";
	$innerHTML = "<table class='footable table_statistic'>";
	$innerHTML .= "<thead>
									<tr>
											<th style='display: none;'></th>
											<th>Турнир</th> 
											<th data-hide='phone,tablet'>М</th>
											<th data-hide='phone,tablet'>Мин</th>
											<th data-hide='phone,tablet'>С</th>
											<th data-hide='phone,tablet'>З</th>
											<th data-hide='phone,tablet'>В/з</th>
											<th data-hide='phone,tablet'>Г(пен.)</th>
											<th data-hide='phone,tablet'>П</th>
											<th data-hide='phone,tablet'>ЖК</th>
											<th data-hide='phone,tablet'>КК</th>
									</tr>
								</thead>";
	$i = 0;
	foreach ($result as $key => $value) {
		$i ++;
		$innerHTML .= "<tr>";
		$innerHTML .= '<td style="display: none;""></td>';
		$innerHTML .= "<td>".$key."</td>";
	  foreach ($value as $item_stat) {
	  	if($item_stat->went_the_field == 0){
	  		$claim[$i] += 1;
	  	}else{
	  		$claim[$i] += 0;
	  	}
	  	if($item_stat->went_the_field == 1){
	  		$start[$i] += 1;
	  	}else{
	  		$start[$i] += 0;
	  	}
	  	if($item_stat->went_the_field > 1){
	  		$substitution[$i] += 1;
	  	}else{
	  		$substitution[$i] += 0;
	  	}
	  	if($item_stat->went_the_field >= 1){
	  		$matches[$i] += 1;
	  	}else{
	  		$matches[$i] += 0;
	  	}
	  	$played_minutes[$i] += $item_stat->played_minutes;
	  	$total_goals[$i] += $item_stat->goals + $item_stat->penalty;
	  	$pass[$i] += $item_stat->pass;
	  	$yellow_cards[$i] += $item_stat->yellow_cards;
	  	$red_cards[$i] += $item_stat->red_cards;
	  	
	  }
	  $innerHTML .= "<td>".$matches[$i]."</td>";
	  $innerHTML .= "<td>".$played_minutes[$i]."</td>";
	  $innerHTML .= "<td>".$start[$i]."</td>";
	  $innerHTML .= "<td>".$substitution[$i]."</td>";
	  $innerHTML .= "<td>".$claim[$i]."</td>";
	  $innerHTML .= "<td>".$total_goals[$i]."</td>";
	  $innerHTML .= "<td>".$pass[$i]."</td>";
	  $innerHTML .= "<td>".$yellow_cards[$i]."</td>";
	  $innerHTML .= "<td>".$red_cards[$i]."</td>";
	  $innerHTML .= "</tr>";
	}
	   $innerHTML .= '<tr>
	   										<td style="display: none;""></td>
		                    <td><strong>Итого</strong></td>
						            <td><strong>'.array_sum($matches).'</strong></td>
						            <td><strong>'.array_sum($played_minutes).'</strong></td>
						            <td><strong>'.array_sum($start).'</strong></td>
						            <td><strong>'.array_sum($substitution).'</strong></td>
						            <td><strong>'.array_sum($claim).'</strong></td>
						            <td><strong>'.array_sum($total_goals).'</strong></td>
						            <td><strong>'.array_sum($pass).'</strong></td>
						            <td><strong>'.array_sum($yellow_cards).'</strong></td>
						            <td><strong>'.array_sum($red_cards).'</strong></td>
						        </tr>';
	$innerHTML .= "</table>";
		$innerHTML .= "<div class='opis_table'><b>М</b> - матчи, <b>Мин</b> - сыграл минут, <b>С</b> - старт, <b>З</b> - замена, <b>В/з</b> - в заявке, <b>Г(пен.)</b> - голы (ПЕН.), <b>П</b> - пасы, <b>ЖК</b> - желтая карточка, <b>КК</b> - красная карточка</div>";
	return $innerHTML;
}
add_shortcode('show_short_statistic_player', 'show_players_statistic_main');