<?php
td_api_autoload::add('td_football_app', td_global::$get_template_directory . '/includes/football_app/td_football_app.php');
td_api_autoload::add('td_statistic_football_matches', td_global::$get_template_directory . '/includes/football_app/td_statistic_football_matches.php');
td_api_autoload::add('td_football_shortcode', td_global::$get_template_directory . '/includes/football_app/td_football_shortcode.php');
td_api_autoload::add('td_football_bukmekers', td_global::$get_template_directory . '/includes/football_app/td_football_bukmekers.php');
td_api_autoload::add('td_model_football', td_global::$get_template_directory . '/includes/football_app/model/td_model_football.php');
td_api_autoload::add('td_football_posts_custom_block_content', td_global::$get_template_directory . '/includes/football_app/td_football_posts_custom_block_content.php');

td_api_autoload::add('td_football_match_center', td_global::$get_template_directory . '/includes/football_app/td_football_match_center.php');
td_api_autoload::add('td_action_ajax', td_global::$get_template_directory . '/includes/football_app/td_action_ajax.php');
td_api_autoload::add('td_action_main_info_post_type', td_global::$get_template_directory . '/includes/football_app/action/td_action_main_info_post_type.php');
td_api_autoload::add('td_action_coments_bk', td_global::$get_template_directory . '/includes/football_app/action/td_action_coments_bk.php');



td_api_autoload::add('td_bukmekers_template', td_global::$get_template_directory . '/includes/football_app/template/td_bukmekers_template.php');
td_api_autoload::add('td_post_blocks_template', td_global::$get_template_directory . '/includes/football_app/template/td_post_blocks_template.php');

/*-----------------------------------------------------------------------------
 * Wiget
 */
 require_once('wigets/td_wiget_tournament_table.php');
require_once('wigets/td_wiget_wc_group_tournament_table.php');
/*
 * регистрация виджета
 */


/*-----------------------------------------------------------------------------
 * Templates
 */
td_api_autoload::add('td_class_category_template_obzor_matches', td_global::$get_template_directory . '/includes/football_app/classes/td_class_category_template_obzor_matches.php');


/*-----------------------------------------------------------------------------
 * Action
 */
add_action( 'carbon_fields_register_fields', array('td_action_main_info_post_type', 'build_main_info_bukmeker') );

// Коментарии Букмекеров
//add_action( 'edit_comment',   array('td_action_coments_bk', 'ff_app_action_change_status_coments_bk') );
add_action('trash_comment',   array('td_action_coments_bk', 'ff_app_action_dell_comments_bk') );
add_action('trash_comment',   array('td_action_coments_bk', 'ff_app_action_count_reviews') );
add_action('untrash_comment', array('td_action_coments_bk', 'ff_app_action_dell_comments_bk') );
add_action('untrash_comment', array('td_action_coments_bk', 'ff_app_action_count_reviews') );
add_action ('comment_unapproved_to_approved' , array('td_action_coments_bk', 'ff_app_action_change_status_coments_bk'  )) ;
add_action ('comment_unapproved_to_approved' , array('td_action_coments_bk', 'ff_app_action_count_reviews'  )) ;
add_action ('comment_approved_to_unapproved' , array('td_action_coments_bk', 'ff_app_action_change_status_coments_bk'  )) ;
add_action ('comment_approved_to_unapproved' , array('td_action_coments_bk', 'ff_app_action_count_reviews'  )) ;

/*-----------------------------------------------------------------------------
 * Shortcode
 */
add_shortcode('ff_app_show_match_center',      array('td_football_shortcode', 'ff_app_show_match_center') );
add_shortcode('ff_app_show_main_info_bk',      array('td_football_shortcode', 'ff_app_show_main_info_bk') );
add_shortcode('ff_app_show_average_rating_bk', array('td_football_shortcode', 'ff_app_show_average_rating_bk') );
add_shortcode('snipet',                        array('td_football_shortcode', 'ff_app_customBoxContent') );
/* ----------------------------------------------------------------------------
 * Ajax support
 */
// ajax: on_ajax_get_result_matches
add_action('wp_ajax_nopriv_td_ajax_get_result_matches', array('td_action_ajax', 'on_ajax_get_result_matches'));
add_action('wp_ajax_td_ajax_get_result_matches',        array('td_action_ajax', 'on_ajax_get_result_matches'));
// ajax: block ajax hooks
add_action('wp_ajax_nopriv_td_ajax_get_schedule_matches', array('td_action_ajax', 'on_ajax_get_schedule_matches'));
add_action('wp_ajax_td_ajax_get_schedule_matches',        array('td_action_ajax', 'on_ajax_get_schedule_matches'));
//ajax: Загрузка матчей по дате
add_action( 'wp_ajax_getmatchecustom', array('td_action_ajax', 'mc_ajax_get_matches_list_calendar') );
add_action( 'wp_ajax_nopriv_getmatchecustom', array('td_action_ajax', 'mc_ajax_get_matches_list_calendar'));

//ajax: Загрузка списку букмекеров
add_action( 'wp_ajax_myfilter',array('td_football_bukmekers', 'get_bukmekers_list'));
add_action( 'wp_ajax_nopriv_myfilter', array('td_football_bukmekers', 'get_bukmekers_list') );

/*------------------------------------------------------------------------------
 *Filters
 */

//add_filter( 'the_content', array('td_football_posts_custom_block_content', 'showBlocksArticle'));


require_once('td_football_function.php');
