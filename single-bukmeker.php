<?php
get_header();
?>
<div class="td-main-content-wrap td-container-wrap">

    <div class="td-container td-post-template-default <?php echo $td_sidebar_position; ?>">
        <?php require_once('breadcrumb.php');  ?>
        <div class="td-pb-row">
            <div class="td-pb-span12 td-main-content" role="main">
                <div class="td-ss-main-content main_content_page main_c_bk">
                    <?php the_content(); ?>

                    <div class="td-container">
                        <div class="td-pb-row">
                            <div class="td-pb-span12">
                                <?php comments_template('', true); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- /.td-pb-row -->
    </div> <!-- /.td-container -->
</div> <!-- /.td-main-content-wrap -->
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/modules/css/main_module.css">
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/modules/css/bookmaker.css">


<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/modules/js/rate.widget.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/modules/js/rating.js"></script>
<?php

get_footer();?>