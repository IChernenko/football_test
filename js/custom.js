jQuery(document).ready(function($){
	$(window).scroll(function (event) {
	    var scroll = $(window).scrollTop();
	    if(scroll > 30) {
	    	$('.td-header-top-menu').addClass('floating');
	    } else {
	    	$('.td-header-top-menu').removeClass('floating');
	    }
	});

	$('.td-logo').clone().appendTo('.td-header-top-menu');
});