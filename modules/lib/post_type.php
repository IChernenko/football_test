<?php 
/* Запись для  Акций*/
add_action('init', 'hnatchuk_type_post_sell');
function hnatchuk_type_post_sell()
{
  $labels = array(
	'name' => 'Все Акции', 
	'singular_name' => 'Акции', 
	'add_new' => 'Добавить акцию',
	'add_new_item' => 'Добавить новую акцию',
	'edit_item' => 'Редактировать запись',
	'new_item' => 'Новое запись',
	'view_item' => 'Посмотреть запись',
	'search_items' => 'Найти запись',
	'not_found' =>  'Запись не найдена',
	'not_found_in_trash' => 'В корзине записи не найдено',
	'menu_name' => 'Акции'
  );
  $args = array(
	'labels' => $labels,
	'public' => true,
	'publicly_queryable' => true,
	'show_ui' => true,
	'show_in_menu' => false,
	'query_var' => true,
	'rewrite' => true,
	'capability_type' => 'post',
	'has_archive' => true,
	'hierarchical' => true,
	'pages' => true,
	'menu_position' => 5,
	'menu_icon' => 'dashicons-nametag',
	'supports' => array('title','thumbnail','editor','author','comments','custom-fields', 'revisions', 'post-formats' , 'page-attributes'),
	'taxonomies' => array( 'post_tag' , 'category'),
  );
  register_post_type('sell' , $args);
};
/* Запись для  Прогнозов*/
add_action('init', 'hnatchuk_type_post_prognoz');
function hnatchuk_type_post_prognoz()
{
  $labels = array(
	'name' => 'Все Прогнозы', 
	'singular_name' => 'Прогнозы', 
	'add_new' => 'Добавить прогноз',
	'add_new_item' => 'Добавить новыйй прогноз',
	'edit_item' => 'Редактировать прогноз',
	'new_item' => 'Новый прогноз',
	'view_item' => 'Посмотреть прогноз',
	'search_items' => 'Найти прогноз',
	'not_found' =>  'Прогноз не найдена',
	'not_found_in_trash' => 'В корзине прогнозов не найдено',
	'menu_name' => 'Прогнозы БК'
  );
  $args = array(
	'labels' => $labels,
	'public' => true,
	'publicly_queryable' => true,
	'show_ui' => true,
	'show_in_menu' => false,
	'query_var' => true,
	'rewrite' => true,
	'capability_type' => 'post',
	'has_archive' => true,
	'hierarchical' => true,
	'pages' => true,
	'menu_position' => 5,
	'menu_icon' => 'dashicons-admin-settings',
	'supports' => array('title','thumbnail','editor','author','comments','custom-fields', 'revisions', 'post-formats' , 'page-attributes'),
	'taxonomies' => array( 'post_tag' , 'category'),
  );
  register_post_type('prognoz' , $args);
};
/* Запись для  Футбольные лигы*/
add_action('init', 'hnatchuk_type_post_liga');
function hnatchuk_type_post_liga()
{
  $labels = array(
	'name' => 'Все Футбольные лиги', 
	'singular_name' => 'Футбольные лиги', 
	'add_new' => 'Добавить лигу',
	'add_new_item' => 'Добавить новую лигу',
	'edit_item' => 'Редактировать лигу',
	'new_item' => 'Новая лигу',
	'view_item' => 'Посмотреть лигу',
	'search_items' => 'Найти лигу',
	'not_found' =>  'Лига не найдена',
	'not_found_in_trash' => 'В корзине лиг не найдено',
	'menu_name' => 'Футбольные лиги'
  );
  $args = array(
	'labels' => $labels,
	'public' => true,
	'publicly_queryable' => true,
	'show_ui' => true,
	'show_in_menu' => true,
	'query_var' => true,
	'rewrite' => true,
	'capability_type' => 'post',
	'has_archive' => true,
	'hierarchical' => true,
	'pages' => true,
	'menu_position' => 20,
	'menu_icon' => 'dashicons-admin-site',
	'supports' => array('title','thumbnail','editor','author','comments','custom-fields', 'revisions', 'post-formats' , 'page-attributes'),
  );
  register_post_type('fl' , $args);
};
/* Запись для  Чемпионат мира*/
add_action('init', 'hnatchuk_type_post_chm');
function hnatchuk_type_post_chm()
{
  $labels = array(
	'name' => 'Чемпионат мира', 
	'singular_name' => 'Чемпионат мира', 
	'add_new' => 'Добавить чемпионат',
	'add_new_item' => 'Добавить новый чемпионат',
	'edit_item' => 'Редактировать чемпионат',
	'new_item' => 'Новый чемпионат',
	'view_item' => 'Посмотреть чемпионат',
	'search_items' => 'Найти чемпионат',
	'not_found' =>  'Чемпионат не найден',
	'not_found_in_trash' => 'В корзине чемпионата не найдено',
	'menu_name' => 'Чемпионат мира'
  );
  $args = array(
	'labels' => $labels,
	'public' => true,
	'publicly_queryable' => true,
	'show_ui' => true,
	'show_in_menu' => true,
	'query_var' => true,
	'rewrite' => true,
	'capability_type' => 'post',
	'has_archive' => true,
	'hierarchical' => true,
	'pages' => true,
	'menu_position' => 20,
	'menu_icon' => 'dashicons-admin-site',
	'supports' => array('title','thumbnail','editor','author','comments','custom-fields', 'revisions', 'post-formats' , 'page-attributes'),
  );
  register_post_type('world-cup' , $args);
};
/* Запись для Обзор турнирного отбора*/
/*add_action('init', 'hnatchuk_type_review_wcl');
function hnatchuk_type_review_wcl()
{
  $labels = array(
	'name' => 'Обзор отбора ЧM', 
	'singular_name' => 'Обзор отбора ЧM', 
	'add_new' => 'Добавить обзор',
	'add_new_item' => 'Добавить новый обзор',
	'edit_item' => 'Редактировать обзор',
	'new_item' => 'Новый обзор',
	'view_item' => 'Посмотреть обзор',
	'search_items' => 'Найти обзор',
	'not_found' =>  'Обзор не найден',
	'not_found_in_trash' => 'В корзине обзоров не найдено',
	'menu_name' => 'Обзор отбора ЧM'
  );
  $args = array(
	'labels' => $labels,
	'public' => true,
	'publicly_queryable' => true,
	'show_ui' => true,
	'show_in_menu' => false,
	'query_var' => true,
	'rewrite' => true,
	'capability_type' => 'post',
	'has_archive' => true,
	'hierarchical' => true,
	'pages' => true,
	'menu_position' => 5,
	'menu_icon' => 'dashicons-nametag',
	'supports' => array('title','thumbnail','editor','author','comments','custom-fields', 'revisions', 'post-formats' , 'page-attributes'),
	'taxonomies' => array( 'post_tag' , 'category'),
  );
  register_post_type('review_wc' , $args);
};*/
/* Запись для  Футбольные клубы*/
add_action('init', 'hnatchuk_type_post_club');
function hnatchuk_type_post_club()
{
  $labels = array(
	'name' => 'Все Футбольные клубы', 
	'singular_name' => 'Футбольные клубы', 
	'add_new' => 'Добавить клуб',
	'add_new_item' => 'Добавить новый клуб',
	'edit_item' => 'Редактировать клуб',
	'new_item' => 'Новый клуб',
	'view_item' => 'Посмотреть клуб',
	'search_items' => 'Найти клуб',
	'not_found' =>  'Клуб не найдена',
	'not_found_in_trash' => 'В корзине футбольного клуба не найдено',
	'menu_name' => 'Football клубы'
  );
  $args = array(
	'labels' => $labels,
	'public' => true,
	'publicly_queryable' => true,
	'show_ui' => true,
	'show_in_menu' => true,
	'query_var' => true,
	'rewrite' => true,
	'capability_type' => 'post',
	'has_archive' => true,
	'hierarchical' => true,
	'pages' => true,
	'menu_position' => 20,
	'menu_icon' => 'dashicons-nametag',
	'supports' => array('title','thumbnail','editor','author','comments','custom-fields', 'revisions', 'post-formats' , 'page-attributes'),
  );
  register_post_type('f-club' , $args);
};
/* Запись для  Футблолистов*/
add_action('init', 'hnatchuk_type_post_fotbolist');
function hnatchuk_type_post_fotbolist()
{
  $labels = array(
	'name' => 'Все Футболисты', 
	'singular_name' => 'Футболисты', 
	'add_new' => 'Добавить футболиста',
	'add_new_item' => 'Добавить нового футболиста',
	'edit_item' => 'Редактировать футболиста',
	'new_item' => 'Новый футболиста',
	'view_item' => 'Посмотреть футболиста',
	'search_items' => 'Найти футболиста',
	'not_found' =>  'Футболист не найден',
	'not_found_in_trash' => 'В корзине записи не найдено',
	'menu_name' => 'Футболисты'
  );
  $args = array(
	'labels' => $labels,
	'public' => true,
	'publicly_queryable' => true,
	'show_ui' => true,
	'show_in_menu' => true,
	'query_var' => true,
	'rewrite' => true,
	'capability_type' => 'post',
	'has_archive' => true,
	'hierarchical' => true,
	'pages' => true,
	'menu_position' => 20,
	'menu_icon' => 'dashicons-universal-access-alt',
	'supports' => array('title','thumbnail','editor','author','comments','custom-fields', 'revisions', 'post-formats' , 'page-attributes'),
  );
  register_post_type('footbolist' , $args);
};
/* Запись для  каталога*/
add_action('init', 'hnatchuk_type_post_bukmeker');
function hnatchuk_type_post_bukmeker()
{
  $labels = array(
	'name' => 'Все букмекеры', 
	'singular_name' => 'Букмекеры', 
	'add_new' => 'Добавить запись',
	'add_new_item' => 'Добавить новая запись',
	'edit_item' => 'Редактировать запись',
	'new_item' => 'Новое запись',
	'view_item' => 'Посмотреть запись',
	'search_items' => 'Найти запись',
	'not_found' =>  'Запись не найдена',
	'not_found_in_trash' => 'В корзине записи не найдено',
	'menu_name' => 'Букмекеры'
  );
  $args = array(
	'labels' => $labels,
	'public' => true,
	'publicly_queryable' => true,
	'show_ui' => true,
	'show_in_menu' => true,
	'query_var' => true,
	'rewrite' => true,
	'capability_type' => 'post',
	'has_archive' => true,
	'hierarchical' => true,
	'pages' => true,
	'menu_position' => 20,
	'menu_icon' => 'dashicons-forms',
	'supports' => array('title','thumbnail','editor','author','comments','custom-fields', 'revisions', 'post-formats' , 'page-attributes'),
	'taxonomies' => array('bukmekers'),
  );
  register_post_type('bukmeker' , $args);
};

add_action( 'init', 'bukmekers', 0 );
 
// функция, создающая таксономию "Разделы" для постов типа "Услуг"
 
function bukmekers(){
 
  // определяем заголовки для 'Разделы'
  $labels = array(
	'name' => _x( 'Букмекеры по странам', 'taxonomy general name' ),
	'singular_name' => _x( 'Букмекеры по странам', 'taxonomy singular name' ),
	'search_items' =>  __( 'Искать страну' ),
	'popular_items' => __( 'Популярные страны' ),
	'all_items' => __( 'Все страны' ),
	'parent_item' => null,
	'parent_item_colon' => null,
	'edit_item' => __( 'Редактировать страну' ),
	'update_item' => __( 'Обновить страну' ),
	'add_new_item' => __( 'Добавить новую страну' ),
	'new_item_name' => __( 'Название новой страны' ),
	'separate_items_with_commas' => __( 'Separate writers with commas' ),
	'add_or_remove_items' => __( 'Добавить или удалить страну' ),
	'choose_from_most_used' => __( 'Choose from the most used writers' ),
	'menu_name' => __( 'Страны мира' ),
  );
// Добавляем древовидную таксономию 'Разделы' (как рубрики), чтобы сделать НЕ девовидную (как метки) значение для 'hierarchical' => false,
 
register_taxonomy('bukmekers', 'articles',array(
	'hierarchical' => true,
	'labels' => $labels,
	'show_ui' => true,
	'query_var' => true,
	'show_admin_column' => true,
	'rewrite' => array( 'slug' => 'bukmekers' ),
  ));
};
function true_taxonomy_filter() {
	global $typenow; // тип поста
	if( $typenow == 'bukmeker' ){ // для каких типов постов отображать
		$taxes = array('bukmekers'); // таксономии через запятую
		foreach ($taxes as $tax) {
			$current_tax = isset( $_GET[$tax] ) ? $_GET[$tax] : '';
			$tax_obj = get_taxonomy($tax);
			$tax_name = mb_strtolower($tax_obj->labels->name);
			// функция mb_strtolower переводит в нижний регистр
			// она может не работать на некоторых хостингах, если что, убирайте её отсюда
			$terms = get_terms($tax);
			if(count($terms) > 0) {
				echo "<select name='$tax' id='$tax' class='postform'>";
				echo "<option value=''>Все $tax_name</option>";
				foreach ($terms as $term) {
					echo '<option value='. $term->slug, $current_tax == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>'; 
				}
				echo "</select>";
			}
		}
	}
}
add_action( 'restrict_manage_posts', 'true_taxonomy_filter' );
function register_my_custom_submenu_page() {
  add_submenu_page( 'edit.php?post_type=bukmeker', 'Прогнозы', 'Прогнозы', 'manage_options', 'edit.php?post_type=prognoz' ); 
  add_submenu_page( 'edit.php?post_type=bukmeker', 'Акции', 'Акции', 'manage_options', 'edit.php?post_type=sell' ); 
//  add_submenu_page( 'edit.php?post_type=world-cup', 'Обзор отбора ЧM', 'Обзор отбора ЧМ', 'manage_options', 'edit.php?post_type=review_wc' ); 
}
add_action('admin_menu', 'register_my_custom_submenu_page');
