<?php 
$cat_template = new td_class_category_template_obzor_matches(); ?>
<div class="td-category-header td-container-wrap"> 
    <div class="td-container">
        <div class="td-pb-row">
            <div class="td-pb-span12">
                <div class="td-crumb-container"><?php echo $cat_template->get_breadcrumbs(); ?></div>
                    <h1 class="entry-title td-page-title title_obzor_matchey"><?php echo $cat_template->get_title(); ?></h1>
                    <?php echo $cat_template->get_description(); ?>
            </div>
        </div>
    </div>
</div>
<div class="td-main-content-wrap td-container-wrap">
    <div class="td-container">
        <!-- content -->
        <div class="td-pb-row">
            <div class="td-pb-span12 td-main-content">
                <div class="td-ss-main-content">
                  <?php echo $cat_template->get_category_list(); ?> 
                </div>
            </div>
           
        </div> <!-- /.td-pb-row -->
    </div> <!-- /.td-container -->
</div> <!-- /.td-main-content-wrap -->