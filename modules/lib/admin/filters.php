<?php 
function true_taxonomy_filter_post() {
	global $typenow; // тип поста
		$taxes = array('post_tag' ); // таксономии через запятую
		foreach ($taxes as $tax) {
			$current_tax = isset( $_GET['tag'] ) ? $_GET['tag'] : '';
			$tax_obj = get_taxonomy($tax);
			$tax_name = mb_strtolower($tax_obj->labels->name);
			// функция mb_strtolower переводит в нижний регистр
			// она может не работать на некоторых хостингах, если что, убирайте её отсюда
			$terms = get_terms($tax);
			if(count($terms) > 0) {
				echo "<select name='tag' id='tag' class='postform'>";
				echo "<option value=''>Все $tax_name</option>";
				foreach ($terms as $term) {
					echo '<option value='. $term->slug, $current_tax == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>'; 
				}
				echo "</select>";
			}
		}
}
 
add_action( 'restrict_manage_posts', 'true_taxonomy_filter_post' );
function true_filtr_po_avtoram() {
	global $typenow; // тип поста
	global $wpdb; 
	$posts = $wpdb->get_col($wpdb->prepare("SELECT post_author FROM $wpdb->posts WHERE post_type = %s AND post_status = 'publish'" , $typenow ));
	$result =  array_unique($posts);
	$parameters = array(
		'name' => 'author', // атрибут name для селекта
		'include' => $result,
		'show_option_all' => 'Все авторы' // лейбл для отображения постов всех авторов
	);
 
	if ( isset($_GET['user']) )
		$parameters['selected'] = $_GET['user']; // выбранный пользователь из списка
 
	wp_dropdown_users( $parameters ); // выводим готовый список
}
 
add_action('restrict_manage_posts', 'true_filtr_po_avtoram');