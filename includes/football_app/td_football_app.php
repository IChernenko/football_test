<?php
class td_football_app{

	/*Название таблиц В БД*/
	//общие таблицы
	static  $table_common_amplua = 'parser_common_amplua';
	static  $table_common_city = 'parser_common_city';
	static  $table_common_club = 'parser_common_club';
	static  $table_common_country = 'parser_common_country';
	static  $table_common_national_team = 'parser_common_national_team';
	static  $table_common_player = 'parser_common_player';
	static  $table_common_tournament = 'parser_common_tournament';
	static  $table_common_tournament_group = 'parser_common_tournament_group';
	static  $table_common_tournament_type = 'parser_common_tournament_type';
	static  $table_common_bookmaker = 'parser_common_bookmaker';

	//Описание клубов
	static  $table_club_description = 'foot_football_club_description';
	//Составы турниров
	static  $table_club_to_competition = 'foot_football_club_to_competition';
	//Турнирная таблица клубов
	static  $table_turnirnaya_tablitsa = 'foot_football_club_turnirnaya_tablitsa';
	//Матчи
	static  $table_club_matches = 'foot_football_club_matches';

	//Асыстенты в соревнованиях
	static  $table_competition_assistants = 'foot_football_competition_assistants';
	//Бомбардиры в соревнованиях
	static  $table_competition_bombardiers = 'foot_football_competition_bombardiers';

	//Матчи национальных зборных
	static  $table_national_team_matchess = 'foot_football_national_team_matches';
	//Турнирная таблица национальных зборных
	static  $table_national_team_tablica = 'foot_football_national_team_tablica';

	//Информация о Игроках
	static  $table_player_description = 'foot_football_player_description';
	//Статистика Игроков
	static  $table_player_statistic = 'foot_football_player_statistic';
	//Статистика Игроков в клубах
	static  $table_player_statistic_club = 'foot_football_player_statistic_club';
	//Составы клубов в турнирах
	static  $table_player_to_club = 'foot_football_player_to_club';

	/** Таблицы с даными **/
	//Букмекеры
	static $football_bookmaker_coments    = 'foot_football_bookmaker_coments';


	//Дериктория логотипов клубов
	static $dir_img_club = '/wp-content/images/club/';

	public function get_yesterday_date()
	{
		$date = date("Y-m-d", time()-(60*60*24));
		return $date;
	}
	public function get_today_date()
	{
		$date = date("Y-m-d", time());
		return $date;
	}
	public function get_tomorrow_date()
	{
		$date = date("Y-m-d", time()+(60*60*24));
		return $date;
	}

	public function array_group($result){
	  foreach ( $result as $row ) {
	            $var_by_ref = get_object_vars( $row );
	            $key = array_shift( $var_by_ref );
	            $new_array[$key][] = $row;
	        }
	  return $new_array;
	}
	public function getCompetitonObject($id_category)
	{
		switch ($id_category) {
			case 29:
		   	$competiton_obj['liga_name'] = 'Россия. Премьер-Лига';
				$competiton_obj['liga_url'] = '/fl/rossiya-premer-liga/';
				break;
			case 30:
				$competiton_obj['liga_name'] = 'Англия. Премьер-Лига';
				$competiton_obj['liga_url'] = '/fl/angliya-premer-liga/';
				break;
			case 31:
				$competiton_obj['liga_name'] = 'Испания. Ла Лига';
				$competiton_obj['liga_url'] = '/fl/ispaniya-la-liga/';
				break;
			case 32:
				$competiton_obj['liga_name'] = 'Германия. Бундеслига';
				$competiton_obj['liga_url'] = '/fl/bundesliga-germanii/';
				break;
			case 33:
				$competiton_obj['liga_name'] = 'Италия. Серия А';
				$competiton_obj['liga_url'] = '/fl/italiya-seriya-a/';
				break;
			case 34:
				$competiton_obj['liga_name'] = 'Франция. Лига 1';
				$competiton_obj['liga_url'] = '/fl/frantsiya-liga-1/';
				break;
			case 35:
				$competiton_obj['liga_name'] = 'Лига Чемпионов';
				$competiton_obj['liga_url'] = '/fl/liga-chempionov/';
				break;
			case 36:
				$competiton_obj['liga_name'] = 'Лига Европы';
				$competiton_obj['liga_url'] = '/fl/liga-evropy/';
				break;
			case 1019:
				$competiton_obj['liga_name'] = 'Финал';
				$competiton_obj['liga_url'] = '/world-cup/chempionat-mira-2018/';
				break;
			default:
				$competiton_obj['liga_name'] = 'full';
				$competiton_obj['liga_url'] = '/futbolnye-ligi/';
				break;
		}
		return $competiton_obj;
	}


	



}
