<?php
// Лиги
/*Расписание матчей*/
$dir_img_club = '/wp-content/images/club/';


//Описание Турниров
function show_competition_description () {
  global $wpdb;
  wp_reset_query();
  $name_liga = get_the_title();
  $name_liga = esc_sql($name_liga);

  $sql= "SELECT comp_desc.name ,  comp_desc.data_foundation ,  comp_desc.count_clubs,
                comp_desc.time_spending , comp_desc.president  , comp_desc.rating,
                comp_desc.sponsor , comp_desc.current_champion
           FROM foot_football_competition_description comp_desc
      LEFT JOIN parser_common_tournament fc
             ON comp_desc.competition_id = fc.id
          WHERE fc.name = '{$name_liga}'
             OR fc.name_post = '{$name_liga}'
      ";

  $result = $wpdb->get_results( $sql );
 // echo "<pre>"; print_r($result); echo "</pre>";
  $innerHTML = "<table class='table_main_khar' id='table_main_khar'>";
      foreach ($result as $item_feald) {
         $innerHTML .= '<tr>
                          <td><strong>Название:&nbsp;</strong></td>
                          <td>'.$item_feald->name.'</td>
                        </tr>
                        <tr>
                          <td><strong>Дата основания:</strong></td>
                          <td>'.$item_feald->data_foundation.'</td>
                        </tr>
                        <tr>
                          <td><strong>Количество клубов:</strong></td>
                          <td>'.$item_feald->count_clubs.'</td>
                        </tr>
                        <tr>
                          <td><strong>Время проведения:</strong></td>
                          <td>'.$item_feald->time_spending.'</td>
                        </tr>
                        <tr>
                          <td><strong>Президент:&nbsp;</strong></td>
                          <td>'.$item_feald->president.'</td>
                        </tr>
                        <tr>
                          <td><strong>Рейтинг:</strong></td>
                          <td>'.$item_feald->rating.'</td>
                        </tr>
                        <tr>
                          <td><strong>Спонсор:</strong></td>
                          <td>'.$item_feald->sponsor.'</td>
                        </tr>
                        <tr>
                          <td><strong>Текущий чемпион:</strong></td>
                          <td>'.$item_feald->current_champion.'</td>
                        </tr>';
      }
  $innerHTML .= "</table>";
  return $innerHTML;
}
add_shortcode('show_competition_description', 'show_competition_description');

function schedule_of_matches ($atts) {
  global $dir_img_club;
  global $wpdb;
  extract(shortcode_atts(array(
        'slug' => "prev",
        'limit' => 5,
  ), $atts));
  switch ($slug) {
  case 'prev':
      wp_reset_query();
      $name_liga = get_the_title();
      break;
  case 'full':
      wp_reset_query();
      global $post;
      $name_liga = get_the_title($post->post_parent);
      break;
  }
  if ($limit == 'full') {
    $lim= '';
  }else{
    $lim = 'LIMIT 0 , '.$limit;
  }
  $name_liga = esc_sql($name_liga);
  $sql= "SELECT fm.competition_tour ,
                ft1.name AS team_1 , ft1.logo_link AS logo_t1 ,
                ft2.name AS team_2 , ft2.logo_link AS logo_t2 ,
                fm.play_date , fm.play_time
           FROM foot_football_club_matches fm
      LEFT JOIN parser_common_tournament fc
             ON fm.competition_id = fc.id
      LEFT JOIN parser_common_club  ft1
             ON fm.club_id_1 = ft1.id
      LEFT JOIN parser_common_club  ft2
             ON fm.club_id_2 = ft2.id
          WHERE fc.name = '{$name_liga}'
             OR fc.name_post = '{$name_liga}'
            AND status = 0
       ORDER BY fm.play_date ASC ,
                fm.competition_tour ASC  {$lim} ";

  $result = $wpdb->get_results( $sql );
  $result = array_group($result);
  //echo "<pre>"; print_r($result); echo "</pre>";
  $innerHTML = "<table class=' table_result_matches'>";
    if($result){
      foreach ($result as $key => $value) {
        $innerHTML .= "<tr><th colspan='3' style='text-align: center;''>Тур ".$key."</th> </tr>";
        foreach ($value as $item_match) {
          $date = date_create($item_match->play_date);
          $innerHTML .= '<tr>
                            <td class="m_team_first">'.$item_match->team_1.'<img src="'.$dir_img_club.''.$item_match->logo_t1.'" alt="'.$item_match->team_1.'" class="logo_club"></td>
                            <td class="result_matches">'.date_format($date, "d.m.").' '.$item_match->play_time.'</td>
                            <td class="team_second"><img src="'.$dir_img_club.''.$item_match->logo_t2.'" alt="'.$item_match->team_2.'" class="logo_club">'.$item_match->team_2.'</td>
                         </tr>';
        }
      }
    }else{
      $innerHTML .= '<tr>
        <td><div class="no_matches_mc">Нет матчей</div></td>
      </tr>';
    }
  $innerHTML .= "</table>";
  return $innerHTML;
}
add_shortcode('schedule_of_matches', 'schedule_of_matches');
/*Последние результаты*/
function result_of_matches($atts) {
  global $dir_img_club;
  global $wpdb;
  extract(shortcode_atts(array(
        'slug' => "prev",
        'limit' => 5,
  ), $atts));
  switch ($slug) {
  case 'prev':
      wp_reset_query();
      $name_liga = get_the_title();
      break;
  case 'full':
      wp_reset_query();
      global $post;
      $name_liga = get_the_title($post->post_parent);
      break;
  }
  if ($limit == 'full') {
    $lim= '';
  }else{
    $lim = 'LIMIT 0 , '.$limit;
  }
  $name_liga = esc_sql($name_liga);
  $sql= "SELECT fm.competition_tour ,
                ft1.name AS team_1 , ft1.logo_link AS logo_t1 ,
                ft2.name AS team_2 , ft2.logo_link AS logo_t2 ,
                fm.play_date , fm.play_time , fm.result_1 , fm.result_2
           FROM foot_football_club_matches fm
      LEFT JOIN parser_common_tournament fc
             ON fm.competition_id = fc.id
      LEFT JOIN parser_common_club  ft1
             ON fm.club_id_1 = ft1.id
      LEFT JOIN parser_common_club  ft2
             ON fm.club_id_2 = ft2.id
          WHERE fc.name = '{$name_liga}'
             OR fc.name_post = '{$name_liga}'
            AND status = 1
       ORDER BY fm.competition_tour DESC ,
                fm.play_date DESC ,
                fm.play_time DESC {$lim} ";
  $result = $wpdb->get_results( $sql );
  $result = array_group($result);
  //echo "<pre>"; print_r($result); echo "</pre>";
  $innerHTML = "<table class='table_result_matches'>";
    foreach ($result as $key => $value) {
      $innerHTML .= "<tr><th colspan='4' style='text-align: center;''>Тур ".$key."</th> </tr>";
      foreach ($value as $item_match) {
        $date = date_create($item_match->play_date);
        $innerHTML .= '<tr>
                          <td>'.date_format($date, 'd.m.Y').'</td>
                          <td class="m_team_first">'.$item_match->team_1.'<img src="'.$dir_img_club.''.$item_match->logo_t1.'" alt="'.$item_match->team_1.'" class="logo_club"></td>
                          <td class="result_matches">'.$item_match->result_1.'-'.$item_match->result_2.'</td>
                          <td class="team_second"><img src="'.$dir_img_club.''.$item_match->logo_t2.'" alt="'.$item_match->team_2.'" class="logo_club">'.$item_match->team_2.'</td>
                          </tr>';
      }
    }
  $innerHTML .= "</table>";
  return $innerHTML;
}
add_shortcode('result_of_matches', 'result_of_matches');
/*Турнирная таблица*/
function turnirnaya_tablitsa($atts) {
  global $dir_img_club;
  global $wpdb;
  extract(shortcode_atts(array(
        'slug' => "prev",
        'limit' => 5,
  ), $atts));
  switch ($slug) {
  case 'prev':
      wp_reset_query();
      $name_liga = get_the_title();
      break;
  case 'full':
      wp_reset_query();
      global $post;
      $name_liga = get_the_title($post->post_parent);
      break;
  }
  if ($limit == 'full') {
    $lim= '';
  }else{
    $lim = 'LIMIT 0 , '.$limit;
  }
  $name_liga = esc_sql($name_liga);
  $sql  = "SELECT com_tour.name AS com_comp ,
                  football_club.name AS f_team, football_club.logo_link ,
                  foot_tt.position , foot_tt.old_position ,
                  foot_tt.games AS quantity_games , foot_tt.wins AS winnings , foot_tt.draws AS draws  , foot_tt.loses AS losing ,
                  foot_tt.scored_goals , foot_tt.missed_goals , foot_tt.points
             FROM foot_football_club_turnirnaya_tablitsa  foot_tt
             JOIN parser_common_tournament com_tour
               ON foot_tt.competition_id = com_tour.id
             JOIN parser_common_club  football_club
               ON foot_tt.club_id = football_club.id
            WHERE com_tour.name = '{$name_liga}'
               OR com_tour.name_post = '{$name_liga}'
         ORDER BY foot_tt.position ASC  {$lim}";
  $result = $wpdb->get_results( $sql);
  foreach ($result as $key => $value) {

    $sql= "(SELECT  ft1.name AS team_1 ,
                    ft2.name AS team_2 ,
                    fm.competition_tour  AS competition_tour,
                    fm.play_date , fm.play_time ,
                    fm.result_1 , fm.result_2 ,
                    fm.result_match
               FROM foot_football_club_matches fm
          LEFT JOIN parser_common_tournament fc
                 ON fm.competition_id = fc.id
          LEFT JOIN parser_common_club  ft1
                 ON fm.club_id_1 = ft1.id
          LEFT JOIN parser_common_club  ft2
                 ON fm.club_id_2 = ft2.id
              WHERE fc.name = '{$value->com_comp}'
                AND ft1.name = '{$value->f_team}'
                AND fm.status = 1
           ORDER BY fm.play_date DESC ,
                    fm.play_time DESC
                LIMIT 0 , 5)
           UNION
          (SELECT   ft1.name AS team_1 ,
                    ft2.name AS team_2 ,
                    fm.competition_tour  AS competition_tour,
                    fm.play_date , fm.play_time ,
                    fm.result_1 , fm.result_2 ,
                    fm.result_match
               FROM foot_football_club_matches fm
          LEFT JOIN parser_common_tournament fc
                 ON fm.competition_id = fc.id
          LEFT JOIN parser_common_club  ft1
                 ON fm.club_id_1 = ft1.id
          LEFT JOIN parser_common_club  ft2
                 ON fm.club_id_2 = ft2.id
              WHERE fc.name = '{$value->com_comp}'
                AND ft2.name = '{$value->f_team}'
                AND fm.status = 1
           ORDER BY fm.play_date DESC ,
                    fm.play_time DESC
                LIMIT 0 , 5)
          ORDER BY competition_tour DESC
                LIMIT 0 , 5 ";

      $results = $wpdb->get_results( $sql);
      $value->last_matches = $results;



    # code...
  }
  //echo "<pre>"; print_r($result); echo "</pre>";

  $innerHTML = "<table class='turnirnaya-tablitsa'><tr><th style='width: 50px;'>№</th><th>Команда</th><th>И</th><th>В</th><th>Н</th><th>П</th><th>ЗГ</th><th>ПГ</th><th>О</th><th>Ф</th></tr>";
  $i =1  ;
  foreach ($result as $item_team) {
    $stan_pos = '';
    if(!empty($item_team->old_position) AND $item_team->old_position !== $item_team->position ){
      if($item_team->position > $item_team->old_position){
        $stan_pos = '<img src="'.get_stylesheet_directory_uri().'/images/delta_min.gif"  title="Предыдущее место: '.$item_team->old_position.'" alt="Предыдущее место: '.$item_team->old_position.'" class="stan_pos" >';
      }else{

        $stan_pos = '<img src="'.get_stylesheet_directory_uri().'/images/delta_plus.gif"  title="Предыдущее место: '.$item_team->old_position.'" alt="Предыдущее место: '.$item_team->old_position.'" class="stan_pos">';
      }
    }
    $las_matches = '';
    foreach ($item_team->last_matches as $key => $value) {
      $date = date_create($value->play_date);
      if($item_team->f_team == $value->team_1){
        switch ($value->result_match) {
          case 1:
            $las_matches .= '<span class="lm_result  _win" title="'.date_format($date, 'd.m.Y').' '.$value->competition_tour.'-й тур '.$value->team_1.' - '.$value->team_2.' ('.$value->result_1.':'.$value->result_2.')"></span>';
            break;
          case 2:
            $las_matches .= '<span class="lm_result  _lose" title="'.date_format($date, 'd.m.Y').' '.$value->competition_tour.'-й тур '.$value->team_1.' - '.$value->team_2.' ('.$value->result_1.':'.$value->result_2.')"></span>';
            break;
          case 3:
            $las_matches .= '<span class="lm_result  _draws" title="'.date_format($date, 'd.m.Y').' '.$value->competition_tour.'-й тур '.$value->team_1.' - '.$value->team_2.' ('.$value->result_1.':'.$value->result_2.')"></span>';
            break;
        }
      }elseif($item_team->f_team == $value->team_2){
        switch ($value->result_match) {
          case 1:
            $las_matches .= '<span class="lm_result  _lose" title="'.date_format($date, 'd.m.Y').' '.$value->competition_tour.'-й тур '.$value->team_1.' - '.$value->team_2.' ('.$value->result_1.':'.$value->result_2.')"></span>';
            break;
          case 2:
            $las_matches .= '<span class="lm_result admin/ _win" title="'.date_format($date, 'd.m.Y').' '.$value->competition_tour.'-й тур '.$value->team_1.' - '.$value->team_2.' ('.$value->result_1.':'.$value->result_2.')"></span>';
            break;
          case 3:
            $las_matches .= '<span class="lm_result  _draws" title="'.date_format($date, 'd.m.Y').' '.$value->competition_tour.'-й тур '.$value->team_1.' - '.$value->team_2.' ('.$value->result_1.':'.$value->result_2.')"></span>';
            break;
        }
      }
    }
   $innerHTML .= '<tr>
              <td class="position">'.$i++.''.$stan_pos.'</td>
              <td> <img src="'.$dir_img_club.''.$item_team->logo_link.'" alt="'.$item_team->f_team.'" class="logo_club">'.$item_team->f_team.'</td>
              <td>'.$item_team->quantity_games.'</td>
              <td>'.$item_team->winnings.'</td>
              <td>'.$item_team->draws.'</td>
              <td>'.$item_team->losing.'</td>
              <td>'.$item_team->scored_goals.'</td>
              <td>'.$item_team->missed_goals.'</td>
              <td>'.$item_team->points.'</td>
              <td class="last_matches">'.$las_matches.'</td>
          </tr>';
   // print_r($item_team);
  }
  $innerHTML .="</table>";
  $innerHTML .= "<div class='opis_table'><b>И</b> - игры, <b>В</b> - выигрыши, <b>Н</b> - ничья, <b>П</b> - поражения, <b>ЗГ/з</b> - забитые голы, <b>ПГ</b> - пропущенные голы, <b>О</b> - очки, <b>Ф</b> - форма</div>";
  return $innerHTML;
}
add_shortcode('turnirnaya_tablitsa', 'turnirnaya_tablitsa');
/*Лучшие бомбардиры*/
function foot_best_bombardir($atts) {
  global $dir_img_club;
  global $wpdb;
  extract(shortcode_atts(array(
        'slug' => "prev",
        'limit' => 5,
  ), $atts));
  switch ($slug) {
  case 'prev':
      wp_reset_query();
      $name_liga = get_the_title();
      break;
  case 'full':
      wp_reset_query();
      global $post;
      $name_liga = get_the_title($post->post_parent);
      break;
  }
  if ($limit == 'full') {
    $lim= '';
  }else{
    $lim = 'LIMIT 0 , '.$limit;
  }
  $name_liga = esc_sql($name_liga);
  $sql= "SELECT players.name AS pleyer_name , players.short_name AS short_name ,
                country.name AS country ,
                club.name AS team ,
                club.logo_link AS logo_team ,
                com_b.goals , com_b.penalties , com_b.minutes , com_b.games
           FROM foot_football_competition_bombardiers com_b
           JOIN parser_common_tournament fc
             ON com_b.competition_id = fc.id
           JOIN parser_common_player players
             ON com_b.player_id = players.id
           JOIN parser_common_club club
             ON com_b.club_id = club.id
           JOIN parser_common_country country
             ON com_b.ethnicity_id = country.id
          WHERE fc.name = '{$name_liga}'
             OR fc.name_post = '{$name_liga}'
       ORDER BY com_b.goals DESC   {$lim}";
  $result = $wpdb->get_results( $sql);
  //echo "<pre>"; print_r($result); echo "</pre>";
  $innerHTML = "<table class='foot_best_bombardir'><tr><th style='width: 50px;''>№</th><th>Игрок</th> <th>Команда</th> <th>Голы</th><th>Пенальти</th><th>Минуты</th><th>Игры</th></tr>";
  $i =1  ;
  foreach ($result as $item_match) {
  if(empty($item_match->pleyer_name)){
    $pleyer_name = $item_match->pleyer_name;
  }else{
    $pleyer_name = $item_match->short_name;
  }
  $innerHTML .= '<tr>
            <td>'.$i++.'</td>
            <td><i class="flag_nt flag_'.translit($item_match->country).'" title="'.$item_match->country.'" alt="'.$item_match->country.'"></i>'.$pleyer_name.'</td>
            <td><img src="'.$dir_img_club.''.$item_match->logo_team.'" alt="'.$item_match->team.'" class="logo_club">'.$item_match->team.'</td>
            <td>'.$item_match->goals.'</td>
            <td>'.$item_match->penalties .'</td>
            <td>'.$item_match->minutes .'</td>
            <td>'.$item_match->games.'</td>
        </tr>';
  }
  $innerHTML .= "</table>";
  return $innerHTML;
}
add_shortcode('foot_best_bombardir', 'foot_best_bombardir');
/*Лучшие ассистенты*/
function foot_best_asistent($atts) {
  global $dir_img_club;
  global $wpdb;
  extract(shortcode_atts(array(
        'slug' => "prev",
        'limit' => 5,
  ), $atts));
  switch ($slug) {
  case 'prev':
      wp_reset_query();
      $name_liga = get_the_title();
      break;
  case 'full':
      wp_reset_query();
      global $post;
      $name_liga = get_the_title($post->post_parent);
      break;
  }
  if ($limit == 'full') {
    $lim= '';
  }else{
    $lim = 'LIMIT 0 , '.$limit;
  }
  $name_liga = esc_sql($name_liga);
  $sql= "SELECT players.name AS pleyer_name ,  players.short_name AS short_name ,
                country.name AS country ,
                club.name AS team ,
                club.logo_link AS logo_team ,
                com_as.passes , com_as.games , com_as.position
           FROM foot_football_competition_assistants com_as
           JOIN parser_common_tournament fc
             ON com_as.competition_id = fc.id
           JOIN parser_common_player players
             ON com_as.player_id = players.id
           JOIN parser_common_club club
             ON com_as.club_id = club.id
           JOIN parser_common_country country
             ON com_as.ethnicity_id = country.id
          WHERE fc.name = '{$name_liga}'
             OR fc.name_post = '{$name_liga}'
       ORDER BY com_as.position ASC   {$lim}";
  $result = $wpdb->get_results( $sql);
  $innerHTML = "<table class='foot_best_bombardir'><tr><th style='width: 50px;''>№</th><th>Игрок</th> <th>Команда</th> <th>Пасы</th><th>Игры</th></tr>";
  $i =1  ;
  foreach ($result as $item_match) {
    if(empty($item_match->pleyer_name)){
      $pleyer_name = $item_match->pleyer_name;
    }else{
      $pleyer_name = $item_match->short_name;
    }
  $innerHTML .= '<tr>
            <td>'.$i++.'</td>
             <td><i class="flag_nt flag_'.translit($item_match->country).'" title="'.$item_match->country.'" alt="'.$item_match->country.'"></i>'.$pleyer_name.'</td>
            <td><img src="'.$dir_img_club.''.$item_match->logo_team.'" alt="'.$item_match->team.'" class="logo_club">'.$item_match->team.'</td>
            <td>'.$item_match->passes.'</td>
            <td>'.$item_match->games.'</td>
        </tr>';
  }
  $innerHTML .= "</table>";
  return $innerHTML;
}
add_shortcode('foot_best_asistent', 'foot_best_asistent');
/*Команды турнира*/
function foot_liga_clubs($atts) {
  global $dir_img_club;
  global $wpdb;
  extract(shortcode_atts(array(
        'slug' => "prev",
        'limit' => 5,
  ), $atts));
  switch ($slug) {
  case 'prev':
      wp_reset_query();
      $name_liga = get_the_title();
      break;
  case 'full':
      wp_reset_query();
      global $post;
      $name_liga = get_the_title($post->post_parent);
      break;
  }
  if ($limit == 'full') {
    $lim= '';
  }else{
    $lim = 'LIMIT 0 , '.$limit;
  }
  $name_liga = esc_sql($name_liga);
  $sql= "SELECT com_club.name , com_club.logo_link  ,
                f_posts.post_name
           FROM  foot_football_club_to_competition club_c
    INNER  JOIN  parser_common_tournament com_com
             ON  club_c.competition_id = com_com.id
    INNER  JOIN  parser_common_club com_club
             ON  club_c.club_id = com_club.id
     LEFT  JOIN  foot_posts f_posts
             ON  com_club.name_post = f_posts.post_title
            AND  f_posts.post_type = 'f-club'
            AND  f_posts.post_status = 'publish'
          WHERE  com_com.name_post = '{$name_liga}'
       ORDER BY  com_club.name ASC   {$lim}";
  $result = $wpdb->get_results( $sql);
  //echo "<pre>"; print_r($sql); echo "</pre>";
  if ($limit == 'full') {
    $innerHTML = "<div class='list_clubs'>";
      foreach ($result as $item_club) {
        if ($item_club->post_name) {
          $innerHTML .= '<div class="item_club_liga">
                          <a href="/f-club/'.$item_club->post_name.'" title="'.$item_club->name.'">
                            <img src="'.$dir_img_club.''.$item_club->logo_link.'" alt="'.$item_club->name.'">
                            <span>'.$item_club->name.'</span>
                          </a>
                       </div>';
        }else{
          $innerHTML .= '<div class="item_club_liga">
                          '.$item_club->post_name.'
                          <img src="'.$dir_img_club.''.$item_club->logo_link.'" alt="'.$item_club->name.'">
                          <span>'.$item_club->name.'</span>
                       </div>';
        }

    }
  }else{
    $innerHTML = "<div class='list_clubs list_clubs_prev'>";
      foreach ($result as $item_club) {
        if ($item_club->post_name) {
          $innerHTML .= '<div class="item_club_liga">
                          <a href="/f-club/'.$item_club->post_name.'" title="'.$item_club->name.'">
                            <img src="'.$dir_img_club.''.$item_club->logo_link.'" alt="'.$item_club->name.'">
                            <span>'.$item_club->name.'</span>
                          </a>
                       </div>';
        }else{
          $innerHTML .= '<div class="item_club_liga">
                          '.$item_club->post_name.'
                          <img src="'.$dir_img_club.''.$item_club->logo_link.'" alt="'.$item_club->name.'">
                          <span>'.$item_club->name.'</span>
                       </div>';
        }

    }
  }
  $innerHTML .= "</div>";
  return $innerHTML;
}
add_shortcode('foot_liga_clubs', 'foot_liga_clubs');
