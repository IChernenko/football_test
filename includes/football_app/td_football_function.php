<?php
  /*
   * обновления общих показателей рейтинга отзыва для поста
   *
	 	//    Коментарии        // \\      Посты                  \\   //  Названия     //
	  bk_rating_coefficients      bk_rating_common_coefficients       -- Коэффициенты
		bk_rating_football_betting  bk_rating_common_football_betting   -- Ставки на футбол
		bk_rating_line 				      bk_rating_common_line      		      -- Линия
		bk_rating_live_betting      bk_rating_common_live_betting	  		-- Live-ставки
		bk_rating_offers 						bk_rating_common_offers             -- Бонусы
		bk_rating_site 							bk_rating_common_site               -- Сайт
		bk_rating_app 		          bk_rating_common_app     						-- Приложение
		bk_rating_finance 					bk_rating_common_finance          	-- Ввод/вывод средств
		bk_rating_final_result      bk_rating_common_all_raiting  			-- Общий рейтинг

	*/
	function ff_app_update_metadata_review_for_post($post_id = false)
	{
			global $post;
			if($post_id == ''){
				if($post->post_parent  == 0){
					$post_id = $post->ID;
				}else{
					$post_id = $post->post_parent;
				}
			}else{
				$post_id = $post_id;
			}

			$rating_coefficients = ff_app_get_average_review_metadata($post_id , 'bk_rating_coefficients');
			$rating_football_betting = ff_app_get_average_review_metadata($post_id , 'bk_rating_football_betting');
			$rating_line = ff_app_get_average_review_metadata($post_id , 'bk_rating_line');
			$rating_live_betting = ff_app_get_average_review_metadata($post_id , 'bk_rating_live_betting');
			$rating_offers = ff_app_get_average_review_metadata($post_id , 'bk_rating_offers');
			$rating_site = ff_app_get_average_review_metadata($post_id , 'bk_rating_site');
			$rating_app = ff_app_get_average_review_metadata($post_id , 'bk_rating_app');
			$rating_finance = ff_app_get_average_review_metadata($post_id , 'bk_rating_finance');
			$count_rate_all = ff_app_get_average_review_metadata($post_id , 'bk_rating_final_result');

			update_post_meta($post_id, 'bk_rating_common_coefficients', $rating_coefficients);
			update_post_meta($post_id, 'bk_rating_common_football_betting', $rating_football_betting);
			update_post_meta($post_id, 'bk_rating_common_line', $rating_line);
			update_post_meta($post_id, 'bk_rating_common_live_betting', $rating_live_betting);
			update_post_meta($post_id, 'bk_rating_common_offers', $rating_offers);
			update_post_meta($post_id, 'bk_rating_common_site', $rating_site);
			update_post_meta($post_id, 'bk_rating_common_app', $rating_app);
			update_post_meta($post_id, 'bk_rating_common_finance', $rating_finance);
			update_post_meta($post_id, 'bk_rating_common_all_raiting', $count_rate_all);
	}



	/*
	 * Получение средного значение по оценкам.
	 *
	 * Полечаем все коментарии для выбраного поста.
	 * Считаем количество коментариев
	 * Сумируем все коментарии и выводим среднее арефмитическое значение
	 * Пример (5 + 4 + 3 + 2) / 4
	 * @var string
	 */
 	function ff_app_get_average_review_metadata($post_id ,  $meta_key = false){
		$args = array(
			'post_id'             => $post_id,
			'count'               => false,
			'status'              => 'approve',
			'type__in'						=> 'reviews',
			'meta_key'            => '',
			'meta_value'          => '',
			'meta_query'          => '',
			'date_query'          => null, // See WP_Date_Query
			'hierarchical'        => false,
			'update_comment_meta_cache'  => true,
			'update_comment_post_cache'  => true,
		);
		$comments = get_comments( $args );

		if( $comments ){
			$comment_meta_key_all = 0;
			$count_comments = count($comments);
			foreach( $comments as $comment ){
				$comment_id = $comment->comment_ID;
				$single = true;

				$comment_meta_key = get_comment_meta( $comment_id, $meta_key, $single );
				$comment_meta_key = floatval ($comment_meta_key);

				$comment_meta_key_all = $comment_meta_key_all+$comment_meta_key;
			}
			$comment_meta_key_all = $comment_meta_key_all / $count_comments;
			return  round($comment_meta_key_all , 1);
		}else{
			return  0;
		}
	}
	// Add to our admin_init function
	add_filter('manage_post_posts_columns', 'shiba_add_post_columns');

	function shiba_add_post_columns($columns) {
	    $columns['bukmeker'] = 'Widget Set';
	    return $columns;
	}
	// Add to our admin_init function
	add_action('manage_posts_custom_column', 'shiba_render_post_columns', 10, 2);

	function shiba_render_post_columns($column_name, $id) {
	    switch ($column_name) {
	    case 'bukmeker':
	        // show widget set
	        $widget_id = get_post_meta( $id, 'post_widget', TRUE);
	        $widget_set = NULL;
	        if ($widget_id)
	            $widget_set = get_post($widget_id);
	        if (is_object($widget_set)) echo $widget_set->post_title;
	        else echo 'None';
	        break;
	    }
	}
		// Add to our admin_init function
	add_action('quick_edit_custom_box',  'shiba_add_quick_edit', 10, 2);

	function shiba_add_quick_edit($column_name, $post_type) {
	    if ($column_name != 'bukmeker') return;
	    ?>
	    <fieldset class="inline-edit-col-left">
	    <div class="inline-edit-col">
	        <span class="title">Widget Set</span>
	        <input type="hidden" name="shiba_widget_set_noncename" id="shiba_widget_set_noncename" value="" />
	        <?php // Get all widget sets
	            $widget_sets = get_posts( array( 'post_type' => 'bukmeker',
	                            'numberposts' => -1,
	                            'post_status' => 'publish') );
	        ?>
	        <select name='post_widget_set' id='post_widget_set'>
	            <option class='widget-option' value='0'>None</option>
	            <?php
	            foreach ($widget_sets as $widget_set) {
	                echo "<option class='widget-option' value='{$widget_set->ID}'>{$widget_set->post_title}</option>\n";
	            }
	                ?>
	        </select>
	    </div>
	    </fieldset>
	    <?php
	}
	add_action( 'widgets_init', 'true_register_wp_sidebars' );
	function true_register_wp_sidebars() {

		/* Сайдбар для Матч Ценрт (Левый) */
		register_sidebar(
				array(
					'id' => 'mc_left', // уникальный id
					'name' => 'Левый сайдбар (Матч-центр)', // название сайдбара
					'description' => 'Перетащите сюда виджеты, чтобы добавить их в сайдбар.', // описание
					'before_widget' => '<div id="%1$s" class="side widget %2$s">', // по умолчанию виджеты выводятся <li>-списком
					'after_widget' => '</div>',
					'before_title' => '<h3 class="widget-title">', // по умолчанию заголовки виджетов в <h2>
					'after_title' => '</h3>'
			)
		);

		/* Сайдбар для Матч Ценрт (Правый) */
		register_sidebar(
			array(
				'id' => 'mc_right',
				'name' => 'Правый сайдбар (Матч-центр)',
				'description' => 'Перетащите сюда виджеты, чтобы добавить их в футер.',
				'before_widget' => '<div id="%1$s" class="foot widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="widget-title">',
				'after_title' => '</h3>'
			)
		);
	}
//	add_filter('the_content', 'kama_content_advertise', -10 );
function kama_content_advertise( $text, $num = false ){
	if( ! is_singular() ) return $text; // убедимся что мы на отдельной странице

	if( ! $num ) $num = 700;

	// Код рекламы
	ob_start();
	?>

	<div class="kama-inline-ads" style="float:right; margin:0 0 1em 1.5em;">
	здесь рекламный код
	</div>

	<?php
	$adsense = ob_get_clean();

	// Раскомментируйте, если нужно вставить блок сразу перед тегом <!--more-->
	# return str_replace('<!--more-->', $adsense.'<!--more-->', $text);

	return preg_replace('~[^^]{'. $num .'}.*?(?:\r?\n\r?\n|</figure>|</p>|</table>)~su', "\${0}$adsense", trim( $text ), 1);
}

/**
* Получение количество репостов страницы в соцсетях
*
* @param string $url_share - url страницы
* @param string $social vk|fb|ok - соц.сеть
*
* @return int - количество репостов
*/
function getCountSharePage($url_share, $social){
    $result = 0;
    switch($social){
        case "vk":
            $result = file_get_contents("https://vk.com/share.php?act=count&index=1&url=" . $url_share);
            $result = str_replace("VK.Share.count(1, ", "", $result);
            $result = (int)str_replace(");", "", $result);
            break;

        case "fb":
            $result = file_get_contents("http://graph.facebook.com/" . $url_share);
            $result = json_decode($result);
            $result = (int)$result->share->share_count;
            break;

        case "ok":
            $result = file_get_contents("https://connect.ok.ru/dk?st.cmd=extLike&tp=json&ref=" . $url_share);
            $result = json_decode($result);
            $result = (int)$result->count;
            break;

        default:
            $result = 0;
            break;

    }
    return $result;
}
