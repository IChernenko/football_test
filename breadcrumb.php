<?php $parent = get_post_ancestors( $post_id ); ?>
<?php if(is_singular( 'fl' )): ?>
<div class="breadcrumb-container theme5" itemprop="breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList">
    <ul itemtype="http://schema.org/BreadcrumbList" itemscope="">
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" title="Главная" href="https://football-fun.ru">Главная</a>
        </li>
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" title="Главная" href="/futbolnye-ligi/">Футбольные лиги</a>
        </li>
        <?php if(!empty($parent)):?>
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" title="Главная" href="<?php echo  get_permalink($parent[0]) ?>"><?php echo  get_the_title($parent[0])  ?></a>
        </li>
    <?php endif; ?>
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <span class="separator"></span>
            <p itemprop="item" title="<?php the_title(); ?>" ><?php the_title(); ?></p>
            <span class="separator"></span>
        </li>
    </ul>
</div>
<?php else: ?>
<div class="breadcrumb-container theme5" itemprop="breadcrumb" itemscope="" itemtype="http://schema.org/BreadcrumbList">
    <ul itemtype="http://schema.org/BreadcrumbList" itemscope="">
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" title="Главная" href="https://football-fun.ru">Главная</a>
        </li>
        <?php if(!empty($parent)):?>
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <a itemprop="item" title="Главная" href="<?php echo  get_permalink($parent[0]) ?>"><?php echo  get_the_title($parent[0])  ?></a>
        </li>
    <?php endif; ?>
        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
            <span class="separator"></span>
            <p itemprop="item" title="<?php the_title(); ?>" ><?php the_title(); ?></p>
            <span class="separator"></span>
        </li>
    </ul>
</div>
<?php endif; ?>
<style type="text/css">
.breadcrumb-container {
    font-size: 11px !important;
    margin-top: 25px;
}
.breadcrumb-container.theme5 li {
    display: inline-block;
    margin: 0 14px;
    padding: 0;
}
.breadcrumb-container li a , 
.breadcrumb-container li p {
    box-sizing: unset;
    padding: 0 10px;
}
.breadcrumb-container li a,
.breadcrumb-container li p {
    color: #ffffff !important;
    font-size: 11px !important;
    line-height: 11px !important;
}
.breadcrumb-container.theme5 a ,
.breadcrumb-container.theme5 p {
    background: #009688;
    color: rgb(102, 102, 102);
    display: inline-block;
    font-size: 14px;
    height: 16px;
    margin: 0;
    padding: 5px 10px;
    text-decoration: none;
    position: relative;
}
.breadcrumb-container.theme5 a::before ,
.breadcrumb-container.theme5 p::before  {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: #009688 #009688 #009688 rgba(0, 0, 0, 0);
    border-image: none;
    border-style: solid;
    border-width: 13px;
    content: " ";
    display: block;
    height: 0;
    left: -18px;
    position: absolute;
    top: 0;
    width: 0;
}
.breadcrumb-container.theme5 a::after ,
.breadcrumb-container.theme5 p::after{
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) #009688;
    border-image: none;
    border-style: solid;
    border-width: 13px;
    content: " ";
    display: inline-block;
    height: 0;
    line-height: 0;
    position: absolute;
    right: -26px;
    top: 0;
    width: 0;
}
</style>