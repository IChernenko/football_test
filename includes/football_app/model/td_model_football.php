<?php 
class td_model_football{

	static function get_post_id_by_comment_id($comment_id)
	{
		global $wpdb;

		$comment_post_ID = $wpdb->get_col("SELECT comment_post_ID FROM $wpdb->comments WHERE comment_ID='$comment_id'") ;
		$post_id = $comment_post_ID[0];
		return $post_id;
	}

	static function get_post_by_title($page_title,  $post_type , $output = OBJECT) {
	    global $wpdb;
      $post = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type= %s", $page_title , $post_type ));
      if ( $post ){
      	return $post;
      }else{
      	 return null;
      }
	}

	static function get_bk_reviews_data($period = false)
	{
		if($period){
			$sql_requst = "bookmaker_coments.posting_date > NOW() - INTERVAL ".$period;
		}else{
			$sql_requst = 1;
		}
		global $wpdb;
		$sql = "SELECT  com_bookmaker.name_post ,  bookmaker_coments.message
							FROM  ".td_football_app::$football_bookmaker_coments." bookmaker_coments 
				      JOIN  ".td_football_app::$table_common_bookmaker." com_bookmaker
				        ON  bookmaker_coments.bookmaker_id = com_bookmaker.id 
				     WHERE  ".$sql_requst."
			    ORDER BY  com_bookmaker.name_post ASC ";
		$data_reviews = $wpdb->get_results( $sql);
		
		return $data_reviews;
	}

}