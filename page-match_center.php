<?php
/* Template Name: Матч центр */
$td_football_match_center = new td_football_match_center();
get_header(); ?>


<div class="td-main-content-wrap td-container-wrap page_bk_reiting">
    <div class="td-container tdc-content-wrap <?php echo $td_sidebar_position; ?>">
        <div class="td-crumb-container">
            <?php echo td_page_generator::get_page_breadcrumbs(get_the_title()); ?>
        </div>
        <div class="td-pb-row">
            <?php if (have_posts()) :  while ( have_posts() ) : the_post(); ?>
            <div class="td-pb-span12">
              <h1 class="entry-title td-page-title title_obzor_matchey"><?php the_title() ?></h1>
              <div class="td-container ff_match_center">
                    <div class="td-pb-row">
                        <div class="td-pb-span4"><a href="#" id="prev_date_mc" class="date_caL_link prev_date_cal" >Назад</a></div>
                        <div class="td-pb-span4 maneg_date_block">
                            <a href="#" id="prev_date_mc" class="date_caL_link mob_date_caL_link prev_date_cal" >Назад</a>
                            <div class="mc_filter_caledar" id="flatpickr_delivery_date">
                              <a class="input-button" title="toggle" data-toggle><i class="fa fa-calendar"></i></a>
                              <input class="form-control" id="delivery_date" name="delivery_date" data-input placeholder="Select.." >
                            </div>
                            <a href="#" id="next_date_mc" class="date_caL_link mob_date_caL_link next_date_cal" >Вперед</a>
                        </div>
                        <div class="td-pb-span4"><a href="#" id="next_date_mc" class="date_caL_link next_date_cal" >Вперед</a></div>
                    </div>
                    <div class="td-pb-row mob_flex">
                        <div class="td-pb-span2_5 ff_mc_sidebar">
                            <?php dynamic_sidebar('mc_left') ;?>
                        </div>
                        <div class="td-pb-span7 ff_match_center_main">
                            <div class="football_matches schedule_football_matches">
                                <div class="list_competition_mc">
                                    <?php echo $td_football_match_center->get_list_competition_mc() ; ?>
                                </div>

                                <?php echo $td_football_match_center->get_list_matches() ; ?>
                            </div>
                        </div>
                        <div class="td-pb-span2_5 ff_mc_sidebar">
                             <?php dynamic_sidebar( 'mc_right' ) ;?>
                        </div>
                    </div>
                </div>
            </div>
            <?php endwhile; endif; wp_reset_query();?>
        </div> <!-- /.td-pb-row -->
        <h2 style="font-size: 18px;text-align: left;font-family:Open Sans;font-weight:600;font-style:normal" class="vc_custom_heading vc_custom_1507810903769 block-title"><a style="margin-bottom: 0;">Последние новости</a></h2>

        <?php $atts = array('limit'          => 5,
                                'category_id'     => 19 ,
                                'td_column_number' => 3,
                                'ajax_pagination' => 'load_more' );
                 $post_news = new td_block_15();
                 $td_column_number = 3;
                 td_global::vc_set_custom_column_number(3);
                 echo $post_news->render($atts); ?>

    </div> <!-- /.td-container -->
</div> <!-- /.td-main-content-wrap -->
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/modules/css/main_module.css">
<?php
get_footer();
