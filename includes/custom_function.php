<?php 
 //    $menu_name = 'top-menu';
	// $locations = get_nav_menu_locations();

	// $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
	// $menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );
 //    echo "<pre>";
 //    die(var_dump($menuitems));


if(!is_admin()) 
    add_filter('wp_get_nav_menu_items','custom_user_profile_menu',20,3);
function custom_user_profile_menu($items, $menu, $args){
    global $user_ID,$rcl_options;
   
    if(!isset($rcl_options['rmb_menu'])||!$rcl_options['rmb_menu']) return $items;
    $key = array_search($menu->slug,$rcl_options['rmb_menu']);



    if($key===false) return $items;
    $buttons = add_profile_dropdown_button();
    
    if(!$buttons) return $items;
    $items = array_merge($items,$buttons);
    foreach($items as $k=>$item){
        if($item->menu_order) continue;
        $items[$k]->menu_order = $k+1;
    }

    return $items;
}

function add_profile_dropdown_button() {
	global $user_ID,$rcl_options;

    $avatar = '';

    if(strpos(get_avatar_url($user_ID), 'http://1.gravatar.com') === false ) {
        $avatar = '<img alt="" src="' . get_avatar_url($user_ID) . ' " class="avatar avatar-28 photo wpfla round-avatars " width="28" height="28">';
    } else {
        $avatar = get_avatar($user_ID, 28, false, false);
    }

	if($user_ID){
		$user_info = get_userdata($user_ID); 
		$array[] = array(
                'ID' => 1000005,
                'title'=>$avatar . $user_info->user_login,
                'url'=>get_author_posts_url($user_ID),
                'menu_item_parent'=> 0,
                'menu_order' => '3',
                'order' => 5,
                'type' => 'custom',
                'object' => 'custom',
                'object_id' => '1000005',
                'db_id' => '1000005',
                'classes' => array('profile-submenu'),
                'post_type' => 'nav_menu_item',
            );
		$array[] = array(
            	'ID'=> 1000006,
                'title'=>'<i class="fa fa-user"></i> Личный кабинет',
                'url'=>get_author_posts_url($user_ID),
                'order' => 6,
                'post_type' => 'nav_menu_item',
                'menu_item_parent'=> '1000005',
                'menu_order' => '4',
                'type' => 'custom',
                'object' => 'custom',
                'object_id' => '1000006',
                'db_id' => '1000006',
                'classes' => array('profile-submenu', 'rcl-user-lk')
		);
		$array[] = array(
            	'ID'=> 1000007,
                'title'=>'<i class="fa fa-comments-o"></i> Чат',
                'url'=>get_author_posts_url($user_ID). '&tab=chat',
                'order' => 7,
                'post_type' => 'nav_menu_item',
                'menu_item_parent'=> '1000005',
                'menu_order' => '5',
                'type' => 'custom',
                'object' => 'custom',
                'object_id' => '1000007',
                'db_id' => '1000007',
                'classes' => array('profile-submenu')
		);
		$array[] = array(
            	'ID'=> 1000008,
                'title'=>'<i class="fa fa-pencil"></i> Комментарии',
                'url'=>get_author_posts_url($user_ID). '&tab=postform',
                'order' => 8,
                'post_type' => 'nav_menu_item',
                'menu_item_parent'=> '1000005',
                'menu_order' => '6',
                'type' => 'custom',
                'object' => 'custom',
                'object_id' => '1000008',
                'db_id' => '1000008',
                'classes' => array('profile-submenu')
		);
		$array[] = array(
            	'ID'=> 1000009,
                'title'=>'<i class="fa fa-list"></i> ' . __('Posts','wp-recall'),
                'url'=>get_author_posts_url($user_ID). '&tab=publics',
                'order' => 9,
                'post_type' => 'nav_menu_item',
                'menu_item_parent'=> '1000005',
                'menu_order' => '7',
                'type' => 'custom',
                'object' => 'custom',
                'object_id' => '1000009',
                'db_id' => '1000009',
                'classes' => array('profile-submenu')
		);
		$array[] = array(
            	'ID'=> 1000010,
                'title'=>'<i class="fa fa-external-link"></i> Выход' ,
                'url'=>wp_logout_url('/'),
                'order' => 8,
                'post_type' => 'nav_menu_item',
                'menu_item_parent'=> '1000005',
                'menu_order' => '8',
                'type' => 'custom',
                'object' => 'custom',
                'object_id' => '1000010',
                'db_id' => '1000010',
                'classes' => array('profile-submenu')
		);
        foreach($array as $button){
           $buttons[] = (object)$button;
        }
        return $buttons; 
	} else {
        return [];
    }
}


add_action('register_form','rcl_add_google_captcha_register_form');
function rcl_add_google_captcha_register_form(){
    rcl_get_simple_captcha();
}