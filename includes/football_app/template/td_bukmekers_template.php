<?php 
class td_bukmekers_template
{
	static function get_bk_list()
	{ 
		$post_meta_date = get_metadata('post', get_the_ID() , '', true); 

		if($post_meta_date["_bk_desc_bonus"][0] > 0){
		  $bonus = $post_meta_date["_bk_desc_bonus"][0].' '.$post_meta_date["_bk_desc_bonus_currency"][0];
		}else{
			$bonus = $post_meta_date["_bk_desc_bonus_currency"][0];
		}
		
	?>
	  <tr>
		    <td class="img_tb_list"><a href="<?php the_permalink()?>"><?php the_post_thumbnail() ?></a></td>
		    <td class="f_tb_list"><?php echo $bonus; ?></td>
		    <td class="f_tb_list"><?php echo $post_meta_date["bk_rating_common_all_raiting"][0] ?></td>
		    <td class="f_tb_list"><a href="<?php the_permalink()?>/otzyvy" class=""><?php echo $post_meta_date["_bk_desc_player_reviews"][0] ?></a></td>
		    <td class="btn_tb_list"><a href="<?php the_permalink()?>" class="bnt_bk_obzor">ОБЗОР</a></td>
		    <td class="btn_tb_list"><a href="<?php echo $post_meta_date['_bk_desc_official_site'][0] ?>" target="_blank" class="bnt_bk_site">Перейти на сайт</a></td>
		</tr>
  <?
	}

}