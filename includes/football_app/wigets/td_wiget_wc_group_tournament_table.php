<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 15.06.2018
 * Time: 10:17
 */
class td_wiget_wc_group_tournament_table extends WP_Widget {

    /*
     * создание виджета
     */
    function __construct() {
        parent::__construct(
            'wc_tt_widget',
            'Турнирная таблица ЧМ 2018', // заголовок виджета
            array( 'description' => 'Позволяет вывести туриную таблицу лиги' ) // описание
        );
    }

    /*
     * фронтэнд виджета
     */
    public function widget( $args, $instance ) {

        $title = apply_filters( 'widget_title', $instance['title'] ); // к заголовку применяем фильтр (необязательно)

        global $post;
        $category = get_the_category( $post->ID );
        $liga_obj = td_football_app::getCompetitonObject($category[0]->term_id);

        global $wpdb;
        $name_liga = esc_sql($liga_obj['liga_name']);
        $sql= "SELECT com_t_g.name AS group_name ,
                    fc.name AS competition ,
                    com_t_type.name AS competition_type ,
                    com_nt.name AS sbornaya ,
                    nt_t.games , nt_t.wins , nt_t.draws , nt_t.loses , nt_t.scored_goals , nt_t.missed_goals , nt_t.points
              FROM  foot_football_national_team_tournament_table  nt_t
         LEFT JOIN  parser_common_tournament fc
                ON  nt_t.competition_id = fc.id
         LEFT JOIN  parser_common_tournament_type com_t_type
                ON  nt_t.competition_type_id = com_t_type.id
         LEFT JOIN  parser_common_tournament_group com_t_g
                ON  nt_t.competition_group_id = com_t_g.id
         LEFT JOIN  parser_common_national_team com_nt
                ON  nt_t.national_team_id = com_nt.id
             WHERE  fc.name_post = 'Финал'
             ORDER BY com_t_g.name ASC,
                      nt_t.points DESC ,
                      nt_t.scored_goals DESC ,
                      nt_t.missed_goals DESC
            LIMIT 0 , 8
             ";
       $result = $wpdb->get_results( $sql);
       $result = array_group($result);
        //echo "<pre>"; print_r($result); echo "</pre>";
        foreach ($result as $key => $value) {
           $i =1  ;
          $innerHTML .= "<div class='row_table_in_g'><table class='turnirnaya_tablitsa_wc turnirnaya_tablitsa_wc_home turnirnaya_tablitsa_in_g wiget_table_info'>";
           $innerHTML .= "<tr class='group'><th colspan='9' >".$key."</th> </tr>";
          $innerHTML .= "<tr>
                            <th class='wc_team'>Команда</th>
                            <th>И</th>
                            <th>О</th>
                        </tr>";
          foreach ($value as $item_team_wc) {
           $innerHTML .= '<tr>
                      <td class="wc_team wiget_team"> <i class="flag_nt flag_'.translit($item_team_wc->sbornaya).'" title="'.$item_team_wc->sbornaya.'" alt="'.$item_team_wc->sbornaya.'"></i>'.$item_team_wc->sbornaya.'</td>
                      <td>'.$item_team_wc->games.'</td>
                      <td>'.$item_team_wc->points.'</td>
                  </tr>';
          }
          $innerHTML .="</table></div>";
        }



        echo $args['before_widget'];

        if ( ! empty( $title ) )
            echo $args['before_title'] . $title . $args['after_title'];

        $innerHTML .='<a class="wiget_a" href="/world-cup/chempionat-mira-2018/turnirnaya-tablitsa/">Подробная таблица</a>';

        echo $innerHTML;

        echo $args['after_widget'];
    }

    /*
     * бэкэнд виджета
     */
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        if ( isset( $instance[ 'posts_per_page' ] ) ) {
            $posts_per_page = $instance[ 'posts_per_page' ];
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>">Заголовок</label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'posts_per_page' ); ?>">Количество постов:</label>
            <input id="<?php echo $this->get_field_id( 'posts_per_page' ); ?>" name="<?php echo $this->get_field_name( 'posts_per_page' ); ?>" type="text" value="<?php echo ($posts_per_page) ? esc_attr( $posts_per_page ) : '5'; ?>" size="3" />
        </p>
        <?php
    }

    /*
     * сохранение настроек виджета
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['posts_per_page'] = ( is_numeric( $new_instance['posts_per_page'] ) ) ? $new_instance['posts_per_page'] : '5'; // по умолчанию выводятся 5 постов
        return $instance;
    }
}

/*
 * регистрация виджета
 */
function true_td_wiget_wc_group_tournament_table_load() {
    register_widget( 'td_wiget_wc_group_tournament_table' );
}
add_action( 'widgets_init', 'true_td_wiget_wc_group_tournament_table_load' );
