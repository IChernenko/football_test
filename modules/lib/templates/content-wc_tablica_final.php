<?php
$type_table = '';
	if ($_GET['group'] == 'A'){
		$competition_type = 'Группа A';
		$type_table = 'group';
	}elseif($_GET['group'] == 'B'){
		$competition_type = 'Группа B';
		$type_table = 'group';
	}elseif($_GET['group'] == 'C'){
		$competition_type = 'Группа C';
		$type_table = 'group';
	}elseif($_GET['group'] == 'D'){
		$competition_type = 'Группа D';
		$type_table = 'group';
	}elseif($_GET['group'] == 'E'){
		$competition_type = 'Группа E';
		$type_table = 'group';
	}elseif($_GET['group'] == 'F'){
		$competition_type = 'Группа F';
		$type_table = 'group';
	}elseif($_GET['group'] == 'G'){
		$competition_type = 'Группа G';
		$type_table = 'group';
	}elseif($_GET['group'] == 'H'){
		$competition_type = 'Группа H';
		$type_table = 'group';
	}elseif($_GET['group'] == ''){
		$competition_type = 'Групповой турнир';
	}
?>
<div class="bottom_header_wc bottom_header_wc_no_c">
	<div class="competition_type_wc">
		<ul>
			<li><a <?php set_carent_a_wc() ?> href="?competition_type=2"><span>Групповой турнир</span></a></li>
			<li><a href="?competition_type=3"><span>Плей-офф</span></a></li>
		</ul>
	</div>
</div>
<div class="bottom_header_wc bottom_header_wc_no_c bottom_header_wc_group">
	<div class="competition_type_wc">
		<ul>
			<li><a href="?<?php get_url_wc()?>group=A"><span>Группа A</span></a></li>
			<li><a href="?<?php get_url_wc()?>group=B"><span>Группа B</span></a></li>
			<li><a href="?<?php get_url_wc()?>group=C"><span>Группа C</span></a></li>
			<li><a href="?<?php get_url_wc()?>group=D"><span>Группа D</span></a></li>
			<li><a href="?<?php get_url_wc()?>group=E"><span>Группа E</span></a></li>
			<li><a href="?<?php get_url_wc()?>group=F"><span>Группа F</span></a></li>
			<li><a href="?<?php get_url_wc()?>group=G"><span>Группа G</span></a></li>
			<li><a href="?<?php get_url_wc()?>group=H"><span>Группа H</span></a></li>
		</ul>
	</div>
</div>
<div class="content_wc">
	<?php do_action( 'get_content_wc', 'Финальный раунд' , $competition_type , $type_table); ?>
</div>