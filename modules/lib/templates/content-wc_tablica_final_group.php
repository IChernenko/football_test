<?php
$type_table = '';
$competition_type = false;
switch ($_GET['group']) {
	case 'A':
		$competition_group = 'Группа A';
		break;
	case 'B':
		$competition_group = 'Группа B';
		break;
	case 'C':
		$competition_group = 'Группа C';
		break;
	case 'D':
		$competition_group = 'Группа D';
		break;
	case 'E':
		$competition_group = 'Группа E';
		break;
	case 'F':
		$competition_group = 'Группа F';
		break;
	case 'G':
		$competition_group = 'Группа G';
		break;
	case 'H':
		$competition_group = 'Группа H';
		break;
	default:
		$$competition_group = '';
		break;
}
?>
<div class="bottom_header_wc bottom_header_wc_no_c">
	<div class="competition_type_wc">
		<ul>
			<li><a <?php set_carent_a_wc() ?> href="?competition_type=group"><span>Групповой турнир</span></a></li>
			<li><a href="?competition_type=playoff"><span>Плей-офф</span></a></li>
		</ul>
	</div>
</div>
<div class="bottom_header_wc bottom_header_wc_no_c bottom_header_wc_group">
	<div class="competition_type_wc">
		<ul>
			<li><a href="?<?php get_url_wc()?>group=A"><span>Группа A</span></a></li>
			<li><a href="?<?php get_url_wc()?>group=B"><span>Группа B</span></a></li>
			<li><a href="?<?php get_url_wc()?>group=C"><span>Группа C</span></a></li>
			<li><a href="?<?php get_url_wc()?>group=D"><span>Группа D</span></a></li>
			<li><a href="?<?php get_url_wc()?>group=E"><span>Группа E</span></a></li>
			<li><a href="?<?php get_url_wc()?>group=F"><span>Группа F</span></a></li>
			<li><a href="?<?php get_url_wc()?>group=G"><span>Группа G</span></a></li>
			<li><a href="?<?php get_url_wc()?>group=H"><span>Группа H</span></a></li>
		</ul>
	</div>
</div>
<div class="content_wc">
	<?php do_action( 'get_content_wc', 'Финал' , false , $competition_group  ); ?>
</div>