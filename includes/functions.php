<?php
/*
    Our portfolio:  http://themeforest.net/user/tagDiv/portfolio
    Thanks for using our theme!
    tagDiv - 2017
*/


/**
 * Load the speed booster framework + theme specific files
 */

// load the deploy mode
require_once('td_deploy_mode.php');

// load the config
require_once('includes/td_config.php');
require_once('includes/custom_function.php');
add_action('td_global_after', array('td_config', 'on_td_global_after_config'), 9); //we run on 9 priority to allow plugins to updage_key our apis while using the default priority of 10


// load the wp booster
require_once('includes/wp_booster/td_wp_booster_functions.php');


require_once('includes/td_css_generator.php');
require_once('includes/shortcodes/td_misc_shortcodes.php');
require_once('includes/widgets/td_page_builder_widgets.php'); // widgets


/*
 * mobile theme css generator
 * in wp-admin the main theme is loaded and the mobile theme functions are not included
 * required in td_panel_data_source
 * @todo - look for a more elegant solution(ex. generate the css on request)
 */
require_once('mobile/includes/td_css_generator_mob.php');


/* ----------------------------------------------------------------------------
 * Woo Commerce
 */

// breadcrumb
add_filter('woocommerce_breadcrumb_defaults', 'td_woocommerce_breadcrumbs');
function td_woocommerce_breadcrumbs() {
	return array(
		'delimiter' => ' <i class="td-icon-right td-bread-sep"></i> ',
		'wrap_before' => '<div class="entry-crumbs" itemprop="breadcrumb">',
		'wrap_after' => '</div>',
		'before' => '',
		'after' => '',
		'home' => _x('Home', 'breadcrumb', 'woocommerce'),
	);
}

// use own pagination
if (!function_exists('woocommerce_pagination')) {
	// pagination
	function woocommerce_pagination() {
		echo td_page_generator::get_pagination();
	}
}

// Override theme default specification for product 3 per row


// Number of product per page 8
add_filter('loop_shop_per_page', create_function('$cols', 'return 4;'));

if (!function_exists('woocommerce_output_related_products')) {
	// Number of related products
	function woocommerce_output_related_products() {
		woocommerce_related_products(array(
			'posts_per_page' => 4,
			'columns' => 4,
			'orderby' => 'rand',
		)); // Display 4 products in rows of 1
	}
}




/* ----------------------------------------------------------------------------
 * bbPress
 */
// change avatar size to 40px
function td_bbp_change_avatar_size($author_avatar, $topic_id, $size) {
	$author_avatar = '';
	if ($size == 14) {
		$size = 40;
	}
	$topic_id = bbp_get_topic_id( $topic_id );
	if ( !empty( $topic_id ) ) {
		if ( !bbp_is_topic_anonymous( $topic_id ) ) {
			$author_avatar = get_avatar( bbp_get_topic_author_id( $topic_id ), $size );
		} else {
			$author_avatar = get_avatar( get_post_meta( $topic_id, '_bbp_anonymous_email', true ), $size );
		}
	}
	return $author_avatar;
}
add_filter('bbp_get_topic_author_avatar', 'td_bbp_change_avatar_size', 20, 3);
add_filter('bbp_get_reply_author_avatar', 'td_bbp_change_avatar_size', 20, 3);
add_filter('bbp_get_current_user_avatar', 'td_bbp_change_avatar_size', 20, 3);



//add_action('shutdown', 'test_td');

function test_td () {
    if (!is_admin()){
        td_api_base::_debug_get_used_on_page_components();
    }

}


/**
 * tdStyleCustomizer.js is required
 */
if (TD_DEBUG_LIVE_THEME_STYLE) {
    add_action('wp_footer', 'td_theme_style_footer');
		// new live theme demos
	    function td_theme_style_footer() {
			    ?>
			    <div id="td-theme-settings" class="td-live-theme-demos td-theme-settings-small">
				    <div class="td-skin-body">
					    <div class="td-skin-wrap">
						    <div class="td-skin-container td-skin-buy"><a target="_blank" href="http://themeforest.net/item/newspaper/5489609?ref=tagdiv">BUY NEWSPAPER NOW!</a></div>
						    <div class="td-skin-container td-skin-header">GET AN AWESOME START!</div>
						    <div class="td-skin-container td-skin-desc">With easy <span>ONE CLICK INSTALL</span> and fully customizable options, our demos are the best start you'll ever get!!</div>
						    <div class="td-skin-container td-skin-content">
							    <div class="td-demos-list">
								    <?php
								    $td_demo_names = array();

								    foreach (td_global::$demo_list as $demo_id => $stack_params) {
									    $td_demo_names[$stack_params['text']] = $demo_id;
									    ?>
									    <div class="td-set-theme-style"><a href="<?php echo td_global::$demo_list[$demo_id]['demo_url'] ?>" class="td-set-theme-style-link td-popup td-popup-<?php echo $td_demo_names[$stack_params['text']] ?>" data-img-url="<?php echo td_global::$get_template_directory_uri ?>/demos_popup/large/<?php echo $demo_id; ?>.jpg"><span></span></a></div>
								    <?php } ?>
									<div class="td-set-theme-style-empty"><a href="#" class="td-popup td-popup-empty1"></a></div>
									<div class="td-set-theme-style-empty"><a href="#" class="td-popup td-popup-empty2"></a></div>
								    <div class="clearfix"></div>
							    </div>
						    </div>
						    <div class="td-skin-scroll"><i class="td-icon-read-down"></i></div>
					    </div>
				    </div>
				    <div class="clearfix"></div>
				    <div class="td-set-hide-show"><a href="#" id="td-theme-set-hide"></a></div>
				    <div class="td-screen-demo" data-width-preview="380"></div>
			    </div>
			    <?php
	    }

}

//td_demo_state::update_state("art_creek", 'full');

//print_r(td_global::$all_theme_panels_list);

////added by Ekamaks
//remove shortlink, prev and next links
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
function remove_meta_generators($html) {
    $pattern = '/<meta name(.*)=(.*)"generator"(.*)>/i';
    $html = preg_replace($pattern, '', $html);
    return $html;
}
function clean_meta_generators($html) {
    ob_start('remove_meta_generators');
}
add_action('get_header', 'clean_meta_generators', 100);
add_action('wp_footer', function(){ ob_end_flush(); }, 100);
//Clean up menu classes
add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 3);
add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 3);

function my_css_attributes_filter($var,$item,$args) {
    if(!is_array($var)){
         return '';
    }
    if (is_object($item) && is_array($item->classes) ) {
         $classes_allowed = array('current-menu-item','menu-item', 'current-menu-ancestor', 'td-menu-item', 'td-normal-menu','menu-item-has-children');

             $classes_allowed[] = 'menu-item-has-children';

         $classes = array_intersect($item->classes, $classes_allowed);

     } else {
         return '';
    }
    return $classes;
}

/****** Ïðîãðàìèñò Èãîðü  ********/
//////// Ñîçäàíèå ïðîèçâîëüíûõ çàïèñåé äëÿ Áóêìåêåðîâ ////////////
require_once('modules/lib/post_type.php');
require_once('modules/lib/shortcode.php');
require_once('modules/lib/bukmekers.php');
require_once('modules/lib/admin/filters.php');

// âêëþ÷èì ðåãèñòðàöèþ ðåêîëë êîãäà â íàñòðîéêàõ âîðäïðåññà îíà îòêëþ÷åíà
function dd3_open_rcl_register(){
    $option = 1;
    return $option;
}
add_filter('rcl_users_can_register','dd3_open_rcl_register');


delete_option("mc_tournament_children"); 

function ff_trim_text($text , $maxchar = 10)
{
  if ( iconv_strlen( $text ,'UTF-8') > $maxchar ){
      $text = mb_substr( $text, 0, $maxchar ) .'...';
  }
  return $text; // выведет: Этот текст...
}
add_action('rcl_setup_tabs','my_add_sub_tab',10);
function my_add_sub_tab(){
   
    $subtab = array(
        'id'=> 'subtab-2',
        'name'=> 'Моя вкладка',
        'icon' => 'fa-icon',
        'callback'=>array(
            'name'=>'my_custom_function',
            'args'=>array($arg_1,$arg_2)
        )
    );
   
    rcl_add_sub_tab('parent-tab-id',$subtab);
   
}

add_action('init','sign_out_user_tab');
function sign_out_user_tab(){
   

        $tab_data = array(
            'id'=>'sign_out', 
            'name'=>__('Выход','wp-recall'),
            'supports'=>array('cache'),
            'public'=>rcl_get_option('view_publics_block_rcl'),
            'icon'=>'fa-sign-out',
            'output'=>'menu',
            'content' => array(
                array(
                    'callback' => array(
                        'id' => 'type-post',
                        'name'=>'rcl_signout',
                    )
                )
            )
        );

        rcl_tab($tab_data);
}

function rcl_signout() {
	wp_logout();
	wp_redirect('/');
}

