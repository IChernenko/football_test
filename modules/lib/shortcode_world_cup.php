<?php

// Чемпионат Мира
/*Показать информацию о чемпионате*/
function show_detal_world_cup() {
    wp_reset_query();
    $name_club = get_the_title();
    $db = DatabBase();
    $date_now = date("Y-m-d");
    $sql = "SELECT name, host_country, сities, stadiums, period_holding, amount_team,  DATEDIFF(start_championship , '".$date_now."') AS start_game  FROM foot_football_world_cup  WHERE  name = '{$name_club}' ";
    $dbresult = $db->prepare($sql);
    $dbresult->execute();
    $result = $dbresult->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE);
    //print_r($result);
    $innerHTML ="<table class='detal_info_wc'>";
    foreach ($result as $item_wc) {
    $date = date_create($item_club->foundation_year);
    $innerHTML .='
    <tr><td class="td"><strong>Место проведения: </strong></td><td>'.$item_wc->host_country.'</td></tr>
    <tr><td class="td"><strong>Города проведения:</strong></td><td>'.$item_wc->сities.'</td></tr>
    <tr><td class="td"><strong>Период проведения:</strong></td><td>'.$item_wc->period_holding.'</td></tr>
    <tr><td class="td"><strong>Количество участников: </strong></td><td>'.$item_wc->amount_team.'</td></tr>
    <tr><td class="td"><strong>К началу осталось:</strong></td><td>'.$item_wc->start_game.' дней</td></tr>';
    };
    $innerHTML .= "</table>";
    return $innerHTML;
}
add_shortcode('show_detal_world_cup', 'show_detal_world_cup');

/*Турнирная таблица*/
function turnirnaya_tablitsa_wc($atts) {
  global $wpdb;
  extract(shortcode_atts(array(
        'slug' => "prev",
        'type' => "group"
  ), $atts));
  switch ($slug) {
  case 'prev':
      wp_reset_query();
      $name_liga = get_the_title();
      break;
  case 'full':
      wp_reset_query();
      global $post;
      $name_liga = get_the_title($post->post_parent);
      break;
  }
  if ($limit == 'full'){
    $lim= '';
  }else{
    $lim = 'LIMIT 0 , '.$limit;
  }

      $sql= "SELECT com_t_g.name AS group_name ,
                  fc.name AS competition ,
                  com_t_type.name AS competition_type ,
                  com_nt.name AS sbornaya ,
                  nt_t.games , nt_t.wins , nt_t.draws , nt_t.loses , nt_t.scored_goals , nt_t.missed_goals , nt_t.points
            FROM  foot_football_national_team_tournament_table  nt_t
       LEFT JOIN  parser_common_tournament fc
              ON  nt_t.competition_id = fc.id
       LEFT JOIN  parser_common_tournament_type com_t_type
              ON  nt_t.competition_type_id = com_t_type.id
       LEFT JOIN  parser_common_tournament_group com_t_g
              ON  nt_t.competition_group_id = com_t_g.id
       LEFT JOIN  parser_common_national_team com_nt
              ON  nt_t.national_team_id = com_nt.id
           WHERE  fc.name_post = 'Финал'
           LIMIT  0 , 12";
     $result = $wpdb->get_results( $sql);
     $result = array_group($result);
      //echo "<pre>"; print_r($result); echo "</pre>";
      foreach ($result as $key => $value) {
         $i =1  ;
        $innerHTML .= "<div class='row_table_in_g'><table class='turnirnaya_tablitsa_wc turnirnaya_tablitsa_wc_home turnirnaya_tablitsa_in_g'>";
         $innerHTML .= "<tr class='group'><th colspan='9' >".$key."</th> </tr>";
        $innerHTML .= "<tr>
                          <th style='width: 50px;'>№</th>
                          <th class='wc_team'>Команда</th>
                          <th>И</th>
                          <th>О</th>
                      </tr>";
        foreach ($value as $item_team_wc) {
         $innerHTML .= '<tr>
                    <td>'.$i++.'</td>
                    <td class="wc_team"> <i class="flag_nt flag_'.translit($item_team_wc->sbornaya).'" title="'.$item_team_wc->sbornaya.'" alt="'.$item_team_wc->sbornaya.'"></i>'.$item_team_wc->sbornaya.'</td>
                    <td>'.$item_team_wc->games.'</td>
                    <td>'.$item_team_wc->points.'</td>
                </tr>';
        }
        $innerHTML .="</table></div>";
      }

    return $innerHTML;
}
add_shortcode('turnirnaya_tablitsa_wc', 'turnirnaya_tablitsa_wc');
//--- Краткий вывод Турнирной таблицы
//--- Большой вывод Турнирной таблицы
/*Последние результаты*/
function result_matches_wc($atts) {
    global $wpdb;
    extract(shortcode_atts(array(
          'slug' => "prev",
          'limit' => 5
    ), $atts));
    switch ($slug) {
    case 'prev':
        wp_reset_query();
        $name_liga = get_the_title();
        break;
    case 'full':
        wp_reset_query();
        global $post;
        $name_liga = get_the_title($post->post_parent);
        break;
    }
    if ($limit == 'full') {
      $lim= '';
    }else{
      $lim = 'LIMIT 0 , '.$limit;
    }
    $sql= "SELECT fc.name_post AS tournament,
                  com_t_type.name AS tournament_type,
                  com_t_g.name AS tournament_group ,
                  matches_nt.competition_tour ,
                  ft1.name AS team_1 ,
                  ft2.name AS team_2 ,
                  matches_nt.play_date , matches_nt.play_time , matches_nt.result_1 , matches_nt.result_2
             FROM foot_football_national_team_matches matches_nt
        LEFT JOIN parser_common_tournament fc
               ON matches_nt.competition_id = fc.id
        LEFT JOIN parser_common_tournament_type com_t_type
               ON matches_nt.competition_type_id = com_t_type.id
        LEFT JOIN parser_common_tournament_group com_t_g
               ON matches_nt.competition_group_id = com_t_g.id
        LEFT JOIN parser_common_national_team  ft1
               ON matches_nt.nt_id_1 = ft1.id
        LEFT JOIN parser_common_national_team  ft2
               ON matches_nt.nt_id_2 = ft2.id
            WHERE fc.name_post = 'Финал'
              AND matches_nt.status = 1
              AND matches_nt.season_id = '2018'
         ORDER BY matches_nt.play_date DESC {$lim} ";

    $result = $wpdb->get_results( $sql );
  	//echo "<pre>".print_r($result)."</pre>";
    $innerHTML = "<table class='footable next_match match_in_g'>";
        $innerHTML .= "<thead>
                          <tr>
                              <th style='display: none;'></th>
                              <th class='competition'>Турнир</th>
                              <th data-hide='phone,tablet'>Тур</th>
                              <th data-hide='phone,tablet'>Дата игры</th>
                              <th data-hide='phone,tablet'>Хозяева</th>
                              <th data-hide='phone,tablet'></th>
                              <th data-hide='phone,tablet'>Гости</th>
                          </tr>
                  </thead>";
         if($result) {
             foreach ($result as $item_match) {
              $innerHTML .= '<tr>
                               <td style="display: none;"></td>
                               <td>'.$item_match->tournament_type.', '.$item_match->tournament_group.'</td>
                               <td>'.$item_match->competition_tour.'</td>
                               <td>'.$item_match->play_date.'</td>
                               <td><i class="flag_nt flag_'.translit($item_match->team_1).'" title="'.$item_match->team_1.'" alt="'.$item_match->team_1.'"></i>'.$item_match->team_1.'</td>
                               <td>'.$item_match->result_1.'-'.$item_match->result_2.'</td>
                               <td><i class="flag_nt flag_'.translit($item_match->team_2).'" title="'.$item_match->team_2.'" alt="'.$item_match->team_2.'"></i>'.$item_match->team_2.'</td>
                            </tr>';
            }
        }else{
           $innerHTML .= '<tr>
                           <td style="display: none;"></td>
                           <td colspan="8"><div class="no_date">Нет данных</div></td>
                          </tr>';
        }
    $innerHTML .= "</table>";
    return $innerHTML;
}
add_shortcode('result_matches_wc', 'result_matches_wc');
/*Расписание матчей*/
function schedule_matches_wc($atts) {
    global $wpdb;
    extract(shortcode_atts(array(
          'slug' => "prev",
          'limit' => 5
    ), $atts));

    if ($limit == 'full') {
      $lim= '';
      $innerHTML = get_template_part( 'modules/lib/templates/content', 'wc_final_schedule_matches');
    }else{
      $lim = 'LIMIT 0 , '.$limit;
        $sql= "SELECT fc.name_post AS tournament,
                    com_t_type.name AS tournament_type,
                    com_t_g.name AS tournament_group ,
                    matches_nt.competition_tour ,
                    ft1.name AS team_1 ,
                    ft2.name AS team_2 ,
                    matches_nt.play_date , matches_nt.play_time , matches_nt.result_1 , matches_nt.result_2
               FROM foot_football_national_team_matches matches_nt
          LEFT JOIN parser_common_tournament fc
                 ON matches_nt.competition_id = fc.id
          LEFT JOIN parser_common_tournament_type com_t_type
                 ON matches_nt.competition_type_id = com_t_type.id
          LEFT JOIN parser_common_tournament_group com_t_g
                 ON matches_nt.competition_group_id = com_t_g.id
          LEFT JOIN parser_common_national_team  ft1
                 ON matches_nt.nt_id_1 = ft1.id
          LEFT JOIN parser_common_national_team  ft2
                 ON matches_nt.nt_id_2 = ft2.id
              WHERE fc.name_post = 'Финал'
                AND matches_nt.status = 0
           ORDER BY matches_nt.play_date ASC ,
                    matches_nt.play_time ASC {$lim} ";

      $result = $wpdb->get_results( $sql );
      //echo "<pre>".print_r($result)."</pre>";
      $innerHTML = "<table class='footable next_match match_in_g'>";
          $innerHTML .= "<thead>
                            <tr>
                                <th style='display: none;'></th>
                                <th class='competition'>Турнир</th>
                                <th data-hide='phone,tablet'>Тур</th>
                                <th data-hide='phone,tablet'>Дата игры</th>
                                <th data-hide='phone,tablet'>Время игры</th>
                                <th data-hide='phone,tablet'>Хозяева</th>
                                <th data-hide='phone,tablet'></th>
                                <th data-hide='phone,tablet'>Гости</th>
                            </tr>
                    </thead>";
          foreach ($result as $item_match) {
            // '.$item_match->tournament.','.$item_match->tournament_type.',
            $innerHTML .= '<tr>
                             <td style="display: none;"></td>
                             <td>'.$item_match->tournament_group.'</td>
                             <td>'.$item_match->competition_tour.'</td>
                             <td>'.$item_match->play_date.' </td>
                             <td>'.$item_match->play_time.'</td>
                             <td><i class="flag_nt flag_'.translit($item_match->team_1).'" title="'.$item_match->team_1.'" alt="'.$item_match->team_1.'"></i>'.$item_match->team_1.'</td>
                             <td> -:-</td>
                             <td><i class="flag_nt flag_'.translit($item_match->team_2).'" title="'.$item_match->team_2.'" alt="'.$item_match->team_2.'"></i>'.$item_match->team_2.'</td>
                          </tr>';
          }
      $innerHTML .= "</table>";
    }
    return $innerHTML;
}
add_shortcode('schedule_matches_wc', 'schedule_matches_wc');
/*Лучшие бомбардиры*/
function best_bombardir_wc($atts) {
    global $wpdb;
    extract(shortcode_atts(array(
          'slug' => "prev",
          'limit' => 5
    ), $atts));

    if ($limit == 'full') {
      $lim= '';
    }else{
      $lim = 'LIMIT 0 , '.$limit;
    }
    $sql= "SELECT fc.name_post AS tournament,
                  com_nt.name AS team,
                  com_player.short_name AS pleyer_name ,
                  com_amplua.name AS amplua ,
                  bombardires_nt.goals , bombardires_nt.penalties , bombardires_nt.minutes , bombardires_nt.games  , bombardires_nt.position
             FROM foot_football_national_team_bombardires bombardires_nt
        LEFT JOIN parser_common_tournament fc
               ON bombardires_nt.competition_id = fc.id
        LEFT JOIN parser_common_national_team com_nt
               ON bombardires_nt.national_team_id = com_nt.id
        LEFT JOIN parser_common_player com_player
               ON bombardires_nt.player_id = com_player.id
        LEFT JOIN parser_common_amplua  com_amplua
               ON bombardires_nt.amplua_id = com_amplua.id
            WHERE fc.name_post = 'Финал'
         ORDER BY bombardires_nt.position ASC {$lim} ";

    $result = $wpdb->get_results( $sql );
    //echo "<pre>".print_r($result)."</pre>";

    $innerHTML = "<table class='footable next_match match_in_g'>";
        $innerHTML .= "<thead>
                          <tr>
                              <th style='display: none;'></th>
                              <th data-hide='phone,tablet'>№</th>
                              <th class='competition'>Игрок</th>
                              <th data-hide='phone,tablet'>Команда</th>
                              <th data-hide='phone,tablet'>Амплуа</th>
                              <th data-hide='phone,tablet'>Голы</th>
                              <th data-hide='phone,tablet'>Пенальти</th>
                              <th data-hide='phone,tablet'>Минуты</th>
                              <th data-hide='phone,tablet'>Игры</th>
                          </tr>
                  </thead>";
    if($result) {
      foreach ($result as $item_player) {
          $innerHTML .= '<tr>
                           <td style="display: none;"></td>
                           <td>'.$item_player->position.'</td>
                           <td><i class="flag_nt flag_'.translit($item_player->team).'" title="'.$item_player->team.'" alt="'.$item_player->team.'"></i>'.$item_player->pleyer_name.'</td>
                           <td><i class="flag_nt flag_'.translit($item_player->team).'" title="'.$item_player->team.'" alt="'.$item_player->team.'"></i>'.$item_player->team.' </td>
                           <td>'.$item_player->amplua.'</td>
                           <td>'.$item_player->goals.'</td>
                           <td>'.$item_player->penalties.'</td>
                           <td>'.$item_player->minutes.'</td>
                           <td>'.$item_player->games.'</td>
                        </tr>';
        }
      }else{
         $innerHTML .= '<tr>
                         <td style="display: none;"></td>
                         <td colspan="8"><div class="no_date">Нет данных</div></td>
                        </tr>';
      }

    $innerHTML .= "</table>";
    return $innerHTML;
}
add_shortcode('best_bombardir_wc', 'best_bombardir_wc');
/*Лучшие асистенты*/
function best_asistent_wc($atts) {
  global $wpdb;
    extract(shortcode_atts(array(
          'slug' => "prev",
          'limit' => 5
    ), $atts));

    if ($limit == 'full') {
      $lim= '';
    }else{
      $lim = 'LIMIT 0 , '.$limit;
    }
    $sql= "SELECT fc.name_post AS tournament,
                  com_nt.name AS team,
                  com_player.short_name AS pleyer_name ,
                  com_amplua.name AS amplua ,
                  assistant_nt.passes , assistant_nt.games , assistant_nt.position
             FROM foot_football_national_team_assistant assistant_nt
        LEFT JOIN parser_common_tournament fc
               ON assistant_nt.competition_id = fc.id
        LEFT JOIN parser_common_national_team com_nt
               ON assistant_nt.national_team_id = com_nt.id
        LEFT JOIN parser_common_player com_player
               ON assistant_nt.player_id = com_player.id
        LEFT JOIN parser_common_amplua  com_amplua
               ON assistant_nt.amplua_id = com_amplua.id
            WHERE fc.name_post = 'Финал'
         ORDER BY assistant_nt.position ASC {$lim} ";

    $result = $wpdb->get_results( $sql );
    //echo "<pre>".print_r($result)."</pre>";
    $innerHTML = "<table class='footable next_match match_in_g'>";
        $innerHTML .= "<thead>
                          <tr>
                              <th style='display: none;'></th>
                              <th data-hide='phone,tablet'>№</th>
                              <th class='competition'>Игрок</th>
                              <th data-hide='phone,tablet'>Команда</th>
                              <th data-hide='phone,tablet'>Амплуа</th>
                              <th data-hide='phone,tablet'>Пасы</th>
                              <th data-hide='phone,tablet'>Игры</th>
                          </tr>
                  </thead>";
        if($result) {
             foreach ($result as $item_player) {
              $innerHTML .= '<tr>
                               <td style="display: none;"></td>
                               <td>'.$item_player->position.'</td>
                               <td><i class="flag_nt flag_'.translit($item_player->team).'" title="'.$item_player->team.'" alt="'.$item_player->team.'"></i>'.$item_player->pleyer_name.'</td>
                               <td><i class="flag_nt flag_'.translit($item_player->team).'" title="'.$item_player->team.'" alt="'.$item_player->team.'"></i>'.$item_player->team.' </td>
                               <td>'.$item_player->amplua.'</td>
                               <td>'.$item_player->passes.'</td>
                               <td>'.$item_player->games.'</td>
                            </tr>';
            }
        }else{
           $innerHTML .= '<tr>
                           <td style="display: none;"></td>
                           <td colspan="8"><div class="no_date">Нет данных</div></td>
                          </tr>';
        }

    $innerHTML .= "</table>";
    return $innerHTML;
}
add_shortcode('best_asistent_wc', 'best_asistent_wc');

/*Вывод групы в записи*/
function show_group_wc($atts) {
  extract(shortcode_atts(array(
        'season' => "2018",
        'competition' => "Европа",
        'competition_type' => "Групповой турнир",
        'group' => "Группа A"
  ), $atts));
  $db = DatabBase();
  switch ($atts['competition']) {
  case 'Европа':
      switch ($atts['competition_type']) {
        case "Групповой турнир":
            $sql= "SELECT gr.name AS  grup , country.name AS sbornaya , nt_m.games , nt_m.wins , nt_m.draws , nt_m.loses , nt_m.scored_goals , nt_m.missed_goals , nt_m.points   FROM foot_football_national_team_tablica nt_m JOIN foot_common_season season ON nt_m.season_id = season.id_season JOIN  foot_common_competition  comp ON nt_m.competition_id = comp.id_competition JOIN  foot_common_competition_type  comp_type ON nt_m.competition_type_id = comp_type.id_competition_type JOIN  foot_common_group  gr ON nt_m.group_id = gr.id_group JOIN  foot_common_country  country ON nt_m.national_team_id = country.id_country WHERE  comp.name = '{$atts["competition"]}' AND gr.name = '{$atts["group"]}' ORDER BY nt_m.points DESC , nt_m.scored_goals DESC ";

            break;
        case 'full':

            break;
      }
      break;
  case 'Африка':
      switch ($atts['competition_type']) {
        case "Групповой турнир":
            $sql= "SELECT gr.name AS  grup , country.name AS sbornaya , nt_m.games , nt_m.wins , nt_m.draws , nt_m.loses , nt_m.scored_goals , nt_m.missed_goals , nt_m.points   FROM foot_football_national_team_tablica nt_m JOIN foot_common_season season ON nt_m.season_id = season.id_season JOIN  foot_common_competition  comp ON nt_m.competition_id = comp.id_competition JOIN  foot_common_competition_type  comp_type ON nt_m.competition_type_id = comp_type.id_competition_type JOIN  foot_common_group  gr ON nt_m.group_id = gr.id_group JOIN  foot_common_country  country ON nt_m.national_team_id = country.id_country WHERE  comp.name = '{$atts["competition"]}' AND gr.name = '{$atts["group"]}' ORDER BY nt_m.points DESC , nt_m.scored_goals DESC ";

            break;
        case 'full':

            break;
      }
      break;
  case 'Южная Америка':
      switch ($atts['competition_type']) {
        case "Групповой турнир":
            $sql= "SELECT gr.name AS  grup , country.name AS sbornaya , nt_m.games , nt_m.wins , nt_m.draws , nt_m.loses , nt_m.scored_goals , nt_m.missed_goals , nt_m.points   FROM foot_football_national_team_tablica nt_m JOIN foot_common_season season ON nt_m.season_id = season.id_season JOIN  foot_common_competition  comp ON nt_m.competition_id = comp.id_competition JOIN  foot_common_competition_type  comp_type ON nt_m.competition_type_id = comp_type.id_competition_type JOIN  foot_common_group  gr ON nt_m.group_id = gr.id_group JOIN  foot_common_country  country ON nt_m.national_team_id = country.id_country WHERE  comp.name = '{$atts["competition"]}' ORDER BY nt_m.points DESC , nt_m.scored_goals DESC ";

            break;
        case 'full':

            break;
      }
      break;
  case 'Северная Америка':
      switch ($atts['competition_type']) {
        case "Групповой турнир":
            $sql= "SELECT gr.name AS  grup , country.name AS sbornaya , nt_m.games , nt_m.wins , nt_m.draws , nt_m.loses , nt_m.scored_goals , nt_m.missed_goals , nt_m.points   FROM foot_football_national_team_tablica nt_m JOIN foot_common_season season ON nt_m.season_id = season.id_season JOIN  foot_common_competition  comp ON nt_m.competition_id = comp.id_competition JOIN  foot_common_competition_type  comp_type ON nt_m.competition_type_id = comp_type.id_competition_type JOIN  foot_common_group  gr ON nt_m.group_id = gr.id_group JOIN  foot_common_country  country ON nt_m.national_team_id = country.id_country WHERE  comp.name = '{$atts["competition"]}' AND gr.name = '{$atts["group"]}' ORDER BY nt_m.points DESC , nt_m.scored_goals DESC ";

            break;
        case 'full':

            break;
      }
      break;
  case 'Океания':
      switch ($atts['competition_type']) {
        case "Предварительный турнир":
            $sql= "SELECT gr.name AS  grup , country.name AS sbornaya , nt_m.games , nt_m.wins , nt_m.draws , nt_m.loses , nt_m.scored_goals , nt_m.missed_goals , nt_m.points   FROM foot_football_national_team_tablica nt_m JOIN foot_common_season season ON nt_m.season_id = season.id_season JOIN  foot_common_competition  comp ON nt_m.competition_id = comp.id_competition JOIN  foot_common_competition_type  comp_type ON nt_m.competition_type_id = comp_type.id_competition_type JOIN  foot_common_group  gr ON nt_m.group_id = gr.id_group JOIN  foot_common_country  country ON nt_m.national_team_id = country.id_country WHERE  comp.name = '{$atts["competition"]}' AND comp_type.name = '{$atts['competition_type']}'  AND gr.name = '{$atts["group"]}' ORDER BY  nt_m.position ASC";

            break;
        case 'Групповой турнир':
            $sql= "SELECT gr.name AS  grup , country.name AS sbornaya , nt_m.games , nt_m.wins , nt_m.draws , nt_m.loses , nt_m.scored_goals , nt_m.missed_goals , nt_m.points   FROM foot_football_national_team_tablica nt_m JOIN foot_common_season season ON nt_m.season_id = season.id_season JOIN  foot_common_competition  comp ON nt_m.competition_id = comp.id_competition JOIN  foot_common_competition_type  comp_type ON nt_m.competition_type_id = comp_type.id_competition_type JOIN  foot_common_group  gr ON nt_m.group_id = gr.id_group JOIN  foot_common_country  country ON nt_m.national_team_id = country.id_country WHERE  comp.name = '{$atts["competition"]}' AND comp_type.name = '{$atts['competition_type']}'  AND gr.name = '{$atts["group"]}' ORDER BY  nt_m.position ASC";
            break;
      }
      break;
  }
  $dbresult = $db->prepare($sql);
  $dbresult->execute();
  $result = $dbresult->fetchAll(PDO::FETCH_GROUP |  PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE);
  //echo "<pre>"; print_r($result); echo "</pre>";
   $i =1  ;
  foreach ($result as $key => $value) {
    $innerHTML = "<table class='turnirnaya_tablitsa_wc turnirnaya_tablitsa_wc_home'>";
     $innerHTML .= "<tr class='group'><th colspan='9' >".$key."</th> </tr>";
    $innerHTML .= "<tr><th style='width: 50px;'>№</th><th class='wc_team'>Команда</th><th>И</th><th>В</th><th>Н</th><th>П</th><th>ЗГ</th><th>ПГ</th><th>О</th></tr>";
    foreach ($value as $item_team_wc) {
     $innerHTML .= '<tr>
                <td>'.$i++.'</td>
                <td class="wc_team"><i class="flag_nt flag_'.translit($item_team_wc->sbornaya).'" title="'.$item_team_wc->sbornaya.'" alt="'.$item_team_wc->sbornaya.'"></i>'.$item_team_wc->sbornaya.'</td>
                <td>'.$item_team_wc->games.'</td>
                <td>'.$item_team_wc->wins.'</td>
                <td>'.$item_team_wc->draws.'</td>
                <td>'.$item_team_wc->loses.'</td>
                <td>'.$item_team_wc->scored_goals.'</td>
                <td>'.$item_team_wc->missed_goals.'</td>
                <td>'.$item_team_wc->points.'</td>
            </tr>';
    }
    $innerHTML .="</table>";
  }
  $innerHTML .= "<div class='opis_table opis_table_home'><b>И</b> - игры, <b>В</b> - выигрыши, <b>Н</b> - ничья, <b>П</b> - поражения, <b>ЗГ/з</b> - забитые голы, <b>ПГ</b> - пропущенные голы, <b>О</b> - очки</div>";
  return $innerHTML;
}
add_shortcode('show_group_wc', 'show_group_wc');
/*Результаты матчей*/
function show_matches_wc($atts) {
  extract(shortcode_atts(array(
        'season' => "2018",
        'competition' => "Европа",
        'competition_type' => "Групповой турнир",
        'group' => "Группа A"
  ), $atts));
//echo "<pre>"; print_r($result); echo "</pre>";
  $innerHTML = "Тут будет информация по матчам групы<b> ".$group."</b> конфедерации<b> ".$competition."</b>";

return $innerHTML;
}
add_shortcode('show_matches_wc', 'show_matches_wc');

require_once('world_cup_main.php');
