<?php
$type_table = '';

?>
<div class="content_wc">
	<?php $result = get_world_cup_matches_list( '2018' , 'Финал' , 'Плей-офф' , false , 0 , 'play_date' , 'ASC'  );
	
  $innerHTML = "<table class='footable next_match match_in_g'>";
      $innerHTML .= "<thead>
                        <tr>
                            <th style='display: none;'></th>
                            <th class='competition'>Турнир</th>
                            <th data-hide='phone,tablet'>Дата игры</th>
                            <th data-hide='phone,tablet'>Время игры</th>
                            <th data-hide='phone,tablet'>Хозяева</th>
                            <th data-hide='phone,tablet'></th>
                            <th data-hide='phone,tablet'>Гости</th>
                        </tr>
                </thead>";
      foreach ($result as $item_match) {
        // '.$item_match->tournament.','.$item_match->tournament_type.',
        $innerHTML .= '<tr>
                         <td style="display: none;"></td>
                         <td>'.$item_match->tournament_group.'</td>

                         <td>'.$item_match->play_date.' </td>
                         <td>'.$item_match->play_time.'</td>
                         <td><i class="flag_nt flag_'.translit($item_match->team_1).'" title="'.$item_match->team_1.'" alt="'.$item_match->team_1.'"></i>'.$item_match->team_1.'</td>
                         <td> -:-</td>
                         <td><i class="flag_nt flag_'.translit($item_match->team_2).'" title="'.$item_match->team_2.'" alt="'.$item_match->team_2.'"></i>'.$item_match->team_2.'</td>
                      </tr>';
      }
  $innerHTML .= "</table>";
  echo $innerHTML;
	//print_r($result)
	?>
</div>
