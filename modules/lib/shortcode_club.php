<?php
// Клубы
/*Показать информацию о клубе*/
function show_detal_club() {
  global $wpdb;
  global $ff_cfg;
  wp_reset_query();
  $name_club = get_the_title();
  $post_id = get_the_ID();
  $name_club = esc_sql($name_club);
  $post_metadate = get_post_meta($post_id);

  //Все соревнования клуба
  $sql = "SELECT c_comp.name_post AS competition_name  
            FROM {$ff_cfg['db_table']['football_club_to_competition']}   club_to_comp
            JOIN {$ff_cfg['db_table']['common_club']} с_club 
              ON club_to_comp.club_id = с_club.id 
       LEFT JOIN {$ff_cfg['db_table']['common_tournament']} c_comp 
              ON club_to_comp.competition_id = c_comp.id 
           WHERE с_club.name_post = '{$name_club}' ";
  $result = $wpdb->get_results( $sql);

  foreach ($result as $key => $value) {
    $competitions_name[] = $value->competition_name;
  }
  $competitions_name = implode("; ", $competitions_name);
  
  
  $innerHTML = "<table>";
  $innerHTML .= '
                  <tr><td><strong>Название:</strong></td><td>'.$post_metadate['_club_desc_name_club'][0].'</td></tr>
                  <tr><td><strong>Прозвища:</strong></td><td>'.$post_metadate['_club_desc_nickname'][0].'</td></tr>
                  <tr><td><strong>Страна:&nbsp;</strong></td><td>'.$post_metadate['_club_desc_country'][0].'</td></tr>
                  <tr><td><strong>Год основания:</strong></td><td>'.$post_metadate['_club_desc_foundation_year'][0].'</td></tr>
                  <tr><td><strong>Стадион:</strong></td><td>'.$post_metadate['_club_desc_stadium'][0].'</td></tr>
                  <tr><td><strong>Президент:&nbsp;</strong></td> <td>'.$post_metadate['_club_desc_president'][0].'</td></tr>
                  <tr><td><strong>Тренер:</strong></td><td>'.$post_metadate['_club_desc_trener_main'][0].'</td></tr>
                  <tr><td><strong>Капитан:</strong></td><td>'.$post_metadate['_club_desc_capitan'][0].'</td></tr>
                  <tr><td><strong>Рейтинг:</strong></td><td>'.$post_metadate['_club_desc_rating_uefa'][0].' позиция (рейтинг УЕФА)</td></tr>
                  <tr><td><strong>Спонсор:</strong></td><td>'.$post_metadate['_club_desc_sponsor'][0].'</td></tr>
                  <tr><td><strong>Соревнование:</strong></td><td>'.$competitions_name.'</td></tr>';
  $innerHTML .= "</table>";
  return $innerHTML;
}
add_shortcode('show_detal_club', 'show_detal_club');
/* Состав */
function show_sostav_club($atts) {
  global $wpdb;
  global $ff_cfg;
	extract(shortcode_atts(array(
        'slug' => "prev",
        'limit' => 5,
  ), $atts));
  switch ($slug) {
  case 'prev':
      wp_reset_query();
      $name_club = get_the_title();
      $class_table = 'table_sostav table_sostav_prev footable';
      break;
  case 'full':
      wp_reset_query();
      global $post;
      $name_club = get_the_title($post->post_parent);
      $class_table = 'table_sostav footable';
      break;
  }
  if ($limit == 'full') {
    $lim= '';
  }else{
    $lim = 'LIMIT 0 , '.$limit;
  }
  //print_r($post);

  $sql = "SELECT com_player.short_name AS player_name , 
                 TIMESTAMPDIFF(YEAR, com_player.birthday ,curdate()) as Age , 
                 com_player.player_photo  AS foto ,
                 p_to_club.growth, p_to_club.weight , p_to_club.player_number ,  
                 com_amp.name AS amplua , 
                 com_county.name AS national_team 
            FROM {$ff_cfg['db_table']['football_player_to_club']}  p_to_club  
       LEFT JOIN {$ff_cfg['db_table']['common_player']} com_player 
              ON p_to_club.player_id = com_player.id   
       LEFT JOIN {$ff_cfg['db_table']['common_club']} com_club 
              ON p_to_club.club_id = com_club.id   
       LEFT JOIN {$ff_cfg['db_table']['common_amplua']} com_amp 
              ON p_to_club.amplua_id = com_amp.id  
            JOIN {$ff_cfg['db_table']['common_country']} com_county 
              ON p_to_club.country_id = com_county.id 
           WHERE com_club.name_post = '{$name_club}'
             AND p_to_club.player_status = 1  {$lim} ";
  $result = $wpdb->get_results( $sql);

  $result_array = json_decode(json_encode($result), True);
  $result_array = SuperUnique($result_array , 'player_name');
  $result = json_decode(json_encode($result_array), FALSE);
  $innerHTML = "<table class='".$class_table."'>";
  $innerHTML .= "<thead>
                          <tr>
                            <th style='display: none;'></th>
                            <th>Фото</th>
                            <th>Игрок</th>
                            <th data-hide='phone,tablet'>Номер</th>
                            <th data-hide='phone,tablet'>Сборная</th>
                            <th data-hide='phone,tablet'>Возраст</th>
                            <th data-hide='phone,tablet'>Рост</th>
                            <th data-hide='phone,tablet'>Вес</th>
                            <th data-hide='phone,tablet'>Амплуа</th>
                          </tr>
                  </thead>";
	foreach ($result as $item_sostav) {
    if (empty($item_sostav->growth)) {
       $growth = '';
    }else{
       $growth = $item_sostav->growth.' см';
    }
    if (empty($item_sostav->weight)) {
       $weight = '';
    }else{
       $weight = $item_sostav->weight.' кг';
    }
	 $innerHTML .= '<tr>
                      <td style="display: none;""></td>
					            <td class="img_player"><img src="'.$item_sostav->foto.'" alt="" class="img_sostav"></td>
					            <td>'.$item_sostav->player_name.'</td>
					            <td class="number_club">'.$item_sostav->player_number.'</td>
					            <td class="national_team"><i class="flag_nt flag_'.translit($item_sostav->national_team).'" title="'.$item_sostav->national_team.'" alt="'.$item_match->national_team.'"></i>'.$item_sostav->national_team.'</td>
					            <td class="age">'.$item_sostav->Age.'</td>
					            <td class="growth">'.$growth.'</td>
					            <td class="weight">'.$weight.'</td>
					            <td class="amplua">'.$item_sostav->amplua.'</td>
					        </tr>';
  }
  $innerHTML .= "</table>";
  return $innerHTML;
}
add_shortcode('show_sostav_club', 'show_sostav_club');
/*Расписание матчей*/
function schedule_of_matches_club ($atts) {
  global $wpdb;
  extract(shortcode_atts(array(
        'slug' => "prev",
        'limit' => 5,
  ), $atts));
  switch ($slug) {
  case 'prev':
      wp_reset_query();
      $name_club = get_the_title();
      break;
  case 'full':
      wp_reset_query();
      global $post;
      $name_club = get_the_title($post->post_parent);
      break;
  }
  if ($limit == 'full') {
    $lim= '';
  }else{
    $lim = 'LIMIT 0 , '.$limit;
  }
  $sql= "   SELECT cm.play_date ,  cm.play_time, cm.competition_tour_txt,
                 com_turnament.name AS competition  ,  
                 t_1.name AS team_1 ,  
                 t_1.name_post AS team_1_short ,  
                 t_2.name AS team_2 , 
                 t_2.name_post AS team_2_short , 
                 t_1.logo_link AS logo_t1 , 
                 t_2.logo_link AS logo_t2 
            FROM foot_football_club_matches   cm 
            JOIN parser_common_tournament com_turnament 
              ON cm.competition_id = com_turnament.id 
            JOIN parser_common_club t_1 
              ON cm.club_id_1 = t_1.id  
            JOIN parser_common_club t_2 
              ON cm.club_id_2 = t_2.id   
            WHERE t_1.name = '{$name_club}'   
              AND cm.status = 0
               OR t_1.name_post = '{$name_club}'
              AND cm.status = 0
               OR t_2.name = '{$name_club}' 
              AND cm.status = 0
               OR t_2.name_post = '{$name_club}' 
              AND cm.status = 0
          ORDER BY cm.play_date ASC ,
                   cm.competition_tour ASC  {$lim} ";
  $result = $wpdb->get_results( $sql);
  $innerHTML = "<table class='footable table_schedule_of_matches_club'>";
	$innerHTML .= "<thead>
                        <tr>
                            <th style='display: none;'></th>
                            <th class='competition'>Турнир</th>
                            <th data-hide='phone,tablet'></th>
                            <th data-hide='phone,tablet'>Дата игры</th>
                            <th data-hide='phone,tablet'>Хозяева</th>
                            <th data-hide='phone,tablet'></th>
                            <th data-hide='phone,tablet'>Гости</th>
                        </tr>  
                </thead>";          
  foreach ($result as $item_match) {
  	if ($item_match->team_1 ==  $name_club ) {
		    $football_team_1 = '<strong>'.$item_match->team_1.'</strong>';
		}else{
			 $football_team_1 = $item_match->team_1;
		}

		if ($item_match->team_2 ==  $name_club) {
		    $football_team_2 = '<strong>'.$item_match->team_2.'</strong>';
		}else{
			 $football_team_2 = $item_match->team_2;
		}


    $date = date_create($item_match->play_date);
    $innerHTML .=  "<tr>
                        <td style='display: none;'></td>
                        <td>".$item_match->competition."</td>
                        <td>".$item_match->competition_tour_txt."</td>
    										<td>".date_format($date, 'd.m.')."".$item_match->play_time."</td>
    										<td class='ft_1'>".$football_team_1."</td>
					              <td><strong>-:-</strong></td>
					              <td class='ft_2'>".$football_team_2."</td>
    							  </tr>";
     }
  $innerHTML .= "</table>";
  return $innerHTML;
}
add_shortcode('schedule_of_matches_club', 'schedule_of_matches_club');
/*Последние результаты*/
function result_of_matches_club($atts) {
  global $wpdb;
  extract(shortcode_atts(array(
        'slug' => "prev",
        'limit' => 5,
  ), $atts));
  switch ($slug) {
  case 'prev':
      wp_reset_query();
      $name_club = get_the_title();
      break;
  case 'full':
      wp_reset_query();
      global $post;
      $name_club = get_the_title($post->post_parent);
      break;
  }
  if ($limit == 'full') {
    $lim= '';
  }else{
    $lim = 'LIMIT 0 , '.$limit;
  }
  $sql= "   SELECT cm.play_date ,  cm.play_time, cm.competition_tour_txt,
                 com_turnament.name AS competition  ,  
                 t_1.name AS team_1 ,  
                 t_1.name_post AS team_1_short ,  
                 t_2.name AS team_2 , 
                 t_2.name_post AS team_2_short , 
                 t_1.logo_link AS logo_t1 , 
                 t_2.logo_link AS logo_t2 ,
                 cm.result_match, cm.result_1 , cm.result_2
            FROM foot_football_club_matches   cm 
            JOIN parser_common_tournament com_turnament 
              ON cm.competition_id = com_turnament.id 
            JOIN parser_common_club t_1 
              ON cm.club_id_1 = t_1.id  
            JOIN parser_common_club t_2 
              ON cm.club_id_2 = t_2.id   
            WHERE t_1.name = '{$name_club}'   
              AND cm.status = 1
               OR t_1.name_post = '{$name_club}'
              AND cm.status = 1
               OR t_2.name = '{$name_club}' 
              AND cm.status = 1
               OR t_2.name_post = '{$name_club}' 
              AND cm.status = 1   
          ORDER BY 
                   cm.play_date DESC {$lim} ";
  $result = $wpdb->get_results( $sql);
  $innerHTML = "<table class='footable table_schedule_of_matches_club'>";
	$innerHTML .= "<thead>
                        <tr>
                            <th style='display: none;'></th>
                            <th  >Турнир</th>
                            <th data-hide='phone,tablet'>Тур</th>
                            <th data-hide='phone,tablet'>Хозяева</th>
                            <th data-hide='phone,tablet'>Счет</th>
                            <th data-hide='phone,tablet'>Гости</th>
                            <th data-hide='phone,tablet'>Дата игры</th>
                        </tr>
                </thead>";            
  foreach ($result as $item_match) {
  	if ($item_match->team_1 ==  $name_club ) {
		    $football_team_1 = '<strong>'.$item_match->team_1.'</strong>';
		}else{
			 $football_team_1 = $item_match->team_1;
		}

		if ($item_match->team_2 ==  $name_club) {
		    $football_team_2 = '<strong>'.$item_match->team_2.'</strong>';
		}else{
			 $football_team_2 = $item_match->team_2;
		}

    $date = date_create($item_match->play_date);
    $innerHTML .=  "<tr>
                        <td style='display: none;'></td>
    										<td>".$item_match->competition."</td>
    										<td>".$item_match->competition_tour_txt."</td>
    										<td class='ft_1'>".$football_team_1."</td>
					              <td><strong>".$item_match->result_1." : ".$item_match->result_2."</strong></td>
					              <td class='ft_2'>".$football_team_2."</td>
												<td>".date_format($date, 'd.m.Y')."</td>
    							  </tr>";
     }
  $innerHTML .= "</table>";
  return $innerHTML;
}
add_shortcode('result_of_matches_club', 'result_of_matches_club');
/*Статистика игроков*/
function players_statistic_club($atts) {
  global $wpdb;
  extract(shortcode_atts(array(
        'slug' => "prev",
        'limit' => 5,
  ), $atts));
  switch ($slug) {
  case 'prev':
      wp_reset_query();
      $name_club = get_the_title();
      $innerHTML = prev_statistic_club($name_club);
      break;
  case 'full':
      wp_reset_query();
      global $post;
      $name_club = get_the_title($post->post_parent);
      $innerHTML .= prev_statistic_club($name_club);
      $innerHTML .= full_statistic_club($name_club);
      break;
  }
  return $innerHTML;
}
add_shortcode('players_statistic_club', 'players_statistic_club');
//Вывод статистики на главной странице карточки
function prev_statistic_club($name_club){
  global $wpdb;
  $sql= "SELECT fc.name AS competition  , com_club.name AS club, 
                club_stat.games , club_stat.wins ,  club_stat.draws ,  club_stat.loses ,  club_stat.scored_goals  ,  club_stat.missed_goals 
           FROM foot_football_club_statistic club_stat
      LEFT JOIN parser_common_tournament fc 
             ON club_stat.competition_id = fc.id 
      LEFT JOIN parser_common_club  com_club 
             ON club_stat.club_id = com_club.id 
          WHERE com_club.name_post = '{$name_club}'  ";
  $result = $wpdb->get_results( $sql );

  $innerHTML = "<table class='footable table_main_club_statistic'>";
  $innerHTML .= "<thead>
                        <tr>
                            <th style='display: none;'></th>
                            <th data-hide='phone,tablet'>Турнир</th>
                            <th>И</th>
                            <th data-hide='phone,tablet'>В</th>
                            <th data-hide='phone,tablet'>Н</th>
                            <th data-hide='phone,tablet'>П</th>
                            <th data-hide='phone,tablet'>ЗГ</th>
                            <th data-hide='phone,tablet'>ПГ</th>
                        </tr>
                </thead>";     
  foreach ($result as $item_stat) {
    $games =  $games + $item_stat->games;
    $wins =  $wins + $item_stat->wins;
    $draws =  $draws + $item_stat->draws;
    $loses =  $loses + $item_stat->loses;
    $scored_goals =  $scored_goals + $item_stat->scored_goals;
    $missed_goals =  $missed_goals + $item_stat->missed_goals;
    $innerHTML .=  "<tr>
                        <td style='display: none;'></td>
                        <td class='left_text competition_name'>".$item_stat->competition."</td>
                        <td class='center_text games'>".$item_stat->games."</td>
                        <td class='center_text minutes'>".$item_stat->wins."</td>
                        <td class='center_text was_replaced'>".$item_stat->draws."</td>
                        <td class='center_text came_to_replace'>".$item_stat->loses."</td>
                        <td class='center_text goals'>".$item_stat->scored_goals."</td>
                        <td class='center_text penalty'>".$item_stat->missed_goals."</td>
                    </tr>";
  }     
     $innerHTML .=  "<tr>
                        <td style='display: none;'></td>
                        <td class='left_text competition_name'><strong>Общая статистика</strong></td>
                        <td class='center_text games'><strong>".$games."</strong></td>
                        <td class='center_text minutes'><strong>".$wins."</strong></td>
                        <td class='center_text was_replaced'><strong>".$draws."</strong></td>
                        <td class='center_text came_to_replace'><strong>".$loses."</strong></td>
                        <td class='center_text goals'><strong>".$scored_goals."</strong></td>
                        <td class='center_text penalty'><strong>".$missed_goals."</strong></td>
                    </tr>";
  $innerHTML .= '</table>';
  $innerHTML .= '<div class="opis_table">
                      <b>И</b>&nbsp;-&nbsp;игры, 
                      <b>В</b>&nbsp;-&nbsp;количество выигршей, 
                      <b>Н</b>&nbsp;-&nbsp;количество ничьих, 
                      <b>П</b>&nbsp;-&nbsp;количество поражений, 
                      <b>ЗГ</b>&nbsp;-&nbsp;количество забитых голов, 
                      <b>ПГ</b>&nbsp;-&nbsp;количество пропущеных голов, 
                  </div>';
   
 // print_r($result);

  return $innerHTML;
}

//Вывод статистики на странице (Статистика)
function full_statistic_club($name_club ){
  global $wpdb;
  $sql= "SELECT com_player.name AS pleyer_name ,  com_player.short_name AS short_name ,
                com_country.name AS country,
                psc.games , psc.minutes  , psc.was_replaced  , psc.came_to_replace , psc.goals  , psc.penalty  , 
                psc.passes  , psc.yellow_cards  , psc.red_cards   , psc.player_number 
          FROM  foot_football_player_statistic_club psc 
          JOIN  parser_common_player com_player 
            ON  psc.player_id = com_player.id  
          JOIN  parser_common_club com_club
            ON  psc.club_id = com_club.id 
          JOIN  parser_common_country com_country
            ON  psc.player_country_id = com_country.id  
         WHERE  com_club.name_post = '{$name_club}'
      ORDER BY  psc.games DESC {$lim} ";

  $result = $wpdb->get_results( $sql);
  
  $innerHTML = "<table class='footable table_statistic_player_club'>";
  $innerHTML .= "<thead>
                        <tr>
                            <th style='display: none;'></th>
                            <th data-hide='phone,tablet'>Номер</th>
                            <th>Игрок</th>
                            <th data-hide='phone,tablet'>М</th>
                            <th data-hide='phone,tablet'>Мин</th>
                            <th data-hide='phone,tablet'>З</th>
                            <th data-hide='phone,tablet'>ВнЗ</th>
                            <th data-hide='phone,tablet'>Г</th>
                            <th data-hide='phone,tablet'>Пен</th>
                            <th data-hide='phone,tablet'>П</th>
                            <th data-hide='phone,tablet'>ЖК</th>
                            <th data-hide='phone,tablet'>КК</th>
                        </tr>
                </thead>";          
  foreach ($result as $item_stat) {
      if(empty($item_stat->short_name)){
        $pleyer_name = $item_stat->pleyer_name;
      }else{
        $pleyer_name = $item_stat->short_name;
      }
      $innerHTML .=  "<tr>
                          <td style='display: none;'></td>
                          <td class='center_text number_player'>".$item_stat->player_number."</td>
                          <td class='left_text player'><i class='flag_nt flag_".translit($item_stat->country)."' title='".$item_stat->country."' alt='".$item_stat->country."'></i>".$pleyer_name."</td>
                          <td class='center_text games'>".$item_stat->games."</td>
                          <td class='center_text minutes'>".$item_stat->minutes."</td>
                          <td class='center_text was_replaced'>".$item_stat->was_replaced."</td>
                          <td class='center_text came_to_replace'>".$item_stat->came_to_replace."</td>
                          <td class='center_text goals'>".$item_stat->goals."</td>
                          <td class='center_text penalty'>".$item_stat->penalty."</td>
                          <td class='center_text passes'>".$item_stat->passes."</td>
                          <td class='center_text yelov_cart'>".$item_stat->yellow_cards."</td>
                          <td class='center_text red_cart'>".$item_stat->red_cards."</td>
                      </tr>";
    }
    $innerHTML .= "</table>";
    $innerHTML .= '<div class="opis_table">
                      <b>М</b>&nbsp;-&nbsp;сыграл матчей, 
                      <b>Мин</b>&nbsp;-&nbsp;сыграл минут, 
                      <b>З</b>&nbsp;-&nbsp;был заменен, 
                      <b>ВнЗ</b>&nbsp;-&nbsp;вышел на замену, 
                      <b>Г</b>&nbsp;-&nbsp;забил голов, 
                      <b>Пен</b>&nbsp;-&nbsp;забил голов с пенальти, 
                      <b>П</b>&nbsp;-&nbsp;сделал голевых передач, 
                      <b>ЖК</b>&nbsp;-&nbsp;желтые карточки, 
                      <b>КК</b>&nbsp;-&nbsp;красные карточки
                  </div>';
   
    return $innerHTML;

}

/*Турнирная таблица*/
function show_tournament_table_club($atts)
{
  global $wpdb;
  global $dir_img_club;
  extract(shortcode_atts(array(
        'slug' => "prev",
        'limit' => 5,
  ), $atts));
  switch ($slug) {
  case 'prev':
      wp_reset_query();
      $name_club = get_the_title();
      break;
  case 'full':
      wp_reset_query();
      global $post;
      $name_club = get_the_title($post->post_parent);
      break;
  }
  if ($limit == 'full') {
    $lim= '';
  }else{
    $lim = 'LIMIT 0 , '.$limit;
  }
  $sql  = "SELECT fc.name
             FROM foot_football_competition_term_relationships  com_tr
             JOIN parser_common_tournament fc 
               ON com_tr.competition_id = fc.id 
             JOIN foot_football_club_to_competition  club_to_competition 
               ON club_to_competition.competition_id = fc.id
             JOIN parser_common_club  com_club 
               ON club_to_competition.club_id = com_club.id
            WHERE com_club.name_post = '{$name_club}' 
              AND com_tr.competition_term_id = 2 
             LIMIT 0 , 1";
  $result = $wpdb->get_results( $sql);

  $name_liga = $result[0]->name;
  $name_liga = esc_sql($name_liga);

  $sql  = "SELECT fc.name AS com_comp ,
                  com_club.name AS f_team, com_club.logo_link ,  
                  foot_tt.position , foot_tt.old_position , 
                  foot_tt.games AS quantity_games , foot_tt.wins AS winnings , foot_tt.draws AS draws  , foot_tt.loses AS losing , 
                  foot_tt.scored_goals , foot_tt.missed_goals , foot_tt.points 
             FROM foot_football_club_turnirnaya_tablitsa  foot_tt  
             JOIN parser_common_tournament fc 
               ON foot_tt.competition_id = fc.id 
             JOIN parser_common_club  com_club 
               ON foot_tt.club_id = com_club.id
            WHERE fc.name = '{$name_liga}' 
               OR fc.name_post = '{$name_liga}'  
         ORDER BY foot_tt.position ASC  {$lim}";
  $result = $wpdb->get_results( $sql);
  foreach ($result as $key => $value) {
    
    $sql= "(SELECT  ft1.name AS team_1 ,
                    ft2.name AS team_2 ,
                    fm.competition_tour  AS competition_tour,
                    fm.play_date , fm.play_time , 
                    fm.result_1 , fm.result_2 ,
                    fm.result_match
               FROM foot_football_club_matches fm 
          LEFT JOIN parser_common_tournament fc 
                 ON fm.competition_id = fc.id 
          LEFT JOIN parser_common_club  ft1 
                 ON fm.club_id_1 = ft1.id 
          LEFT JOIN parser_common_club  ft2 
                 ON fm.club_id_2 = ft2.id  
              WHERE fc.name = '{$value->com_comp}' 
                AND ft1.name = '{$value->f_team}' 
                AND fm.status = 1
           ORDER BY fm.play_date DESC ,
                    fm.play_time DESC
                LIMIT 0 , 5) 
           UNION 
          (SELECT   ft1.name AS team_1 ,
                    ft2.name AS team_2 ,
                    fm.competition_tour  AS competition_tour,
                    fm.play_date , fm.play_time , 
                    fm.result_1 , fm.result_2 ,
                    fm.result_match
               FROM foot_football_club_matches fm 
          LEFT JOIN parser_common_tournament fc 
                 ON fm.competition_id = fc.id 
          LEFT JOIN parser_common_club  ft1 
                 ON fm.club_id_1 = ft1.id 
          LEFT JOIN parser_common_club  ft2 
                 ON fm.club_id_2 = ft2.id  
              WHERE fc.name = '{$value->com_comp}' 
                AND ft2.name = '{$value->f_team}' 
                AND fm.status = 1
           ORDER BY fm.play_date DESC ,
                    fm.play_time DESC
                LIMIT 0 , 5)
          ORDER BY competition_tour DESC 
                LIMIT 0 , 5 ";
   
      $results = $wpdb->get_results( $sql);
      $value->last_matches = $results;
  }
  
  $innerHTML = "<table class='turnirnaya-tablitsa'><tr><th style='width: 50px;'>№</th><th>Команда</th><th>И</th><th>В</th><th>Н</th><th>П</th><th>ЗГ</th><th>ПГ</th><th>О</th><th>Ф</th></tr>";  
  $i =1  ;   
  foreach ($result as $item_team) {
    $stan_pos = '';
    if(!empty($item_team->old_position) AND $item_team->old_position !== $item_team->position ){
      if($item_team->position > $item_team->old_position){
        $stan_pos = '<img src="'.get_stylesheet_directory_uri().'/images/delta_min.gif"  title="Предыдущее место: '.$item_team->old_position.'" alt="Предыдущее место: '.$item_team->old_position.'" class="stan_pos" >';
      }else{
        
        $stan_pos = '<img src="'.get_stylesheet_directory_uri().'/images/delta_plus.gif"  title="Предыдущее место: '.$item_team->old_position.'" alt="Предыдущее место: '.$item_team->old_position.'" class="stan_pos">';
      }
    }
    $las_matches = '';
    foreach ($item_team->last_matches as $key => $value) {
      $date = date_create($value->play_date);
      if($item_team->f_team == $value->team_1){
        switch ($value->result_match) {
          case 1:
            $las_matches .= '<span class="_win" title="'.date_format($date, 'd.m.Y').' '.$value->competition_tour.'-й тур '.$value->team_1.' - '.$value->team_2.' ('.$value->result_1.':'.$value->result_2.')"></span>';
            break;
          case 2:
            $las_matches .= '<span class="_lose" title="'.date_format($date, 'd.m.Y').' '.$value->competition_tour.'-й тур '.$value->team_1.' - '.$value->team_2.' ('.$value->result_1.':'.$value->result_2.')"></span>';
            break;
          case 3:
            $las_matches .= '<span class="_draws" title="'.date_format($date, 'd.m.Y').' '.$value->competition_tour.'-й тур '.$value->team_1.' - '.$value->team_2.' ('.$value->result_1.':'.$value->result_2.')"></span>';
            break;
        }
      }elseif($item_team->f_team == $value->team_2){
        switch ($value->result_match) {
          case 1:
            $las_matches .= '<span class="_lose" title="'.date_format($date, 'd.m.Y').' '.$value->competition_tour.'-й тур '.$value->team_1.' - '.$value->team_2.' ('.$value->result_1.':'.$value->result_2.')"></span>';
            break;
          case 2:
            $las_matches .= '<span class="_win" title="'.date_format($date, 'd.m.Y').' '.$value->competition_tour.'-й тур '.$value->team_1.' - '.$value->team_2.' ('.$value->result_1.':'.$value->result_2.')"></span>';
            break;
          case 3:
            $las_matches .= '<span class="_draws" title="'.date_format($date, 'd.m.Y').' '.$value->competition_tour.'-й тур '.$value->team_1.' - '.$value->team_2.' ('.$value->result_1.':'.$value->result_2.')"></span>';
            break;
        }
      }
    }
   $innerHTML .= '<tr>
              <td class="position">'.$i++.''.$stan_pos.'</td>
              <td> <img src="'.$dir_img_club.''.$item_team->logo_link.'" alt="'.$item_team->f_team.'" class="logo_club">'.$item_team->f_team.'</td>
              <td>'.$item_team->quantity_games.'</td>
              <td>'.$item_team->winnings.'</td>
              <td>'.$item_team->draws.'</td>
              <td>'.$item_team->losing.'</td>
              <td>'.$item_team->scored_goals.'</td>
              <td>'.$item_team->missed_goals.'</td>
              <td>'.$item_team->points.'</td>
              <td class="last_matches">'.$las_matches.'</td>
          </tr>';
   // print_r($item_team);
  }
  $innerHTML .="</table>";
  $innerHTML .= "<div class='opis_table'><b>И</b> - игры, <b>В</b> - выигрыши, <b>Н</b> - ничья, <b>П</b> - поражения, <b>ЗГ/з</b> - забитые голы, <b>ПГ</b> - пропущенные голы, <b>О</b> - очки, <b>Ф</b> - форма</div>";

  return $innerHTML;
}
add_shortcode('show_tournament_table_club', 'show_tournament_table_club');

/*Трансферы*/
function show_transfers_club($atts)
{
  global $wpdb;
  global $dir_img_club;
  extract(shortcode_atts(array(
        'slug' => "prev",
        'limit' => 5,
  ), $atts));

  if($slug == 'full'){
    $innerHTML = show_transfers_club_full();
  }else{
   // $innerHTML = show_transfers_club_prev();
  }
 

  return $innerHTML;
}
add_shortcode('show_transfers_club', 'show_transfers_club');
function show_transfers_club_full()
{
  global $post;
  $name_club = get_the_title($post->post_parent);

  global $wpdb;
  global $ff_cfg;
  $sql= "   SELECT com_season.name , alias_season.alias
            FROM {$ff_cfg['db_table']['common_season']} com_season
       LEFT JOIN {$ff_cfg['db_table']['common_alias_season']} alias_season
              ON com_season.id = alias_season.season_id
           WHERE com_season.type_season = 'transfer' 
        ORDER BY alias_season.id DESC";

  $seasons = $wpdb->get_results( $sql);

  $transfers = get_transfers_all();
  
  $innerHTML .='<div class="item_transfers">';
    $innerHTML .='<h2 class="title_transfer_block">Пришли</h2>';
    $innerHTML .= '<div class="title_transfer_table">Сезон:
                      <select name="season_tr" id="season_tr_in" data-club_name="'.$name_club.'" data-type_transfer = "in">';
                        foreach ($seasons as $key => $value) {
                          if($value->name == $ff_cfg['carent_season_transfer'] ){
                            $innerHTML .= '<option selected value="'.$value->name.'">'.$value->alias.' </option>';
                          }else{
                            $innerHTML .= '<option value="'.$value->name.'">'.$value->alias.' </option>';
                          }
                          
                        }
    $innerHTML .= '   </select>
                   </div>';
    $innerHTML .= "<table class='footable table_schedule_of_matches_club ff_transfers_in'>";
      $innerHTML .= "<thead>
                            <tr>
                                <th style='display: none;'></th>
                                <th class='competition'>Имя</th>
                                <th data-hide='phone,tablet'>Дата перехода</th>
                                <th data-hide='phone,tablet'>Пришел из</th>
                                <th data-hide='phone,tablet'>Тип трансфера</th>
                                <th data-hide='phone,tablet'>Сумма</th>
                            </tr>  
                    </thead>";
            $innerHTML .= '<tbody class="sdf">'  ;
            $innerHTML .= show_transfers_club_full_render($transfers , 'in');  
            $innerHTML .= '</tbody>'  ;
     
    $innerHTML .= "</table>";
  $innerHTML .= '</div>';
  $innerHTML .='<div class="item_transfers">';
    $innerHTML .='<h2 class="title_transfer_block">Ушли</h2>';
    $innerHTML .= '<div class="title_transfer_table">Сезон:
                      <select name="season_tr" id="season_tr_out" data-club_name="'.$name_club.'" data-type_transfer = "out">';
                        foreach ($seasons as $key => $value) {
                          if($value->name == $ff_cfg['carent_season_transfer'] ){
                            $innerHTML .= '<option selected value="'.$value->name.'">'.$value->alias.' </option>';
                          }else{
                            $innerHTML .= '<option value="'.$value->name.'">'.$value->alias.' </option>';
                          }
                        }
    $innerHTML .= '   </select>
                   </div>';
    $innerHTML .= "<table class='footable table_schedule_of_matches_club ff_transfers_out'>";
      $innerHTML .= "<thead>
                            <tr>
                                <th style='display: none;'></th>
                                <th class='competition'>Имя</th>
                                <th data-hide='phone,tablet'>Дата перехода</th>
                                <th data-hide='phone,tablet'>Ущел в</th>
                                <th data-hide='phone,tablet'>Тип трансфера</th>
                                <th data-hide='phone,tablet'>Сумма</th>
                            </tr>  
                    </thead>";    
             $innerHTML .= show_transfers_club_full_render($transfers , 'out');
    $innerHTML .= "</table>";
  $innerHTML .= '</div>';
  return $innerHTML;
}
function get_transfers_all()
{
  global $post;
  $name_club = get_the_title($post->post_parent);

  global $wpdb;
  global $ff_cfg;

  $sql= "   SELECT *
            FROM {$ff_cfg['db_table']['football_transfers_club']} club_tr
            WHERE club_tr.club_name = '{$name_club}' 
              AND club_tr.season = '{$ff_cfg['carent_season_transfer'] }'
         ORDER BY club_tr.date ASC";

  $result = $wpdb->get_results( $sql);

  return $result;
}
function show_transfers_club_full_render($transfers , $type_transfer){
  if(count($transfers) > 1){
    foreach ($transfers as $key => $value) {
        $cost = '';
        $date = date_create($value->date);
        if($value->cost !== '0'){
          $cost = '€ '.$value->cost.' млн.';
        }
        
        if($value->type == $type_transfer && $type_transfer == 'in' ){
          $innerHTML .= '<tr>
                              <td style="display: none;"></td>
                              <td>'.$value->player_name.'</td>
                              <td>'.date_format($date, 'd.m.Y').'</td>
                              <td>'.$value->team_from_name.'</td>
                              <td>'.$value->type_transfer.'</td>
                              <td>'.$cost.'</td>
                          </tr>';     
        }elseif($value->type == $type_transfer && $type_transfer == 'out' ){
          $innerHTML .= '<tr>
                          <td style="display: none;"></td>
                          <td>'.$value->player_name.'</td>
                          <td>'.date_format($date, 'd.m.Y').'</td>
                          <td>'.$value->team_to_name.'</td>
                          <td>'.$value->type_transfer.'</td>
                          <td>'.$cost.'</td>
                      </tr>';     
        }
    } 
  }else{
    $innerHTML .= '<tr>
                    <td style="display: none;"></td>
                    <td colspan="5"><div class="no_date">Нет данных</div></td>
                  </tr>';
  }
  

  return $innerHTML;
}
/***Краткая карточка***/
function show_statistic_short_club() {
  wp_reset_query();
  $name_club = get_the_title();

  
  $innerHTML .= get_tournament_table_club($name_club);
  $innerHTML .= '<h2 class="block_title">Состав клуба</h2>';
  $innerHTML .= get_sostav_club($name_club);
  $innerHTML .= '<h2 class="block_title">Расписания матчей</h2>';
  $innerHTML .= get_schedule__matches_club($name_club);
  $innerHTML .= '<h2 class="block_title">Результаты матчей</h2>';
  $innerHTML .= get_result_matches_club($name_club);
  
  
  
  return $innerHTML;
}
add_shortcode('show_statistic_short_club', 'show_statistic_short_club');
//Результати матчей
function get_result_matches_club($name_club)
{
  global $wpdb;
  global $ff_cfg;
  $sql= "   SELECT c_matches.play_date ,  c_matches.play_time, c_matches.competition_tour_txt,
                   com_comp.name AS competition  ,  
                   t_1.name AS team_1 ,  
                   t_1.name_post AS team_1_short ,  
                   t_2.name AS team_2 , 
                   t_2.name_post AS team_2_short , 
                   t_1.logo_link AS logo_t1 , 
                   t_2.logo_link AS logo_t2 ,
                   c_matches.result_match, c_matches.result_1 , c_matches.result_2
              FROM {$ff_cfg['db_table']['football_club_matches']} c_matches 
              JOIN {$ff_cfg['db_table']['common_tournament']} com_comp
                ON c_matches.competition_id = com_comp.id 
              JOIN {$ff_cfg['db_table']['common_club']} t_1 
                ON c_matches.club_id_1 = t_1.id  
              JOIN {$ff_cfg['db_table']['common_club']} t_2 
                ON c_matches.club_id_2 = t_2.id   
             WHERE t_1.name_post = '{$name_club}'
               AND c_matches.status = 1
               AND c_matches.season_id = {$ff_cfg['carent_season']}
                OR t_2.name_post = '{$name_club}' 
               AND c_matches.status = 1   
               AND c_matches.season_id = {$ff_cfg['carent_season']}
          ORDER BY c_matches.play_date DESC  ";
  $result = $wpdb->get_results( $sql);

  $innerHTML = '<div class="item_short_stat_club"><table class="footable table_schedule_of_matches_club" data-page="false">';
  $innerHTML .= "<thead>
                        <tr>
                            <th style='display: none;'></th>
                            <th data-hide='phone,tablet' >Турнир</th>
                            <th data-hide='phone,tablet' >Тур</th>
                            <th >Хозяева</th>
                            <th >Счет</th>
                            <th >Гости</th>
                            <th >Дата игры</th>
                        </tr>
                </thead>";            
  foreach ($result as $key => $item_match) {
    if ($item_match->team_1 ==  $name_club ) {
        $football_team_1 = '<strong>'.$item_match->team_1.'</strong>';
    }else{
       $football_team_1 = $item_match->team_1;
    }

    if ($item_match->team_2 ==  $name_club) {
        $football_team_2 = '<strong>'.$item_match->team_2.'</strong>';
    }else{
       $football_team_2 = $item_match->team_2;
    }

    $date = date_create($item_match->play_date);
    if ($key < 5) {
      $innerHTML .=  "<tr>
                        <td style='display: none;'></td>
                        <td>".$item_match->competition."</td>
                        <td>".$item_match->competition_tour_txt."</td>
                        <td class='ft_1'>".ff_trim_text($football_team_1 , 12)."</td>
                        <td><strong>".$item_match->result_1." : ".$item_match->result_2."</strong></td>
                        <td class='ft_2'>".ff_trim_text($football_team_2 , 12)."</td>
                        <td>".date_format($date, 'd.m.Y')."</td>
                    </tr>";
    }else{
      $innerHTML .=  "<tr class='more_info' style='display: none;'>
                        <td  class='hide' style='display: none;'></td>
                        <td>".$item_match->competition."</td>
                        <td>".$item_match->competition_tour_txt."</td>
                        <td class='ft_1'>".$football_team_1."</td>
                        <td><strong>".$item_match->result_1." : ".$item_match->result_2."</strong></td>
                        <td class='ft_2'>".$football_team_2."</td>
                        <td>".date_format($date, 'd.m.Y')."</td>
                    </tr>";
    }
    
  }
  $innerHTML .= "</table>";
  $innerHTML .='<a href="#" class="show_more_s">Развернуть...</a></div>';
  return $innerHTML;
}
// Расписания матчей
function get_schedule__matches_club ($name_club) {
  global $wpdb;
  global $ff_cfg;
  $sql= "   SELECT cm.play_date ,  cm.play_time, cm.competition_tour_txt,
                   com_turnament.name AS competition  ,  
                   t_1.name AS team_1 ,  
                   t_1.name_post AS team_1_short ,  
                   t_2.name AS team_2 , 
                   t_2.name_post AS team_2_short , 
                   t_1.logo_link AS logo_t1 , 
                   t_2.logo_link AS logo_t2 
              FROM {$ff_cfg['db_table']['football_club_matches']}   cm 
              JOIN {$ff_cfg['db_table']['common_tournament']} com_turnament 
                ON cm.competition_id = com_turnament.id 
              JOIN {$ff_cfg['db_table']['common_club']} t_1 
                ON cm.club_id_1 = t_1.id  
              JOIN {$ff_cfg['db_table']['common_club']} t_2 
                ON cm.club_id_2 = t_2.id   
              WHERE t_1.name = '{$name_club}'   
                AND cm.status = 0
                 OR t_1.name_post = '{$name_club}'
                AND cm.status = 0
                 OR t_2.name = '{$name_club}' 
                AND cm.status = 0
                 OR t_2.name_post = '{$name_club}' 
                AND cm.status = 0
            ORDER BY cm.play_date ASC ,
                     cm.competition_tour ASC  {$lim} ";
  $result = $wpdb->get_results( $sql);
  $innerHTML = '<div class="item_short_stat_club"><table class="footable table_schedule_of_matches_club" data-page="false">';
  $innerHTML .= "<thead>
                        <tr>
                            <th style='display: none;'></th>
                            <th data-hide='phone,tablet' class='competition'>Турнир</th>
                            <th data-hide='phone,tablet'></th>
                            <th >Дата игры</th>
                            <th >Хозяева</th>
                            <th></th>
                            <th >Гости</th>
                        </tr>  
                </thead>";          
  foreach ($result as $key => $item_match) {
    if ($item_match->team_1 ==  $name_club ) {
        $football_team_1 = '<strong>'.$item_match->team_1.'</strong>';
    }else{
       $football_team_1 = $item_match->team_1;
    }

    if ($item_match->team_2 ==  $name_club) {
        $football_team_2 = '<strong>'.$item_match->team_2.'</strong>';
    }else{
       $football_team_2 = $item_match->team_2;
    }


    $date = date_create($item_match->play_date);
    if($key < 5){
      $innerHTML .=  "<tr>
                        <td style='display: none;'></td>
                        <td>".$item_match->competition."</td>
                        <td>".$item_match->competition_tour_txt."</td>
                        <td>".date_format($date, 'd.m.')."".$item_match->play_time."</td>
                        <td class='ft_1'>".ff_trim_text($football_team_1 , 12)."</td>
                        <td><strong>-:-</strong></td>
                        <td class='ft_2'>".ff_trim_text($football_team_2 , 12)."</td>
                    </tr>";
    }else{
      $innerHTML .=  "<tr class='more_info' style='display: none;'>
                        <td class='hide' style='display: none;'></td>
                        <td>".$item_match->competition."</td>
                        <td>".$item_match->competition_tour_txt."</td>
                        <td>".date_format($date, 'd.m.')."".$item_match->play_time."</td>
                        <td class='ft_1'>".$football_team_1."</td>
                        <td><strong>-:-</strong></td>
                        <td class='ft_2'>".$football_team_2."</td>
                    </tr>";
    }
    
  }
  $innerHTML .= "</table>";
   $innerHTML .='<a href="#" class="show_more_s">Развернуть...</a></div>';
  return $innerHTML;
}
// Состав
function get_sostav_club($name_club) {
  global $wpdb;
  global $ff_cfg; 
  $sql = "SELECT com_player.short_name AS player_name , 
                 TIMESTAMPDIFF(YEAR, com_player.birthday ,curdate()) as Age , 
                 com_player.player_photo  AS foto ,
                 p_to_club.growth, p_to_club.weight , p_to_club.player_number ,  
                 com_amp.name AS amplua , 
                 com_county.name AS national_team 
            FROM {$ff_cfg['db_table']['football_player_to_club']}  p_to_club  
       LEFT JOIN {$ff_cfg['db_table']['common_player']} com_player 
              ON p_to_club.player_id = com_player.id   
       LEFT JOIN {$ff_cfg['db_table']['common_club']} com_club 
              ON p_to_club.club_id = com_club.id   
       LEFT JOIN {$ff_cfg['db_table']['common_amplua']} com_amp 
              ON p_to_club.amplua_id = com_amp.id  
            JOIN {$ff_cfg['db_table']['common_country']} com_county 
              ON p_to_club.country_id = com_county.id 
           WHERE com_club.name_post = '{$name_club}'
             AND p_to_club.player_status = 1  ";
  $result = $wpdb->get_results( $sql);

  $result_array = json_decode(json_encode($result), True);
  $result_array = SuperUnique($result_array , 'player_name');
  $result = json_decode(json_encode($result_array), FALSE);

  $innerHTML = '<div class="item_short_stat_club"><table class="footable table_sostav" data-page="false">';
  $innerHTML .= "<thead>
                          <tr>
                            <th style='display: none;'></th>
                            <th>Фото</th>
                            <th>Игрок</th>
                            <th data-hide='phone,tablet'>Номер</th>
                            <th data-hide='phone,tablet'>Сборная</th>
                            <th data-hide='phone,tablet'>Возраст</th>
                            <th data-hide='phone,tablet'>Рост</th>
                            <th data-hide='phone,tablet'>Вес</th>
                            <th data-hide='phone,tablet'>Амплуа</th>
                          </tr>
                  </thead>";
  foreach ($result as $key => $item_sostav) {
    if (empty($item_sostav->growth)) {
       $growth = '';
    }else{
       $growth = $item_sostav->growth.' см';
    }
    if (empty($item_sostav->weight)) {
       $weight = '';
    }else{
       $weight = $item_sostav->weight.' кг';
    }

    if($key < 5){
       $innerHTML .= '<tr>
                      <td style="display: none;""></td>
                      <td class="img_player"><img src="'.$item_sostav->foto.'" alt="" class="img_sostav"></td>
                      <td>'.$item_sostav->player_name.'</td>
                      <td class="number_club">'.$item_sostav->player_number.'</td>
                      <td class="national_team"><i class="flag_nt flag_'.translit($item_sostav->national_team).'" title="'.$item_sostav->national_team.'" alt="'.$item_match->national_team.'"></i>'.$item_sostav->national_team.'</td>
                      <td class="age">'.$item_sostav->Age.'</td>
                      <td class="growth">'.$growth.'</td>
                      <td class="weight">'.$weight.'</td>
                      <td class="amplua">'.$item_sostav->amplua.'</td>
                  </tr>';
    }else{
       $innerHTML .= '<tr class="more_info" style="display: none;">
                      <td class="hide" style="display: none;""></td>
                      <td class="img_player"><img src="'.$item_sostav->foto.'" alt="" class="img_sostav"></td>
                      <td>'.$item_sostav->player_name.'</td>
                      <td class="number_club">'.$item_sostav->player_number.'</td>
                      <td class="national_team"><i class="flag_nt flag_'.translit($item_sostav->national_team).'" title="'.$item_sostav->national_team.'" alt="'.$item_match->national_team.'"></i>'.$item_sostav->national_team.'</td>
                      <td class="age">'.$item_sostav->Age.'</td>
                      <td class="growth">'.$growth.'</td>
                      <td class="weight">'.$weight.'</td>
                      <td class="amplua">'.$item_sostav->amplua.'</td>
                  </tr>';
    }
  
  }
  
  $innerHTML .= "</table>";
   $innerHTML .='<a href="#" class="show_more_s">Развернуть...</a></div>';
  return $innerHTML;
}
/*Турнирная таблица*/
function get_tournament_table_club($name_club)
{
  global $wpdb;
  global $dir_img_club;
  global $ff_cfg;
  $sql  = "SELECT fc.name
             FROM {$ff_cfg['db_table']['football_competition_term_relationships']}  com_tr
             JOIN {$ff_cfg['db_table']['common_tournament']} fc 
               ON com_tr.competition_id = fc.id 
             JOIN {$ff_cfg['db_table']['football_club_to_competition']}  club_to_competition 
               ON club_to_competition.competition_id = fc.id
             JOIN {$ff_cfg['db_table']['common_club']}  com_club 
               ON club_to_competition.club_id = com_club.id
            WHERE com_club.name_post = '{$name_club}' 
              AND com_tr.competition_term_id = 2 
         ORDER BY club_to_competition.id ASC    
             LIMIT 0 , 1";
  $result = $wpdb->get_results( $sql);

  $name_liga = $result[0]->name;
  $name_liga = esc_sql($name_liga);

 

  $sql  = "SELECT fc.name AS com_comp ,
                  com_club.name AS f_team, com_club.logo_link ,  
                  foot_tt.position , foot_tt.old_position , 
                  foot_tt.games AS quantity_games , foot_tt.wins AS winnings , foot_tt.draws AS draws  , foot_tt.loses AS losing , 
                  foot_tt.scored_goals , foot_tt.missed_goals , foot_tt.points 
             FROM {$ff_cfg['db_table']['football_club_turnirnaya_tablitsa']}  foot_tt  
             JOIN {$ff_cfg['db_table']['common_tournament']} fc 
               ON foot_tt.competition_id = fc.id 
             JOIN {$ff_cfg['db_table']['common_club']}  com_club 
               ON foot_tt.club_id = com_club.id
            WHERE fc.name = '{$name_liga}' 
               OR fc.name_post = '{$name_liga}'  
         ORDER BY foot_tt.position ASC  {$lim}";
  $result = $wpdb->get_results( $sql);
  foreach ($result as $key => $value) {
    
    $sql= "(SELECT  ft1.name AS team_1 ,
                    ft2.name AS team_2 ,
                    fm.competition_tour  AS competition_tour,
                    fm.play_date , fm.play_time , 
                    fm.result_1 , fm.result_2 ,
                    fm.result_match
               FROM {$ff_cfg['db_table']['football_club_matches']} fm 
          LEFT JOIN {$ff_cfg['db_table']['common_tournament']} fc 
                 ON fm.competition_id = fc.id 
          LEFT JOIN {$ff_cfg['db_table']['common_club']}  ft1 
                 ON fm.club_id_1 = ft1.id 
          LEFT JOIN {$ff_cfg['db_table']['common_club']}  ft2 
                 ON fm.club_id_2 = ft2.id  
              WHERE fc.name = '{$value->com_comp}' 
                AND ft1.name = '{$value->f_team}' 
                AND fm.status = 1
           ORDER BY fm.play_date DESC ,
                    fm.play_time DESC
                LIMIT 0 , 5) 
           UNION 
          (SELECT   ft1.name AS team_1 ,
                    ft2.name AS team_2 ,
                    fm.competition_tour  AS competition_tour,
                    fm.play_date , fm.play_time , 
                    fm.result_1 , fm.result_2 ,
                    fm.result_match
               FROM {$ff_cfg['db_table']['football_club_matches']} fm 
          LEFT JOIN {$ff_cfg['db_table']['common_tournament']} fc 
                 ON fm.competition_id = fc.id 
          LEFT JOIN {$ff_cfg['db_table']['common_club']}  ft1 
                 ON fm.club_id_1 = ft1.id 
          LEFT JOIN {$ff_cfg['db_table']['common_club']}  ft2 
                 ON fm.club_id_2 = ft2.id  
              WHERE fc.name = '{$value->com_comp}' 
                AND ft2.name = '{$value->f_team}' 
                AND fm.status = 1
           ORDER BY fm.play_date DESC ,
                    fm.play_time DESC
                LIMIT 0 , 5)
          ORDER BY competition_tour DESC 
                LIMIT 0 , 5 ";
   
      $results = $wpdb->get_results( $sql);
      $value->last_matches = $results;
  }
  if ($result) {
    $innerHTML .= '<h2 class="block_title">Турнирная таблица</h2>';
    $innerHTML .= '<div class="item_short_stat_club"><table class="turnirnaya-tablitsa footable" data-page="false">';  
    $innerHTML .= '<thead>
                          <tr>
                            <th style="display: none;""></th>
                            <th data-hide="phone,tablet" style="width: 50px;">№</th>
                            <th>Команда</th>
                            <th data-hide="phone,tablet">И</th>
                            <th data-hide="phone,tablet">В</th>
                            <th data-hide="phone,tablet">Н</th>
                            <th data-hide="phone,tablet">П</th>
                            <th data-hide="phone,tablet">ЗГ</th>
                            <th data-hide="phone,tablet">ПГ</th>
                            <th data-hide="phone,tablet">О</th>
                            <th data-hide="phone,tablet">Ф</th>
                          </tr>
                  </thead>';
  $i =1  ;   
  foreach ($result as $key_main =>$item_team) {
    $stan_pos = '';
    if(!empty($item_team->old_position) AND $item_team->old_position !== $item_team->position ){
      if($item_team->position > $item_team->old_position){
        $stan_pos = '<img src="'.get_stylesheet_directory_uri().'/images/delta_min.gif"  title="Предыдущее место: '.$item_team->old_position.'" alt="Предыдущее место: '.$item_team->old_position.'" class="stan_pos" >';
      }else{
        
        $stan_pos = '<img src="'.get_stylesheet_directory_uri().'/images/delta_plus.gif"  title="Предыдущее место: '.$item_team->old_position.'" alt="Предыдущее место: '.$item_team->old_position.'" class="stan_pos">';
      }
    }
    $las_matches = '';
    foreach ($item_team->last_matches as $key => $value) {
      $date = date_create($value->play_date);
      if($item_team->f_team == $value->team_1){
        switch ($value->result_match) {
          case 1:
            $las_matches .= '<span class="lm_result _win" title="'.date_format($date, 'd.m.Y').' '.$value->competition_tour.'-й тур '.$value->team_1.' - '.$value->team_2.' ('.$value->result_1.':'.$value->result_2.')"></span>';
            break;
          case 2:
            $las_matches .= '<span class="lm_result _lose" title="'.date_format($date, 'd.m.Y').' '.$value->competition_tour.'-й тур '.$value->team_1.' - '.$value->team_2.' ('.$value->result_1.':'.$value->result_2.')"></span>';
            break;
          case 3:
            $las_matches .= '<span class="lm_result _draws" title="'.date_format($date, 'd.m.Y').' '.$value->competition_tour.'-й тур '.$value->team_1.' - '.$value->team_2.' ('.$value->result_1.':'.$value->result_2.')"></span>';
            break;
        }
      }elseif($item_team->f_team == $value->team_2){
        switch ($value->result_match) {
          case 1:
            $las_matches .= '<span class="lm_result _lose" title="'.date_format($date, 'd.m.Y').' '.$value->competition_tour.'-й тур '.$value->team_1.' - '.$value->team_2.' ('.$value->result_1.':'.$value->result_2.')"></span>';
            break;
          case 2:
            $las_matches .= '<span class="lm_result _win" title="'.date_format($date, 'd.m.Y').' '.$value->competition_tour.'-й тур '.$value->team_1.' - '.$value->team_2.' ('.$value->result_1.':'.$value->result_2.')"></span>';
            break;
          case 3:
            $las_matches .= '<span class="lm_result _draws" title="'.date_format($date, 'd.m.Y').' '.$value->competition_tour.'-й тур '.$value->team_1.' - '.$value->team_2.' ('.$value->result_1.':'.$value->result_2.')"></span>';
            break;
        }
      }
    }

    if ($key_main < 5) {
      $innerHTML .= '<tr>
              <td style="display: none;""></td>
              <td class="position">'.$i++.''.$stan_pos.'</td>
              <td> <img src="'.$dir_img_club.''.$item_team->logo_link.'" alt="'.$item_team->f_team.'" class="logo_club">'.$item_team->f_team.'</td>
              <td>'.$item_team->quantity_games.'</td>
              <td>'.$item_team->winnings.'</td>
              <td>'.$item_team->draws.'</td>
              <td>'.$item_team->losing.'</td>
              <td>'.$item_team->scored_goals.'</td>
              <td>'.$item_team->missed_goals.'</td>
              <td>'.$item_team->points.'</td>
              <td class="last_matches">'.$las_matches.'</td>
          </tr>';
    }else{
      $innerHTML .= '<tr class="more_info" style="display: none;">
              <td class="hide" style="display: none;""></td>
              <td class="position">'.$i++.''.$stan_pos.'</td>
              <td> <img src="'.$dir_img_club.''.$item_team->logo_link.'" alt="'.$item_team->f_team.'" class="logo_club">'.$item_team->f_team.'</td>
              <td>'.$item_team->quantity_games.'</td>
              <td>'.$item_team->winnings.'</td>
              <td>'.$item_team->draws.'</td>
              <td>'.$item_team->losing.'</td>
              <td>'.$item_team->scored_goals.'</td>
              <td>'.$item_team->missed_goals.'</td>
              <td>'.$item_team->points.'</td>
              <td class="last_matches">'.$las_matches.'</td>
          </tr>';
    }
   
   // print_r($item_team);
  }
  $innerHTML .= "</table>"; 
  $innerHTML .= "<div class='opis_table'><b>И</b> - игры, <b>В</b> - выигрыши, <b>Н</b> - ничья, <b>П</b> - поражения, <b>ЗГ/з</b> - забитые голы, <b>ПГ</b> - пропущенные голы, <b>О</b> - очки, <b>Ф</b> - форма</div>";
  $innerHTML .='<a href="#" class="show_more_s">Развернуть...</a></div>';
  }
  
  
  return $innerHTML;
}