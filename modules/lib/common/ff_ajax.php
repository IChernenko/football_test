<?php 

wp_enqueue_style( 'flatpickr_css',  get_template_directory_uri() . '/modules/assets/flatpickr/flatpickr.min.css' , array() , null );
wp_enqueue_script( 'flatpickr_js', get_template_directory_uri() . '/modules/assets/flatpickr/flatpickr.min.js' , array('jquery'), null, true );

wp_enqueue_script( 'ff_app', get_template_directory_uri() . '/modules/js/ff_main.js' , false , false, true );
wp_localize_script( 'ff_app', 'ff_app', array( 
																							'ajaxurl' => admin_url( 'admin-ajax.php' ) , 
));



//Sorting
function ff_sorting_table_ajax() { 
  get_tbody_top_100_club();
  die();
}
add_action( 'wp_ajax_sorting_table', 'ff_sorting_table_ajax' ); 
add_action( 'wp_ajax_nopriv_sorting_table', 'ff_sorting_table_ajax' );


function ff_get_transfers_club_ajax() { 
	global $wpdb;
  global $ff_cfg;

  $sql= "   SELECT *
            FROM {$ff_cfg['db_table']['football_transfers_club']} club_tr
            WHERE club_tr.club_name = '{$_POST['club_name']}'
              AND club_tr.season = '{$_POST['season_t']}'
         ORDER BY club_tr.date ASC";

  $result = $wpdb->get_results( $sql);

  $innerHTML = show_transfers_club_full_render($result , $_POST['type_transfer']);
  echo $innerHTML;
  die();
}
add_action( 'wp_ajax_ff_get_transfers_club', 'ff_get_transfers_club_ajax' ); 
add_action( 'wp_ajax_nopriv_ff_get_transfers_club', 'ff_get_transfers_club_ajax' );

