<?php
class td_football_shortcode {

    public function ff_app_show_match_center() {
        $td_football_match_center = new td_football_match_center();

        $innerHTML = '
                <div class="td-container ff_match_center">
                    <div class="td-pb-row">
                        <div class="td-pb-span4"><a href="#" class="date_caL_link prev_date_cal">Назад</a></div>
                        <div class="td-pb-span4">
                            <div class="mc_filter_caledar" id="flatpickr_delivery_date">
                              <a class="input-button" title="toggle" data-toggle><i class="fa fa-calendar"></i></a>
                              <input class="form-control" id="delivery_date" name="delivery_date" data-input placeholder="Select.." >
                            </div>
                        </div>
                        <div class="td-pb-span4"><a href="#" class="date_caL_link next_date_cal">Вперед</a></div>
                    </div>
                    <div class="td-pb-row">
                        <div class="td-pb-span2_5 ff_match_center_list_comp">
                            '.dynamic_sidebar('mc_left').'
                        </div>
                        <div class="td-pb-span7 ff_match_center_main">
                            <div class="block-title ff_match_center_main_title">
                                <ul class="mc_tab_list">
                                    <li>
                                        <a href="#mc_tab_yesterday" data-date_carent="'.td_football_app::get_yesterday_date().'" class="date_show_match" >Вчера (<span>'.$td_football_match_center->get_count_matches_yesterday().'</span>)</a>
                                    </li>
                                    <li>
                                        <a href="#mc_tab_today" data-date_carent="'.td_football_app::get_today_date().'" class="date_show_match _selected" >Сегодня (<span>'.$td_football_match_center->get_count_matches_today().'</span>)</a>
                                    </li>
                                    <li>
                                        <a href="#mc_tab_tomorrow" data-date_carent="'.td_football_app::get_tomorrow_date().'" class="date_show_match" >Завтра (<span>'.$td_football_match_center->get_count_matches_tomorrow().'</span>)</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="football_matches schedule_football_matches">
                                '.$td_football_match_center->get_list_matches().'
                            </div>
                        </div>
                        <div class="td-pb-span2_5 ff_match_center_list_comp">
                            '.dynamic_sidebar( 'mc_right' ).'
                        </div>
                    </div>
                </div>
            ';
      return $innerHTML;
    }

    /**
    * Шорткод вывода таблицы общей информации
    * @var string
    */
    public function ff_app_show_main_info_bk(){
        global $post;
        wp_reset_query();
        $post_meta_date = get_metadata('post', $post->ID, '', true);
        $innerHTML = '<table class="ff_app_main_info_bk" id="table_main_khar">
                        <tr><td><strong>Официальное название:&nbsp;</strong></td><td>'.$post_meta_date["_bk_desc_official_name"][0].'</td></tr>
                        <tr><td><strong>Дата регистрации:</strong></td><td>'.$post_meta_date["_bk_desc_date_registration"][0].'</td></tr>
                        <tr><td><strong>Работает в:&nbsp;</strong></td><td>'.$post_meta_date["_bk_desc_works_in"][0].'</td></tr>
                        <tr><td><strong>Лицензия:</strong></td><td>'.$post_meta_date["_bk_desc_license"][0].'</td></tr>
                        <tr><td><strong>Бонус:</strong></td><td>'.$post_meta_date["_bk_desc_bonus"][0].' '.$post_meta_date["_bk_desc_bonus_currency"][0].'</td></tr>
                        <tr><td><strong>Минимальная ставка:&nbsp;</strong></td><td>'.$post_meta_date["_bk_desc_min_rate"][0].'</td></tr>
                        <tr><td><strong>Налог на выигрыш в России:</strong></td><td>'.$post_meta_date["_bk_desc_tax_win_russia"][0].'</td></tr>
                        <tr><td><strong>Отзывы игроков:</strong></td><td>'.$post_meta_date["_bk_desc_player_reviews"][0].'</td></tr>
                        <tr >
                            <td><strong>Общий рейтинг:</strong></td>
                            <td class="bk_desc_overall_rating">
                                <span class="stars">'.get_stars($post_meta_date["bk_rating_common_all_raiting"][0]).'</span>
                                <span class="count_rating">'.$post_meta_date["bk_rating_common_all_raiting"][0].'/5</span>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Официальный сайт:</strong></td>
                            <td><a href="'.$post_meta_date["_bk_desc_official_site"][0].'" target="_blank">Посетить сайт</a></td>
                        </tr>
                        <tr><td><strong>Скачать приложение:</strong></td><td>'.$post_meta_date["_bk_desc_download_application"][0].'</td></tr>
                     </table>';


        return $innerHTML;
    }

    /**
        * Шорткод вывода таблицы рейтинга (Букмекеры)
        * Коэффициенты
        * Ставки на футбол
        * Линия
        * Live-ставки
        * Бонусы
        * Сайт
        * Приложение
        * Ввод/вывод средств
        * Общий показатель
        * @var string
    */
    public function ff_app_show_average_rating_bk(){
        global $post;
        wp_reset_query();
        $post_meta_date = get_metadata('post', $post->ID, '', true);

        $count_rate_all = $post_meta_date["bk_rating_common_all_raiting"][0];
        $count_rating_coefficients = $post_meta_date["bk_rating_common_coefficients"][0];
        $rating_football_betting = $post_meta_date["bk_rating_common_football_betting"][0];
        $rating_line = $post_meta_date["bk_rating_common_line"][0];
        $rating_live_betting = $post_meta_date["bk_rating_common_live_betting"][0];
        $rating_offers = $post_meta_date["bk_rating_common_offers"][0];
        $rating_site = $post_meta_date["bk_rating_common_site"][0];
        $rating_app = $post_meta_date["bk_rating_common_app"][0];
        $rating_finance = $post_meta_date["bk_rating_common_finance"][0];

        $innerHTML  =   '<div class="show_average_rating_bk" >
                            <div class="td-pb-row">
                                <div class="td-pb-span6 item_rating_reviews">
                                    <span>Коэффициенты</span>
                                    <div class="result_rating"><span>'.$count_rating_coefficients.'</span>/5</div>
                                    <div class="stars">'.get_stars($count_rating_coefficients).'</div>
                                </div>
                                <div class="td-pb-span6 item_rating_reviews">
                                    <span>Ставки на футбол</span>
                                    <div class="result_rating"><span>'.$rating_football_betting.'</span>/5</div>
                                    <div class="stars">'.get_stars($rating_football_betting).'</div>
                                </div>
                                <div class="td-pb-span6 item_rating_reviews">
                                    <span>Линия</span>
                                    <div class="result_rating"><span>'.$rating_line.'</span>/5</div>
                                    <div class="stars">'.get_stars($rating_line).'</div>
                                </div>
                                <div class="td-pb-span6 item_rating_reviews">
                                    <span>Live-ставки </span>
                                    <div class="result_rating"><span>'.$rating_live_betting.'</span>/5</div>
                                    <div class="stars">'.get_stars($rating_live_betting).'</div>
                                </div>
                                <div class="td-pb-span6 item_rating_reviews">
                                    <span>Бонусы</span>
                                    <div class="result_rating"><span>'.$rating_offers.'</span>/5</div>
                                    <div class="stars">'.get_stars($rating_offers).'</div>
                                </div>
                                <div class="td-pb-span6 item_rating_reviews">
                                    <span>Сайт</span>
                                    <div class="result_rating"><span>'.$rating_site.'</span>/5</div>
                                    <div class="stars">'.get_stars($rating_site).'</div>
                                </div>
                                <div class="td-pb-span6 item_rating_reviews">
                                    <span>Приложение</span>
                                    <div class="result_rating"><span>'.$rating_app.'</span>/5</div>
                                    <div class="stars">'.get_stars($rating_app).'</div>
                                </div>
                                <div class="td-pb-span6 item_rating_reviews">
                                    <span>Ввод/вывод средств</span>
                                    <div class="result_rating"><span>'.$rating_finance.'</span>/5</div>
                                    <div class="stars">'.get_stars($rating_finance).'</div>
                                </div>
                                <div class="td-pb-span12">
                                    <div class="hr_row"></div>
                                </div>
                                <div class="td-pb-span6 item_rating_reviews">
                                    <span><strong>Общий рейтинг</strong></span>
                                    <div class="result_rating"><span>'.$count_rate_all.'</span>/5</div>
                                    <div class="stars">'.get_stars($count_rate_all).'</div>
                                </div>
                            </div>
                        </div>';

        return $innerHTML;
    }
    public function customBoxContent($atts, $shortcode_content = null )
    {
      $params = shortcode_atts( array(
    		'title' => 'Редакция рекомендует'
    	), $atts );

      $innerHTML = '<div class="ff_custom_block"><div class="name">редакция рекомендует</div><div class="staff-recommends-content">
                    <a href="http://www.forbes.ru/biznes/362629-sposob-iz-devyanostyh-kak-zastavit-benzin-podeshevet">Способ из девяностых. Как заставить бензин подешеветь</a>
                    <a href="http://forbes.ru/biznes/357191-polyhnulo-pochemu-rost-cen-na-benzin-operezhaet-inflyaciyu">Полыхнуло. Почему рост цен на бензин опережает инфляцию</a>
                    <a href="http://forbes.ru/finansy-i-investicii/362673-bank-rossii-rasskazal-kak-ostanovit-rost-cen-na-benzin?_ga=2.213824852.1914125274.1529582064-1205846002.1529582064">Банк России рассказал, почему остановился рост цен на бензин</a>
                    </div></div>';
    }


}
