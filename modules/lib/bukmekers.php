<?php 

function hnatchuk_get_post_basket() { 
  wp_reset_query();
  global $wp_query; 
  $args = array(
                'post_type'  => 'bukmeker',
                'post__in' => $_POST['ids'],   
                'posts_per_page' => -1,
                 );
  $wp_query = new WP_Query($args); 
  while ( have_posts() ) : the_post(); 
     get_template_part( 'modules/bk/content', 'basket-compare-list'); 
  endwhile; 
  die();
}
add_action( 'wp_ajax_myfilters', 'hnatchuk_get_post_basket' ); 
add_action( 'wp_ajax_nopriv_myfilters', 'hnatchuk_get_post_basket' );