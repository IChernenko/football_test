<?php
	switch ($_GET['group']) {
	case 'A':
		$competition_group = 'Группа A';
		break;
	case 'B':
		$competition_group = 'Группа B';
		break;
	case 'C':
		$competition_group = 'Группа C';
		break;
	case 'D':
		$competition_group = 'Группа D';
		break;
	case 'E':
		$competition_group = 'Группа E';
		break;
	case 'F':
		$competition_group = 'Группа F';
		break;
	case 'G':
		$competition_group = 'Группа G';
		break;
	case 'H':
		$competition_group = 'Группа H';
		break;
	case 'I':
		$competition_group = 'Группа I';
		break;
	default:
		$competition_group = 'Группа A';
		break;
}
?>
<?php
$type_table = '';
	if ($_GET['competition_type'] == 'group'){
		$competition_type = 'Групповой турнир';
	}elseif($_GET['competition_type'] == 'playoff'){
		$competition_type = 'Плей-офф';
		$competition_group = '';
		$type_table = 'matches';
	}else{
		$competition_type = 'Групповой турнир';
	}
?>
<ul class="list_confediration">
		<li ><a class="carent" href="?competition=uefa">Европа (UEFA)</a></li>
		<li><a href="?competition=afc">Азия (AFC)</a></li>
		<li><a href="?competition=caf">Африка (CAF)</a></li>
		<li><a href="?competition=ofc">Океания (OFC)</a></li>
		<li><a href="?competition=conmebol">Южная Америка (CONMEBOL)</a></li>
		<li><a href="?competition=concacaf">Северная и Центральная Америка (CONCACAF)</a></li>
		<li><a href="?competition=contenintal">Межконтинентальный плей-офф </a></li>
</ul>
<div class="bottom_header_wc">
	<div class="competition_type_wc">
		<ul>
			<li><a <?php set_carent_a_wc() ?> href="?competition=uefa&competition_type=group"><span>Групповой турнир</span></a></li>
			<li><a href="?competition=uefa&competition_type=playoff"><span>Плей-офф</span></a></li>
		</ul>
	</div>
</div>
<?php if($_GET['competition_type'] == 'playoff'):?>
<?php else: ?>
<div class="bottom_header_wc bottom_header_wc_no_c bottom_header_wc_group">
	<div class="competition_type_wc ">
		<ul>
			<li><a <?php set_carent_a_group_wc() ?> href="?competition=uefa&&group=A"><span>Группа A</span></a></li>
			<li><a href="?competition=uefa&group=B"><span>Группа B</span></a></li>
			<li><a href="?competition=uefa&group=C"><span>Группа C</span></a></li>
			<li><a href="?competition=uefa&group=D"><span>Группа D</span></a></li>
			<li><a href="?competition=uefa&group=E"><span>Группа E</span></a></li>
			<li><a href="?competition=uefa&group=F"><span>Группа F</span></a></li>
			<li><a href="?competition=uefa&group=G"><span>Группа G</span></a></li>
			<li><a href="?competition=uefa&group=H"><span>Группа H</span></a></li>
			<li><a href="?competition=uefa&group=I"><span>Группа I</span></a></li>
		</ul>
	</div>
</div>
<?php endif; ?>
<div class="content_wc">
	<?php  do_action( 'get_content_wc', 'Европа' ,  $competition_type , $competition_group,  $type_table); ?>
</div>