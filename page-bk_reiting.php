<?php
/* Template Name: BK Reiting */

get_header(); ?>


<div class="td-main-content-wrap td-container-wrap page_bk_reiting">
    <div class="td-container tdc-content-wrap <?php echo $td_sidebar_position; ?>">
        <div class="td-crumb-container">
            <?php echo td_page_generator::get_page_breadcrumbs(get_the_title()); ?>
        </div>
        <div class="td-pb-row">
            <div class="td-pb-span3 td-main-sidebar" role="complementary">
                <div class="td-s s-main-sidebar"> 
                    <h2 class="title_block border_bottom_line">Рейтинг по странам</h2>
                    <ul class="shild_page_list bukmekers_strany">
                        <?php
                            $args = array( 'orderby'  => 'id',  'order' => 'ASC' , 'hide_empty' => false , 'parent' => 0);
                            $terms = get_terms("bukmekers" , $args );
                             foreach ($terms as $term) : ?>
                                <li class=" vc_btn3-container vc_btn3-leftitem_cat_bk">
                                   <a href="#" class="vc_general vc_btn3 bukmekers_btn" data-id="<?php echo $term->term_id ?>"><?php echo $term->name ?></a>
                                </li>
                            <? endforeach;  ?>
                    </ul>
                    <h2 class="title_block border_bottom_line">Сравнить</h2>
                    <div class="list_filter_title_bk">
                        <?php $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                          $args = array( 'post_type' => 'bukmeker','order' => 'ASC',  'orderby' => 'data', 'publish' => true, 'post_parent' => 0, 'paged' => $paged, 'posts_per_page' => -1);
                           if (have_posts()) :  $loop = new WP_Query( $args );
                           while ( $loop->have_posts() ) : $loop->the_post(); ?>
                              <div class="item_title_bk">
                                  <input type="checkbox" id="title_filter_<?php the_ID(); ?>" name="title_bk" value="<?php the_ID(); ?>" data-img_url="<?php the_post_thumbnail_url(  ); ?>">
                                  <label for="title_filter_<?php the_ID(); ?>"> <?php $title = explode(' ', get_the_title()); echo $title[2]; echo " ";  echo $title[3]; echo $title[4]; ?></label>
                              </div>        
                           <?php endwhile; ?>   
                          <?php endif; ?> 
                          <a href="#" class="link_all_bk">Все букмекеры</a>
                    </div>
                </div>
            </div>
                <?php if (have_posts()) :  while ( have_posts() ) : the_post(); ?>
            <div class="td-pb-span9 td-main-content" role="main">
                <div class="td-ss-main-content">
                    <h1 class="border_bottom_line"> <?php the_title(); ?></h1>
                    <div class="sorting_block">
                        <span>Сортировать по:</span>
                        <div class="list_sort">
                            <div class="main_sort item_sort">
                                  <input type="checkbox" id="bk_value_of_th_coefficients" name="sort_bk" value="bk_value_of_th_coefficients">
                                  <label for="bk_value_of_th_coefficients">Величине коэффициентов</label>
                            </div> 
                            <div class="main_sort item_sort">
                                  <input type="checkbox" id="bk_the_best_mobile_app" name="sort_bk" value="bk_the_best_mobile_app">
                                  <label for="bk_the_best_mobile_app">Лучшему мобильному приложени</label>
                            </div>     
                            <div class="main_sort item_sort">
                                  <input type="checkbox" id="bk_license" name="sort_bk" value="bk_license">
                                  <label for="bk_license">С лицензией</label>
                            </div>     
                            <div class="main_sort item_sort" style="width: 180px;">
                                  <input type="checkbox" id="bk_amount_of_bonuses" name="sort_bk" value="bk_amount_of_bonuses">
                                  <label for="bk_amount_of_bonuses">По размеру бонусов</label>
                            </div>     
                            <div class="main_sort item_sort" style="width: 226.75px;">
                                  <input type="checkbox" id="bk_best_mobile_version" name="sort_bk" value="bk_best_mobile_version">
                                  <label for="bk_best_mobile_version">Лучшей мобильной версии</label>
                            </div>   
                            <div class=" item_sort" >
                                  <input type="checkbox" id="otziv" name="sort_bk" value="otziv" class="otziv">
                                  <label for="otziv">Отзывам за:</label>
                            </div>
                            <div class="radio_otz item_sort" >
                                  <input type="radio" id="bk_reviews_for_the_last_month" name="sort_bk" value="_bk_desc_player_reviews_last_month">
                                  <label for="bk_reviews_for_the_last_month">Последний месяц</label>
                            </div>     
                            <div class="radio_otz item_sort" >
                                  <input type="radio" id="bk_reviews_for_the_last_six_months" name="sort_bk" value="_bk_desc_player_reviews_last_6_month">
                                  <label for="bk_reviews_for_the_last_six_months">Последние полгода</label>
                            </div>     
                            <div class="radio_otz item_sort" >
                                  <input type="radio" id="bk_reviews_for_the_last_year" name="sort_bk" value="_bk_desc_player_reviews_last_year">
                                  <label for="bk_reviews_for_the_last_year">Последний год</label>
                            </div>     
                            <div class="radio_otz item_sort" >
                                  <input type="radio" id="bk_reviews_for_all_time" name="sort_bk" value="_bk_desc_player_reviews">
                                  <label for="bk_reviews_for_all_time">Все время</label>
                            </div>              
                        </div>
                    </div>
                <?php endwhile; ?>      <?php endif; ?> 
                    <table class="bk_table_list">
                        <thead>
                                <tr>
                                    <th>БК контора</th>
                                    <th>Бонус</th>
                                    <th>Оценка</th>
                                    <th>Отзывы</th>
                                    <th>Обзор</th>
                                    <th>Сайт</th>
                                </tr>
                        </thead>
                        <tbody>
                          <?php td_football_bukmekers::get_bukmekers_list(); ?>
                        </tbody>
                        <tfoot>
                          <tr>
                            <td colspan="6">
                                <a href="" class="all_bk_list bukmekers_btn">Все букмекерские конторы</a>
                            </td>
                          </tr>
                        </tfoot>
                         
                    </table>
                </div>
            </div>
            <?php  wp_reset_query();  if (have_posts()) :  while ( have_posts() ) : the_post(); ?>
            <div class="td-pb-span12 td-main-sidebar" role="complementary">
                <div class="td-ss-main-content"> 
                    <div class="td-page-content">
                       <?php the_content(); ?>
                    </div>
                </div>
            </div>            
        </div> <!-- /.td-pb-row -->
        <?php endwhile; endif; wp_reset_query();?>
    </div> <!-- /.td-container -->
</div> <!-- /.td-main-content-wrap -->
<?php  get_template_part( 'modules/bk/content', 'compare'); ?>
<link type="text/css" rel="stylesheet" media="all" href="<?php bloginfo('template_directory'); ?>/fancybox/jquery.fancybox.min.css"/>
<link type="text/css" rel="stylesheet" media="all" href="<?php bloginfo('template_directory'); ?>/modules/bk/css/bk.css"/>
<script   type="text/javascript" src="<?php bloginfo('template_directory'); ?>/fancybox/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/modules/bk/js/bk.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/modules/bk/js/compare.js"></script>

<?php

get_footer();