<?php
// Букмекеры
/*Отзывы*/
//**Список коментариев
function show_coments_bookmaker($atts) {
	global $wp_query;
	global $wpdb;
  extract(shortcode_atts(array(
        'slug' => "prev",
        'limit' => 5,
  ), $atts));
  switch ($slug) {
  case 'prev':
      wp_reset_query();
      $name_bookmaker = get_the_title();
      break;
  case 'full':
      wp_reset_query();
      global $post;
      $name_bookmaker = get_the_title($post->post_parent);
      break;
  }
  if ($limit == 'full') {
		$per_page = 10; //Количество отзівов на странице;
		//получаем номер страницы и значение для лимита 
		$cur_page = 1;
		if (isset($wp_query->query_vars['page']) &&  $wp_query->query_vars['page'] > 0) 
		{
		    $cur_page = $wp_query->query_vars['page'];
		}
		$start = ($cur_page - 1) * $per_page;

		$sql = "SELECT  *
						FROM  foot_football_bookmaker_coments bookmaker_coments 
			      JOIN  parser_common_bookmaker com_bookmaker
			        ON  bookmaker_coments.bookmaker_id = com_bookmaker.id 
			     WHERE  com_bookmaker.name_post = '{$name_bookmaker}' ";
		$rows = count($wpdb->get_results( $sql));

    $lim= 'LIMIT '.$start.','.$per_page;
  }else{
    $lim = 'LIMIT 0 , '.$limit;
  }
	$sql = "SELECT  *
						FROM  foot_football_bookmaker_coments bookmaker_coments 
			      JOIN  parser_common_bookmaker com_bookmaker
			        ON  bookmaker_coments.bookmaker_id = com_bookmaker.id 
			     WHERE  com_bookmaker.name_post = '{$name_bookmaker}' 
			     ORDER BY bookmaker_coments.posting_date DESC ,
                   bookmaker_coments.posting_time DESC  {$lim} ";
	$result = $wpdb->get_results( $sql);
	
	$innerHTML .= reviews_block();
	foreach ($result as $item_coment) {
		if($item_coment->avatar == ''){
			$avatar = preg_replace ("![a-z|а-я|\s]!u","",$item_coment->user_name);
			$avatar = '<div class="avatar avatar_st" data-initials="'.$avatar.'"></div>';
		}else{
			$avatar = '<div class="avatar" style="background-image: url('.$item_coment->avatar.')"></div>';
		}

		$innerHTML .= '<div class="bk_item_coment">';
			$innerHTML .= '<div class="left_bk_item_coment">
											'.$avatar.'
											<span class="name_bk_item_coment">'.$item_coment->user_name.'</span>
											<div class="all_reiting"><div class="stars">'. get_stars($item_coment->rate).'</div><span>Рейтинг: '.$item_coment->rate.'/5</span></div>

										 </div>';
			$innerHTML .= '<div class="right_bk_item_coment">
															<span class="text_bk_item_coment">'.show_more_review($item_coment->message).'</span>
															<span class="date_posting">'.$item_coment->posting_date.' в '.$item_coment->posting_time.'</span>
										 </div>';
		$innerHTML .= "</div>";
  }
  	if ($limit == 'full') {
  								$innerHTML .= pagination_reviews($per_page , $rows , $cur_page  );
  	}
		return $innerHTML;
}
add_shortcode('show_coments_bookmaker', 'show_coments_bookmaker');

//*** Показать больше коментария
function show_more_review($reviews){
	$text=explode(" ",$reviews); 

	if(count($text) < 90){
		$reviews = $reviews ;
	}else{
		$reviews = '<span>';
		for($i = 0; $i < 90; ++$i) {
			$reviews .= $text[$i].' ';
		}
		$reviews .= '</span>';
		$reviews .='<div class="show_more"> <p class="wpsm-show" style="color: #009688; font-size: 100%; text-align: right;"> Читать полностью </p>';
			$reviews .= '<div class="wpsm-content wpsm-content-hide">';
		for($i = 90; $i < count($text); ++$i) {
			$reviews .= $text[$i].' ';
		}
			$reviews .= '<p class="wpsm-hide" style="color: #009688; font-size: 100%; text-align: right;"> Свернуть</p></div>';
		$reviews .= '</div>';	
	}

	return $reviews;
}

//***Пагинация отзывов
function pagination_reviews($per_page , $all_rows , $cur_page ){

	//узнаем общее количество страниц и заполняем массив со ссылками
  $max_page = ceil($all_rows / $per_page);

  $paged = $cur_page; 
  $per_page = $per_page;

  $num_pages = 5; // сколько ссылок показывать
  $stepLink = 10; // после навигации ссылки с определенным шагом (значение = число (какой шаг) или '', если не нужно показывать). Пример: 1,2,3...10,20,30
  $pages_to_show = intval($num_pages);


  $pages_to_show_minus_1 = $pages_to_show-1;
 
	$half_page_start = floor($pages_to_show_minus_1/2); //сколько ссылок до текущей страницы
	$half_page_end = ceil($pages_to_show_minus_1/2); //сколько ссылок после текущей страницы

	$dotright_text = '…'; // промежуточный текст "до".
	$dotright_text2 = '…'; // промежуточный текст "после".
	$backtext = '<i class="td-icon-menu-left"></i>'; // текст "перейти на предыдущую страницу". Ставим '', если эта ссылка не нужна.
	$nexttext = '<i class="td-icon-menu-right"></i>'; // текст "перейти на следующую страницу". Ставим '', если эта ссылка не нужна.
	$first_page_text = ''; // текст "к первой странице" или ставим '', если вместо текста нужно показать номер страницы.
	$last_page_text = ''; // текст "к последней странице" или пишем '', если вместо текста нужно показать номер страницы.
 
	$start_page = $paged - $half_page_start; //первая страница
	$end_page = $paged + $half_page_end; //последняя страница (условно)

	if($start_page <= 0) $start_page = 1;
	if(($end_page - $start_page) != $pages_to_show_minus_1) $end_page = $start_page + $pages_to_show_minus_1;
	if($end_page > $max_page) {
		$start_page = $max_page - $pages_to_show_minus_1;
		$end_page = (int) $max_page;
	}

	if($start_page <= 0) $start_page = 1;
 
	$out=''; //выводим навигацию
		$out.= '<div class="page-nav td-pb-padding-side">';
				if ($backtext && $paged!=1) $out.= '<a href="?page='.($paged - 1).'">'.$backtext.'</a>';

				if ($start_page >= 2 && $pages_to_show < $max_page) {
					$out.= '<a href="?page=1">'. ($first_page_text?$first_page_text:1) .'</a>';
					if($dotright_text && $start_page!=2) $out.= '<span class="extend">'.$dotright_text.'</span>';
				}
 
				
 
				for($i = $start_page; $i <= $end_page; $i++) {
					if($i == $paged) {
						$out.= '<span class="current">'.$i.'</span>';
					} else {
						$out.= '<a href="?page='.$i.'">'.$i.'</a>';
					}
				}

				if ($end_page < $max_page) {
					if($dotright_text && $end_page!=($max_page-1)) $out.= '<span class="extend">'.$dotright_text2.'</span>';
					$out.= '<a href="?page='.$max_page.'">'. ($last_page_text?$last_page_text:$max_page) .'</a>';
				}
				if ($nexttext && $paged!=$end_page) $out.= '<a href="?page='.($paged + 1).'">'.$nexttext.'</a>';
				$out  .='<span class="pages">Страница '.$cur_page.' из '.$max_page.'</span><div class="clearfix"></div>';
		$out.= "</div>".$after."\n";
	if ($echo) echo $out;
	else return $out;
}

// @ Блок формы добавления отзывов
function reviews_block(){
	$innerHTML = '<div class="comment_form review_block">';
			$innerHTML .= reviews_form_top();
			$innerHTML .= reviews_form();
	$innerHTML .='</div>';
	return $innerHTML;
}
//** Верх формы
function reviews_form_top(){
	global $post;
	if($post->post_parent  == 0){
		$post_id = $post->ID;
	}else{
		$post_id = $post->post_parent;
	}
	$post_meta_date = get_metadata('post', $post_id, '', true);
	//print_r($post);

	$count_rate_all = $post_meta_date["bk_rating_common_all_raiting"][0];
	$count_rating_coefficients = $post_meta_date["bk_rating_common_coefficients"][0];
	$rating_football_betting = $post_meta_date["bk_rating_common_football_betting"][0];
	$rating_line = $post_meta_date["bk_rating_common_line"][0];
	$rating_live_betting = $post_meta_date["bk_rating_common_live_betting"][0];
	$rating_offers = $post_meta_date["bk_rating_common_offers"][0];
	$rating_site = $post_meta_date["bk_rating_common_site"][0];
	$rating_app = $post_meta_date["bk_rating_common_app"][0];
	$rating_finance = $post_meta_date["bk_rating_common_finance"][0];
	

	//Проверка авторизации пользователя
	if($post->post_parent  == 0){
		$btn_action = '<a href="otzyvy/" class="review-btn review_btn">Оставить отзыв</a>';
	}else{
		if ( is_user_logged_in() ) {
			$btn_action = '<a href="#" class="review-btn review_btn " id="comment_open_add_form">Оставить отзыв</a>';
		}else {
			$btn_action = '<a href="#" class="review-btn review_btn rcl-login ">Оставить отзыв</a>';
		}
	}
		$innerHTML .='<div class="top_block_review">
									<div class="td-pb-row">
										<div class="td-pb-span6 average_stats item_rating_reviews">
												<span>Пользовательский рейтинг:</span> 	
											  <div class="result_rating"><span>'.$count_rate_all.'</span>/5</div>
											  <div class="stars">'.get_stars($count_rate_all).'</div>
										</div>
										<div class="td-pb-span3">
											<a href="#" class="show_more_rating" ><i class="fa fa-info-circle" aria-hidden="true"></i> <span>Показать подробно</span></a>
										</div>
										<div class="td-pb-span3">'.$btn_action.'</div>
									</div>
									<div class="full_view_rating" style="display: none;">
										<div class="td-pb-row">
											<div class="td-pb-span6 item_rating_reviews">
												  <span>Коэффициенты</span> 	
												  <div class="result_rating"><span>'.$count_rating_coefficients.'</span>/5</div>
												  <div class="stars">'.get_stars($count_rating_coefficients).'</div>
				              </div>
											<div class="td-pb-span6 item_rating_reviews">
													<span>Ставки на футбол</span>	 	
													<div class="result_rating"><span>'.$rating_football_betting.'</span>/5</div>
													<div class="stars">'.get_stars($rating_football_betting).'</div>
				              </div>
											<div class="td-pb-span6 item_rating_reviews">
													<span>Линия</span> 
													<div class="result_rating"><span>'.$rating_line.'</span>/5</div>
													<div class="stars">'.get_stars($rating_line).'</div>
				              </div>
											<div class="td-pb-span6 item_rating_reviews">
												  <span>Live-ставки </span>	
												  <div class="result_rating"><span>'.$rating_live_betting.'</span>/5</div>
												  <div class="stars">'.get_stars($rating_live_betting).'</div>
				              </div>
				              <div class="td-pb-span6 item_rating_reviews">
													<span>Бонусы</span> 	
													<div class="result_rating"><span>'.$rating_offers.'</span>/5</div>
													<div class="stars">'.get_stars($rating_offers).'</div>
				              </div>
				              <div class="td-pb-span6 item_rating_reviews">
												  <span>Сайт</span> 	
												  <div class="result_rating"><span>'.$rating_site.'</span>/5</div>
												  <div class="stars">'.get_stars($rating_site).'</div>
				              </div>
				              <div class="td-pb-span6 item_rating_reviews">
												  <span>Приложение</span> 	
												  <div class="result_rating"><span>'.$rating_app.'</span>/5</div>
												  <div class="stars">'.get_stars($rating_app).'</div>
				              </div>
				              <div class="td-pb-span6 item_rating_reviews">
											  	<span>Ввод/вывод средств</span> 	
											  	<div class="result_rating"><span>'.$rating_finance.'</span>/5</div>
											  	<div class="stars">'.get_stars($rating_finance).'</div>
				              </div>
										</div>
									</div>
               </div>';

		
		
		return $innerHTML;
}

//** Форма для отзыов с оценкой рейтингов
function reviews_form(){
	global $post;
	$innerHTML .='<form id="feedbacks_form" class="feedbacks_form" method="POST" style="display: none;">
									<div class="block_title block_title_form"> Оставьте отзыв <span class="counter-info-row display-none low-symbols">Минимум 60 знаков</span> </div>
									<div class="td-pb-row">
										<div class="td-pb-span12 item_fealds_form">
											<textarea name="comment" placeholder="Текст, опишите ваш опыт взаимодействия с этим букмекером" data-minimum="Минимум 60 знаков" data-count1="знак" data-count2="знака" data-count3="знаков"></textarea>
										</div>
									</div>
									<div class="td-pb-row">
										<div class="td-pb-span6 item_rating_reviews">
											  <span>Коэффициенты</span> 	
											  <div class="result_rating"><span>0</span>/5</div>
												<a href="#" class="no-underline rate" data-rate="0" id="rating_coefficients">
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                  </a>
			              </div>
										<div class="td-pb-span6 item_rating_reviews">
												<span>Ставки на футбол</span>	 	
												<div class="result_rating"><span>0</span>/5</div>
												<a href="#" class="no-underline rate" data-rate="0" id="rating_football_betting">
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                  </a>
			              </div>
										<div class="td-pb-span6 item_rating_reviews">
												<span>Линия</span> 
												<div class="result_rating"><span>0</span>/5</div>
												<a href="#" class="no-underline rate" data-rate="0" id="rating_line">
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                  </a>
			              </div>
										<div class="td-pb-span6 item_rating_reviews">
											  <span>Live-ставки </span>	
											  <div class="result_rating"><span>0</span>/5</div>
												<a href="#" class="no-underline rate" data-rate="0" id="rating_live_betting">
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                  </a>
			                  
			              </div>
			              <div class="td-pb-span6 item_rating_reviews">
												<span>Бонусы</span> 	
												<div class="result_rating"><span>0</span>/5</div>
												<a href="#" class="no-underline rate" data-rate="0" id="rating_offers">
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                  </a>
			                  
			              </div>
			              <div class="td-pb-span6 item_rating_reviews">
											  <span>Сайт</span> 	
											  <div class="result_rating"><span>0</span>/5</div>
												<a href="#" class="no-underline rate" data-rate="0" id="rating_site">
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                  </a>
			                  
			              </div>
			              <div class="td-pb-span6 item_rating_reviews">
											  <span>Приложение</span> 	
											  <div class="result_rating"><span>0</span>/5</div>
												<a href="#" class="no-underline rate" data-rate="0" id="rating_app">
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                  </a>
			                  
			              </div>
			              <div class="td-pb-span6 item_rating_reviews">
										  	<span>Ввод/вывод средств</span> 	
										  	<div class="result_rating"><span>0</span>/5</div>
												<a href="#" class="no-underline rate" data-rate="0" id="rating_finance">
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>
			                            <i class="star_rat icon16  star-empty"></i>

			                  </a>
			                  
			              </div>
									</div>
									<div class="td-pb-row">
										<div class="td-pb-span12">
										<div class="bottom_form"></div>
										</div>
										 <div class="td-pb-span6 item_rating_reviews">
										  	<span>Итоговый результат</span> 	
										  	<div class="result_rating result_rating_all"><span>0</span>/5</div>
												<div class = "rating_final_result">
									        <i class="star_rat icon16  star-empty"></i>
			                    <i class="star_rat icon16  star-empty"></i>
			                    <i class="star_rat icon16  star-empty"></i>
			                    <i class="star_rat icon16  star-empty"></i>
			                    <i class="star_rat icon16  star-empty"></i>
												</div>

			              </div>
			              <div class="td-pb-span6 item_rating_reviews">
			              	<input type="hidden" name="rating_coefficients" value="0">
											    <input type="hidden" name="rating_football_betting" value="0">
											    <input type="hidden" name="rating_line" value="0">
											    <input type="hidden" name="rating_live_betting" value="0">
											    <input type="hidden" name="rating_offers" value="0">
											    <input type="hidden" name="rating_site" value="0">
											    <input type="hidden" name="rating_app" value="0">
											    <input type="hidden" name="rating_finance" value="0">
											    <input type="hidden" name="rating_final_result" value="0">
											    <input type="hidden" name="postid" value="'.$post->post_parent.'">
											    <input type="hidden" name="type" value="reviews">
											    <input type="hidden" name="action-type" value="add">
											    <input type="hidden" name="feedback-id" value="">
											    <button type="submit" class="feedbacks-button" disabled="disabled">Отправить</button>
			              </div>
									</div>

							</form>';
	$innerHTML .= '<div id="feedbacks-popup" data-type="success" class="">
								    <div class="feedbacks-popup">
								        <div class="feedbacks-popup-title" data-success-title="Отзыв добавлен" data-error-title="Сообщение об ошибке">
								            <i class="fa fa-times feedbacks-popup-close" aria-hidden="true"></i>
								        </div>
								        <div class="feedbacks-popup-content">Ваш отзыв отправлен на проверку. В скором времени он будет добавлен.</div>
								    </div>
								</div> ' ;
	return $innerHTML;
}

// Добавление ного отзыва
function sent_feedbacks_new(){
	$formText = $_POST['formText'] ;
	$formType = $_POST['formType'];
	$post_id =  $_POST['post_id'];
	$formActionType = $_POST['formActionType'] ;

	//Данные о пользователе
	$carent_user = wp_get_current_user();
	$carent_user = $carent_user->data;

	//проверка оставлял ли рание коментарий для етого поста
	//**Ели да то оповещаем его
	//**Если нет то добавляем отзів в БД
	$args = array(
		'user_id' => $carent_user->ID,
		'post_id' => $post_id,
	);
	$comments = get_comments($args);


	if(count($comments)){
		$return = array(
			'status'   => 'not_add',
			'post'   => $formType,
			'user'   => $carent_user->ID,
		);

		wp_send_json_success( $return );
	}else{
			// создаем массив данных нового комментария
			$commentdata = array(
				'comment_post_ID'      => $post_id,
				'comment_author'       => $carent_user->user_nicename,
				'comment_author_email' => $carent_user->user_email,
				'comment_author_url'   => $carent_user->user_url,
				'comment_content'      => $formText,
				'comment_type'         => $formType,
				'comment_parent'       => 0,
				'user_id'              => $carent_user->ID,
			);
			// добавляем данные в Базу Данных
			$comment_id = wp_new_comment( wp_slash($commentdata) );

			if(isset($comment_id)){
				$return = array(
					'status'   => 'add',
					'post'   => $formType,
					'user'   => $carent_user->ID,
				);

				wp_send_json_success( $return );
			}else{

			}
	}
	die();
}
add_action('wp_ajax_feedbacks_new', 'sent_feedbacks_new');
add_action('wp_ajax_nopriv_feedbacks_new', 'sent_feedbacks_new');

 
//Отправка мета даних коментария
/*
 bk_rating_coefficients       -- Коэффициенты
 bk_rating_football_betting   -- Ставки на футбол 
 bk_rating_line 							-- Линия
 bk_rating_live_betting 			-- Live-ставки
 bk_rating_offers 						-- Бонусы
 bk_rating_site 							-- Сайт
 bk_rating_app 								-- Приложение
 bk_rating_finance 						-- Ввод/вывод средств
 bk_rating_final_result 			-- Общий рейтинг
*/
function save_comment_meta_data ( $comment_id ) { 
    add_comment_meta ( $comment_id , 'bk_rating_coefficients' , $_POST [ 'rating_coefficients' ] ); 
    add_comment_meta ( $comment_id , 'bk_rating_football_betting' , $_POST [ 'rating_football_betting' ] ); 
    add_comment_meta ( $comment_id , 'bk_rating_line' , $_POST [ 'rating_line' ] ); 
    add_comment_meta ( $comment_id , 'bk_rating_live_betting' , $_POST [ 'rating_live_betting' ] ); 
    add_comment_meta ( $comment_id , 'bk_rating_offers' , $_POST [ 'rating_offers' ] ); 
    add_comment_meta ( $comment_id , 'bk_rating_site' , $_POST [ 'rating_site' ] ); 
    add_comment_meta ( $comment_id , 'bk_rating_app' , $_POST [ 'rating_app' ] ); 
    add_comment_meta ( $comment_id , 'bk_rating_finance' , $_POST [ 'rating_finance' ] ); 
    add_comment_meta ( $comment_id , 'bk_rating_final_result' , $_POST [ 'rating_final_result' ] ); 


    
}    
add_action ( 'comment_post' , 'save_comment_meta_data' );   





