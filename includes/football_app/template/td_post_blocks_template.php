<?php
class td_post_blocks_template
{
	static function showTopPlayersTheGuardian()
	{
		$innerHTML = '<cite class="cite_block">Читать ещё: «<a href="/the-guardian/" target="_blank">Рейтинг лучших футболистов</a>» </cite>';
		return $innerHTML;
	}

	static function showTopClubs()
	{
		$innerHTML = '<cite class="cite_block">Читать ещё: «<a href="/top-100-futbolnyh-klubov-mira/" target="_blank">Новый рейтинг лучших клубов мира</a>» </cite>';
		return $innerHTML;
	}
}
