<?php
// Обзор
add_action('init', 'do_rewrite');

function Qualification_Overview_wc() {
	//echo get_query_var('competition'); // > sup
 // echo get_query_var('competition_type'); // > risoviy
	if($_GET['competition'] == 'uefa'){
		 return get_template_part( 'modules/lib/templates/content', 'wc_tablica_uefa');
	}elseif($_GET['competition'] == 'afc'){
		 return get_template_part( 'modules/lib/templates/content', 'wc_tablica_afc');
	}elseif($_GET['competition'] == 'caf'){
		 return get_template_part( 'modules/lib/templates/content', 'wc_tablica_caf');
	}elseif($_GET['competition'] == 'ofc'){
		 return get_template_part( 'modules/lib/templates/content', 'wc_tablica_ofc');
	}elseif($_GET['competition'] == 'concacaf'){
		 return get_template_part( 'modules/lib/templates/content', 'wc_tablica_concacaf');
	}elseif($_GET['competition'] == 'conmebol'){
		 return get_template_part( 'modules/lib/templates/content', 'wc_tablica_conmebol');
	}elseif($_GET['competition'] == 'contenintal'){
		 return get_template_part( 'modules/lib/templates/content', 'wc_tablica_intercontenintal');
	}else{
		 return get_template_part( 'modules/lib/templates/content', 'wc_tablica_uefa');
	}


}
add_shortcode('Qualification_Overview_wc', 'Qualification_Overview_wc');
/// Финал
function turnirnaya_tablitsa_wc_main() {
	$render_tournament_table = $_GET['competition_type'];
	switch ($render_tournament_table) {
		case 'group':
			$render = get_template_part( 'modules/lib/templates/content', 'wc_tablica_final_group');
			break;
		case 'playoff':
			$render = get_template_part( 'modules/lib/templates/content', 'wc_tablica_final_playoff');
			break;
		default:
			$render = get_template_part( 'modules/lib/templates/content', 'wc_tablica_final_group');
			break;
	}

	return $render;
}
add_shortcode('turnirnaya_tablitsa_wc_main', 'turnirnaya_tablitsa_wc_main');

/*Установка класа на типы соревнования*/
function set_carent_a_wc(){
	if( empty($_GET['competition_type'])){
		echo ' class="carent" ';
	}
}
function set_carent_a_group_wc(){
	if( empty($_GET['group'])){
		echo ' class="carent" ';
	}
}
/*СОздание URL*/
function get_url_wc(){
	if($_GET['competition'] != ''):
		echo 'competition='.$_GET['competition'].'&';
	endif;
}
/*Вывод даных */
function get_content_wc($competition , $competition_type = false , $competition_group = false  , $type_table = false){
	global $wpdb;
	$innerHTML  = '';
	switch ($type_table) {
		case 'matches':
			//Краткое описания
			$data_result = get_world_cup_qualification_overview($competition , $competition_type , $competition_group );
			foreach ($data_result as $wc_item_desc) {
				 $innerHTML .= '<div class="description_wc">'.$wc_item_desc->post_content.'</div>';
			}
			$data_matches = get_matches_nt_from_db($competition , $competition_type);
			//echo "<pre>".print_r($data_matches)."</pre>";
		  $innerHTML .= "<div class='playoff_list'>";
		  foreach ($data_matches as $key => $value) {

				$i =1  ;
				$innerHTML .= "<div class='playoff_item'>";
				foreach ($value as $item_team_wc) {
					 $date = date_create($item_team_wc->play_date);
					 $time_play = date_create($item_team_wc->play_time);
					 $extra_time = '';
					 if ($item_team_wc->extra_time) {
					 		$extra_time = '<span class="extra_time">ДВ</span>';
					 }
					 switch ($item_team_wc->result_match) {
						  case 1:
						  		$result_match_1 = 'bold';
						      break;
						  case 2:
						  		$result_match_2 = 'bold';
						      break;
						  case 0:
						      break;
					  }

						if($item_team_wc->tour == 1){
							 $innerHTML .= '<div class="playoff_match_1">
							 									<div class="playoff_match">
							 										<span class="team_home '.$result_match_1.'"><i class="flag_nt flag_'.translit($key).'" title="'.$key.'" alt="'.$key.'"></i>'.$key.'</span>
							 										<span class="team_away '.$result_match_2.'"><i class="flag_nt flag_'.translit($item_team_wc->team_2).'" title="'.$item_team_wc->team_2.'" alt="'.$item_team_wc->team_2.'"></i>'.$item_team_wc->team_2.'</span>
							 								  </div>
							 								  <div class="inform_match">
							 								  	<div class="result_match">'.$item_team_wc->result_1.'-'.$item_team_wc->result_2.''.$extra_time.'</div>
							 								  	<div class="data_play_match"><span>'.date_format($date, 'd.m. ') .'</span><span>'.date_format($time_play, 'H:i ').' мск</span></div>
							 								  </div>
							 							  </div>';
							 	$result_match_1 = '';
							 	$result_match_2 = '';
						}else{
							 $innerHTML .= '<div class="playoff_match_2">
							 									<div class="playoff_match">
							 										<span class="team_away '.$result_match_1.'"><i class="flag_nt flag_'.translit($item_team_wc->team_2).'" title="'.$item_team_wc->team_2.'" alt="'.$item_team_wc->team_2.'"></i>'.$item_team_wc->team_2.'</span>
							 										<span class="team_home '.$result_match_2.'"><i class="flag_nt flag_'.translit($key).'" title="'.$key.'" alt="'.$key.'"></i>'.$key.'</span>
							 								   </div>
							 								   <div class="inform_match">
							 								  	<div class="result_match">'.$item_team_wc->result_1.'-'.$item_team_wc->result_2.''.$extra_time.'</div>
							 								  	<div class="data_play_match"><span>'.date_format($date, 'd.m. ') .'</span><span>'.date_format($time_play, 'H:i ').' мск</span></div>
							 								   </div>
							 							  </div>';
							 	$result_match_1 = '';
							 	$result_match_2 = '';

						}
				}
				$innerHTML .="</div>";
			}
	  	$innerHTML .="</div>";
			break;
		default:
			$data_groups = get_national_team_stat_groups($competition , $competition_type , $competition_group );
			//echo "<pre>"; print_r($result); echo "</pre>";
			$innerHTML  = '';
			foreach ($data_groups as $key => $value) {
					$i =1  ;
					$innerHTML .= "<table class='turnirnaya-tablitsa turnirnaya_tablitsa_wc turnirnaya_tablitsa_wc_main'>";
					$innerHTML .= "<tr class='group'><th colspan='9' >".$key." </th> </tr>";
					$innerHTML .= "<tr><th style='width: 50px;'>№</th><th class='wc_team'>Команда</th><th>И</th><th>В</th><th>Н</th><th>П</th><th>ЗГ</th><th>ПГ</th><th>О</th></tr>";
					foreach ($value as $item_team_wc) {
					 $innerHTML .= '<tr>
					            <td>'.$i++.'</td>
					            <td class="wc_team"><i class="flag_nt flag_'.translit($item_team_wc->sbornaya).'" title="'.$item_team_wc->sbornaya.'" alt="'.$item_team_wc->sbornaya.'"></i>'.$item_team_wc->sbornaya.'</td>
					            <td>'.$item_team_wc->games.'</td>
					            <td>'.$item_team_wc->wins.'</td>
					            <td>'.$item_team_wc->draws.'</td>
					            <td>'.$item_team_wc->loses.'</td>
					            <td>'.$item_team_wc->scored_goals.'</td>
					            <td>'.$item_team_wc->missed_goals.'</td>
					            <td>'.$item_team_wc->points.'</td>
					        </tr>';
					}

					$innerHTML .="</table>";
					if(count($data_groups) == 1){
						$innerHTML .= "<div class='opis_table'><b>И</b> - игры, <b>В</b> - выигрыши, <b>Н</b> - ничья, <b>П</b> - поражения, <b>ЗГ/з</b> - забитые голы, <b>ПГ</b> - пропущенные голы, <b>О</b> - очки</div>";
					}

					//Краткое описания
					$data_result = get_world_cup_qualification_overview($competition , $competition_type , $competition_group );
					foreach ($data_result as $wc_item_desc) {
						 $innerHTML .= '<div class="description_wc">'.$wc_item_desc->post_content.'</div>';
						}
					if($competition !== 'Финал'){
						$innerHTML .=  get_world_cup_result_matches_group($competition , $competition_type , $competition_group );
					}

			}
			if(count($data_groups) > 1){
				$innerHTML .= "<div class='opis_table'><b>И</b> - игры, <b>В</b> - выигрыши, <b>Н</b> - ничья, <b>П</b> - поражения, <b>ЗГ/з</b> - забитые голы, <b>ПГ</b> - пропущенные голы, <b>О</b> - очки</div>";
			}
			break;
	}
	echo $innerHTML;
}
add_action( 'get_content_wc', 'get_content_wc', 10, 4 );
/**Получить календарь матчей**/
function get_world_cup_matches_list($season , $competition , $competition_type = false , $competition_group = false ,
																		$type_matches = false , $orderby = false , $order = 'ASC' ){
	global $wpdb;
	if($competition_group){
		$sql_competition_group = 'AND com_t_g.name = "'.$competition_group.'"';
	}
	if($type_matches >= 0){
		$sql_type_matches = 'AND matches_nt.status = "'.$type_matches.'"';
	}
	if($orderby !== ''){
		$sql_order_by = 'ORDER BY matches_nt.'.$orderby.' '.$order;
	}
  $sql= 'SELECT fc.name_post AS tournament,
                com_t_type.name AS tournament_type,
                com_t_g.name AS tournament_group ,
                matches_nt.competition_tour ,
                ft1.name AS team_1 ,
                ft2.name AS team_2 ,
                matches_nt.play_date , matches_nt.play_time , matches_nt.result_1 , matches_nt.result_2
           FROM foot_football_national_team_matches matches_nt
      LEFT JOIN parser_common_tournament fc
             ON matches_nt.competition_id = fc.id
      LEFT JOIN parser_common_tournament_type com_t_type
             ON matches_nt.competition_type_id = com_t_type.id
      LEFT JOIN parser_common_tournament_group com_t_g
             ON matches_nt.competition_group_id = com_t_g.id
      LEFT JOIN parser_common_national_team  ft1
             ON matches_nt.nt_id_1 = ft1.id
      LEFT JOIN parser_common_national_team  ft2
             ON matches_nt.nt_id_2 = ft2.id
          WHERE fc.name_post = "'.$competition.'"
				    AND com_t_type.name = "'.$competition_type.'"
				    '.$sql_competition_group.'
						'.$sql_type_matches.'
            AND matches_nt.season_id = "'.$season.'"
            '.$sql_order_by;


  $result = $wpdb->get_results( $sql );
	//echo "<pre>".print_r($sql_type_matches)."</pre>";
	return $result;

}

function get_matches_nt_from_db($competition , $competition_type)
{
	global $wpdb;
	$sql= "(SELECT team_1.name AS team_1 ,
								 team_2.name AS team_2 ,
								 nt_m.competition_tour AS tour ,
								 nt_m.play_date , nt_m.play_time ,
								 nt_m.result_1 , nt_m.result_2 , nt_m.result_match , nt_m.result_extra_time
						FROM foot_football_national_team_matches  nt_m
						JOIN parser_common_tournament  c_comp
						  ON nt_m.competition_id = c_comp.id
						JOIN parser_common_tournament_type  c_com_t
						  ON nt_m.competition_type_id = c_com_t.id
						JOIN parser_common_national_team  team_1
						  ON nt_m.nt_id_1 = team_1.id
						JOIN parser_common_national_team  team_2
						  ON nt_m.nt_id_2 = team_2.id
					 WHERE c_comp.name_post = '$competition '
					   AND c_com_t.name = '$competition_type '
					   AND nt_m.status = '1'
					   AND nt_m.competition_tour = '1')
					 UNION
					(SELECT team_2.name AS team_2 ,
						      team_1.name AS team_1,
						      nt_m.competition_tour AS tour ,
						      nt_m.play_date ,  nt_m.play_time ,
						      nt_m.result_1 , nt_m.result_2 , nt_m.result_match , nt_m.result_extra_time
						 FROM foot_football_national_team_matches  nt_m
						 JOIN parser_common_tournament  c_comp
						   ON nt_m.competition_id = c_comp.id
						 JOIN parser_common_tournament_type  c_com_t
						   ON nt_m.competition_type_id = c_com_t.id
						 JOIN parser_common_national_team  team_1
						   ON nt_m.nt_id_1 = team_1.id
						 JOIN parser_common_national_team  team_2
						   ON nt_m.nt_id_2 = team_2.id
					  WHERE c_comp.name_post ='$competition '
						  AND c_com_t.name = '$competition_type   '
						  AND nt_m.status = '1'
						  AND nt_m.competition_tour = '2') ";
	$result = $wpdb->get_results( $sql);
	$result = array_group($result);

	return $result;
}
function get_national_team_stat_groups($competition , $competition_type = false, $competition_group = false)
{
	global $wpdb;
	if($competition_type){
		$competition_type_sql = "AND  com_t_type.name ='".$competition_type."'";
	}
	if($competition_group){
		$competition_group_sql = "AND  com_t_g.name = '".$competition_group."'";
	}
	 $sql= "SELECT  com_t_g.name AS group_name ,
                  fc.name AS competition ,
                  com_t_type.name AS competition_type ,
                  com_nt.name AS sbornaya ,
                  nt_t.games , nt_t.wins , nt_t.draws , nt_t.loses , nt_t.scored_goals , nt_t.missed_goals , nt_t.points
            FROM  foot_football_national_team_tournament_table  nt_t
       LEFT JOIN  parser_common_tournament fc
              ON  nt_t.competition_id = fc.id
       LEFT JOIN  parser_common_tournament_type com_t_type
              ON  nt_t.competition_type_id = com_t_type.id
       LEFT JOIN  parser_common_tournament_group com_t_g
              ON  nt_t.competition_group_id = com_t_g.id
       LEFT JOIN  parser_common_national_team com_nt
              ON  nt_t.national_team_id = com_nt.id
           WHERE  fc.name_post = '$competition'
           		    $competition_type_sql
             			$competition_group_sql ";
	$result = $wpdb->get_results( $sql);
	$result = array_group($result);

	return $result;
}

function get_world_cup_qualification_overview($competition , $competition_type = false, $competition_group = false)
{
	global $wpdb;
	if($competition_type){
		$competition_type_sql = "AND  com_t_type.name ='".$competition_type."'";
	}
	if($competition_group){
		$competition_group_sql = "AND  com_t_g.name = '".$competition_group."'";
	}
	$sql= "SELECT	 wc_qo.post_content
			   	FROM	 foot_football_world_cup_qualification_overview  wc_qo
		 LEFT JOIN   parser_common_tournament fc
            ON   wc_qo.competition_id = fc.id
     LEFT JOIN   parser_common_tournament_type com_t_type
            ON   wc_qo.competition_type_id = com_t_type.id
     LEFT JOIN   parser_common_tournament_group com_t_g
            ON   wc_qo.competition_group_id = com_t_g.id
			   WHERE   fc.name_post = '$competition'
			           $competition_type_sql
           	     $competition_group_sql ";
	$result = $wpdb->get_results( $sql);
	return $result;
}
function get_world_cup_result_matches_group($competition , $competition_type = false, $competition_group = false) {
    global $wpdb;
    $sql= "SELECT fc.name_post AS tournament,
                  com_t_type.name AS tournament_type,
                  com_t_g.name AS tournament_group ,
                  matches_nt.competition_tour ,
                  ft1.name AS team_1 ,
                  ft2.name AS team_2 ,
                  matches_nt.play_date , matches_nt.play_time , matches_nt.result_1 , matches_nt.result_2
             FROM foot_football_national_team_matches matches_nt
        LEFT JOIN parser_common_tournament fc
               ON matches_nt.competition_id = fc.id
        LEFT JOIN parser_common_tournament_type com_t_type
               ON matches_nt.competition_type_id = com_t_type.id
        LEFT JOIN parser_common_tournament_group com_t_g
               ON matches_nt.competition_group_id = com_t_g.id
        LEFT JOIN parser_common_national_team  ft1
               ON matches_nt.nt_id_1 = ft1.id
        LEFT JOIN parser_common_national_team  ft2
               ON matches_nt.nt_id_2 = ft2.id
            WHERE fc.name_post = '$competition '
					    AND com_t_type.name = '$competition_type '
					    AND com_t_g.name = '$competition_group '
              AND matches_nt.status = 1
              AND matches_nt.season_id = '2018'
         ORDER BY matches_nt.play_date DESC";


    $result = $wpdb->get_results( $sql );
  	//echo "<pre>".print_r($sql)."</pre>";
    $innerHTML = "<table class='footable next_match match_in_g'>";
        $innerHTML .= "<thead>
                          <tr>
                              <th style='display: none;'></th>
                              <th class='competition'>Турнир</th>
                              <th data-hide='phone,tablet'>Тур</th>
                              <th data-hide='phone,tablet'>Дата игры</th>
                              <th data-hide='phone,tablet'>Хозяева</th>
                              <th data-hide='phone,tablet'></th>
                              <th data-hide='phone,tablet'>Гости</th>
                          </tr>
                  </thead>";
         if($result) {
             foreach ($result as $item_match) {
              $innerHTML .= '<tr>
                               <td style="display: none;"></td>
                               <td>'.$item_match->tournament.'</td>
                               <td>'.$item_match->competition_tour.'</td>
                               <td>'.$item_match->play_date.'</td>
                               <td><i class="flag_nt flag_'.translit($item_match->team_1).'" title="'.$item_match->team_1.'" alt="'.$item_match->team_1.'"></i>'.$item_match->team_1.'</td>
                               <td>'.$item_match->result_1.'-'.$item_match->result_2.'</td>
                               <td><i class="flag_nt flag_'.translit($item_match->team_2).'" title="'.$item_match->team_2.'" alt="'.$item_match->team_2.'"></i>'.$item_match->team_2.'</td>
                            </tr>';
            }
        }else{
           $innerHTML .= '<tr>
                           <td style="display: none;"></td>
                           <td colspan="8"><div class="no_date">Нет данных</div></td>
                          </tr>';
        }
    $innerHTML .= "</table>";
    return $innerHTML;
}
