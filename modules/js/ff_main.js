jQuery(function($) {
   //Краткоя карточка клуба
   var btn_active;
   $('.show_more_s').on('click' , function(){
        var main_block = $(this).parents('.item_short_stat_club');
        main_block.find('tr.more_info').toggle();



        if(btn_active == true){
          btn_active = false;
          $(this).html('Развернуть...');
             $('body, html').animate({ scrollTop: main_block.offset().top - 50}, 700);
        }else{
          btn_active = true;
          $(this).html('Свернуть...');
        }
        return false;
   })
   $('#season_tr_in , #season_tr_out').change( function(){
    club_name = $(this).data('club_name');
    type_transfer = $(this).data('type_transfer');
    season_t = $(this).val();
    var data = {
            'action': 'ff_get_transfers_club',
            'club_name': club_name,
            'season_t': season_t,
            'type_transfer': type_transfer
        };
    $.ajax({
      url: 'https://football-fun.ru/wp-admin/admin-ajax.php', // обработчик
      data: data,
      type: 'POST', // тип запроса
      success:function(data){
        console.log(data);
        if(type_transfer == 'in'){
          $('.ff_transfers_in tbody').html(data);
        }else{
          $('.ff_transfers_out tbody').html(data);
        }

      }
    });
    return false;
   })



  $('#flatpickr_delivery_date').flatpickr({
        "wrap": "true",
        "defaultDate" : "today" ,
        "altInput" : true,
        "altFormat": "l, d F Y" ,
        "dateFormat": "Y-m-d" ,
        locale: {
          firstDayOfWeek: 1,
          weekdays: {
            shorthand: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            longhand: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
          },
          months: {
            shorthand: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            longhand: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            longhandMod: ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'],
          },
        },
  });
  $('.date_caL_link').on('click' , function(event){
      event.preventDefault();
      var id = $(this).attr('id'),
          type_chenge = false;

      if(id === 'next_date_mc'){
        type_chenge = true;
      }

      console.log(type_chenge);

      var delivery_date = $('#delivery_date').val();

      var carent_date = get_custom_date(delivery_date , type_chenge );

      console.log(carent_date);


      $('#flatpickr_delivery_date').flatpickr({
        "wrap": "true",
        "defaultDate" : carent_date ,
        "altInput" : true,
        "altFormat": "l, d F Y" ,
        "dateFormat": "Y-m-d" ,
        locale: {
          firstDayOfWeek: 1,
          weekdays: {
            shorthand: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            longhand: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
          },
          months: {
            shorthand: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
            longhand: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            longhandMod: ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'],
          },
        },
      });
      var data = {
            'action': 'getmatchecustom',
            "curent_date" : carent_date

        };
      $.ajax({
        url: 'https://football-fun.ru/wp-admin/admin-ajax.php', // обработчик
        data: data,
        type: 'POST', // тип запроса
        success:function( data, textStatus, XMLHttpRequest ){
          var td_data_object = jQuery.parseJSON( data ); //get the data object
          console.log(td_data_object);
          $('#mc_tab_custom_date').html( td_data_object[1]);
          $('.mc-tab-data').removeClass('_selected');
          $('#mc_tab_custom_date').addClass('_selected');

        },
         error : function(error){ console.log(error) }
      });

      console.log(carent_date);



  })

  $('.flatpickr-input').on('change' , function(){
    $('.date_show_match').removeClass('_selected');
    console.log($(this).val());
    var curent_date = $(this).val();
    var data = {
            'action': 'getmatchecustom',
            "curent_date" : curent_date

        };
    $.ajax({
      url: 'https://football-fun.ru/wp-admin/admin-ajax.php', // обработчик
      data: data,
      type: 'POST', // тип запроса
      success:function( data, textStatus, XMLHttpRequest ){
        var td_data_object = jQuery.parseJSON( data ); //get the data object
        console.log(td_data_object);
        $('#mc_tab_custom_date').html( td_data_object[1]);
        $('.mc-tab-data').removeClass('_selected');
        $('#mc_tab_custom_date').addClass('_selected');

      },
       error : function(error){ console.log(error) }
    });

    console.log(curent_date);


  })
  $('.curent_matches_list_comp').on('click' , function(){
    var curent_competition = $(this).data('comp');
    var curent_date = $('.flatpickr-input').val();
    var data = {
            'action': 'getmatchecustom',
            "curent_competition" : curent_competition ,
            "curent_date" : curent_date

        };
    $.ajax({
      url: 'https://football-fun.ru/wp-admin/admin-ajax.php', // обработчик
      data: data,
      type: 'POST', // тип запроса
      success:function( data, textStatus, XMLHttpRequest ){
        var td_data_object = jQuery.parseJSON( data ); //get the data object
        console.log(td_data_object);
        $('#mc_tab_custom_date').html( td_data_object[1]);
        $('.mc-tab-data').removeClass('_selected');
        $('#mc_tab_custom_date').addClass('_selected');

      },
       error : function(error){ console.log(error) }
    });

    console.log(curent_competition);
    console.log(curent_date);
    return false;
  })

  function get_custom_date(delivery_date , $type_chenge){
    date_r = new Date(delivery_date);
    if($type_chenge == true){
      final_date = new Date(date_r.getTime() + (24 * 60 * 60 * 1000));
    }else{
      final_date = new Date(date_r.getTime() - (24 * 60 * 60 * 1000));
    }
    var dd = final_date.getDate();
    var mm = final_date.getMonth()+1; //January is 0!
    var yyyy = final_date.getFullYear();
    if(dd<10){
        dd='0'+dd;
    }
    if(mm<10){
        mm='0'+mm;
    }
    return  yyyy+'-'+mm+'-'+dd;

  }
  function td_subcat_leight_list()
  {
    width_all = 0 ;
    $('.ff_match_center_comp_list > li').each(function( index ) {
      whidth_item = $( this ).outerWidth( true );
      width_all = width_all + whidth_item;

    });
    return width_all;
  }

  function subcat_func(){
   var whidth_block = $('.list_competition_mc ').width() ;
   var whidth_list = $('.ff_match_center_comp_list').width() ;
   var  whidth_more = 50;
   var  width_all = 0 ;

   var   width_all = td_subcat_leight_list();

    if(width_all > whidth_block -  whidth_more ){
      $('.td_subcat_list_more').append( $('.ff_match_center_comp_list >  li:last-child') );
      subcat_func();
    }

    /*if(whidth_block <  width_all -  whidth_more ){
      $('.td_subcat_more').hide();
    }else{
      $('.td_subcat_more').show();
    }*/
  }
  if($('ul').hasClass('ff_match_center_comp_list')){
    subcat_func();
  }



});
