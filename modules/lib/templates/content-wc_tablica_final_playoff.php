
<div class="bottom_header_wc bottom_header_wc_no_c">
	<div class="competition_type_wc">
		<ul>
			<li><a href="?competition_type=group"><span>Групповой турнир</span></a></li>
			<li><a  <?php set_carent_a_wc() ?> href="?competition_type=playoff"><span>Плей-офф</span></a></li>
		</ul>
	</div>
</div>
<div class="content_wc">
	<?php global $wpdb;
				$sql= "SELECT com_t_g.name AS tournament_group ,
											fc.name_post AS tournament,
											com_t_type.name AS tournament_type,
											matches_nt.competition_tour ,
											ft1.name AS team_1 ,
											ft2.name AS team_2 ,
											matches_nt.play_date , matches_nt.play_time , matches_nt.result_1 , matches_nt.result_2 ,
											matches_nt.result_match
								 FROM foot_football_national_team_matches matches_nt
						LEFT JOIN parser_common_tournament fc
									 ON matches_nt.competition_id = fc.id
						LEFT JOIN parser_common_tournament_type com_t_type
									 ON matches_nt.competition_type_id = com_t_type.id
						LEFT JOIN parser_common_tournament_group com_t_g
									 ON matches_nt.competition_group_id = com_t_g.id
						LEFT JOIN parser_common_national_team  ft1
									 ON matches_nt.nt_id_1 = ft1.id
						LEFT JOIN parser_common_national_team  ft2
									 ON matches_nt.nt_id_2 = ft2.id
								WHERE fc.name_post = 'Финал'
									AND com_t_type.name = 'Плей-офф'
									AND matches_nt.season_id = '2018'
						 ORDER BY matches_nt.play_date DESC {$lim} ";

				$result = $wpdb->get_results( $sql );
				$result = array_group($result);
				//echo "<pre>".print_r($result)."</pre>";
				foreach ($result as $key => $item_tournament_group) {
			    $innerHTML  .= '<table class="table_tout_in_g table_tout_in_g_nt">';
			      $innerHTML  .= '<tr><th colspan="6" class="tournament_group">'.$key.'</th></tr>';
			      foreach ($item_tournament_group as $key => $item_match) {
			        $team_winner = '';

			        //Результат 1 встечи
			        if ($item_match->result_match == 0) {
			          $result_match = '-:-';
			        }else{
			          $result_match = ''.$item_match->result_1.' : '.$item_match->result_2.' <span> '.$item_match->result_extra_time.'</span>';

			        }

			        switch ($item_match->result_match) {
			          case  1:

			          case  2:

			            break;
			          case  3:

			            break;
			          default:

			            break;

			        }



			        $innerHTML  .= '<tr>
																<td class="play_date">'.$item_match->play_date.'</td>
																<td class="play_time">'.$item_match->play_time.'</td>
																<td class="m_team_first">'.$item_match->team_1.'<i class="flag_nt flag_'.translit($item_match->team_1).'" title="'.$item_match->team_1.'" alt="'.$item_match->team_1.'"></i></td>
                                <td class="result_matches">'.$result_match.'</td>
                                <td class="team_second"><i class="flag_nt flag_'.translit($item_match->team_2).'" title="'.$item_match->team_2.'" alt="'.$item_match->team_2.'"></i>'.$item_match->team_2.'</td>
			                        </tr>';
			      }

			    $innerHTML .= '</table>';
			  }

				echo $innerHTML;

	 ?>
</div>
