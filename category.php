<?php
/*  ----------------------------------------------------------------------------
    the blog index template
 */

get_header();
global $loop_module_id, $loop_sidebar_position;

$current_category_id = get_query_var('cat');
$current_category_obj = get_category($current_category_id);

if ($current_category_obj->term_id == 19) {
    require_once('category_obzor_matches_main.php'); 
}elseif($current_category_obj->category_parent == 19){
    require_once('category_obzor_matches_child.php'); 
}else{
    require_once('category_default.php'); 
} ?>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/category.css">
<?php

get_footer();