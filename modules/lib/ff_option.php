<?php
//Текущий сезон
$ff_cfg['carent_season'] = 2017; 
$ff_cfg['carent_season_transfer'] = '2018_winter'; 

//Дериктория картинок
$ff_cfg['dir_img']['dir_img_club'] = '/wp-content/images/club/';

$ff_cfg['db']['prefix'] = 'foot_';
$ff_cfg['db']['prefix_2'] = 'parser_';

// Общии таблицы
$ff_cfg['db_table']['common_amplua']           = $ff_cfg['db']['prefix_2'].'common_amplua';
$ff_cfg['db_table']['common_bookmaker']        = $ff_cfg['db']['prefix_2'].'common_bookmaker';
$ff_cfg['db_table']['common_city']             = $ff_cfg['db']['prefix_2'].'common_city';
$ff_cfg['db_table']['common_club']             = $ff_cfg['db']['prefix_2'].'common_club';
$ff_cfg['db_table']['common_country']          = $ff_cfg['db']['prefix_2'].'common_country';
$ff_cfg['db_table']['common_national_team']    = $ff_cfg['db']['prefix_2'].'common_national_team';
$ff_cfg['db_table']['common_player']           = $ff_cfg['db']['prefix_2'].'common_player';
$ff_cfg['db_table']['common_tournament']       = $ff_cfg['db']['prefix_2'].'common_tournament';
$ff_cfg['db_table']['common_tournament_group'] = $ff_cfg['db']['prefix_2'].'common_tournament_group';
$ff_cfg['db_table']['common_tournament_type']  = $ff_cfg['db']['prefix_2'].'common_tournament_type';
$ff_cfg['db_table']['common_season']           = $ff_cfg['db']['prefix_2'].'common_season';

// Алиасы
$ff_cfg['db_table']['common_alias_season']     = $ff_cfg['db']['prefix_2'].'common_alias_season';

/** Таблицы с даными **/
//Букмекеры
$ff_cfg['db_table']['football_bookmaker_coments']    = $ff_cfg['db']['prefix'].'football_bookmaker_coments';
//Клубы
$ff_cfg['db_table']['football_club']                     = $ff_cfg['db']['prefix'].'football_club';
$ff_cfg['db_table']['football_club_description']         = $ff_cfg['db']['prefix'].'football_club_description';
$ff_cfg['db_table']['football_club_matches']             = $ff_cfg['db']['prefix'].'football_club_matches';
$ff_cfg['db_table']['football_club_statistic']           = $ff_cfg['db']['prefix'].'football_club_statistic';
$ff_cfg['db_table']['football_club_to_competition']      = $ff_cfg['db']['prefix'].'football_club_to_competition';
$ff_cfg['db_table']['football_club_turnirnaya_tablitsa'] = $ff_cfg['db']['prefix'].'football_club_turnirnaya_tablitsa';
//Турниры
$ff_cfg['db_table']['football_competition_assistants']         = $ff_cfg['db']['prefix'].'football_competition_assistants';
$ff_cfg['db_table']['football_competition_bombardiers']        = $ff_cfg['db']['prefix'].'football_competition_bombardiers';
$ff_cfg['db_table']['football_competition_description']        = $ff_cfg['db']['prefix'].'football_competition_description';
$ff_cfg['db_table']['football_competition_term']               = $ff_cfg['db']['prefix'].'football_competition_term';
$ff_cfg['db_table']['football_competition_term_relationships'] = $ff_cfg['db']['prefix'].'football_competition_term_relationships';
//Национальные сборные
$ff_cfg['db_table']['football_national_team_assistant']        = $ff_cfg['db']['prefix'].'football_national_team_assistant';
$ff_cfg['db_table']['football_national_team_bombardires']      = $ff_cfg['db']['prefix'].'football_national_team_bombardires';
$ff_cfg['db_table']['football_national_team_matches']          = $ff_cfg['db']['prefix'].'football_national_team_matches';
$ff_cfg['db_table']['football_national_team_tournament_table'] = $ff_cfg['db']['prefix'].'football_national_team_tournament_table';
//Игроки
$ff_cfg['db_table']['football_player_description']    = $ff_cfg['db']['prefix'].'football_player_description';
$ff_cfg['db_table']['football_player_statistic']      = $ff_cfg['db']['prefix'].'football_player_statistic';
$ff_cfg['db_table']['football_player_statistic_club'] = $ff_cfg['db']['prefix'].'football_player_statistic_club';
$ff_cfg['db_table']['football_player_to_club']        = $ff_cfg['db']['prefix'].'football_player_to_club';
//Рейтинги
$ff_cfg['db_table']['football_rating_guardian_top_100_players'] = $ff_cfg['db']['prefix'].'football_rating_guardian_top_100_players';
$ff_cfg['db_table']['football_rating_uefa_top_100_clubs']       = $ff_cfg['db']['prefix'].'football_rating_uefa_top_100_clubs';
//Чемпионат мира
$ff_cfg['db_table']['football_world_cup']                        = $ff_cfg['db']['prefix'].'football_world_cup';
$ff_cfg['db_table']['football_world_cup_qualification_overview'] = $ff_cfg['db']['prefix'].'football_world_cup_qualification_overview';



//таблицы парсера
$ff_cfg['db_table']['football_transfers_club'] = $ff_cfg['db']['prefix_2'].'sports_ru_transfers';

function true_russian_date_forms($the_date = '') {
	if ( substr_count($the_date , '---') > 0 ) {
	return str_replace('---', '', $the_date);
	}

	$replacements = array(
	"Январь" => "января",
	"Февраль" => "февраля",
	"Март" => "марта",
	"Апрель" => "апреля",
	"Май" => "мая",
	"Июнь" => "июня",
	"Июль" => "июля",
	"Август" => "августа",
	"Сентябрь" => "сентября",
	"Октябрь" => "октября",
	"Ноябрь" => "ноября",
	"Декабрь" => "декабря"
	);
	
	return strtr($the_date, $replacements);
}
add_filter('the_time', 'true_russian_date_forms');
add_filter('get_the_time', 'true_russian_date_forms');
add_filter('the_date', 'true_russian_date_forms');
add_filter('get_the_date', 'true_russian_date_forms');
add_filter('the_modified_time', 'true_russian_date_forms');
add_filter('get_the_modified_date', 'true_russian_date_forms');
add_filter('get_post_time', 'true_russian_date_forms');
add_filter('get_comment_date', 'true_russian_date_forms');