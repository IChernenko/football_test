<?php

class td_action_ajax {

	static function on_ajax_get_result_matches()
	{
		$json_result_matches_fail = json_encode(array('result_matches', 0, 'Ошибка', TD_THEME_NAME));

		if (!empty($_POST['competition'])) {
			$competitiom_name = $_POST['competition'];
		}
		if (!empty($_POST['period'])) {
			$period = $_POST['period'];

			switch ($period) {
				case 'yesterday':
					$show_on_date = td_football_app::get_yesterday_date();
					break;
				case 'today':
					$show_on_date = td_football_app::get_today_date();
					break;
				case 'tomorrow':
					$show_on_date = td_football_app::get_tomorrow_date();
					break;
				default:
					$show_on_date = false;
					break;
			}
			if (!empty($_POST['limit'])) {
				$limit_show = $_POST['limit'];
			}else{
				$limit_show = false;
			}
		}

		//
		if (!empty($competitiom_name)) {
			$result_matches = td_statistic_football_matches::get_result_matches($competitiom_name,  "DESC" , $limit_show , $show_on_date);
			if( count($result_matches) > 0){
				$result = $result_matches;
			}else{
				$result = '';
			}
			die(json_encode(array('result_matches', $result , $period , 'OK')));

		} else {
			die($json_result_matches_fail);
		}

	}

	static function on_ajax_get_schedule_matches()
	{
		$json_schedule_matches_fail = json_encode(array('schedule_matches', 0, 'Ошибка', TD_THEME_NAME));

		if (!empty($_POST['competition'])) {
			$competitiom_name = $_POST['competition'];
		}
		if (!empty($_POST['period'])) {
			$period = $_POST['period'];

			switch ($period) {
				case 'yesterday':
					$show_on_date = td_football_app::get_yesterday_date();
					break;
				case 'today':
					$show_on_date = td_football_app::get_today_date();
					break;
				case 'tomorrow':
					$show_on_date = td_football_app::get_tomorrow_date();
					break;
				default:
					$show_on_date = false;
					break;
			}
		}
		if (!empty($_POST['limit'])) {
			$limit_show = $_POST['limit'];
		}else{
			$limit_show = false;
		}

		//
		if (!empty($competitiom_name) ) {
			$schedule_matches = td_statistic_football_matches::get_schedule_matches($competitiom_name,  "ASC" , $limit_show , $show_on_date);
			if( count($schedule_matches) > 0){
				$result = $schedule_matches;
			}else{
				$result = '';
			}
			die(json_encode(array('schedule_matches', $result , $period , 'OK')));

		} else {
			die($json_schedule_matches_fail);
		}

	}

	// Ajax запрос
  static function mc_ajax_get_matches_list_calendar()
  {
  	$list_matches = new td_football_match_center;

  	$list_matches_array = $list_matches->get_matches_list_array($_POST['curent_competition'] , $_POST['curent_date'] );

  	if($list_matches_array){
  		$data_array = array_group_by($list_matches_array , 'play_date' , 'competition' );

	  	foreach ($data_array as $key => $date_mc) {
	  		$list_matches_render .= $list_matches->get_list_matches_render($date_mc);
	  	}
  		die(json_encode(array('matches_list', $list_matches_render , 'OK')));
  	}else{
  		$list_matches_render =  '<div class="no_matches_mc">Нет матчей</div>';
  		die(json_encode(array('matches_list', $list_matches_render , 'false')));
  	}

  	

  }
  
}