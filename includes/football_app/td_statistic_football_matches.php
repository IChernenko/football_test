<?php
class td_statistic_football_matches extends td_football_app  {
 
	static function get_static_football_matches_all()
	{ 
		$result_matches = self::mc_get_result_matches('news_matches_result' );
		$schedule_matches = self::mc_get_schedule_matches('news_matches_shedule' );
		$schedule_matches_rfpl = self::mc_get_schedule_matches('news_matches_shedule_rfpl');

		//self::render(self::get_name_competition_whith_id_category($current_category_obj->term_id));
		?>
		<div class="td-container-wrap td_statistic_matches">
			<div class="td-container">
				<div class="td-pb-row">
					<div class="td-pb-span4 item_block_matches">
						<div class="name_block_item_sfm name_block_item_sfm_main">
							<span>Результаты  матчей</span>
						</div>
						<div class='football_matches result_football_matches'>
							<?php echo $result_matches;  ?>
						</div>
					</div>
					<div class="td-pb-span4 item_block_matches">
						<div class="name_block_item_sfm name_block_item_sfm_main">
							<span>Расписание матчей</span>
						</div>
						<div class='football_matches schedule_football_matches'>
					  	<?php echo $schedule_matches ; ?>
			  		</div>
					</div>
					<div class="td-pb-span4 item_block_matches">
						<div class="name_block_item_sfm name_block_item_sfm_main">
							<span>Расписание РФПЛ</span>
						</div>
						<div class='football_matches schedule_football_matches schedule_football_matches_rfpl'>
           		 <?php echo $schedule_matches_rfpl;  ?>
            </div>
					</div>
				</div>
			</div>
		</div>
	<?}
	static function get_static_football_matches($current_category_obj)
	{
		$competitiom_name = self::get_name_competition_whith_id_category($current_category_obj->term_id);
		$result_matches = self::get_result_matches($competitiom_name , 'DESC' ,  10);
		$schedule_matches = self::get_schedule_matches($competitiom_name, 'ASC' , 10);
		?>
		<div class="td-container-wrap td_statistic_matches">
			<div class="td-container">
				<div class="td-pb-row">
					<div class="td-pb-span6 item_block_matches">
						<div class="name_block_item_sfm">
							<span>Результаты  матчей</span>
						</div>
						<div class='football_matches result_football_matches'>
							<?php echo $result_matches;  ?>
						</div>
					</div>
					<div class="td-pb-span6 item_block_matches">
						<div class="name_block_item_sfm">
							<span>Расписание матчей</span>
						</div>
						<div class='football_matches schedule_football_matches'>
				    	<?php echo $schedule_matches ; ?>
		  	   	</div>
					</div>
				</div>
			</div>
		</div>
		<? //self::render(self::get_name_competition_whith_id_category($current_category_obj->term_id));
	}
	
	static function get_schedule_matches($competitiom_name , $sort ,  $limit , $show_on_date = false)
  {
  	$dir_img_club =  parent::$dir_img_club;
  	$result = self::get_matches($competitiom_name , 0 , $sort ,  $limit , $show_on_date);

		  foreach ($result as $item_match) {
		  	$date = date_create($item_match->play_date);
		  	if($item_match->play_time){
		  		$play_time = $item_match->play_time;
		  	}else{
		  		$play_time = '-:-';
		  	}
		  	if(!empty($item_match->team_1_short)){
		  		$name_team_1 = $item_match->team_1_short;
		  	}else{
		  		$name_team_1 = $item_match->team_1;
		  	}
		  	
		  		$name_team_2 = $item_match->team_2_short;
		  
		    $innerHTML .= '<div class="item_result_match item_result_match_teams item_result_match_2">
		    								<span class="play_date">'.date_format($date, 'd.m.').'</span>
		    								<span class="team_1">'.$name_team_1.'<img src="'.$dir_img_club.''.$item_match->logo_t1.'" alt="'.$item_match->team_1.'" class="logo_club"></span> <span class="result result_1">'.$play_time.'</span> <span class="team_2"><img src="'.$dir_img_club.''.$item_match->logo_t2.'" alt="'.$item_match->team_2.'" class="logo_club">'.$name_team_2.'</span>

		    							</div>';
		}

		return $innerHTML;
  }
  static function get_result_matches($competitiom_name , $sort ,  $limit , $show_on_date = false)
  {
  	$dir_img_club =  parent::$dir_img_club;
  	$result = self::get_matches($competitiom_name , 1 , $sort ,  $limit , $show_on_date);
		foreach ($result as $item_match) {
		  	 $date = date_create($item_match->play_date);
		  	switch ($item_match->result_match) {
				  case 1:
				  $teams = '<span class="team_1"><b>'.$item_match->team_1.'</b><img src="'.$dir_img_club.''.$item_match->logo_t1.'" alt="'.$item_match->team_1.'" class="logo_club"></span> <span class="result result_1">'.$item_match->result_1.':'.$item_match->result_2.'</span> <span class="team_2"><img src="'.$dir_img_club.''.$item_match->logo_t2.'" alt="'.$item_match->team_1.'" class="logo_club">'.$item_match->team_2.'</span> ';
				      break;
				  case 2:
				  			  $teams = '<span class="team_1">'.$item_match->team_1.'<img src="'.$dir_img_club.''.$item_match->logo_t1.'" alt="'.$item_match->team_1.'" class="logo_club"></span>  <span class="result result_1">'.$item_match->result_1.':'.$item_match->result_2.'</span> <span class="team_2"><img src="'.$dir_img_club.''.$item_match->logo_t2.'" alt="'.$item_match->team_1.'" class="logo_club"><b>'.$item_match->team_2.'</b></span>';
				      break;
				  case 3:
				      	  $teams = '<span class="team_1">'.$item_match->team_1.'<img src="'.$dir_img_club.''.$item_match->logo_t1.'" alt="'.$item_match->team_1.'" class="logo_club"></span>  <span class="result result_1">'.$item_match->result_1.':'.$item_match->result_2.'</span> <span class="team_2"><img src="'.$dir_img_club.''.$item_match->logo_t2.'" alt="'.$item_match->team_1.'" class="logo_club">'.$item_match->team_2.'</span>';
				      break;
			  }
		    $innerHTML .= "<div class='item_result_match item_result_match_teams item_result_match_2'>
		    								<span class='play_date'>".date_format($date, 'd.m.')."</span>
		    								".$teams."
		    							</div>";
		}
		return $innerHTML;

  }
  static function mc_get_result_matches($competitiom_name )
  {

  	$result = self::mc_get_matches_array($competitiom_name);

  	if(count($result)){
  		foreach ($result[$competitiom_name] as $post) {
				$team_1 = carbon_get_post_meta( $post->ID, 'crb_team_1' );
	      $team_2 = carbon_get_post_meta( $post->ID, 'crb_team_2' );
	      $result_1 = carbon_get_post_meta( $post->ID, 'crb_result_1' );
	      $result_2 = carbon_get_post_meta( $post->ID, 'crb_result_2' );
	      $play_date = carbon_get_post_meta( $post->ID, 'crb_play_date' );
	      $play_time = carbon_get_post_meta( $post->ID, 'crb_play_time' );
	      $date = DateTime::createFromFormat('Y-m-d', $play_date);

	      $show_post = self::mc_filter_matches_play('result_matches' , $play_date , $play_time );

	      if($show_post == true){

		      switch (true) {
		      	case $result_1 > $result_2:
		      		$teams = '<span class="team_1"><b>'.$team_1.'</b></span><span class="result result_1">'.$result_1.':'.$result_2.'</span> <span class="team_2">'.$team_2.'</span>';
		      		break;
		      	case $result_2 > $result_1:
		      		$teams = '<span class="team_1"><b>'.$team_1.'</b></span><span class="result result_1">'.$result_1.':'.$result_2.'</span> <span class="team_2">'.$team_2.'</span>';
		      		break;
		      	default:
		      		$teams = '<span class="team_1">'.$team_1.'</span> <span class="result result_1">'.$result_1.':'.$result_2.'</span> <span class="team_2">'.$team_2.'</span>';
		      		break;
		      }


		       $innerHTML .= "<div class='item_result_match item_result_match_teams item_result_match_2'>
					  								<span class='play_date'>".date_format($date, 'd.m.')."</span>
					  								".$teams."
					  							</div>";
				}else{

				}

			}
  	}
		

		return $innerHTML;

  }
  static function mc_get_schedule_matches($competitiom_name )
  {
		$result = self::mc_get_matches_array($competitiom_name);

		if(count($result)){
			foreach ($result[$competitiom_name] as $item) {

		  	$team_1 = carbon_get_post_meta( $item->ID, 'crb_team_1' );
	      $team_2 = carbon_get_post_meta( $item->ID, 'crb_team_2' );
	      $play_date = carbon_get_post_meta( $item->ID, 'crb_play_date' );
	      $play_time = carbon_get_post_meta( $item->ID, 'crb_play_time' );
	      $result_1 = carbon_get_post_meta( $item->ID, 'crb_result_1' );
	      $result_2 = carbon_get_post_meta( $item->ID, 'crb_result_2' );
	      $date = DateTime::createFromFormat('Y-m-d', $play_date);

	      $show_post = self::mc_filter_matches_play('schedule_matches' , $play_date , $play_time );

		      if ( !$play_time ) $play_time = '-:-';
			
				   $innerHTML .= "<div class='item_result_match item_result_match_teams item_result_match_2'>
		    								<span class='play_date'>".date_format($date, 'd.m.')."</span>
		    								<span class='team_1'>".$team_1 ."</span> <span class='result result_1'>".$play_time."</span> <span class='team_2'>".$team_2."</span>
		    							</div>";


			}
		}
		

		return $innerHTML;

  }
  static function mc_get_matches_array($position_type) {
        $args = array(
            'post_type' => 'mc_match_center',
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'mc_view_pos',
                    'field'    => 'slug',
                    'terms'    => array($position_type),
                ),
            ),
        );
        $posts = get_posts( $args );

        $posts_in_date = [];

        foreach ( $posts as $post ) {
            $items = carbon_get_post_meta( $post->ID, 'crb_broadcast_data' );

            foreach ( $items as $item ) {
                $start_date = $item['crb_start'];
                $end_date = $item['crb_end'];
                $now = self::mc_get_datetime_now();

                if ( self::mc_check_in_range($start_date, $end_date, $now ) ) {
                    $term = get_term_by( 'id' , $item['crb_position_tax_id'], 'mc_view_pos' );
                    $posts_in_date[$term->slug][] = $post;
                }
            }

        }

        return $posts_in_date;
  }

  static function mc_check_in_range($start_date, $end_date, $date_from_user) {
      $start_ts = strtotime($start_date);
      $end_ts = strtotime($end_date);
      $user_ts = strtotime($date_from_user);

      return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
  }
  static function mc_get_datetime_now() {
      $tz_object = new DateTimeZone('Europe/Kiev');
      $datetime = new DateTime();
      $datetime->setTimezone($tz_object);

      return $datetime->format('Y\-m\-d\ H:i:s');
  }

  static function get_matches($competitiom_name = 'full' , $status_matches , $sort = 'DESC' ,  $limit  , $show_on_date = false )
  {
  	global $wpdb;
  	$competitiom_name = $competitiom_name ; // Названия турнира
  	$status_matches = $status_matches ; // Стус матча1
  	$sort = $sort ; // Сортировать
  	$limit = $limit ; // Количество записей выводить

  	//Таблицы
  	$table_common_tournament =  parent::$table_common_tournament;
  	$table_common_club =  parent::$table_common_club;
  	$table_club_matches =  parent::$table_club_matches;

  	//Названия турнира
  	if($competitiom_name == 'full'){
  		$competition = '';
  	}else{
  		$competition = "AND com_turnament.name_post = '".$competitiom_name."' ";
  	}

  	// Показать матчи на указаную дату
  	if($show_on_date == false){
  		$show_matches_date = '';
  	}else{
  		$show_matches_date = "AND cm.play_date = '".$show_on_date."'";
  	}
  	
  	//Количество выводимых постов
  	if($limit == false){
  		$limit_show = '';
  	}else{
  		$limit_show = "LIMIT 0 , ".$limit."";
  	}


  	$sql = "SELECT cm.play_date ,  cm.play_time,
  								 com_turnament.name AS competition  ,  
  								 t_1.name AS team_1 ,  
  								 t_1.name_post AS team_1_short ,  
  								 t_2.name AS team_2 , 
  								 t_2.name_post AS team_2_short , 
  								 t_1.logo_link AS logo_t1 , 
  								 t_2.logo_link AS logo_t2 ,
  								 cm.result_match, cm.result_1 , cm.result_2
  					  FROM $table_club_matches   cm 
  					  JOIN $table_common_tournament com_turnament 
  					    ON cm.competition_id = com_turnament.id 
  					  JOIN $table_common_club t_1 
  					    ON cm.club_id_1 = t_1.id  
  					  JOIN $table_common_club t_2 
  					    ON cm.club_id_2 = t_2.id   
  					 WHERE cm.status = $status_matches
  					 			 $competition
  					 			 $show_matches_date
  				ORDER BY cm.play_date $sort  
  				         $limit_show";
  	//echo $sql;
  	$result = $wpdb->get_results( $sql);
  	return $result;
  }

	private function get_name_competition_whith_id_category($id_category)
	{
		switch ($id_category) {
			case 29:
				$name_competition = 'Россия. Премьер-Лига';
				break;
			case 30:
				$name_competition = 'Англия. Премьер-Лига';
				break;
			case 31:
				$name_competition = 'Испания. Ла Лига';
				break;
			case 32:
				$name_competition = 'Германия. Бундеслига';
				break;
			case 33:
				$name_competition = 'Италия. Серия А';
				break;
			case 34:
				$name_competition = 'Франция. Лига 1';
				break;
			case 35:
				$name_competition = 'Лига Чемпионов';
				break;
			case 36:
				$name_competition = 'Лига Европы';
				break;
			default: 
				$name_competition = 'full';
				break;
		}
		return $name_competition;
	}

	public function mc_filter_matches_play($type , $date , $time)
    {

       $datatime_now = self::mc_get_datetime_now();

       $datatime_now = explode(' ', $datatime_now);
       $date_now = strtotime($datatime_now[0]);
       $time_now = strtotime($datatime_now[1]);

       $play_date = strtotime($date);
       $play_time = strtotime($time);

      
       
       if($type == 'schedule_matches'){

         if($play_date >= $date_now and $play_time > $time_now){
            $show_match = true;

         }else{
             $show_match = false;
         }


         
      }elseif($type == 'result_matches'){
            if($date_now == $play_date && $time_now > $play_time  ){
                $show_match = true;
            }elseif($date_now > $play_date ){
                $show_match = true;

            }else{
                $show_match = false;
            }
      }
      return $show_match;
    }
}