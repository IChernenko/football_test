jQuery(document).ready(function(){
    jQuery('a.action_compare_act').fancybox({});
    // the compare basket
    function CompareBasket() {
        compare_basket = jQuery('.compare_basket ');
        
        this.itemsAllowed = 2;
        this.totalItems = 0;
        this.items = [];

        
    }
    CompareBasket.prototype.add = function(item) {
        // check limit
        if( this.isFull() ) {
            return false;
        }
        compare_list = jQuery('.compare_list');
        item_compare = '<div class="compare_item" data-id="'+jQuery(item).val()+'"> <img src="'+jQuery(item).data("img_url")+'" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt=""> <button class="action action_remove"  type="submit" value="'+jQuery(item).val()+'" ><i class="fa fa-remove"></i></button> </div>';
        compare_list.prepend(item_compare);
        this.totalItems++;

    };
    CompareBasket.prototype.remove = function(item) {
        this.totalItems--;
        var preview = document.querySelector('[data-id= "' + jQuery(item).val() + '"]');
        preview.remove();
    };
    CompareBasket.prototype.activeCompareBasket = function() {
        if(this.totalItems >0){
            compare_basket.addClass('compare_basket_active');
        }else{
            compare_basket.removeClass('compare_basket_active');
        }
        if(this.totalItems == 2){
            jQuery('.action_compare').addClass('action_compare_act');
        }else{
            jQuery('.action_compare').removeClass('action_compare_act');
        }
    }
    CompareBasket.prototype.actionCompare = function(action) {
        if(this.totalItems == 2){
            jQuery.fancybox.open({
                src  : '#compare_b',
                type : 'inline',
            });
            compare_list = jQuery(".compare_list");
            ids=[]
            compare_list.find('.compare_item').each(function( index ) {
               ids.push(jQuery( this ).data('id'));
            })
            console.log(ids);
            var data = {
                    'action': 'myfilters',
                    'ids': ids,
                };
            jQuery.ajax({
              url: 'https://football-fun.ru/wp-admin/admin-ajax.php', // обработчик
              data: data,
              type: 'POST', // тип запроса
              success:function(data){
                jQuery('.list_compare_post').html(data);
                console.log(data);
              }
            });
        }else{
            return false;
        }
    };
    CompareBasket.prototype.isFull = function() {
        return this.totalItems === this.itemsAllowed;
    };
    function init() {
        // initialize an empty basket
        basket = new CompareBasket();
        initEvents();
    }
    function initEvents() {
       checkbox = jQuery('.item_title_bk').find('input[type = "checkbox"]');
       checkbox.checked = false;
            // ctrl to add to the "compare basket"
            checkbox.on('click', function(ev) {
                if( ev.target.checked ) {
                    if( basket.isFull() ) {
                        ev.preventDefault();
                        return false;
                    }
                    basket.add(this);
                    basket.isFull();
                }
                else {
                    basket.remove(this);
                }
                basket.activeCompareBasket();
                action_remove =  jQuery('.action_remove');
                action_remove.on('click', function(action) {
                    id_ch = jQuery(this).val();
                    checkbox_act = jQuery('.item_title_bk').find('input#title_filter_'+ id_ch);
                    checkbox_act.click();
                    return false;
                })
            });
        action_compare = jQuery('.action_compare');
            action_compare.on('click', function(action) {
                basket.actionCompare(this);
            })
    }

    init();
});