<?php
// Рейтинги
/*100 лучших футболистов The Guardian*/
function the_guardian_top_100_players($atts) {
  extract(shortcode_atts(array(
        'season' => 2017,
  ), $atts));
  global $wpdb;
  global $ff_cfg;
  global $dir_img_club;
  $sql= "SELECT DISTINCT  com_player.short_name AS player_name , 
                 com_nt.name AS national_team  ,
                 com_club.name AS club  ,
                 com_club.logo_link AS logo_link  ,
                 com_amplua.name AS amplua,
                 com_player.player_photo AS foto,
                 f_posts.post_name,
                 top_p_g.position
           FROM  {$ff_cfg['db_table']['football_rating_guardian_top_100_players']} top_p_g 
      LEFT JOIN  {$ff_cfg['db_table']['common_player']} com_player 
             ON  top_p_g.player_id = com_player.id  
      LEFT JOIN  {$ff_cfg['db_table']['common_club']} com_club 
             ON  top_p_g.club_id = com_club.id  
      LEFT JOIN  {$ff_cfg['db_table']['football_player_to_club']} player_club 
             ON  player_club.player_id = com_player.id  
      LEFT JOIN  {$ff_cfg['db_table']['common_country']} com_nt 
             ON  player_club.country_id = com_nt.id  
      LEFT JOIN  {$ff_cfg['db_table']['common_amplua']} com_amplua
             ON  player_club.amplua_id = com_amplua.id  
      LEFT JOIN  foot_posts f_posts 
             ON  com_player.name_post = f_posts.post_title 
            AND  f_posts.post_type = 'footbolist' 
            AND  f_posts.post_status = 'publish' 
          WHERE  top_p_g.season = '{$season}'
       ORDER BY top_p_g.position ASC LIMIT 0 , 100";
  $result = $wpdb->get_results( $sql);
  $innerHTML = "<table class='footable table_top_100_club'>";
    $innerHTML .= "<thead>
                    <tr>
                        <th style='display: none;'></th>
                        <th data-hide='phone,tablet'></th>
                        <th>Игрок</th>
                        <th data-hide='phone,tablet'>Клуб</th>
                        <th data-hide='phone,tablet'>Сборная</th>
                        <th data-hide='phone,tablet'>Амплуа</th>
                    </tr>
                 </thead>";
    foreach ($result as $item_top_player) {
        $innerHTML .=  "<tr>
                        <td style='display: none;'></td>
                        <td class='new_position'>".$item_top_player->position."</td>";
          if (  $item_top_player->post_name ) {
            $innerHTML .= '<td class="top_player">
                            <a href="/footbolist/'.$item_top_player->post_name.'">'.$item_top_player->player_name.'</a>
                            <a href="/footbolist/'.$item_top_player->post_name.'" target="_blank" class="btn btn_more_go">Жми <i class="fa fa-futbol-o" aria-hidden="true"></i></a>
                            </td>';
          }else{
            $innerHTML .= '<td class="top_player">
                               '.$item_top_player->player_name.'
                               <a href="#" target="_blank" class="btn btn_more_go">Жми <i class="fa fa-futbol-o" aria-hidden="true"></i></a>
                          </td>';
          }
          
         $innerHTML .= "
                        <td class='top_club'><img src='".$dir_img_club."".$item_top_player->logo_link."' class='logo_club' alt='".$item_top_player->club."'>".$item_top_player->club."</td>
                        <td class='strana'><i class='flag_nt flag_".translit($item_top_player->national_team)."' title='".$item_top_player->national_team."' alt='".$item_top_player->national_team."'></i>".$item_top_player->national_team."</td>
                        <td class='stat_2'>".$item_top_player->amplua."</td>
                    </tr>";
       }
  $innerHTML .= "</table>";
  return $innerHTML;
}
add_shortcode('the_guardian_top_100_players', 'the_guardian_top_100_players');
/*ТОП 100 клубов*/
function show_top_100_club() {
  global $wpdb;
  global $dir_img_club;
  $sql= "SELECT com_club.name AS club_name , com_club.logo_link,
                com_country.name AS country , 
                f_posts.post_name,
                top_c.season_id AS season , top_c.old_position , top_c.position ,  top_c.points_1 , top_c.points_2 , top_c.points_3 , top_c.points_4 , top_c.points_5 , top_c.points_all 
           FROM foot_football_rating_uefa_top_100_clubs top_c 
     INNER JOIN parser_common_club com_club 
             ON top_c.club_id = com_club.id  
     INNER JOIN parser_common_country com_country 
             ON top_c.country_id = com_country.id  
      LEFT JOIN foot_posts f_posts 
             ON com_club.name_post = f_posts.post_title 
            AND f_posts.post_type = 'f-club' 
            AND f_posts.post_status = 'publish' 
          WHERE 1
       ORDER BY  top_c.position ASC LIMIT 0 , 100";
  $result = $wpdb->get_results( $sql);
  
  //$result = ff_sorting_table($result , 'points_5' , 'DESC');

  $season_carent = $result[0]->season;

  $season = array();
  for ($x=-1; $x++<4;){
    $season[] = $season_carent - $x;
  }


  $innerHTML = '<table class="footable table_top_100_club tablesorter" data-page="false">';
  $innerHTML .= '<thead>
                  <tr>
                    <th style="display: none;"></th>
                    <th data-hide="phone,tablet" data-sort="false"></th>
                    <th data-sort="false">Клубы</th>
                    <th data-hide="phone,tablet" data-sort="false">Страна</th>
                    <th data-hide="phone,tablet" data-sort="true"> '.$season[4].'<i class="fa icon_sort" aria-hidden="true"></i></th>
                    <th data-hide="phone,tablet">'.$season[3].'<i class="fa icon_sort" aria-hidden="true"></i></th>
                    <th data-hide="phone,tablet">'.$season[2].'<i class="fa icon_sort" aria-hidden="true"></i></th>
                    <th data-hide="phone,tablet">'.$season[1].'<i class="fa icon_sort" aria-hidden="true"></i></th>
                    <th data-hide="phone,tablet"> '.$season[0].'<i class="fa icon_sort" aria-hidden="true"></i></th>
                    <th >Очки <i class="fa icon_sort" aria-hidden="true"></i></th>
                  </tr>
               </thead>';
    foreach ($result as $item_top_club) {
        $stan_pos = '';
        if(!empty($item_top_club->old_position) AND $item_top_club->old_position !== $item_top_club->position ){
          if($item_top_club->position > $item_top_club->old_position){
            $stan_pos = '<img src="'.get_stylesheet_directory_uri().'/images/delta_min.gif"  title="Предыдущее место: '.$item_top_club->old_position.'" alt="Предыдущее место: '.$item_top_club->old_position.'" class="stan_pos" >';
          }else{
            
            $stan_pos = '<img src="'.get_stylesheet_directory_uri().'/images/delta_plus.gif"  title="Предыдущее место: '.$item_top_club->old_position.'" alt="Предыдущее место: '.$item_top_club->old_position.'" class="stan_pos">';
          }
        }
        $innerHTML .=  '<tr>
                        <td style="display: none;"></td>
                        <td class="new_position">'.round($item_top_club->position).' '.$stan_pos.'</td>';
        if ($item_top_club->post_name) {
          $innerHTML .=   '<td class="top_club">
                          <a href="/f-club/'.$item_top_club->post_name.'" target="_blank" > <img src="'.$dir_img_club.''.$item_top_club->logo_link.'" class="logo_club" alt="'.$item_top_club->club_name.'">
                          '.$item_top_club->club_name.' </a>
                            <a href="/f-club/'.$item_top_club->post_name.'" target="_blank" class="btn btn_more_go">Жми <i class="fa fa-futbol-o" aria-hidden="true"></i></a>
                          </td>';
        }else{
          $innerHTML .=   '<td class="top_club"><img src="'.$dir_img_club.''.$item_top_club->logo_link.'" class="logo_club" alt="'.$item_top_club->club_name.'">
                        '.$item_top_club->club_name.' <a href="#" target="_blank" class="btn btn_more_go">Жми <i class="fa fa-futbol-o" aria-hidden="true"></i></a></td>';

        }
        $innerHTML .=   
                        '<td class="strana"><i class="flag_nt flag_'.translit($item_top_club->country).'" title="'.$item_top_club->country.'" alt="'.$item_stat->country.'"></i>'.$item_top_club->country.'</td>
                        <td class="stat_1">'.round($item_top_club->points_1 , 3).'</td>
                        <td class="stat_2">'.round($item_top_club->points_2 , 3).'</td>
                        <td class="stat_3">'.round($item_top_club->points_3 , 3).'</td>
                        <td class="stat_4">'.round($item_top_club->points_4 , 3).'</td>
                        <td class="stat_5">'.round($item_top_club->points_5 , 3).'</td>
                        <td class="points">'.round($item_top_club->points_all , 3).'</td>
                    </tr>';
       }
  $innerHTML .= "</table>";
  return $innerHTML;
}
add_shortcode('show_top_100_club', 'show_top_100_club');
https://www.sports.ru/core/stat/tag/transfer/?args={%22tag_id%22:1363867,%22tag_type%22:%22team%22,%22transfer_period%22:%222018_winter%22,%22transfer_type%22:%22in%22,%22page%22:1,%22sort_field%22:%22date%22,%22sort_direction%22:%22DESC%22,%22version%22:2}