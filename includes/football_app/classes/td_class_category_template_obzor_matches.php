<?php
class td_class_category_template_obzor_matches extends td_category_template {

    private $current_category_id;
    private $current_category_obj;
    private $current_category_link;

    function __construct() {
        $this->current_category_id =  td_global::$current_category_obj->cat_ID;
        $this->current_category_obj =  td_global::$current_category_obj;
        $this->current_category_link = get_category_link($this->current_category_id);

         parent::__construct();
    }

    function render()
    {

    }
    public function get_breadcrumbs()
    {
        return parent::get_breadcrumbs(); 
    }
    public function get_title()
    {
        return parent::get_title();
    }
    public function get_description()
    {
        return parent::get_description();
    }
    public function get_category_list()
    {
        $buffy = '';
        $fmt = new IntlDateFormatter(
            'ru_RU',
            IntlDateFormatter::FULL,
            IntlDateFormatter::FULL,
            'Europe/Chisinau', // https://php.ru/manual/timezones.html
            IntlDateFormatter::GREGORIAN,
            'd MMMM y' // http://userguide.icu-project.org/formatparse/datetime
        );
            $buffy .= '<div class="obzor_categoli_list">';
                $posts = $this->get_category_post($this->current_category_obj->slug);

                foreach ($posts as $key => $value) {
                    $d1 = strtotime($value->post_date); // переводит из строки в дату
                    $date = date("Y-m-d", $d1); // переводит в новый формат
                    $value->post_date =  $date;
                    

                }
              //   print_r($posts);
                $result_array = json_decode(json_encode($posts), True);
                $result_array = array_group_by($result_array , 'post_date');
                $result = json_decode(json_encode($result_array), FALSE);
                
                foreach ($result as $key => $post_data) {
                    $buffy .=  '<div class="obzor_categoli_item">
                                            <div class="title_obzor_categoli_item" >
                                              <span>'.$fmt->format(strtotime($key)).'</span>
                                            </div>';
                                    $buffy .= $this->get_render_post($post_data);
                            $buffy .= '</div>';       
                }
            $buffy .= '</div>';
        $buffy .= '<div class="clearfix"></div>';

        return $buffy;
    }

    public function get_category_post($category_slug)
    {
        $td_query = "";
        $args = array(
                        'post_type' => 'post',
                        'posts_per_page' => -1,
                        'tax_query' => array( 
                                                'relation' => 'AND', // объединяем результаты по двум инструкциям
                                                array(
                                                    'taxonomy'         => 'category',
                                                    'field'                    => 'slug',
                                                    'include_children' => true ,
                                                    'terms'            => array($category_slug),
                                                ) 
                            ),
                        'date_query' => array(
                            'after' => '1 weeks ago',
                        ),
                        );
        $result = new WP_Query( $args );
        return $result->posts; 
    }
    public function get_render_post($posts , $single_cateroty = '')
    {

        $innerHTML = '';

        $td_block_layout = new td_block_layout();

        $td_post_count = 0; // the number of posts rendered

        $innerHTML .= $td_block_layout->open_row();
        foreach ($posts as $post ) {
            $td_module_8 = new td_module_8($post);
            $td_module_mx4 = new td_module_mx4($post);
                if ($td_post_count == 0) {
                    $innerHTML .= $td_block_layout->open4();
                    $innerHTML .= $td_module_mx4->render();
                    $innerHTML .= $td_block_layout->close4();
                }else{
                    $innerHTML .= $td_block_layout->open8();
                    $innerHTML .= $td_module_8->render();
                    $innerHTML .= $td_block_layout->close8();
                }


            $td_post_count++;
        }
        $innerHTML .= $td_block_layout->close_row();
        return $innerHTML ;

    }
}
