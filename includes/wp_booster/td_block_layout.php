<?php

/*
 *
 * This class makes the layout for blocks
 * used by:
 *  - blocks
 *  - includes/td_page_generator/td_template_layout.php (author page, tag page)
 */

class td_block_layout {


    var $row_is_open = false;
    var $span1_is_open = false;
    var $span2_is_open = false;
    var $span3_is_open = false;
    var $span4_is_open = false;
    var $span5_is_open = false;
    var $span6_is_open = false;
    var $span7_is_open = false;
    var $span8_is_open = false;
    var $span9_is_open = false;
    var $span10_is_open = false;
    var $span11_is_open = false;
    var $spans12_is_open = false;



    var $row_class = 'td-block-row';
    var $span1_class = 'td-block-span1';
    var $span2_class = 'td-block-span2';
    var $span3_class = 'td-block-span3';
    var $span4_class = 'td-block-span4';
    var $span5_class = 'td-block-span5';
    var $span6_class = 'td-block-span6';
    var $span7_class = 'td-block-span7'; // this one does not use rows
    var $span8_class = 'td-block-span8'; // this one does not use rows
    var $span9_class = 'td-block-span9'; // this one does not use rows
    var $span10_class = 'td-block-span10'; // this one does not use rows
    var $span11_class = 'td-block-span11'; // this one does not use rows
    var $span12_class = 'td-block-span12'; // this one does not use rows
    
    


    function open_row() {
        if ($this->row_is_open) {
            //open row only onece
            return;
        }

        $this->row_is_open = true;
        return "\n\n\t" . '<div class="' . $this->row_class . '">';
    }

    /**
     * closes the row - it does not check if the row is already closed
     * @return string
     */
    function close_row() {
        $this->row_is_open = false;
        return '</div><!--./row-fluid-->';
    }

    //span 1
    function open1() {
        if ($this->span1_is_open) {
            //open row only onece
            return;
        }
        $this->span1_is_open = true;
        return "\n\n\t" . '<div class="' . $this->span1_class . '">' . "\n";
    }

    function close1() {
        $this->span1_is_open = false;
        return "\n\t" . '</div> <!-- ./' . $this->span1_class . ' -->';
    }


    //span 2
    function open2() {
        if ($this->span2_is_open) {
            //open row only onece
            return;
        }
        $this->span2_is_open = true;
        return "\n\n\t" . '<div class="' . $this->span2_class . '">' . "\n";
    }

    function close2() {
        $this->span2_is_open = false;
        return "\n\t" . '</div> <!-- ./' . $this->span2_class . ' -->';
    }


    //span 3
    function open3() {
        if ($this->span3_is_open) {
            //open row only onece
            return;
        }
        $this->span3_is_open = true;
        return "\n\n\t" . '<div class="' . $this->span3_class . '">' . "\n";
    }

    function close3() {
        $this->span3_is_open = false;
        return "\n\t" . '</div> <!-- ./' . $this->span3_class . ' -->';
    }


    //span 4
    function open4() {
        if ($this->span4_is_open) {
            //open row only onece
            return;
        }
        $this->span4_is_open = true;
        return "\n\n\t" . '<div class="' . $this->span4_class . '">' . "\n";
    }

    function close4() {
        $this->span4_is_open = false;
        return "\n\t" . '</div> <!-- ./' . $this->span4_class . ' -->';
    }

    //span 5
    function open5() {
        if ($this->span5_is_open) {
            //open row only onece
            return;
        }
        $this->span5_is_open = true;
        return "\n\n\t" . '<div class="' . $this->span5_class . '">' . "\n";
    }

    function close5() {
        $this->span5_is_open = false;
        return "\n\t" . '</div> <!-- ./' . $this->span5_class . ' -->';
    }


    //span 6
    function open6() {
        if ($this->span6_is_open) {
            //open row only onece
            return;
        }
        $this->span6_is_open = true;
        return "\n\n\t" . '<div class="' . $this->span6_class . '">' . "\n";
    }

    function close6() {
        $this->span6_is_open = false;
        return "\n\t" . '</div> <!-- ./' . $this->span6_class . ' -->';
    }

    //span 7
    function open7() {
        if ($this->span7_is_open) {
            //open row only onece
            return;
        }
        $this->span7_is_open = true;
        return "\n\n\t" . '<div class="' . $this->span7_class . '">' . "\n";
    }

    function close7() {
        $this->span7_is_open = false;
        return "\n\t" . '</div> <!-- ./' . $this->span7_class . ' -->';
    }

    //span 8
    function open8() {
        if ($this->span8_is_open) {
            //open row only onece
            return;
        }
        $this->span8_is_open = true;
        return "\n\n\t" . '<div class="' . $this->span8_class . '">' . "\n";
    }

    function close8() {
        $this->span8_is_open = false;
        return "\n\t" . '</div> <!-- ./' . $this->span8_class . ' -->';
    }

    //span 9
    function open9() {
        if ($this->span9_is_open) {
            //open row only onece
            return;
        }
        $this->span9_is_open = true;
        return "\n\n\t" . '<div class="' . $this->span9_class . '">' . "\n";
    }

    function close9() {
        $this->span9_is_open = false;
        return "\n\t" . '</div> <!-- ./' . $this->span9_class . ' -->';
    }

    //span 10
    function open10() {
        if ($this->span10_is_open) {
            //open row only onece
            return;
        }
        $this->span10_is_open = true;
        return "\n\n\t" . '<div class="' . $this->span10_class . '">' . "\n";
    }

    function close10() {
        $this->span10_is_open = false;
        return "\n\t" . '</div> <!-- ./' . $this->span10_class . ' -->';
    }

    //span 11
    function open11() {
        if ($this->span11_is_open) {
            //open row only onece
            return;
        }
        $this->span11_is_open = true;
        return "\n\n\t" . '<div class="' . $this->span11_class . '">' . "\n";
    }

    function close11() {
        $this->span11_is_open = false;
        return "\n\t" . '</div> <!-- ./' . $this->span11_class . ' -->';
    }


    //span 12 - doesn't use rows
    function open12() {
        if ($this->span12_is_open) {
            //open only once
            return;
        }
        $this->span12_is_open = true;
        return "\n\n\t" . '<div class="' . $this->span12_class . '">' . "\n";
    }

    function close12() {
        $this->span12_is_open = false;
        return "\n\t" . '</div> <!-- ./' . $this->span12_class . ' -->';
    }


    /**
     * closes all the spans that are open and also the rows
     * @return string
     */
    function close_all_tags() {
        $buffy = '';
        if ($this->span6_is_open) {
            $buffy .= $this->close6();
        }

        if ($this->span4_is_open) {
            $buffy .= $this->close4();
        }

        if ($this->span3_is_open) {
            $buffy .= $this->close3();
        }

        if ($this->row_is_open) {
            $buffy .= $this->close_row();
        }

        return $buffy;
    }
}
