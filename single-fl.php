<?php
get_header();
?>
<div class="td-main-content-wrap td-container-wrap">

    <div class="td-container td-post-template-default <?php echo $td_sidebar_position; ?>">
        <?php require_once('breadcrumb.php');  ?>
        <div class="td-pb-row">
            <div class="td-pb-span12 td-main-content" role="main">
                <div class="td-ss-main-content main_content_page main_c_bk">
                    <?php the_content(); ?>

                    <div class="td-container">
                        <div class="td-pb-row">
                            <div class="td-pb-span12">
                                <?php comments_template('', true); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- /.td-pb-row -->
    </div> <!-- /.td-container -->
</div> <!-- /.td-main-content-wrap -->
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/modules/css/main_module.css">
<style type="text/css">
.main_content_page{ }
.main_content_page table td{
    font-size: 13px;
}
.main_content_page table tr td:first-child{
    width: 42%;
}
tr:nth-of-type(odd) {
    background: #eee;
}
.positiv_list ul{}
.positiv_list ul li , .negativ_list ul li{
    line-height: 22px;
    padding-left: 25px;
    position: relative;
    margin-bottom: 10px;
    list-style: none;
}
.positiv_list ul li:before{
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    content: "\f055";
    display: inline-block;
    color: #009688;
    font-size: 22px;
    position: absolute;
    left: 0;
}
.negativ_list ul li:before{
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    content: "\f056";
    display: inline-block;
    color: red;
    font-size: 22px;
    position: absolute;
    left: 0;
}
body.td-animation-stack-type0 .td-animation-stack .entry-thumb, body.td-animation-stack-type0 .post images{
    opacity: 1;
}
.wpb_animate_when_almost_visible{
    opacity: 1;
}
.main_content_page table.next_match_h tr td{
    width: 42%!important;
    text-align: left;
}
.main_content_page table.next_match_h tr td:first-child{
    width: 15%!important;
    text-align: center;
}
.main_content_page table tr{     text-align: center;}
.main_content_page table.turnirnaya-tablitsa tr td:first-child{
    width: 50px!important;
}
.main_content_page table.turnirnaya-tablitsa tr td:nth-child(2){
    width: auto!important;
        text-align: left;
}
.main_content_page table.turnirnaya-tablitsa tr td{
    width: 50px!important;
}
.turnirnaya-tablitsa th , .foot_best_bombardir th {text-align: center;}
.turnirnaya-tablitsa th:nth-child(2) , .foot_best_bombardir th:nth-child(2) ,
.foot_best_bombardir th:nth-child(3) {text-align: left;}
.main_content_page table.foot_best_bombardir tr td:first-child{
    width: 50px!important;
    text-align: center;
}
.main_content_page table.foot_best_bombardir tr td:nth-child(2),
.main_content_page table.foot_best_bombardir tr td:nth-child(3){
    width: auto!important;
    text-align: left;
}
.main_content_page table.foot_best_bombardir tr{     text-align: center;}
    </style>
<?php

get_footer();?>