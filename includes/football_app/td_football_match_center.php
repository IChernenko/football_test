<?php
class td_football_match_center  {

	public  $yesterday_date;
  public  $today_date;
  public  $tomorrow_date;


	public  $count_matches_yesterday = 0; //Количество матчей вчера
	public  $count_matches_today = 0; // Количество матчей сегодня
	public  $count_matches_tomorrow = 0; // Количество матчей завтра

	private $list_matches_obj ;  // Обьект списка матчей

	private $competition_obj ; // Обьект турниров
	private $competition_id_array ;

	public  function __construct() {

			$this->yesterday_date = td_football_app::get_yesterday_date();
			$this->today_date = td_football_app::get_today_date();
			$this->tomorrow_date = td_football_app::get_tomorrow_date();

			$this->competition_obj = $this->get_mc_competition_data();

			/*Получить массив ид турниров*/
	    foreach ($this->competition_obj as $key => $value) {
	    	$this->competition_id_array[] = $value->competition_id;
	    }

			$this->list_matches_obj = $this->get_matches_list_array();






  }
  public function get_list_competition_mc()
  {
  	$data_list_comp = array_group($this->competition_obj);
  	$innerHTML .= '<ul class="ff_match_center_comp_list">';
  		foreach ($data_list_comp as $key => $list_comp) {
        if( $key  == "Остальные чемпионаты" or $key  == "Сборные. Товарищеские матчи"  or $key  == "Лига Европы" or $key  == "Лига Чемпионов" ){
            foreach ($list_comp as $key2 => $value) {
              $innerHTML .='<li class="ff_li_parent">';
                if($value->competition == 'Финал'){
                  $innerHTML .= '<a class="curent_matches_list_comp" href="#" data-comp="'.$value->competition_id.'">Финал ЧМ 2018</a>';
                }else{
                  $innerHTML .= '<a class="curent_matches_list_comp" href="#" data-comp="'.$value->competition_id.'">'.$value->competition.'</a>';
                }


              $innerHTML .='</li>';
            }

        }else{
          $innerHTML .='<li class="ff_li_parent">';
            $innerHTML .= '<a href="#" >'.$key.'</a>' ;
            $innerHTML .= '<ul class="ff_ul_child">';
              foreach ($list_comp as $key2 => $value) {
                $innerHTML .= '<li><a class="curent_matches_list_comp" href="#" data-comp="'.$value->competition_id.'">'.$value->competition.'</a></li>';
              }
            $innerHTML .= '</ul>';
          $innerHTML .='</li>';
        }
  		}
  	$innerHTML .= '</ul>';
    $innerHTML .= '<div class="td_subcat_more" >
                          <a href="#">Еще <i class="td-icon-read-down"></i></a>
                          <ul class="td_subcat_list_more"></ul>
                        </div>';

  	return $innerHTML;
  }
  public function get_mc_competition_data()
  {
  	global $ff_cfg;
  	global $wpdb;
  	$sql=  "SELECT comp_term.name AS competition_group , com_comp.name_post AS competition , comp_term_rel.competition_id
	           FROM {$ff_cfg['db_table']['football_competition_term_relationships']} comp_term_rel
	     INNER JOIN {$ff_cfg['db_table']['football_competition_term']} comp_term
	             ON comp_term.id  = comp_term_rel.competition_term_id
	     INNER JOIN {$ff_cfg['db_table']['common_tournament']} com_comp
	             ON comp_term_rel.competition_id = com_comp.id
	          WHERE comp_term.parent_id = 1
	       ORDER BY  comp_term.sort ASC  , comp_term_rel.id ASC   ";
    $result = $wpdb->get_results( $sql);



  	return $result;
  }

  public function get_list_matches($data_array = false)
  {

    if($data_array){
    	$data_array = $data_array;
    }else{
    	$data_array = $this->list_matches_obj;
    }
	//print_r($data_array);
  	if($data_array){

  		$data_array = array_group_by($data_array , 'play_date' , 'competition' );

  		$innerHTML .= '<div class="ff_list_matches">';
  			foreach ($data_array as $key => $date_mc) {
  				if($key == $this->today_date){
  					$innerHTML .= '<div class="mc-tab-data today _selected" id="mc_tab_today">';
  				}

  				$innerHTML .= $this->get_list_matches_render($date_mc);

  				$innerHTML .= '</div>';
  			}
  			$innerHTML .= '<div class="mc-tab-data custom_date" id="mc_tab_custom_date"></div>';
  		$innerHTML .= '</div>';


  	}else{
			$innerHTML .= '<div class="ff_list_matches">';
				$innerHTML .= '<div class="mc-tab-data today _selected" id="mc_tab_today">';
							$innerHTML .= '<div class="no_matches_mc">Нет матчей</div>';
				$innerHTML .= '</div>';
				$innerHTML .= '<div class="mc-tab-data custom_date" id="mc_tab_custom_date"></div>';
			$innerHTML .= '</div>';
  	}

  	return $innerHTML;
  }

  public function get_list_matches_render($data_array)
  {
  	global $ff_cfg;
  	$dir_img_club = $ff_cfg['dir_img']['dir_img_club'];

  	foreach ($data_array as $key2 => $item_matches) {
			if($key2 == 'Финал'){
				$key2 = 'ЧМ-2018';
			}
			$comp_group = $item_matches[0]->competition_group;
			if($comp_group ){
				$ff_tour = $key2.', '.$comp_group;
			}else{
				if($item_matches[0]->competition_tour){
					$ff_tour = $key2.', '.$item_matches[0]->competition_tour.'-й тур';
				}else{
					$ff_tour = $key2;
				}

			}

			$innerHTML .= '<div class="ff_title_list-matches">'.$ff_tour.'</div>';
			$innerHTML .= '<table class="ff_mc_table_matches">';
				foreach ($item_matches as $key3 => $value) {
					if($value->status == '1'){
						$result_match = $value->result_1.'-'.$value->result_2;
						$status = 'окончен';
					}else{
						$result_match = '-:-';
						$status = 'не начался';
					}

					if($value->type_matches == '_club'){
						$team_1 = $value->team_1.' <img src="'.$dir_img_club.''.$value->logo_t1.'" alt="'.$value->team_1.'" class="logo_club">';
						$team_2 = '<img src="'.$dir_img_club.''.$value->logo_t2.'" alt="'.$value->team_2.'" class="logo_club"> '.$value->team_2;
					}else{
						$team_1 = '<i class="flag_nt flag_'.translit($value->team_1).'" title="'.$value->team_1.'" alt="'.$value->team_1.'"></i>'.$value->team_1;
						$team_2 = '<i class="flag_nt flag_'.translit($value->team_2).'" title="'.$value->team_2.'" alt="'.$value->team_2.'"></i>'.$value->team_2;
					}


					$innerHTML .= '<tr>
														<td class="ff_mc_play_time">'.$value->play_time.'</td>
														<td class="ff_mc_team_1">'.$team_1.'</td>
														<td class="ff_mc_result_match">'.$result_match.'</td>
														<td class="ff_mc_team_2">'.$team_2.'</td>
														<td class="ff_mc_status">'.$status.'</td>
					              </tr>';
				}
			$innerHTML .= '</table>';
		}

		return $innerHTML;
  }


  public function get_matches_list_array($competitiom_id = false , $period = false , $type_matches = false)
  {
  	switch ($type_matches) {
  		case '_club':
  			$data_array = $this->get_matches_club_list_array($competitiom_id , $period);
  			break;
  		case '_nt':
  			$data_array = $this->get_matches_nt_list_array($competitiom_id , $period);
  			break;

  		default:
  			$data_club = $this->get_matches_club_list_array($competitiom_id , $period);
      	$data_nt = $this->get_matches_nt_list_array($competitiom_id , $period);
      	switch (true) {
      		case count($data_club) > 0 and count($data_nt) > 0:
      			$data_array = array_merge($data_club , $data_nt);
      			break;
      		case count($data_club) > 0:
      			$data_array = $data_club;
      			break;
      		case count($data_nt) > 0:
      			$data_array = $data_nt;
      			break;
      		default:
      			$data_array = false;
      			break;
      	}
  			break;
  	}



  	return $data_array;
  }

  public function get_matches_club_list_array($competitiom_id = false , $period = false)
  {
  	global $ff_cfg;
  	global $wpdb;
    if($competitiom_id){
      $competition_id_array =  $competitiom_id;
    }else{
      $competition_id_array = implode(',', (array)$this->competition_id_array);
    }


  	if($period){
  		$period_sql = "AND cm.play_date = '".$period."'";
  	}else{
  		$period_sql = "AND cm.play_date =  '".$this->today_date."'";
  	}

  	$sql = "SELECT com_turnament.name_post AS competition  ,
  								 com_t_type.name AS competition_type,
                   com_t_g.name AS competition_group ,
  								 cm.competition_tour ,
  								 CONCAT('', '_club') AS type_matches,
  								 cm.play_date ,  cm.play_time,
  								 t_1.name AS team_1 ,
  								 t_2.name AS team_2 ,
  								 t_1.logo_link AS logo_t1 ,
  								 t_2.logo_link AS logo_t2 ,
  								 cm.result_match, cm.result_1 , cm.result_2 ,
  								 cm.status
  					  FROM {$ff_cfg['db_table']['football_club_matches']}   cm
  					  JOIN {$ff_cfg['db_table']['common_tournament']} com_turnament
  					    ON cm.competition_id = com_turnament.id
  			 LEFT JOIN {$ff_cfg['db_table']['common_tournament_type']} com_t_type
                ON cm.competition_type_id = com_t_type.id
         LEFT JOIN {$ff_cfg['db_table']['common_tournament_group']} com_t_g
                ON cm.competition_group_id = com_t_g.id
  					  JOIN {$ff_cfg['db_table']['common_club']} t_1
  					    ON cm.club_id_1 = t_1.id
  					  JOIN {$ff_cfg['db_table']['common_club']} t_2
  					    ON cm.club_id_2 = t_2.id
  					 WHERE cm.competition_id IN (".$competition_id_array.")
  					 			 {$period_sql}
  				ORDER BY cm.play_date ASC , cm.play_time ASC  ";
  	$result = $wpdb->get_results( $sql);

  	return $result;
  }

  public function get_matches_nt_list_array($competitiom_id = false , $period = false)
  {
  	global $ff_cfg;
  	global $wpdb;

    if($competitiom_id){
      $competition_sql = "AND matches_nt.competition_id = '".$competitiom_id."'";
    }


  	if($period){
  		$period_sql = $period;
  	}else{
  		$period_sql = $this->today_date;
  	}

  	$sql= "SELECT   fc.name_post AS competition,
                    com_t_type.name AS competition_type,
                    com_t_g.name AS competition_group ,
                    matches_nt.competition_tour ,
                    CONCAT('', '_nt') AS type_matches,
                    ft1.name AS team_1 ,
                    ft2.name AS team_2 ,
                    matches_nt.play_date , matches_nt.play_time , matches_nt.result_1 , matches_nt.result_2 ,
                    matches_nt.result_match , matches_nt.result_extra_time ,
                    matches_nt.status
               FROM {$ff_cfg['db_table']['football_national_team_matches']} matches_nt
          LEFT JOIN {$ff_cfg['db_table']['common_tournament']} fc
                 ON matches_nt.competition_id = fc.id
          LEFT JOIN {$ff_cfg['db_table']['common_tournament_type']} com_t_type
                 ON matches_nt.competition_type_id = com_t_type.id
          LEFT JOIN {$ff_cfg['db_table']['common_tournament_group']} com_t_g
                 ON matches_nt.competition_group_id = com_t_g.id
          LEFT JOIN {$ff_cfg['db_table']['common_national_team']}  ft1
                 ON matches_nt.nt_id_1 = ft1.id
          LEFT JOIN {$ff_cfg['db_table']['common_national_team']}  ft2
                 ON matches_nt.nt_id_2 = ft2.id
              WHERE matches_nt.play_date = '{$period_sql}'{$competition_sql}
           ORDER BY matches_nt.play_date ASC ,
                    matches_nt.play_time ASC  ";

  	$result = $wpdb->get_results( $sql);
  	return $result;
  }





}
