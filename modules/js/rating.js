var $form = jQuery('#feedbacks_form');

 //Поля формы
var $formText             = $form.find('textarea[name=comment]'),
    $formType             = $form.find('input[name=type]'),
    $formPostID           = $form.find('input[name=postid]'),
    $formActionType       = $form.find('input[name=type]'),
    $formButton           = $form.find('[type="submit"]');
 //Поля для оценки
var rating_coefficients = $form.find('input[name=rating_coefficients]'),
 		rating_football_betting = $form.find('input[name=rating_football_betting]'),
		rating_line = $form.find('input[name=rating_line]'),
    rating_live_betting = $form.find('input[name=rating_live_betting]'),
    rating_offers = $form.find('input[name=rating_offers]'),
    rating_site = $form.find('input[name=rating_site]'),
    rating_app = $form.find('input[name=rating_app]'),
    rating_finance = $form.find('input[name=rating_finance]'),
    rating_final_result = $form.find('input[name=rating_final_result]');


 


jQuery('#rating_coefficients').rateWidget({
	onUpdate: function(rate) {
            rating_coefficients.val(rate);
        },
  rate: null,
  hold: false
});
jQuery('#rating_football_betting').rateWidget({
	onUpdate: function(rate) {
            rating_football_betting.val(rate);
        },
  rate: null,
  hold: false

});
jQuery('#rating_line').rateWidget({
	onUpdate: function(rate) {
            rating_line.val(rate);
        },
  rate: null,
  hold: false
});
jQuery('#rating_live_betting').rateWidget({
	onUpdate: function(rate) {
            rating_live_betting.val(rate);
        },
  rate: null,
  hold: false
});
jQuery('#rating_offers').rateWidget({
	onUpdate: function(rate) {
            rating_offers.val(rate);
        },
  rate: null,
  hold: false
});
jQuery('#rating_site').rateWidget({
	onUpdate: function(rate) {
            rating_site.val(rate);
        },
  rate: null,
  hold: false
});
jQuery('#rating_app').rateWidget({
	onUpdate: function(rate) {
            rating_app.val(rate);
        },
  rate: null,
  hold: false
});
jQuery('#rating_finance').rateWidget({
	onUpdate: function(rate) {
            rating_finance.val(rate);

        },
  rate: null,
  hold: false
});

function buttonEnableDisable(e) {
    var r_c = rating_coefficients.val(),
		    r_fb = rating_football_betting.val(),
		    r_line = rating_line.val(),
		    r_l_b = rating_live_betting.val(),
		    r_offers = rating_offers.val(),
		    r_site = rating_site.val(),
		    r_app = rating_app.val(),
		    r_f = rating_finance.val(),
		    r_fr = rating_final_result.val();

		var t_size = $formText.val().length,
				countMinimum   = 60;

        // автоматическое выставление среднего значения от всех полю рейтинг
        if (r_c > 0 && r_fb > 0 && r_line > 0 && r_l_b > 0 && r_offers > 0 && r_site > 0 && r_app > 0 && r_f > 0 ) {
            var summ = ((+r_c + +r_fb + +r_line + +r_l_b + +r_offers + +r_site + +r_app + +r_f) / 8).toFixed(1);
            rating_final_result.val(summ);
            rating_final_result_front(summ);
        }
        if (r_c > 0 && r_fb > 0 && r_line > 0 && r_l_b > 0 && r_offers > 0 && r_site > 0 && r_app > 0 && r_f > 0 && t_size  >= countMinimum) {
            $formButton.prop('disabled', false);
        } else {
            $formButton.prop('disabled', true);
        }
}

// bind textarea
$formText.on('keyup', function(e) {
    buttonEnableDisable();
});
buttonEnableDisable();
$form.find('a').on('click' , function(){
		buttonEnableDisable();
		var rate_sum = jQuery(this).data('rate');
 		jQuery(this).parents('.item_rating_reviews').find('.result_rating span').html(rate_sum);
 })

function rating_final_result_front($reiting)
{
	var $innerHTML = '';
	switch (true) {
    case  ($reiting < 1):
      $innerHTML = '<i class="star_rat star_half"></i><i class="star_rat star_empty"></i><i class="star_rat star_empty"></i> <i class="star_rat star_empty"></i><i class="star_rat star_empty"></i>';
      break;
    case  ($reiting == 1):
      $innerHTML = '<i class="star_rat star"></i><i class="star_rat star_empty"></i><i class="star_rat star_empty"></i><i class="star_rat star_empty"></i><i class="star_rat star_empty"></i>';
      break;
    case  ($reiting > 1 &&  $reiting < 2):
      $innerHTML = '<i class="star_rat star"></i><i class="star_rat star_half"></i><i class="star_rat star_empty"></i><i class="star_rat star_empty"></i><i class="star_rat star_empty"></i>';
      break;
    case  ($reiting == 2):
      $innerHTML = '<i class="star_rat star"></i><i class="star_rat star"></i> <i class="star_rat star_empty"></i><i class="star_rat star_empty"></i><i class="star_rat star_empty"></i>';
      break;
    case  ($reiting > 2 &&  $reiting < 3):
      $innerHTML = '<i class="star_rat star"></i><i class="star_rat star"></i><i class="star_rat star_half"></i><i class="star_rat star_empty"></i><i class="star_rat star_empty"></i>';
      break;
    case  ($reiting == 3):
      $innerHTML = '<i class="star_rat star"></i><i class="star_rat star"></i><i class="star_rat star"></i><i class="star_rat star_empty"></i><i class="star_rat star_empty"></i>';
      break;
    case  ($reiting > 3 &&  $reiting < 4):
      $innerHTML = '<i class="star_rat star"></i><i class="star_rat star"></i><i class="star_rat star"></i><i class="star_rat star_half"></i><i class="star_rat star_empty"></i>';
      break;
    case  ($reiting == 4):
      $innerHTML = '<i class="star_rat star"></i><i class="star_rat star"></i><i class="star_rat star"></i><i class="star_rat star"></i><i class="star_rat star_empty"></i>';
      break;
    case  ($reiting > 4 &&  $reiting < 5):
      $innerHTML = '<i class="star_rat star"></i><i class="star_rat star"></i><i class="star_rat star"></i><i class="star_rat star"></i><i class="star_rat star_half"></i>';
      break;
    case  ( $reiting == 5):
      $innerHTML = '<i class="star_rat star"></i><i class="star_rat star"></i><i class="star_rat star"></i><i class="star_rat star"></i><i class="star_rat star"></i>';
      break;
    default:
      $innerHTML = '<i class="star_rat star_empty"></i><i class="star_rat star_empty"></i><i class="star_rat star_empty"></i><i class="star_rat star_empty"></i><i class="star_rat star_empty"></i>';
      break;
  }

  jQuery('#feedbacks_form').find('.rating_final_result').html($innerHTML);
  jQuery('.result_rating_all').find('span').html($reiting);
}

jQuery('#comment_open_add_form').on('click' , function(e){
	e.preventDefault();
	jQuery('.feedbacks_form').toggle();
})

$form.on('submit', function(e) {
    e.preventDefault();
    var data = {
          'action': 'feedbacks_new',
          'formText': $formText.val(),
          'formType': $formType.val(),
          'post_id': $formPostID.val(),
          'formActionType': $formActionType.val(),
          'rating_coefficients': rating_coefficients.val(),
          'rating_football_betting': rating_football_betting.val(),
          'rating_line': rating_line.val(),
          'rating_live_betting': rating_live_betting.val(),
          'rating_offers': rating_offers.val(),
          'rating_site': rating_site.val(),
          'rating_app': rating_app.val(),
          'rating_finance': rating_finance.val(),
          'rating_final_result': rating_final_result.val(),
    };
    console.log(data);
    jQuery.ajax({
      url: 'https://football-fun.ru/wp-admin/admin-ajax.php', // обработчик
      data: data,
      type: 'POST', // тип запроса
      success:function(data){
        console.log(data);
        if(data['data']['status'] == 'add'){
        	jQuery('#feedbacks-popup').addClass('active');
        }else{
        	jQuery('#feedbacks-popup').attr('data-type' , 'error');
        	jQuery('#feedbacks-popup').find('.feedbacks-popup-content').html('Вы уже ранние оставляли отзыв для этой букмекерской конторы');		
        	jQuery('#feedbacks-popup').addClass('active');	
        	
        }
      }
    });
})

// popup close
jQuery('.feedbacks-popup-close').on('click' , function(){
	jQuery('#feedbacks-popup').removeClass('active');
})

//  Подробный рейтинг
jQuery('.show_more_rating').on('click' , function(){
	jQuery('.full_view_rating').toggle();
	if(jQuery(this).find('span').text() == 'Показать подробно'){
		jQuery(this).find('span').text('Скрыть');
	}else{
		jQuery(this).find('span').text('Показать подробно');
	}
	return false;
})